package searchitect.model;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;


import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import searchitect.common.view.SearchResult;
import searchitect.common.view.SearchToken;

@Entity
public class IndexImpl {

	@Id
	@GeneratedValue
	private Long id;

	String repositoryName;

	@Column(name = "indexString", columnDefinition = "NVARCHAR(MAX)")
	String indexString;

	protected IndexImpl() { // jpa only

	}

	public IndexImpl(String indexString)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		// test if index is valid multimap
		new Index(indexString);
		this.indexString = indexString;
		this.repositoryName = UUID.randomUUID().toString();
	}

	public void update(String updateIndex)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		Index upIndex = new Index(updateIndex);
		Index index = new Index(indexString);
		index.getIndex().putAll(upIndex.getIndex());
		ObjectMapper mapper = new ObjectMapper();
		indexString =mapper.writeValueAsString(index.getIndex().asMap());
	}

	public SearchResult search(SearchToken token) {
		try {
			Index index = new Index(indexString);
			Collection<String> result = index.getIndex().get(token.getSearchToken());
			List<String> listresult = new ArrayList<String>(result);
			return new SearchResult(listresult);
		} catch (Exception e) {
			return null;
		}
	}

	public String getRepositoryName() {
		return repositoryName;
	}

	public String getIndexString() {
		return indexString;
	}

	public void setIndex(String indexString) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		// test if index is valid multimap
		new Index(indexString);
		this.indexString = indexString;
	}

}
