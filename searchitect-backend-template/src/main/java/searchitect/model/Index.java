package searchitect.model;

import java.io.IOException;

import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.guava.GuavaModule;
import com.google.common.collect.Multimap;



@XmlRootElement
public class Index {

    private Multimap<String, String> index;
    

    public Index(String setupindex) throws JsonParseException, JsonMappingException,JsonProcessingException, IOException{
  	Multimap<String, String> multimap = createMultimapFromJson(setupindex);
  	this.index = multimap;
    }

    public Multimap<String, String> getIndex() {
	return index;
    }

    public void setIndex(Multimap<String, String> setupindex) {
	this.index = setupindex;
    }

    public Index() {
    }
    
    public String getJsonString() throws JsonProcessingException{
    ObjectMapper mapper = new ObjectMapper();
	String jsonstring = mapper.writeValueAsString(index.asMap());	
	return jsonstring;
    }
    
  
    public Multimap<String, String> createMultimapFromJson(String jsonstring) throws JsonParseException, JsonMappingException,
    JsonProcessingException,IOException{
	ObjectMapper objectMapper = new ObjectMapper();
	objectMapper.registerModule(new GuavaModule());
	Multimap<String, String> multimap = objectMapper.readValue(
			objectMapper.treeAsTokens(objectMapper.readTree(jsonstring)),
			objectMapper.getTypeFactory().constructMapLikeType(Multimap.class, String.class, String.class));
	return multimap;
    }
    
    

}
