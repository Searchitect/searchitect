package searchitect.service;


import java.util.Optional;


import org.springframework.data.jpa.repository.JpaRepository;

import searchitect.model.IndexImpl;

/**
 * IndexRepository - interface to the persisted index repositories
 *
 */
public interface IndexRepository extends JpaRepository<IndexImpl, Long> {
    
    /**
     * findByRepositoryName searches for the repository of the index
     * 
     * @param repositoryName
     * @return IndexImpl or an empty Optional
     */
    Optional<IndexImpl> findByRepositoryName(String repositoryName);

}


  
   
