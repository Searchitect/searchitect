package searchitect.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

import java.io.IOException;
import java.util.List;
import java.util.NoSuchElementException;

import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.core.Is.is;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import com.google.gson.Gson;


import searchitect.Application;
import searchitect.model.IndexImpl;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
public class IndexRepositoryTest {

	@Autowired
	private IndexRepository indexRepo;

	public static Multimap<String, String> getMultimap() {

		// Map<String, List<String>>
		// keyword1 -> doc1, doc2, doc3
		// keyword2 -> doc1, doc4, doc5
		// keyword3 -> doc1, doc6, doc7
		// keyword4 -> doc1, doc3, doc5

		Multimap<String, String> multimap = ArrayListMultimap.create();

		multimap.put("keyword1", "doc1");
		multimap.put("keyword1", "doc2");
		multimap.put("keyword1", "doc3");
		multimap.put("keyword2", "doc1");
		multimap.put("keyword2", "doc4");
		multimap.put("keyword2", "doc5");
		multimap.put("keyword3", "doc1");
		multimap.put("keyword3", "doc6");
		multimap.put("keyword3", "doc7");
		multimap.put("keyword4", "doc1");
		multimap.put("keyword4", "doc3");
		multimap.put("keyword4", "doc5");

		return multimap;
	}
	
	@After
	public void deleteAll() {
		indexRepo.deleteAllInBatch();
	}

	public static String getJsonMultimapAsString() {
		Gson gson = new Gson();
		Multimap<String, String> update = getMultimap();
		return gson.toJson(update.asMap());
	}

	@Test
	public void findByRepositoryNameTestCorrectName()
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		IndexImpl indexExpected = new IndexImpl(getJsonMultimapAsString());
		String repositoryName = indexExpected.getRepositoryName();
		indexRepo.saveAndFlush(indexExpected);
		assertEquals(indexExpected.getRepositoryName(), indexRepo.findByRepositoryName(repositoryName).get().getRepositoryName());
	}
	
	@Test()
	public void findByRepositoryNameTestWrongName()
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		IndexImpl indexExpected = new IndexImpl(getJsonMultimapAsString());
		String wrongRepositoryName = "wrongName";
		indexRepo.saveAndFlush(indexExpected);
		assertEquals(false, indexRepo.findByRepositoryName(wrongRepositoryName).isPresent());
	
	}
	
	@Test(expected = NoSuchElementException.class)
	public void findByRepositoryNameTestWrongNameGetThrowsException()
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		IndexImpl indexExpected = new IndexImpl(getJsonMultimapAsString());
		String wrongRepositoryName = "wrongName";
		indexRepo.saveAndFlush(indexExpected);
		indexRepo.findByRepositoryName(wrongRepositoryName).get();
	}
	
	@Test()
	public void findByRepositoryNameTestNullName()
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		IndexImpl indexExpected = new IndexImpl(getJsonMultimapAsString());
		String nullRepositoryName = null;
		indexRepo.saveAndFlush(indexExpected);
		assertEquals(false, indexRepo.findByRepositoryName(nullRepositoryName).isPresent());
	}
	
	@Test()
	public void findByRepositoryNameTestEmptyName()
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		IndexImpl indexExpected = new IndexImpl(getJsonMultimapAsString());
		String emptyRepositoryName = "";
		indexRepo.saveAndFlush(indexExpected);
		assertEquals(false, indexRepo.findByRepositoryName(emptyRepositoryName).isPresent());
	}
	
	@Test(expected = NoSuchElementException.class)
	public void DeleteRepositoryAndSearchForIt()
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		IndexImpl indexExpected = new IndexImpl(getJsonMultimapAsString());
		indexRepo.saveAndFlush(indexExpected);
		indexRepo.delete(indexRepo.findByRepositoryName(indexExpected.getRepositoryName()).get());
		indexRepo.findByRepositoryName(indexExpected.getRepositoryName()).get();
	}
	
	@Test
	public void DeleteRepositoryNotSavedIndexImpl()
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		indexRepo.delete(new IndexImpl(getJsonMultimapAsString()));
	}
	
	@Test
	public void GetAllRepositoriesNoSavedIndexImpl() {
		List <IndexImpl> indexList = indexRepo.findAll();
		assertThat(indexList.isEmpty(), is(true));
	}
	
	@Test
	public void GetAllRepositoriesTwoSavedIndexImpl() 
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		IndexImpl firstIndex = new IndexImpl(getJsonMultimapAsString());
		indexRepo.saveAndFlush(firstIndex);
		IndexImpl secondIndex = new IndexImpl(getJsonMultimapAsString());
		indexRepo.saveAndFlush(secondIndex);
		List <IndexImpl> indexList = indexRepo.findAll();
		assertThat(indexList.size(), is(2));
	}

}
