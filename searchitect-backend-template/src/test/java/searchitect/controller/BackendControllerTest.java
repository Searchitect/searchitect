package searchitect.controller;

import static org.junit.Assert.assertNotNull;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Arrays;

import org.hamcrest.Matchers;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.mock.http.MockHttpOutputMessage;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import com.google.gson.Gson;

import searchitect.Application;
import searchitect.common.view.RepositoryInfo;
import searchitect.common.view.SearchToken;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
@AutoConfigureMockMvc
public class BackendControllerTest {

	private MediaType contentType = new MediaType(MediaType.APPLICATION_JSON.getType(),
			MediaType.APPLICATION_JSON.getSubtype(), Charset.forName("utf8"));

	@Autowired
	private MockMvc mockMvc;

	@SuppressWarnings("rawtypes")
	private HttpMessageConverter mappingJackson2HttpMessageConverter;

	@Autowired
	private WebApplicationContext webApplicationContext;

	@Autowired
	void setConverters(HttpMessageConverter<?>[] converters) {

		this.mappingJackson2HttpMessageConverter = Arrays.asList(converters).stream()
				.filter(hmc -> hmc instanceof MappingJackson2HttpMessageConverter).findAny().orElse(null);

		assertNotNull("the JSON message converter must not be null", this.mappingJackson2HttpMessageConverter);
	}

	@Before
	public void setup() throws Exception {
		this.mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
	}

	@After
	public void deleteAll() throws Exception {
		this.mockMvc.perform(delete("/repositories"));
	}
	private ObjectMapper mapper = new ObjectMapper();



	public Multimap<String, String> getMultimap() {

		// Map<String, List<String>>s
		// keyword1 -> doc1, doc2, doc3
		// keyword2 -> doc1, doc4, doc5
		// keyword3 -> doc1, doc6, doc7
		// keyword4 -> doc1, doc3, doc5

		Multimap<String, String> multimap = ArrayListMultimap.create();

		multimap.put("keyword1", "doc1");
		multimap.put("keyword1", "doc2");
		multimap.put("keyword1", "doc3");
		multimap.put("keyword2", "doc1");
		multimap.put("keyword2", "doc4");
		multimap.put("keyword2", "doc5");
		multimap.put("keyword3", "doc1");
		multimap.put("keyword3", "doc6");
		multimap.put("keyword3", "doc7");
		multimap.put("keyword4", "doc1");
		multimap.put("keyword4", "doc3");
		multimap.put("keyword4", "doc5");

		return multimap;
	}

	public Multimap<String, String> getUpdateMultimap() {

		// Map<String, List<String>>
		// keyword1 -> doc8, doc9, doc7
		// keyword2 -> doc8, doc9, doc7
		Multimap<String, String> multimap = ArrayListMultimap.create();

		multimap.put("keyword1", "doc8");
		multimap.put("keyword1", "doc9");
		multimap.put("keyword1", "doc7");
		multimap.put("keyword2", "doc8");
		multimap.put("keyword2", "doc9");
		multimap.put("keyword2", "doc7");

		return multimap;
	}

	public String getJsonMultimapAsString() throws JsonProcessingException {
		Multimap<String, String> index = getMultimap();
		return mapper.writeValueAsString(index.asMap()) ;
	}

	public String getJsonUpdateMultimapAsString() throws JsonProcessingException {
		Multimap<String, String> update = getUpdateMultimap();
		return mapper.writeValueAsString(update.asMap());
	}

	public String createRepository() throws Exception {
		MvcResult result = mockMvc.perform(post("/repository").contentType(contentType).content(getJsonMultimapAsString()))
				.andExpect(status().isOk()).andReturn();
		String json = result.getResponse().getContentAsString();
		RepositoryInfo repi = mapper.readValue(json, RepositoryInfo.class);
		return repi.getrepositoryName();
	}

	public String getSearchQuery(String repositoryName) {
		return new StringBuilder().append("/repository/").append(repositoryName).append("/search").toString();
	}

	@Test
	public void setupWithNormalJsonStringTest() throws Exception {
		// test with correct json String of multimap - positiv expectation: 200 OK
		mockMvc.perform(
				post("/repository").contentType(contentType).content(getJsonMultimapAsString()).accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$").value(org.hamcrest.Matchers.hasKey("repositoryName"))).andReturn();
	}

	@Test
	public void setupWithWrongJsonStringTest() throws Exception {
		// test with incorrect json String of multimap - negativ expectation: 404 BAD REQUEST
		mockMvc.perform(post("/repository").contentType(contentType).content(getJsonMultimapAsString().substring(2))
				.accept(MediaType.APPLICATION_JSON)).andExpect(status().is(404)).andReturn();
	}

	@Test
	public void setupWithEmptyJsonStringTest() throws Exception {
		// test with incorrect json String of multimap - negativ expectation: 400 BAD
		// REQUEST
		mockMvc.perform(post("/repository").contentType(contentType).content("")
				.accept(MediaType.APPLICATION_JSON)).andExpect(status().is(400)).andReturn();
	}

	@Test
	public void deleteTest() throws Exception {
		// test delete existing repository - positiv expectation: 200 OK
		String repositoryName = createRepository();
		String query = new StringBuilder().append("/repository/").append(repositoryName).toString();
		mockMvc.perform(delete(query)).andExpect(status().isOk());

		// test delete not existing repository - negativ expectation: 404 NOT FOUND
		mockMvc.perform(delete(query)).andExpect(status().is(404));
	}

	@Test
	public void listRepositoriesWithTwoInsertedRepositoriesTest() throws Exception {
		// create two repositories - positiv expectation: 200 OK and print response
		createRepository();
		createRepository();
		// test list those 2 repositories - positiv expectation: 200 OK and 2 matches
		MvcResult result = mockMvc.perform(get("/repositories").accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk()).andExpect(jsonPath("$", Matchers.hasSize(2))).andReturn();
		System.out.println(result.getResponse().getContentAsString());
	}

	@Test
	public void listRepositoriesWithNoInsertedRepositoriesTest() throws Exception {
		// test list no repositories - negative expectation: 404 NOT FOUND
		mockMvc.perform(get("/repositories").accept(MediaType.APPLICATION_JSON)).andExpect(status().is(404))
				.andReturn();
	}

	@Test
	public void searchWithNormalJsonStringTest() throws Exception {
		// create repository
		String repositoryName = createRepository();
		String query = getSearchQuery(repositoryName);
		// test existing token - positiv expectation: 200 OK and 3 matches in resultList
		// and print response
		mockMvc.perform(post(query).contentType(contentType).content(mapper.writeValueAsString(new SearchToken("keyword1"))).accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk()).andExpect(jsonPath("$.resultList", Matchers.hasSize(3))).andReturn();
	}

	@Test
	public void searchWithWrongFormattedJsonTokenTest() throws Exception {
		// test wrong formatted token - negative expectation: 400 BAD REQUEST
		String repositoryName = createRepository();
		String query = getSearchQuery(repositoryName);
		mockMvc.perform(post(query).contentType(contentType).content(mapper.writeValueAsString(new SearchToken("keyword1")).substring(2)).accept(MediaType.APPLICATION_JSON))
				.andExpect(status().is(400)).andReturn();
	}

	@Test
	public void searchNotExistingTokenTest() throws Exception {
		// test not existing token - positiv expectation: 200 OK and matches in
		// resultList are 0
		String repositoryName = createRepository();
		String query = getSearchQuery(repositoryName);
		mockMvc.perform(
				post(query).contentType(contentType).content(mapper.writeValueAsString(new SearchToken("keyword200"))).accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk()).andExpect(jsonPath("$.resultList", Matchers.hasSize(0))).andReturn();
	}

	@Test
	public void searchWrongMediaTypeNotExistingTokenTest() throws Exception {
		// test wrong MediaType for content - negative expectation 415 Unsupported Media
		// Type
		String repositoryName = createRepository();
		String query = getSearchQuery(repositoryName);
		mockMvc.perform(post(query).contentType(MediaType.TEXT_PLAIN).content(mapper.writeValueAsString(new SearchToken("keyword1")))
				.accept(MediaType.APPLICATION_JSON)).andExpect(status().is(415)).andReturn();
	}

	@Test
	public void searchTestWrongMediaTypeNotExistingRepository() throws Exception {
		// test not existing repository - negativ expectation: 400 BAD REQUEST
		String query = getSearchQuery("notexisting");
		mockMvc.perform(post(query).contentType(contentType).content(mapper.writeValueAsString(new SearchToken("keyword1")).substring(2)).accept(MediaType.APPLICATION_JSON))
				.andExpect(status().is(400)).andReturn();
	}

	@Test
	public void updateWithValidUpdateTest() throws Exception {
		// create repository
		String repositoryName = createRepository();
		String query = new StringBuilder().append("/repository/").append(repositoryName).toString();

		// test valid update - positive expectation: 200 OK
		String jsonUpdateString = getJsonUpdateMultimapAsString();
		mockMvc.perform(
				post(query).contentType(contentType).content(jsonUpdateString).accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk()).andReturn();
		// test search resultList after update - should contain 6 matches for keyword1
		// and print
		String searchquery = getSearchQuery(repositoryName);
		System.out.println(query);
		MvcResult result2 = mockMvc
				.perform(post(searchquery).contentType(contentType).content(mapper.writeValueAsString(new SearchToken("keyword1")))
						.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk()).andExpect(jsonPath("$.resultList", Matchers.hasSize(6))).andReturn();
		System.out.println(result2.getResponse().getContentAsString());
	}

	@Test
	public void updateMultipleUpdatesTest() throws Exception {
		// test if multiple updates possible needed to enhance indexString column size
		// in IndexImpl to max size of varchar (app. 2GB)
		String repositoryName = createRepository();
		String query = new StringBuilder().append("/repository/").append(repositoryName).toString();
		String jsonUpdateString = getJsonUpdateMultimapAsString();
		mockMvc.perform(
				post(query).contentType(contentType).content(jsonUpdateString).accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk()).andReturn();
		mockMvc.perform(
				post(query).contentType(contentType).content(jsonUpdateString).accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk()).andReturn();
		mockMvc.perform(
				post(query).contentType(contentType).content(jsonUpdateString).accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk()).andReturn();
		mockMvc.perform(
				post(query).contentType(contentType).content(jsonUpdateString).accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk()).andReturn();
		mockMvc.perform(
				post(query).contentType(contentType).content(jsonUpdateString).accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk()).andReturn();
		// test search resultList after 5 additional updates - positive expectation 200
		// OK and should contain 3 + 15 matches for keyword1 and print
		String searchquery = getSearchQuery(repositoryName);
		mockMvc.perform(
				post(searchquery).contentType(contentType).content(mapper.writeValueAsString(new SearchToken("keyword1"))).accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk()).andExpect(jsonPath("$.resultList", Matchers.hasSize(18))).andReturn();
	}

	@Test
	public void updateNotExistingRepositoryTest() throws Exception {
		createRepository();
		String jsonUpdateString = getJsonUpdateMultimapAsString();
		// test update of not existing repository - negativ expectation: 404 NOT FOUND
		String wrongQuery = new StringBuilder().append("/repository/").append("notexisting").toString();
		mockMvc.perform(
				post(wrongQuery).contentType(contentType).content(jsonUpdateString).accept(MediaType.APPLICATION_JSON))
				.andExpect(status().is(404)).andReturn();

	}

	@Test
	public void updateWrongFormattedUpdateTest() throws Exception {
		String repositoryName = createRepository();
		String query = new StringBuilder().append("/repository/").append(repositoryName).toString();
		String jsonUpdateString = getJsonUpdateMultimapAsString();
		// test update wrong formatted json string update - negative expectation: 400
		// BAD REQUEST
		String wrongJsonUpdateString = jsonUpdateString.substring(2);
		mockMvc.perform(
				post(query).contentType(contentType).content(wrongJsonUpdateString).accept(MediaType.APPLICATION_JSON))
				.andExpect(status().is(400)).andReturn();
		mockMvc.perform(delete("/repositories"));

	}

	@SuppressWarnings("unchecked")
	protected String json(Object o) throws IOException {
		MockHttpOutputMessage mockHttpOutputMessage = new MockHttpOutputMessage();
		this.mappingJackson2HttpMessageConverter.write(o, MediaType.APPLICATION_JSON, mockHttpOutputMessage);
		return mockHttpOutputMessage.getBodyAsString();
	}

}