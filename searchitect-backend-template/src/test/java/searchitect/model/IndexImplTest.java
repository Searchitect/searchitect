package searchitect.model;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import com.google.gson.Gson;

import searchitect.Application;
import searchitect.common.view.SearchToken;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
public class IndexImplTest {
	
	String emptyJsonString = "";
	String nullJsonString = null;
	String jsonString = getJsonMultimapAsString();
	String updateJsonString = getJsonUpdateMultimapAsString();
	String wrongjsonString = jsonString.substring(3);

	public static Multimap<String, String> getMultimap() {

		// Map<String, List<String>>
		// keyword1 -> doc1, doc2, doc3
		// keyword2 -> doc1, doc4, doc5
		// keyword3 -> doc1, doc6, doc7
		// keyword4 -> doc1, doc3, doc5

		Multimap<String, String> multimap = ArrayListMultimap.create();

		multimap.put("keyword1", "doc1");
		multimap.put("keyword1", "doc2");
		multimap.put("keyword1", "doc3");
		multimap.put("keyword2", "doc1");
		multimap.put("keyword2", "doc4");
		multimap.put("keyword2", "doc5");
		multimap.put("keyword3", "doc1");
		multimap.put("keyword3", "doc6");
		multimap.put("keyword3", "doc7");
		multimap.put("keyword4", "doc1");
		multimap.put("keyword4", "doc3");
		multimap.put("keyword4", "doc5");

		return multimap;
	}

	public static Multimap<String, String> getUpdateMultimap() {

		// Map<String, List<String>>
		// keyword1 -> doc8, doc9, doc7
		// keyword2 -> doc8, doc9, doc7
		Multimap<String, String> multimap = ArrayListMultimap.create();

		multimap.put("keyword1", "doc8");
		multimap.put("keyword1", "doc9");
		multimap.put("keyword1", "doc7");
		multimap.put("keyword2", "doc8");
		multimap.put("keyword2", "doc9");
		multimap.put("keyword2", "doc7");

		return multimap;
	}

	public static String getJsonMultimapAsString() {
		Gson gson = new Gson();
		Multimap<String, String> index = getMultimap();
		return gson.toJson(index.asMap());
	}
	
	public static String getJsonUpdateMultimapAsString() {
		Gson gson = new Gson();
		Multimap<String, String> update = getUpdateMultimap();
		return gson.toJson(update.asMap());
	}
	
	@Test
	public void IndexImplConstructorTestNormalJsonString()
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		IndexImpl index = new IndexImpl(jsonString);
		assertEquals(getJsonMultimapAsString(), index.indexString);
	 	assertFalse(index.repositoryName.isEmpty());
	}

	@Test(expected = JsonParseException.class)
	public void IndexImplConstructorTestWrongJsonString()
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		new IndexImpl(wrongjsonString);
	}

	@Test(expected = NullPointerException.class)
	public void IndexImplConstructorTestNullJsonString()
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		new IndexImpl(nullJsonString);
	}

	@Test(expected = NullPointerException.class)
	public void IndexImplConstructorTestEmptyJsonString()
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		new IndexImpl(emptyJsonString);
	}
	
	@Test
	public void IndexImplUpdateTestNormalJsonString()
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		IndexImpl index = new IndexImpl(jsonString);
		index.update(updateJsonString);
		assertFalse(index.getIndexString().equalsIgnoreCase(jsonString));
		assertFalse(index.getIndexString().equalsIgnoreCase(updateJsonString));
	 	assertFalse(index.repositoryName.isEmpty());
	}

	@Test(expected = JsonParseException.class)
	public void IndexImplUpdateTestWrongJsonString()
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		IndexImpl index = new IndexImpl(jsonString);
		index.update(wrongjsonString);
	}

	@Test(expected = NullPointerException.class)
	public void IndexImplUpdateTestNullJsonString()
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		IndexImpl index = new IndexImpl(jsonString);
		index.update(nullJsonString);
	}

	@Test(expected = NullPointerException.class)
	public void IndexImplUpdateTestEmptyJsonString()
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		IndexImpl index = new IndexImpl(jsonString);
		index.update(emptyJsonString);
	}
	
	@Test
	public void IndexImplUpdateTestEmptyListJsonString()
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		IndexImpl index = new IndexImpl(jsonString);
		String emptyList="{}";
		index.update(emptyList);
		assertTrue(index.getIndexString().equalsIgnoreCase(jsonString));
		assertFalse(index.getIndexString().equalsIgnoreCase(updateJsonString));
	}
	
	@Test
	public void IndexImplSetTestNormalJsonString()
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		IndexImpl index =new IndexImpl(jsonString);
		index.setIndex(updateJsonString);
		assertEquals(getJsonUpdateMultimapAsString(), index.indexString);
	 	assertFalse(index.repositoryName.isEmpty());
	}

	@Test(expected = JsonParseException.class)
	public void IndexImplSetTestWrongJsonString()
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		IndexImpl index =new IndexImpl(jsonString);
		index.setIndex(wrongjsonString);
	}

	@Test(expected = NullPointerException.class)
	public void IndexImplSetTestNullJsonString()
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		IndexImpl index =new IndexImpl(jsonString);
		index.setIndex(nullJsonString);
	}

	@Test(expected = NullPointerException.class)
	public void IndexImplSetTestEmptyJsonString()
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		IndexImpl index =new IndexImpl(jsonString);
		index.setIndex(emptyJsonString);
	}
	
	
	@Test
	public void IndexImplSearchTestNormalToken()
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		IndexImpl index = new IndexImpl(jsonString);
		SearchToken token = new SearchToken("keyword1");
		List <String> result =index.search(token).getResultList();
		assertThat(result , org.hamcrest.collection.IsCollectionWithSize.hasSize(3));
	}
	
	@Test
	public void IndexImplSearchTestEmptyToken()
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		IndexImpl index = new IndexImpl(jsonString);
		SearchToken token = new SearchToken("");
		List <String> result =index.search(token).getResultList();
		assertThat(result , org.hamcrest.collection.IsCollectionWithSize.hasSize(0));
	}
	
	@Test
	public void IndexImplSearchTestNullToken()
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		IndexImpl index = new IndexImpl(jsonString);
		SearchToken token = new SearchToken(null);
		List <String> result =index.search(token).getResultList();
		assertThat(result , org.hamcrest.collection.IsCollectionWithSize.hasSize(0));
	}
	
}
