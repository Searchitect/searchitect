package searchitect.model;

import static org.junit.Assert.assertEquals;

import java.io.IOException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import com.google.gson.Gson;

import searchitect.Application;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
public class IndexTest {

	String emptyJsonString = "";
	String nullJsonString = null;
	String jsonString = getJsonMultimapAsString();
	String wrongjsonString = jsonString.substring(3);

	public static Multimap<String, String> getMultimap() {

		// Map<String, List<String>>
		// keyword1 -> doc1, doc2, doc3
		// keyword2 -> doc1, doc4, doc5
		// keyword3 -> doc1, doc6, doc7
		// keyword4 -> doc1, doc3, doc5

		Multimap<String, String> multimap = ArrayListMultimap.create();

		multimap.put("keyword1", "doc1");
		multimap.put("keyword1", "doc2");
		multimap.put("keyword1", "doc3");
		multimap.put("keyword2", "doc1");
		multimap.put("keyword2", "doc4");
		multimap.put("keyword2", "doc5");
		multimap.put("keyword3", "doc1");
		multimap.put("keyword3", "doc6");
		multimap.put("keyword3", "doc7");
		multimap.put("keyword4", "doc1");
		multimap.put("keyword4", "doc3");
		multimap.put("keyword4", "doc5");

		return multimap;
	}

	public static Multimap<String, String> getUpdateMultimap() {

		// Map<String, List<String>>
		// keyword1 -> doc8, doc9, doc7
		// keyword2 -> doc8, doc9, doc7
		Multimap<String, String> multimap = ArrayListMultimap.create();

		multimap.put("keyword1", "doc8");
		multimap.put("keyword1", "doc9");
		multimap.put("keyword1", "doc7");
		multimap.put("keyword2", "doc8");
		multimap.put("keyword2", "doc9");
		multimap.put("keyword2", "doc7");

		return multimap;
	}

	public static String getJsonMultimapAsString() {
		Gson gson = new Gson();
		Multimap<String, String> update = getMultimap();
		return gson.toJson(update.asMap());
	}

	@Test
	public void IndexConstructorTestNormalString()
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		Index index = new Index(jsonString);
		assertEquals(getMultimap(), index.getIndex());
	}

	@Test(expected = JsonParseException.class)
	public void IndexConstructorTestWrongJsonString()
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		new Index(wrongjsonString);
	}

	@Test(expected = NullPointerException.class)
	public void IndexConstructorTestNullJsonString()
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		new Index(nullJsonString);
	}

	@Test(expected = NullPointerException.class)
	public void IndexConstructorTestEmptyJsonString()
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		new Index(emptyJsonString);
	}
	
	@Test
	public void setMultimapTestNormalMulitmap() throws JsonParseException, JsonMappingException, JsonProcessingException, IOException{
		Index index = new Index(jsonString);
		index.setIndex(getUpdateMultimap());
		assertEquals(getUpdateMultimap(),index.getIndex());
	}
	
	@Test
	public void setMultimapTestNullMultimap() throws JsonParseException, JsonMappingException, JsonProcessingException, IOException{
		Index index = new Index(jsonString);
		index.setIndex(null);
		assertEquals(null,index.getIndex());
	}
	
	@Test
	public void getJsonStringTest()throws JsonParseException, JsonMappingException, JsonProcessingException, IOException{
		Index index = new Index(jsonString);
		assertEquals(jsonString, index.getJsonString());
	}

	@Test
	public void createMultimapFromJsonTestNormalJsonString() throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		Multimap<String, String> multimapExpected = getMultimap();
		Index index = new Index();
		Multimap<String, String> multimapTest = index.createMultimapFromJson(jsonString);
		assertEquals(multimapExpected ,multimapTest);
	}
	
	@Test(expected = NullPointerException.class)
	public void createMultimapFromJsonTestEmptyJsonString() throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		Index index = new Index();
		index.createMultimapFromJson(emptyJsonString);
	}
	@Test(expected = NullPointerException.class)
	public void createMultimapFromJsonTestNullJsonString() throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		Index index = new Index();
		index.createMultimapFromJson(nullJsonString);
	}
	
	
}
