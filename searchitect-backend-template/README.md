# Searchitect-Backend-Template

### Functionality
 * Serverpart of a Searchable Encryption scheme
 * Stores index repositories

### Packages
* searchitect
  * Application.java - started Spring Boot application
* searchitect.controller
  * CheckController.java - just for testing purpose
  * BackendController.java - Restful Interface of backend implementation
* searchitect.model
  * IndexImpl.java
     * Entity which gets persisted
     * contains the repositoryName and the json formatted String of the index
     * holds the dummy implementation of the searchable encryption methods setup, update, searches
  * Index.java - holds an Instance of dummy index, which is a MultiMap <keyword, [documentidentifiers]>
* searchitect.services
  * IndexRepository.java -  like DAO extends JPARepository
* searchitect.view - all DTOs
  * RepositoryInfo - contains repositoryName
  * SearchResult - contains the search results al List of documentidentifiers
  * Token - contains the querystring

### Restful interface in BackendController.java

Deploy for modul test:

    docker build . -t template
    docker run -p 127.0.0.1:8383:8383 template

* **Setup test**

      curl -H "Content-Type: application/json" -X POST -d '{"keyword3":["doc1","doc6","doc7"],"keyword4":["doc1","doc3","doc5"],"keyword1":["doc1","doc2","doc3"],"keyword2":["doc1","doc4","doc5"]}' http://localhost:8383/repository
      {"repositoryName":"6a6b1b0f-f885-4142-b825-ae263d69fb5e"}

* **List repositories test**

      curl localhost:8383/repositories
      {"repositoryName":"6a6b1b0f-f885-4142-b825-ae263d69fb5e"}

* **Update Index test**

      curl -H "Content-Type: application/json" -X POST -d '{"keyword1":["doc7","doc8","doc9"],"keyword2":["doc7","doc8","doc9"]}' http://localhost:8383/repository/6a6b1b0f-f885-4142-b825-ae263d69fb5e

** PROBLEM**
After 2 updates

    {"timestamp":1519042646274,"status":500,"error":"Internal Server Error","exception":"org.springframework.dao.DataIntegrityViolationException","message":"could not execute statement; SQL [n/a]; nested exception is org.hibernate.exception.DataException: could not execute statement","path":"/repository/6a6b1b0f-f885-4142-b825-ae263d69fb5e"}

* **Search **

      curl -H "Content-Type: application/json" -X POST -d '{"token":"keyword1"}' http://localhost:8383/repository/6a6b1b0f-f885-4142-b825-ae263d69fb5e/search
      {"resultList":["doc1","doc2","doc3","doc7","doc8","doc9"]}

* **Delete All**

      curl -X DELETE http://localhost:8383/repository/6a6b1b0f-f885-4142-b825-ae263d69fb5e
