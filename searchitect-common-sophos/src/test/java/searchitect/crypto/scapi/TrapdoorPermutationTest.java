package searchitect.crypto.scapi;

import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.security.KeyException;
import java.security.KeyPair;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.InvalidParameterSpecException;
import java.security.spec.RSAKeyGenParameterSpec;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.context.annotation.PropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import searchitect.common.crypto.Crypto;
import searchitect.common.sophos.scapi.FactoriesException;
import searchitect.common.sophos.scapi.ScRSAPermutationModified;
import searchitect.common.sophos.scapi.TPElement;
import searchitect.common.sophos.scapi.TrapdoorPermutation;


@RunWith(SpringRunner.class)
@PropertySource("classpath:propertiesFiles/")
public class TrapdoorPermutationTest {
	
	KeyPair akeys;
	String type1 = "SHA256";
	int keysizeasym = 2048;
	String keyword = "testKeyword";
	String password = "insecureTestPassword";
	List <String> testList = new ArrayList<String>(
		    Arrays.asList("testKeyword1", "nuans", "vielleicht"));
	
	int icount = 100;
	int keysizesym=128;
	byte[] salt;
	byte[] skey;
	
	
	
	@Before
	public void setup() throws FactoriesException, InvalidParameterSpecException, InvalidKeySpecException, NoSuchAlgorithmException, NoSuchProviderException{
		TrapdoorPermutation trapdoorPermutation = new ScRSAPermutationModified();
		akeys = trapdoorPermutation.generateKey(new RSAKeyGenParameterSpec(keysizeasym, new BigInteger("3")));
		salt = Crypto.randomBytes(8);
		skey = Crypto.keyGenSetM(password, salt, icount, keysizesym);
	}
	
	@Test
	public void getSameElementFromKeywordAndTrapdoorPermutationTest() throws UnsupportedEncodingException, FactoriesException, KeyException{
		TrapdoorPermutation trapdoorPermutation = new ScRSAPermutationModified();
		// map a word to Big Integer
		for (String key:testList){
		byte[] seed = Crypto.hmac(skey, key + 2, type1);
		// need to initialize positive
		BigInteger test = new BigInteger(1,seed);
		System.out.println(test);	
		trapdoorPermutation.setKey(akeys.getPublic(), akeys.getPrivate());
		TPElement tpd = trapdoorPermutation.generateTPElement(test);
		byte[] seed2 = Crypto.hmac(skey, key+ 2, type1);
		TPElement tpd2 = trapdoorPermutation.generateTPElement(new BigInteger(1,seed2));
		TPElement tpd3 = trapdoorPermutation.invert(tpd2);
		System.out.println("Compare" + tpd.getElement() + "length:"+tpd.getElement().bitLength());
		System.out.println("To" + tpd2.getElement()+"length:"+tpd2.getElement().bitLength());
		System.out.println("inversion" + tpd3.getElement()+"length:"+tpd3.getElement().bitLength());
		assertTrue(tpd.getElement().compareTo(tpd2.getElement())==0);
		}
		
	}
	
	
	@Test
	public void invertAndMultiplyPermutationTest() throws UnsupportedEncodingException, FactoriesException, KeyException{
		TrapdoorPermutation trapdoorPermutation = new ScRSAPermutationModified();
		byte[] seed = Crypto.hmac(skey, testList.get(1) + 2, type1);
		trapdoorPermutation.setKey(akeys.getPublic(), akeys.getPrivate());
		TPElement tpd = trapdoorPermutation.generateTPElement(new BigInteger(1,seed));
		TPElement tpdexpected = trapdoorPermutation.multInvert(tpd, new BigInteger("5"));
		TPElement tpdit =tpd;
		for (int i=0;i<5;i++){
			tpdit = trapdoorPermutation.invert(tpdit);
			System.out.println(tpdit.getElement());
		}
		System.out.println("Test mult invert:");
		System.out.println(tpdexpected.getElement());	
		System.out.println(tpdit.getElement());	
		assertTrue(tpdexpected.getElement().compareTo(tpdit.getElement())==0);
	}
	
	@Test
	public void invertAndMultiplyComputePermutationTest() throws UnsupportedEncodingException, FactoriesException, KeyException{
		TrapdoorPermutation trapdoorPermutation = new ScRSAPermutationModified();
		byte[] seed = Crypto.hmac(skey, testList.get(1) + 2, type1);
		trapdoorPermutation.setKey(akeys.getPublic(), akeys.getPrivate());
		TPElement tpd = trapdoorPermutation.generateTPElement(new BigInteger(1,seed));
		TPElement tpdexpected = trapdoorPermutation.multInvert(tpd, new BigInteger("5"));
		TPElement tpdit =tpd;
		for (int i=0;i<5;i++){
			tpdit = trapdoorPermutation.invert(tpdit);
			System.out.println(tpdit.getElement());
		}
		System.out.println("Test mult invert:");
		System.out.println(tpdexpected.getElement());	
		System.out.println(tpdit.getElement());	
		assertTrue(tpdexpected.getElement().compareTo(tpdit.getElement())==0);
		TPElement tpdreturn =tpdexpected;
		for (int i=0;i<5;i++){
			tpdreturn = trapdoorPermutation.compute(tpdreturn);
			System.out.println(tpdreturn.getElement());
		}
		System.out.println("Test compute back:");
		System.out.println(tpd.getElement());	
		System.out.println(tpdreturn.getElement());	

	
	}
		
}
