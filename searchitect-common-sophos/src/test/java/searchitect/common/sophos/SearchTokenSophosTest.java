package searchitect.common.sophos;


import static org.junit.Assert.assertArrayEquals;


import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

import searchitect.common.sophos.view.SearchTokenSophos;



@RunWith(SpringRunner.class)
public class SearchTokenSophosTest {
	   private byte [] wkey = "testkey".getBytes();
	   private byte [] searchToken = "testsearchtoken".getBytes();
	   int counter = 5;

	@Test
	public void constructorWithNormalSearchTokenSuccessTest() {
		SearchTokenSophos searchTokenSophos = new SearchTokenSophos(wkey,searchToken,counter);
		assertArrayEquals(wkey,searchTokenSophos.getWkey());
	} 
	
	@Test
	public void setWithNormalSearchTokenSuccessTest() {
		SearchTokenSophos searchTokenSophos = new SearchTokenSophos();
		searchTokenSophos.setSearchToken(searchToken);
		assertArrayEquals(searchToken,searchTokenSophos.getSearchToken());
	} 
	
	
	
}
