package searchitect.common.sophos.view;

import java.math.BigInteger;
import java.security.KeyPair;
import java.security.spec.InvalidParameterSpecException;
import java.security.spec.RSAKeyGenParameterSpec;
import java.util.HashMap;

import org.bouncycastle.util.encoders.Hex;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import searchitect.common.sophos.scapi.ScRSAPermutationModified;
import searchitect.common.sophos.scapi.TrapdoorPermutation;

@RunWith(SpringRunner.class)
public class UploadIndexSophosTest {

	
	ObjectMapper mapper;
	
	@Test
	public void keySerializationTest() throws InvalidParameterSpecException, JsonProcessingException{
		TrapdoorPermutation trapdoorPermutation = new ScRSAPermutationModified();
		int keysizeasym = 2048;
		KeyPair akeys = trapdoorPermutation
				.generateKey(new RSAKeyGenParameterSpec(keysizeasym, new BigInteger("65537")));
		byte[] bytes = akeys.getPublic().getEncoded();
		System.out.println(Hex.toHexString(bytes));
		ObjectMapper mapper = new ObjectMapper();
		String json = mapper.writeValueAsString(bytes);
		HashMap <String, byte[]> map = new HashMap <String, byte[]>();
		map.put("string", "bytes".getBytes());
		System.out.println(json);
		String json2 = mapper.writeValueAsString(new UploadIndexSophos(akeys.getPublic().getEncoded(),map));
		System.out.println(json2);
		
				
		
	}
	
	
	
}
