package searchitect.sophos;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.KeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.InvalidParameterSpecException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;

import searchitect.common.sophos.Sophos;
import searchitect.common.sophos.ClientStateSophos;
import searchitect.common.sophos.scapi.FactoriesException;
import searchitect.common.sophos.scapi.ScRSAPermutationModified;
import searchitect.common.sophos.scapi.TrapdoorPermutation;
import searchitect.common.sophos.view.SearchTokenSophos;
import searchitect.service.MemoryDictionary;
import searchitect.service.SearchDictionary;


@RunWith(SpringRunner.class)
public class SophosTest {
	
int keysizesym =256;
int keysizeasym =2048;
String password = "insecuretestpassphrase";
	
public static Multimap<String, String> getMultimap() {

	// Map<String, List<String>>
	// keyword1 -> doc1, doc2, doc3
	// keyword2 -> doc1, doc4, doc5
	// keyword3 -> doc1, doc6, doc7
	// keyword4 -> doc1, doc3, doc5

	Multimap<String, String> multimap = ArrayListMultimap.create();

	multimap.put("keyword1", "doc1");
	multimap.put("keyword1", "doc2");
	multimap.put("keyword1", "doc3");
	multimap.put("keyword2", "doc1");
	multimap.put("keyword2", "doc4");
	multimap.put("keyword2", "doc5");
	multimap.put("keyword3", "doc1");
	multimap.put("keyword3", "doc6");
	multimap.put("keyword3", "doc7");
	multimap.put("keyword4", "doc1");
	multimap.put("keyword4", "doc3");
	multimap.put("keyword4", "doc5");
	multimap.put("keyword5", "doc5");

	return multimap;
}

public static Multimap<String, String> getUpdateMultimap() {

	// Map<String, List<String>>
	// keyword1 -> doc8, doc9, doc7
	// keyword2 -> doc8, doc9, doc7
	Multimap<String, String> multimap = ArrayListMultimap.create();

	multimap.put("keyword1", "doc8");
	multimap.put("keyword1", "doc9");
	multimap.put("keyword1", "doc7");
	multimap.put("keyword2", "doc8");
	multimap.put("keyword2", "doc9");
	multimap.put("keyword2", "doc7");

	return multimap;
}
	
	@Test
	public void setupwithValidParameterTest() throws InvalidKeySpecException, NoSuchAlgorithmException, NoSuchProviderException, InvalidParameterSpecException, FactoriesException, InvalidKeyException  {
	//byte[] sk = RR2Lev.keyGen(256, pass, "salt/salt", 100000);
		ClientStateSophos cState = Sophos.setup(keysizesym, keysizeasym, password, 100000);
		System.out.println(cState.getAsymmetricKeys().getPublic());
		TrapdoorPermutation trapdoorPermutation = new ScRSAPermutationModified();
		trapdoorPermutation.setKey(cState.getAsymmetricKeys().getPublic(),cState.getAsymmetricKeys().getPrivate());
		assertTrue(cState.getStateCounter().isEmpty());
		assertTrue(trapdoorPermutation.isKeySet());
		assertEquals(32 ,cState.getSymmetricKey().length);
		}
	
	@Test(expected = IllegalArgumentException.class)
	public void setupwithInvalidParameterTest() throws InvalidKeySpecException, NoSuchAlgorithmException, NoSuchProviderException, InvalidParameterSpecException, FactoriesException, InvalidKeyException  {
	//byte[] sk = RR2Lev.keyGen(256, pass, "salt/salt", 100000);
		ClientStateSophos cState = Sophos.setup(0, 0, password, 100000);
	
		}
	
	@Test
	public void upadeWithValidParameterTest() throws InvalidKeySpecException, NoSuchAlgorithmException, NoSuchProviderException, InvalidParameterSpecException, FactoriesException, UnsupportedEncodingException, KeyException  {
		ClientStateSophos cState = Sophos.setup(keysizesym, keysizeasym, password, 100000);
		HashMap<String, byte[]> map = Sophos.updateClient("add", getMultimap(), cState);
		SearchDictionary gamma = new MemoryDictionary();
		Sophos.updateServer(gamma, map);
		for(byte[] bytes :gamma.keySet("dynamic")){
		System.out.println( bytes);
		}
		assertEquals(getMultimap().get("keyword1").size(),cState.getStateCounter().get("keyword1").intValue());
		assertEquals(getMultimap().size(),map.size());
	
		}
	
	@Test
	public void tokenWithValidParameterTest() throws InvalidKeySpecException, NoSuchAlgorithmException, NoSuchProviderException, InvalidParameterSpecException, FactoriesException, UnsupportedEncodingException, KeyException  {
		ClientStateSophos cState = Sophos.setup(keysizesym, keysizeasym, password, 100000);
		HashMap<String, byte[]> map = Sophos.updateClient("add", getMultimap(), cState);
		SearchDictionary gamma = new MemoryDictionary();
		Sophos.updateServer(gamma, map);
		SearchTokenSophos token = Sophos.token("keyword1", cState);
		System.out.println(new String(token.getSearchToken()));
		assertEquals(getMultimap().get("keyword1").size(),token.getCounter());
		}
	
	@Test
	public void tokenWithOneIdTest() throws InvalidKeySpecException, NoSuchAlgorithmException, NoSuchProviderException, InvalidParameterSpecException, FactoriesException, UnsupportedEncodingException, KeyException  {
		ClientStateSophos cState = Sophos.setup(keysizesym, keysizeasym, password, 100000);
		HashMap<String, byte[]> map = Sophos.updateClient("add", getMultimap(), cState);
		SearchDictionary gamma = new MemoryDictionary();
		Sophos.updateServer(gamma, map);
		SearchTokenSophos token = Sophos.token("keyword5", cState);
		System.out.println(new String(token.getSearchToken()));
		assertEquals(getMultimap().get("keyword5").size(),token.getCounter());
		}
	
	@Test(expected=NullPointerException.class)
	public void tokenWithNotexistingkeywordTest() throws InvalidKeySpecException, NoSuchAlgorithmException, NoSuchProviderException, InvalidParameterSpecException, FactoriesException, UnsupportedEncodingException, KeyException  {
		ClientStateSophos cState = Sophos.setup(keysizesym, keysizeasym, password, 100000);
		HashMap<String, byte[]> map = Sophos.updateClient("add", getMultimap(), cState);
		SearchDictionary gamma = new MemoryDictionary();
		Sophos.updateServer(gamma, map);
		SearchTokenSophos token = Sophos.token("keyword6", cState);

		}
	//not working with byte[] as keys because a map is not able to compare byte[] arrays but rocksdb is!!
	@Test
	public void searchWithValidParameterTest() throws InvalidKeySpecException, NoSuchAlgorithmException, NoSuchProviderException, InvalidParameterSpecException, FactoriesException, UnsupportedEncodingException, KeyException  {
		ClientStateSophos cState = Sophos.setup(keysizesym, keysizeasym, password, 1000);
		HashMap<String, byte[]> map = Sophos.updateClient("add", getMultimap(), cState);
		SearchDictionary gamma = new MemoryDictionary();
		Sophos.updateServer(gamma, map);
		SearchTokenSophos token = Sophos.token("keyword1", cState);
		List <String> result = Sophos.query(token, cState.getAsymmetricKeys().getPublic(), gamma);
		for(String s:result){
		System.out.println(s);
		}
		assertEquals(3, result.size());
	
		}
}
