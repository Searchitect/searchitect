package searchitect.common.sophos.view;


import java.util.HashMap;

import javax.xml.bind.annotation.XmlRootElement;

import searchitect.common.view.Upload;


@XmlRootElement
public class UploadIndexSophos extends Upload{
	
	private HashMap<String, byte[]>dictionary;
	byte[] encodedKey;


	
	
	public UploadIndexSophos(){
		
	}
	/**
	 * Constructor of UploadIndexSophos
	 * 
	 * @param encoded publicKey
	 * @param dictionary
	 */
	public UploadIndexSophos(byte[] encodedKey,HashMap<String, byte[]>dictionary){
	
		this.encodedKey = encodedKey;
		this.dictionary=dictionary;
	}
	
	/**
	 *Get pub key
	 *
	 * @return
	 */
	public byte[] getEncodedKey() {
		return encodedKey;
	}

	/**
	 * Get dictionary
	 * 
	 * @return the dictionary
	 */
	public HashMap<String, byte[]> getDictionary() {
		return dictionary;
	}

	/**
	 * Set dictionary
	 * @param dictionary the dictionary to set
	 */
	public void setDictionary(HashMap<String, byte[]> dictionary) {
		this.dictionary = dictionary;
	}
	

}
