package searchitect.common.sophos.view;



import java.util.HashMap;


import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class UpdateIndexSophos {
	
	private HashMap<String, byte[]>dictionary;

	
	public UpdateIndexSophos(HashMap<String, byte[]> dictionary){
		this.dictionary = dictionary;
	}


	public HashMap <String, byte[]>  getDictionary() {
		return dictionary;
	}

	public void setUpdateIndex(HashMap <String, byte[]> dictionary) {
		this.dictionary= dictionary;
	}
	
	
	public UpdateIndexSophos(){
		
	}
	

}
