package searchitect.common.sophos.view;

import javax.xml.bind.annotation.XmlRootElement;
/**
 * Token - json object used to commit a search contains the token string
 *
 */
@XmlRootElement
public class SearchTokenSophos {

    private byte [] wkey;
    private byte [] searchToken;
    private int counter;

    public SearchTokenSophos() {}
    /**
     * Class constructor SearchTokenSophos
     * 
     * @param wkey - constrained key for the keyword
     * @param searchToken - RSA trapdoor element
     * @param counter - number of updates
     */
    public SearchTokenSophos(byte [] wkey, byte[] searchToken,int counter) {
    this.setWkey(wkey);
	this.setSearchToken(searchToken);
	this.counter =counter;
    }

	/**
	 * @return the wkey
	 */
	public byte [] getWkey() {
		return wkey;
	}

	/**
	 * @param wkey the wkey to set
	 */
	public void setWkey(byte [] wkey) {
		this.wkey = wkey;
	}

	/**
	 * @return the counter
	 */
	public int getCounter() {
		return counter;
	}

	/**
	 * @param counter the counter to set
	 */
	public void setCounter(int counter) {
		this.counter = counter;
	}

	/**
	 * @return the searchToken
	 */
	public byte [] getSearchToken() {
		return searchToken;
	}

	/**
	 * @param searchToken the searchToken to set
	 */
	public void setSearchToken(byte [] searchToken) {
		this.searchToken = searchToken;
	}

}
