package searchitect.common.sophos;

import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.nio.ByteBuffer;
import java.security.KeyException;
import java.security.KeyPair;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.InvalidParameterSpecException;

import java.security.spec.RSAKeyGenParameterSpec;
import java.util.ArrayList;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


import java.util.TreeMap;

import com.google.common.collect.Multimap;

import searchitect.common.sophos.view.SearchTokenSophos;
import searchitect.service.SearchDictionary;
import searchitect.common.crypto.Crypto;
import searchitect.common.sophos.scapi.FactoriesException;
import searchitect.common.sophos.scapi.ScRSAPermutationModified;
import searchitect.common.sophos.scapi.TPElement;
import searchitect.common.sophos.scapi.TrapdoorPermutation;

public class Sophos {

	private static String type1 = "SHA256";
	private static String type2 = "Blake2b";
	
	/**
	 * setup runs at client
	 * 
	 * @param keysizesym 
	 * @param keysizeasym
	 * @param password
	 * @param icount - number of iteration for the keyderivation pkdf2
	 * @return returns a new client state which contains keymaterial
	 * @throws InvalidKeySpecException
	 * @throws NoSuchAlgorithmException
	 * @throws NoSuchProviderException
	 * @throws FactoriesException
	 * @throws InvalidParameterSpecException
	 */
	public static ClientStateSophos setup(int keysizesym, int keysizeasym, String password, int icount)
			throws InvalidKeySpecException, NoSuchAlgorithmException, NoSuchProviderException, FactoriesException,
			InvalidParameterSpecException {
		// get symmetric key
		byte[] salt = Crypto.randomBytes(8);
		byte[] skey = Crypto.keyGenSetM(password, salt, icount, keysizesym);
		// get asymmetric keys
		TrapdoorPermutation trapdoorPermutation = new ScRSAPermutationModified();
		KeyPair akeys = trapdoorPermutation
				.generateKey(new RSAKeyGenParameterSpec(keysizeasym, new BigInteger("65537")));
		ClientStateSophos state = new ClientStateSophos(skey, akeys, salt);
		return state;
	}

	/**
	 * token runs at client
	 * 
	 * @param keyword
	 * @param cState - client states contains keyword counter and key material
	 * @return returns the search token
	 * @throws UnsupportedEncodingException
	 * @throws KeyException
	 * @throws FactoriesException
	 * @throws NullPointerException
	 */
	public static SearchTokenSophos token(String keyword, ClientStateSophos cState)
			throws UnsupportedEncodingException, KeyException, FactoriesException, NullPointerException {

		TrapdoorPermutation trapdoorPermutation = new ScRSAPermutationModified();
		trapdoorPermutation.setKey(cState.getAsymmetricKeys().getPublic(), cState.getAsymmetricKeys().getPrivate());
		// map a word to Big Integer
		byte[] seed = Crypto.hmac(cState.getSymmetricKey(), keyword + 2, type2);
		TPElement generator = trapdoorPermutation.generateTPElement(new BigInteger(1, seed));
		// calculate searchtoken
		int counter = cState.getStateCounter().get(keyword);
		TPElement invertedSearchToken = trapdoorPermutation.multInvert(generator, BigInteger.valueOf(counter - 1));
		byte[] token = invertedSearchToken.getElement().toByteArray();
		// calculate constrained key
		byte[] wkey = Crypto.hmac(cState.getSymmetricKey(), keyword + 1, type1);
		return new SearchTokenSophos(wkey, token, counter);
	}

	/**
	 * query runs at the server
	 * 
	 * @param token
	 * @param pubkey
	 * @param gamma
	 * @return result list of identifiers
	 * @throws UnsupportedEncodingException
	 * @throws KeyException
	 * @throws FactoriesException
	 */
	public static List<String> query(SearchTokenSophos token, PublicKey pubkey, SearchDictionary gamma)
			throws UnsupportedEncodingException, KeyException, FactoriesException {

		TrapdoorPermutation trapdoorPermutation = new ScRSAPermutationModified();
		trapdoorPermutation.setKey(pubkey);
		List<String> resultList = new ArrayList<String>();
		
		// map a word to Big Integer
		TPElement searchToken = trapdoorPermutation.generateTPElement(new BigInteger(1, token.getSearchToken()));
		for (int c = 0; c < token.getCounter(); c++) {
			// generate next search token
			byte[] updateToken = Crypto.hmac(token.getWkey(), searchToken.getElement().toByteArray(), type1);
			byte[] mask = Crypto.hmac(token.getWkey(), searchToken.getElement().toByteArray(), type2);
			byte[] encrypted = gamma.get("dynamic", updateToken);
			byte[] index = Crypto.xor(mask, encrypted);
			String id = (new String(index)).split("\t\t\t")[0];
			resultList.add(id);
			
			searchToken = trapdoorPermutation.compute(searchToken);
		}
		return resultList;
	}

	public static void updateServer(SearchDictionary gamma, HashMap<String, byte[]> tokenUp) {
		gamma.putAllHashMap("dynamic", tokenUp, true);
	}

	/**
	 * update runs at client
	 * 
	 * @param op
	 * @param index
	 * @param cState
	 * @return map of update tokens along with encrypted identifier
	 * @throws UnsupportedEncodingException
	 * @throws KeyException
	 * @throws FactoriesException
	 */
	public static HashMap<String, byte[]> updateClient(String op, Multimap<String, String> index, ClientStateSophos cState)
			throws UnsupportedEncodingException, KeyException, FactoriesException {
		// treemap here because trees are sorting
		Map<String, byte[]> firstMap = new TreeMap<String, byte[]>();
		TPElement searchToken;
		int counter;
		// initialize scapi RSA trapdoor permutation with the keys
		TrapdoorPermutation trapdoorPermutation = new ScRSAPermutationModified();
		trapdoorPermutation.setKey(cState.getAsymmetricKeys().getPublic(), cState.getAsymmetricKeys().getPrivate());
		// do for all keyword/document identifier pairs
		for (String keyword : index.keySet()) {
			// map a word to Big Integer
			byte[] wkey = Crypto.hmac(cState.getSymmetricKey(), keyword + 1, type1);
			byte[] seed = Crypto.hmac(cState.getSymmetricKey(), keyword + 2, type2);
			TPElement generator = trapdoorPermutation.generateTPElement(new BigInteger(1, seed));
			// set search
			if (cState.getStateCounter().get(keyword) == null) {
				// set counter to 1
				cState.getStateCounter().put(keyword, 0);
				counter = 0;
				searchToken = generator;
			}
			// else get the counter and calculate the next search token
			else {
				counter = cState.getStateCounter().get(keyword);
				searchToken = trapdoorPermutation.multInvert(generator, BigInteger.valueOf(counter));
			}

			for (String id : index.get(keyword)) {
				int it = 0;
				byte[] updateToken = Crypto.hmac(wkey, searchToken.getElement().toByteArray(), type1);
				byte[] mask = Crypto.hmac(wkey, searchToken.getElement().toByteArray(), type2);
				byte[] b = new byte[32];
				//sanitize input
				if(!(id.getBytes().length<28)){
					id =id.substring(0,28);
				}
				//add separator
				id = id +"\t\t\t";
				System.arraycopy(id.getBytes(), 0, b, 0, id.getBytes().length);
				byte[] encrypted = Crypto.xor(mask,b);
				// Hex encoding needed for the bytes
				firstMap.put(Base64.getEncoder().encodeToString(updateToken), encrypted);
				// safe counter to client state
				counter++;
				cState.getStateCounter().replace(keyword, counter);

				if (it < index.get(keyword).size()) {
					searchToken = trapdoorPermutation.invert(searchToken);
				}

			}
		}
		// We used a tree map to reorder the strings
		HashMap<String, byte[]> resultMap = new HashMap<String, byte[]>();
		for (String label : firstMap.keySet()) {
			resultMap.put(label, firstMap.get(label));
		}

		return resultMap;
	}

}
