package searchitect.common.sophos;

import java.io.Serializable;
import java.security.KeyPair;
import java.util.HashMap;

/**
 * ClientStateSophos - holds client state of Sophos implementation
 */
public class ClientStateSophos implements Serializable{

	
	private static final long serialVersionUID = -828866469850486251L;
	private static final String implName = "sophos";
	private String repositoryName;
	private final byte[] symmetricKey;
	private final KeyPair asymmetricKeys;
	private final byte[] salt;
	private HashMap<String, Integer> stateCounter;
	
	//public ClientStateSophos() {}
	
	public ClientStateSophos(byte[] sk, KeyPair asymmetricKeys, byte[] salt) {
		super();
		this.repositoryName = null;
		this.symmetricKey = sk;
		this.asymmetricKeys = asymmetricKeys;
		this.salt = salt;
		this.stateCounter = new HashMap<String, Integer> ();
	}

	public String getImplName() {
		return implName;
	}

	public String getRepositoryName() {
		return repositoryName;
	}

	public byte[] getSymmetricKey() {
		return symmetricKey;
	}

	public void setRepositoryName(String repositoryName) {
		this.repositoryName = repositoryName;
	}

	public HashMap<String, Integer> getStateCounter() {
		return stateCounter;
	}
	public void setStateCounter(HashMap<String, Integer> statecounter) {
		this.stateCounter = statecounter;
	}

	/**
	 * @return the asymmetricKeys
	 */
	public KeyPair getAsymmetricKeys() {
		return asymmetricKeys;
	}

	/**
	 * @return the salt
	 */
	public byte[] getSalt() {
		return salt;
	}
	
	
	
	
	
}
