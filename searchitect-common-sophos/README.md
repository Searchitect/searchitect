# Searchitect-Common-Sophos

### Functionality
 * Contains all common classes of the Sophos searchable encryption scheme

### Packages
* searchitect.common.sophos
	* ClientStateSophos.java - maintains the state of the client
	* Sophos.java - implementation of the Sophos searchable encryption scheme
* searchitect.common.sophos.scapi - extracted classes of the SCAPI library https://github.com/cryptobiu/libscapi
	* FactoriesException.java
	* Logging.java
	* RSAElement.java
	* RSAModulus.java
	* RSAPermutation.java
	* ScapiRuntimeException.java
	* ScRSAPermutationModified.java
	* TPElement.java
	* TPElementSendableData.java
	* TPElValidity.java
	* TrapdoorPermutation.java
	* TrapdoorPermutationAbs.java
* searchitect.common.sophos.view - contains Sophos specific classes exchanged between client and server
	* SearchTokenSophos.java
	* UpdateIndexSophos.java
	* UploadIndexSophos.java

### Compile

	mvn clean install