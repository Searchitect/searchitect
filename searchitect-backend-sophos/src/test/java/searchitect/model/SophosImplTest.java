package searchitect.model;
import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.hamcrest.Matchers.hasSize;

import java.io.File;
import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.KeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.InvalidParameterSpecException;
import java.util.concurrent.ExecutionException;

import javax.crypto.NoSuchPaddingException;

import org.apache.commons.codec.DecoderException;
import org.apache.commons.io.FileUtils;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.rocksdb.RocksDBException;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;

import searchitect.Application;
import searchitect.TestUtil;
import searchitect.common.sophos.scapi.FactoriesException;
import searchitect.common.sophos.view.SearchTokenSophos;
import searchitect.common.sophos.view.UpdateIndexSophos;
import searchitect.common.sophos.view.UploadIndexSophos;
import searchitect.common.view.SearchResult;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
public class SophosImplTest {

@BeforeClass
public static void setup() throws InvalidKeySpecException, NoSuchAlgorithmException, NoSuchProviderException, InvalidAlgorithmParameterException, NoSuchPaddingException, IOException, InterruptedException, ExecutionException, InvalidParameterSpecException, KeyException, FactoriesException{
	TestUtil.initialize("/tmp/sophosimpl/");
	
}

@AfterClass
public static void cleanup() throws IOException{
	FileUtils.deleteDirectory(new File("/tmp/sophos/"));
}
	@Test
	public void SophosImplConstructorSuccessTest()throws Exception {
		SophosImpl index = new SophosImpl(TestUtil.init);
		assertArrayEquals(index.getPublicKey().getEncoded(), TestUtil.init.getEncodedKey());
	 	assertFalse(index.getRepositoryName().isEmpty());
	}


	@Test(expected = NullPointerException.class)
	public void IndexImplConstructorUploadIndexNullFailsTest() throws Exception {
		new SophosImpl(null);
	}

	@Test(expected = NullPointerException.class)
	public void SophosImplConstructorUploadIndexDictionaryNullFailsTest() throws Exception {
		new SophosImpl(new UploadIndexSophos(null,null));
	}

	
	@Test
	public void SophosImplUpdateUploadIndexSuccessTest() throws Exception {
		SophosImpl index = new SophosImpl(TestUtil.init);
		UpdateIndexSophos ul = new UpdateIndexSophos(TestUtil.tokenUp1);
		System.out.println(ul.getDictionary().toString());
		index.update(ul);
		assertFalse(index.getRepositoryName().isEmpty());
	}
	
	@Test
	public void SophosImplSearchAfterUpdateSuccessTest() throws Exception {
		SophosImpl index = new SophosImpl(TestUtil.getUploadIndex());
		SearchResult rs = index.search(TestUtil.searchToken1);
		System.out.println(rs.getResultList());
		assertThat(rs.getResultList(),hasSize(3));
		assertFalse(index.getRepositoryName().isEmpty());
	}

	@Test(expected = NullPointerException.class)
	public void IndexImplUpdateUploadIndexDictionaryNullFailsTest() throws Exception {
		SophosImpl index = new SophosImpl(TestUtil.init);
	 	index.update(new UpdateIndexSophos(null));
	}

	@Test(expected = NullPointerException.class)
	public void IndexImplUpdateUploadIndexNullFailsTest() throws JsonParseException, JsonMappingException, JsonProcessingException, NoSuchAlgorithmException, InvalidKeySpecException, IOException, RocksDBException, DecoderException {
		new SophosImpl(null);
	}

	@Test(expected = NullPointerException.class)
	public void IndexImplSearchNullTokenFailsTest()throws Exception {
		SophosImpl index = new SophosImpl(TestUtil.init);
		UpdateIndexSophos ul = new UpdateIndexSophos(TestUtil.tokenUp1);
		index.update(ul);
		index.search(new SearchTokenSophos());
	}
	
	@Test
	public void IndexImplSearchAfterUpdateSuccessTest()throws Exception {
		SophosImpl index = new SophosImpl(TestUtil.init);
		UpdateIndexSophos ul = new UpdateIndexSophos(TestUtil.tokenUp1);
		index.update(ul);
		SearchResult rs = index.search(TestUtil.searchToken2);
		System.out.println(rs.getResultList());
		assertThat(rs.getResultList(),hasSize(5));
	 	assertFalse(index.getRepositoryName().isEmpty());
	}
	

	

	
}
