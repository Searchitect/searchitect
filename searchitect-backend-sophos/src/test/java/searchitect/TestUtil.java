package searchitect;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.InvalidAlgorithmParameterException;
import java.security.KeyException;
import java.security.KeyPair;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.InvalidParameterSpecException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import javax.crypto.NoSuchPaddingException;

import com.google.common.collect.Multimap;
import org.apache.commons.io.FileUtils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.ArrayListMultimap;

import searchitect.common.clusion.TextExtractPar;
import searchitect.common.clusion.TextProc;
import searchitect.common.sophos.ClientStateSophos;
import searchitect.common.sophos.Sophos;
import searchitect.common.sophos.scapi.FactoriesException;
import searchitect.common.sophos.view.SearchTokenSophos;
import searchitect.common.sophos.view.UpdateIndexSophos;
import searchitect.common.sophos.view.UploadIndexSophos;




public class TestUtil {
	

	public static SearchTokenSophos searchToken1;
	public static SearchTokenSophos notExistingSearchToken;
	public static SearchTokenSophos searchToken2;
	public static SearchTokenSophos searchToken3;
	public static UploadIndexSophos init;
	public static HashMap<String, byte[]> setUp;
	public static HashMap<String, byte[]> tokenUp1;
	public static HashMap<String, byte[]> tokenUp2;
	public static HashMap<String, byte[]> tokenUp3;
	public static HashMap<String, byte[]> tokenUp4;
	public static HashMap<String, byte[]> tokenUp5;
	public static String word = "first";
	public static byte[] key;
	public static String emptyJsonString = "";
	public static String nullJsonString = null;
	public static String jsonSearchToken1;
	public static String jsonSearchToken2;
	public static String jsonSearchToken3;
	public static String jsonNotExistingSearchToken;
	public static String notExistingJsonToken;
	public static String wrongJsonToken;
	public static String setupIndexJson;
	public static String updateIndexJson1;
	public static String updateIndexJson2;
	public static String updateIndexJson3;
	public static String updateIndexJson4;
	public static String updateIndexJson5;
	public static ObjectMapper mapper;

	public static String setupPath;
	public static String updatePath;
 	private static int keysizesym =256;
 	private static int keysizeasym =2048;
 	private static int icount = 1000000;
 	public static byte[] salt;
 	public static byte[] skey;
 	public static KeyPair akeys;
 	public static Map <String,Integer> sigma;
 
	
	public TestUtil() throws InvalidKeySpecException, NoSuchAlgorithmException, NoSuchProviderException, InvalidParameterSpecException{
		
	}
	static String path1 = "test/test1.txt";
	static String  path2 = "test/test2.txt";
	static String  path3 = "test/test3.txt";
	static String upath1 = "update/utest1.txt";
	static String  upath2 = "update/utest2.txt";
	static String  upath3 = "update/utest3.txt";
	
	
	public static void createTestFiles(String path) throws IOException {
        Files.createDirectories( Paths.get(path+path1).getParent());
        Files.createFile(Paths.get(path+path1));
        PrintWriter writer1 = new PrintWriter(path+path1, "UTF-8");
        writer1.println("The first line");
        writer1.println("The second line");
        writer1.close();
        Files.createFile(Paths.get(path+path2));
        PrintWriter writer2 = new PrintWriter(path+path2, "UTF-8");
        writer2.println("The first keyword1");
        writer2.println("The second keyword2");
        writer2.close();
        Files.createFile(Paths.get(path+path3));
        PrintWriter writer3 = new PrintWriter(path+path3, "UTF-8");
        writer3.println("weired keyword1");
        writer3.println("The second keyword2");
        writer3.println(" first ");
        writer3.close();
	}
	
	
	public static void createUpdateTestFiles(String path) throws IOException {
        Files.createDirectories(Paths.get(path+upath1).getParent());
        Files.createFile(Paths.get(path+upath1));
        PrintWriter writer4 = new PrintWriter(path+upath1, "UTF-8");
        writer4.println("The first update line");
        writer4.println("The second update line");
        writer4.close();
        Files.createFile(Paths.get(path+upath2));
        PrintWriter writer5 = new PrintWriter(path+upath2, "UTF-8");
        writer5.println("The first update keyword1");
        writer5.println("The second update keyword2");
        writer5.close();
        Files.createFile(Paths.get(path+upath3));
        PrintWriter writer6 = new PrintWriter(path+upath3, "UTF-8");
        writer6.println("weired update keyword1");
        writer6.println("The second update keyword2");
        writer6.close();
	}
	
	public static void deleteTestFiles(String path) throws IOException {
		FileUtils.deleteDirectory(new File(path));
	}
	

    public static void initialize(String path) throws IOException, InvalidKeySpecException, NoSuchAlgorithmException, NoSuchProviderException, InterruptedException, ExecutionException, InvalidAlgorithmParameterException, NoSuchPaddingException, InvalidParameterSpecException, KeyException, FactoriesException {      
    	deleteTestFiles(path);
    	//create the test files
    	createTestFiles(path);
    	createUpdateTestFiles(path);
    	setupPath= path+"test/";
    	updatePath =path+"update/";
    	//initialize Sophos parameters
    	String password = "insecuretestpassphrase";
    	TextExtractPar.lp1 = ArrayListMultimap.create();
		ArrayList<File> fileList = new ArrayList<>();
		fileList.clear();
		mapper = new ObjectMapper();
		////generate client state
		ClientStateSophos cstate = Sophos.setup(keysizesym, keysizeasym, password, icount);
		// Construction of the global multi-map
		Multimap<String, String> lookupinverse = ArrayListMultimap.create();
		TextExtractPar.lp1= lookupinverse;
		TextProc.listf(setupPath, fileList);
		TextExtractPar.extractTextPar(fileList);
		//extract encrypted index as json string
		setUp = Sophos.updateClient("add", TextExtractPar.lp1, cstate);
		init = new UploadIndexSophos(cstate.getAsymmetricKeys().getPublic().getEncoded(),setUp);
		UploadIndexSophos ups = new UploadIndexSophos(cstate.getAsymmetricKeys().getPublic().getEncoded(),setUp);
		setupIndexJson = mapper.writeValueAsString(ups);
		//generate search token
		searchToken1 = Sophos.token(word,cstate);
		jsonSearchToken1 = mapper.writeValueAsString(searchToken1);
		
//		notExistingSearchToken = Sophos.token("wrongword",cstate);
//		jsonNotExistingSearchToken = gson.toJson(notExistingSearchToken);
		// empty multimap!!
		TextExtractPar.lp1 = ArrayListMultimap.create();
		fileList.clear();
		TextProc.listf(updatePath, fileList);
		TextExtractPar.extractTextPar(fileList);
		
		tokenUp1 = Sophos.updateClient("add", TextExtractPar.lp1, cstate);
		updateIndexJson1 = mapper.writeValueAsString(new UpdateIndexSophos(tokenUp1));
		searchToken2 = Sophos.token(word,cstate);
		jsonSearchToken2 = mapper.writeValueAsString(searchToken2);
		tokenUp2 = Sophos.updateClient("add", TextExtractPar.lp1, cstate);
		updateIndexJson2 = mapper.writeValueAsString(new UpdateIndexSophos(tokenUp2));
		tokenUp3 = Sophos.updateClient("add", TextExtractPar.lp1, cstate);
		updateIndexJson3 = mapper.writeValueAsString(new UpdateIndexSophos(tokenUp3));
		tokenUp4 = Sophos.updateClient("add", TextExtractPar.lp1, cstate);
		updateIndexJson4 = mapper.writeValueAsString(new UpdateIndexSophos(tokenUp4));
		tokenUp5 = Sophos.updateClient("add", TextExtractPar.lp1, cstate);
		updateIndexJson5 = mapper.writeValueAsString(new UpdateIndexSophos(tokenUp5));
		searchToken3 = Sophos.token(word,cstate);
		jsonSearchToken3 = mapper.writeValueAsString(searchToken3);
		deleteTestFiles(path);
		
    }

	
	
	
	public static Map<String, byte[]> getMultimap() {

		// Map<String, List<String>>
		// keyword1 -> doc1, doc2, doc3
		// keyword2 -> doc1, doc4, doc5
		// keyword3 -> doc1, doc6, doc7
		// keyword4 -> doc1, doc3, doc5

		Map<String, byte[]> multimap = new HashMap<String, byte[]>();

		multimap.put(Base64.getEncoder().encodeToString("keyword1".getBytes()), "doc1".getBytes());
		multimap.put(Base64.getEncoder().encodeToString("keyword1".getBytes()), "doc2".getBytes());
		multimap.put(Base64.getEncoder().encodeToString("keyword1".getBytes()), "doc3".getBytes());
		multimap.put(Base64.getEncoder().encodeToString("keyword2".getBytes()), "doc1".getBytes());
		multimap.put(Base64.getEncoder().encodeToString("keyword2".getBytes()), "doc4".getBytes());
		multimap.put(Base64.getEncoder().encodeToString("keyword2".getBytes()), "doc5".getBytes());
		multimap.put(Base64.getEncoder().encodeToString("keyword3".getBytes()), "doc1".getBytes());
		multimap.put(Base64.getEncoder().encodeToString("keyword3".getBytes()), "doc6".getBytes());
		multimap.put(Base64.getEncoder().encodeToString("keyword3".getBytes()), "doc7".getBytes());
		multimap.put(Base64.getEncoder().encodeToString("keyword4".getBytes()), "doc1".getBytes());
		multimap.put(Base64.getEncoder().encodeToString("keyword4".getBytes()), "doc3".getBytes());
		multimap.put(Base64.getEncoder().encodeToString("keyword4".getBytes()), "doc5".getBytes());

		return multimap;
	}


	public static Map<String, byte[]> getUpdateMultimap() {

		// Map<String, List<String>>
		// keyword1 -> doc8, doc9, doc7
		// keyword2 -> doc8, doc9, doc7
		Map<String, byte[]> multimap = new HashMap<String, byte[]>();

		multimap.put(Base64.getEncoder().encodeToString("keyword1".getBytes()), "doc8".getBytes());
		multimap.put(Base64.getEncoder().encodeToString("keyword1".getBytes()), "doc9".getBytes());
		multimap.put(Base64.getEncoder().encodeToString("keyword1".getBytes()), "doc7".getBytes());
		multimap.put(Base64.getEncoder().encodeToString("keyword2".getBytes()), "doc8".getBytes());
		multimap.put(Base64.getEncoder().encodeToString("keyword2".getBytes()), "doc9".getBytes());
		multimap.put(Base64.getEncoder().encodeToString("keyword2".getBytes()), "doc7".getBytes());

		return multimap;
	}
	
	public static UploadIndexSophos getUploadIndex() {
		return init;
	}


	public static byte[][] getArray() {
		byte[][] array = new byte[2][];
		array[0]= "test1".getBytes();
		array[1]= "test2".getBytes();
		return array;
	}

	
	
	public static String getUpdateIndexasJsonString() throws JsonProcessingException {
		UpdateIndexSophos ui= new UpdateIndexSophos(TestUtil.setUp);
		return mapper.writeValueAsString(ui);
	}
	
	
	public static String byteArrayToHex(byte[] a) {
		   StringBuilder sb = new StringBuilder(a.length * 2);
		   for(byte b: a)
		      sb.append(String.format("%02x", b));
		   return sb.toString();
		}
	
	
}
