package searchitect.controller;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;

@RestController
public class CheckController {

	/**
	 * check() used to check the availability of the service
	 * 
	 * @return a string "greetings from backend"
	 */
	@RequestMapping("/")
	public String check() {
		return "Greetings from sophos!";
	}

}
