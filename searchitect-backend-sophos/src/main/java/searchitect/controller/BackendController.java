package searchitect.controller;

import org.springframework.web.bind.annotation.RestController;

import searchitect.common.sophos.view.SearchTokenSophos;
import searchitect.common.sophos.view.UpdateIndexSophos;
import searchitect.common.sophos.view.UploadIndexSophos;
import searchitect.common.view.RepositoryInfo;
import searchitect.common.view.SearchResult;
import searchitect.model.SophosImpl;
import searchitect.service.IndexRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@RestController
public class BackendController {

	private IndexRepository indexRepo;

	@Autowired
	BackendController(IndexRepository indexRepository) {
		this.indexRepo = indexRepository;
	}

	/**
	 * listRepositories - List all repositories persisted in the backend
	 * 
	 * @return application/json formatted RepositoryInfo which contain
	 *         repositorynames
	 */

	@RequestMapping(value = "/repositories", method = RequestMethod.GET)
	public ResponseEntity<?> listRepositories() {
		List<SophosImpl> repos = this.indexRepo.findAll();
		List<RepositoryInfo> result = new ArrayList<RepositoryInfo>();
		for (SophosImpl index : repos) {
			result.add(new RepositoryInfo(index.getRepositoryName()));
			System.out.println(index.getRepositoryName());
		}
		if (result.isEmpty()) {
			return ResponseEntity.notFound().build();
		}
		return ResponseEntity.ok(result);
	}

	/**
	 * setup - generates a IndexImpl instance with the uploadIndex
	 * 
	 * @param Base64EncodedRequest contains the encoded uploadIndex
	 * @return a application/json formatted RepositoryInfo which contains the
	 *         repositorynames
	 */
	@RequestMapping(value = "/repository", method = RequestMethod.POST)
	public ResponseEntity<?> setup(@RequestBody UploadIndexSophos uploadIndex ) {
		try {
			// generate and save new instance of IndexImpl and return the name
			// of the repository
			
			SophosImpl repo = this.indexRepo.saveAndFlush(new SophosImpl(uploadIndex));
			return ResponseEntity.ok(new RepositoryInfo(repo.getRepositoryName()));
		} catch (Exception e) {
			return ResponseEntity.notFound().build();
		}

	}

	/**
	 * search - searches over the in r_id specified encrypted index using the
	 * token
	 * 
	 * @param Base64EncodedRequest contains the  encoded token used to search the index
	 * @param r_id  specifies the index
	 * @return ResponseEntity ok (http 200) and a application/json formatted
	 *         SearchResult which contains a list of searched identifiers or not
	 *         found (http 404)
	 */
	@RequestMapping(value = "/repository/{r_id}/search", method = RequestMethod.POST)
	public ResponseEntity<?> search(@PathVariable("r_id") String repositoryName,
			@RequestBody SearchTokenSophos token) {
		try {
			// get saved instance by repositoryName
			Optional<SophosImpl> index = this.indexRepo.findByRepositoryName(repositoryName);
			if (index.isPresent()) {
				// search and reply List of search result wrappd by SearchResult
				// class
				SearchResult result = index.get().search(token);
				return ResponseEntity.ok(result);
			} else {
				return ResponseEntity.notFound().build();
			}
		} catch (Exception e) {
			// we also return Not Found if there is thrown an exception
			e.printStackTrace();
			return ResponseEntity.notFound().build();
		}
	}

	/**
	 * update -updates the encrypted index
	 * 
	 * @param  updateIndex
	 * @param r_id specifies the index by repositoryname
	 * @return ok (http 200) or bad request (http 400) or not found (http 404)
	 */
	@RequestMapping(value = "/repository/{r_id}", method = RequestMethod.POST)
	public ResponseEntity<?> update(@PathVariable("r_id") String repositoryName,
			@RequestBody UpdateIndexSophos updateIndex ) {
		try {
			// get saved instance by repositoryName
			Optional<SophosImpl> indexopt = this.indexRepo.findByRepositoryName(repositoryName);
			SophosImpl index;
			if (indexopt.isPresent()) {
				index = indexopt.get();
				// update index instance and save it
				index.update(updateIndex);
				indexRepo.saveAndFlush(index);
				return ResponseEntity.ok().build();
			} else {
				return ResponseEntity.notFound().build();
			}
		} catch (Exception e) {
			return ResponseEntity.badRequest().build();
		}

	}

	/**
	 * delete repository - deletes a repositoy identified by repositoryname
	 * 
	 * @param r_id
	 *            name of the repository
	 * @return ok (http 200) or not found (http 404)
	 */
	@RequestMapping(value = "/repository/{r_id}", method = RequestMethod.DELETE)
	public ResponseEntity<?> deleteRepository(@PathVariable("r_id") String repositoryName) {
		Optional<SophosImpl> index = this.indexRepo.findByRepositoryName(repositoryName);
		if (index.isPresent()) {
			this.indexRepo.delete(index.get());
			return ResponseEntity.ok().build();
		} else {
			return ResponseEntity.notFound().build();
		}
	}

	/**
	 * deletes all reposiories
	 */
	@RequestMapping(value = "/repositories", method = RequestMethod.DELETE)
	public void deleteRepositories() {
		this.indexRepo.deleteAll();
	}

}
