package searchitect.model;

import java.io.IOException;
import java.security.KeyException;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;
import java.util.List;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Lob;

import org.apache.commons.codec.DecoderException;
import org.rocksdb.RocksDBException;


import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.github.ddth.commons.rocksdb.RocksDbException;


import searchitect.service.RocksDbWrapperModified;
import searchitect.common.sophos.Sophos;
import searchitect.common.sophos.scapi.FactoriesException;
import searchitect.common.sophos.view.SearchTokenSophos;
import searchitect.common.sophos.view.UpdateIndexSophos;
import searchitect.common.sophos.view.UploadIndexSophos;
import searchitect.common.view.SearchResult;

/**
 * 
 * SophosImpl holds the dictionary, array and update hashmap and uses the Clusion
 * library for the operations
 *
 */
@Entity
public class SophosImpl {

	@Id
	@GeneratedValue
	private Long id;

	private String repositoryName;
	private String path ="/tmp/sophos/";

	
	@Lob
	@Column(name = "pubKey", columnDefinition = "BLOB")
	private byte[] pubKey;
	
	private static String[] columnFamilies = { "dynamic"};



	protected SophosImpl() { // jpa only
	}

	public String getRepositoryName() {
		return repositoryName;
	}
	public PublicKey getPublicKey() throws IOException, InvalidKeySpecException, NoSuchAlgorithmException {
		//PublicKey rsaKey = BouncyCastleProvider.getPublicKey(SubjectPublicKeyInfo.getInstance(this.pubKey));
		PublicKey publicKey = KeyFactory.getInstance("RSA").generatePublic(new X509EncodedKeySpec(pubKey));
		return publicKey;
	}

	/**
	 * Constructor for SophosImpl generation
	 * 
	 * @param uploadIndex
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws JsonProcessingException
	 * @throws IOException
	 * @throws InvalidKeySpecException 
	 * @throws NoSuchAlgorithmException 
	 * @throws DecoderException 
	 * @throws RocksDBException 
	 */
	public SophosImpl(UploadIndexSophos uploadIndex)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException, NoSuchAlgorithmException, InvalidKeySpecException, RocksDBException, DecoderException {
		this.repositoryName = UUID.randomUUID().toString();
		this.pubKey = uploadIndex.getEncodedKey();
		this.path = path.concat(this.repositoryName);
		RocksDbWrapperModified rockwrap = RocksDbWrapperModified.openReadWrite(path, columnFamilies);
		rockwrap.putAllHashMap("dynamic", uploadIndex.getDictionary(),true);
		rockwrap.close();
	}

	/**
	 * update - the update hashmap using UpdateIndexSophos which contains a
	 * navigableMap <String, byte[]>
	 * 
	 * @param updateIndex
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws JsonProcessingException
	 * @throws IOException
	 * @throws RocksDBException 
	 * @throws DecoderException 
	 */
	public void update(UpdateIndexSophos updateIndex)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException, RocksDBException, DecoderException {
		RocksDbWrapperModified rockwrap = RocksDbWrapperModified.openReadWrite(path, columnFamilies);
		rockwrap.putAllHashMap("dynamic", updateIndex.getDictionary(), true);
		rockwrap.close();
	}

	/**
	 * search - uses SearchtokenSophos array to search for
	 * 
	 * @param token
	 * @return
	 * @throws FactoriesException 
	 * @throws KeyException 
	 * @throws IOException 
	 * @throws RocksDbException 
	 * @throws NoSuchAlgorithmException 
	 * @throws InvalidKeySpecException 
	 */
	public SearchResult search(SearchTokenSophos token) throws KeyException, FactoriesException, RocksDbException, IOException, InvalidKeySpecException, NoSuchAlgorithmException {
		RocksDbWrapperModified rockwrap = RocksDbWrapperModified.openReadOnly(path);
		List <String> resultList = Sophos.query(token, getPublicKey(), rockwrap);
		rockwrap.close();
		return new SearchResult(resultList);
	

	}

}
