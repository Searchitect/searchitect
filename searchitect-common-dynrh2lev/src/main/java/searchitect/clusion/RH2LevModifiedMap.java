/** * Copyright (C) 2016 Tarik Moataz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//***********************************************************************************************//

/////////////////////    Implementation of 2Lev scheme of NDSS'14 

/////////////////////				Response Hiding 					///////////

//***********************************************************************************************//	

package searchitect.clusion;

import com.google.common.collect.Multimap;

import javax.crypto.NoSuchPaddingException;

import org.bouncycastle.crypto.prng.ThreadedSeedGenerator;

import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.*;

public class RH2LevModifiedMap {

	// define the number of character that a file identifier can have
	public static int sizeOfFileIdentifer = 100;
	public static String separator = "seperator";

	public static byte[] master = null;
	public static boolean lmm = false;
	public static String eval = "";
	public static boolean finished;

	public static int counter = 0;

	public HashMap<String, byte[]> dictionary = new HashMap<String, byte[]>();
	public static List<Integer> free = Collections.synchronizedList(new ArrayList<Integer>());
	static byte[][] array = null;
	byte[][] arr = null;


	public RH2LevModifiedMap(HashMap<String, byte[]> dictionary, byte[][] arr, boolean finished) {
		this.dictionary = dictionary;
		this.arr = arr;
		this.finished=finished;
	}

	public HashMap<String, byte[]> getDictionary() {
		return dictionary;
	}

	public void setDictionary(HashMap<String, byte[]> dictionary) {
		this.dictionary = dictionary;
	}

	public byte[][] getArray() {
		return arr;
	}

	public void setArray(byte[][] array) {
		this.arr = array;
	}
	public boolean getFinished(){
		return finished;
	}
	public static RH2LevModifiedMap constructEMMPar(final byte[] key, final Multimap<String, String> lookup, final int bigBlock,
			final int smallBlock, final int dataSize) throws InterruptedException, ExecutionException, IOException {

		final HashMap<String, byte[]> dictionary = new HashMap <String, byte[]> ();
		array = new byte[dataSize][];
		free.clear();
		for (int i = 0; i < dataSize; i++) {
			// initialize all buckets with random values
			free.add(i);
		}

		List<String> listOfKeyword = new ArrayList<String>(lookup.keySet());
		int threads = 0;
		if (Runtime.getRuntime().availableProcessors() > listOfKeyword.size()) {
			threads = listOfKeyword.size();
		} else {
			threads = Runtime.getRuntime().availableProcessors();
		}

		ExecutorService service = Executors.newFixedThreadPool(threads);
		ArrayList<String[]> inputs = new ArrayList<String[]>(threads);

		final Map<Integer, String> concurrentMap = new ConcurrentHashMap<Integer, String>();
		for (int i = 0; i < listOfKeyword.size(); i++) {
			concurrentMap.put(i, listOfKeyword.get(i));
		}

		for (int j = 0; j < threads; j++) {
			service.execute(new Runnable() {
				@SuppressWarnings("unused")
				@Override
				public void run() {

					while (concurrentMap.keySet().size() > 0) {
						Set<Integer> possibleValues = concurrentMap.keySet();

						Random rand = new Random();
						
						int temp = rand.nextInt(possibleValues.size());
						
						List<Integer> listOfPossibleKeywords = new ArrayList<Integer>(possibleValues);

						// set the input as randomly selected from the remaining
						// possible keys
						String[] input = { concurrentMap.get(listOfPossibleKeywords.get(temp)) };

						// remove the key
						concurrentMap.remove(listOfPossibleKeywords.get(temp));

						try {

							Map<String, byte[]> output = setup(key, input, lookup, bigBlock, smallBlock, dataSize);
							Set<String> keys = output.keySet();

							for (String k : keys) {
								dictionary.put(k, output.get(k));
							}
						} catch (InvalidKeyException | NoSuchAlgorithmException | NoSuchProviderException
								| NoSuchPaddingException | IOException | InvalidAlgorithmParameterException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				}
			});
		}

		service.shutdown();

		// Blocks until all tasks have completed execution after a shutdown
		// request
		service.awaitTermination(Long.MAX_VALUE, TimeUnit.DAYS);

		return new RH2LevModifiedMap(dictionary, array, true);
	}

	public static RH2LevModifiedMap constructEMMParGMM(final byte[] key, final Multimap<String, String> lookup, final int bigBlock,
			final int smallBlock, final int dataSize) throws InterruptedException, ExecutionException, IOException {

		final HashMap<String, byte[]> dictionary = new HashMap<String, byte[]>();
	

		for (int i = 0; i < dataSize; i++) {
			// initialize all buckets with random values
			free.add(i);
		}

		List<String> listOfKeyword = new ArrayList<String>(lookup.keySet());
		int threads = 0;
		if (Runtime.getRuntime().availableProcessors() > listOfKeyword.size()) {
			threads = listOfKeyword.size();
		} else {
			threads = Runtime.getRuntime().availableProcessors();
		}

		ExecutorService service = Executors.newFixedThreadPool(threads);
		ArrayList<String[]> inputs = new ArrayList<String[]>(threads);

		for (int i = 0; i < threads; i++) {
			String[] tmp;
			if (i == threads - 1) {
				tmp = new String[listOfKeyword.size() / threads + listOfKeyword.size() % threads];
				for (int j = 0; j < listOfKeyword.size() / threads + listOfKeyword.size() % threads; j++) {
					tmp[j] = listOfKeyword.get((listOfKeyword.size() / threads) * i + j);
				}
			} else {
				tmp = new String[listOfKeyword.size() / threads];
				for (int j = 0; j < listOfKeyword.size() / threads; j++) {

					tmp[j] = listOfKeyword.get((listOfKeyword.size() / threads) * i + j);
				}
			}
			inputs.add(i, tmp);
		}

		System.out.println("\t End of Partitionning  \n");

		List<Future<HashMap<String, byte[]>>> futures = new ArrayList<Future<HashMap<String, byte[]>>>();
		for (final String[] input : inputs) {
			Callable<HashMap<String, byte[]>> callable = new Callable<HashMap<String, byte[]>>() {
				public HashMap<String, byte[]> call() throws Exception {

					HashMap<String, byte[]> output = setup(key, input, lookup, bigBlock, smallBlock, dataSize);
					return output;
				}
			};
			futures.add(service.submit(callable));
		}

		service.shutdown();

		for (Future<HashMap<String, byte[]>> future : futures) {
			Set<String> keys = future.get().keySet();

			for (String k : keys) {
				dictionary.put(k, future.get().get(k));
			}

		}

		return new RH2LevModifiedMap(dictionary, array, true);
	}

	// ***********************************************************************************************//

	///////////////////// Setup /////////////////////////////

	// ***********************************************************************************************//

	public static HashMap<String, byte[]> setup(byte[] key, String[] listOfKeyword, Multimap<String, String> lookup,
			int bigBlock, int smallBlock, int dataSize) throws InvalidKeyException, InvalidAlgorithmParameterException,
			NoSuchAlgorithmException, NoSuchProviderException, NoSuchPaddingException, IOException {

		// determine the size f the data set and therefore the size of the array
		
		HashMap<String, byte[]> gamma = new HashMap<String, byte[]> ();
		
		//changed iv generation
				ThreadedSeedGenerator thread = new ThreadedSeedGenerator();
				SecureRandom random = new SecureRandom();
				random.setSeed(thread.generateSeed(20, true));
				byte[] iv = new byte[16];

		for (String word : listOfKeyword) {

			counter++;
			if (((float) counter / 10000) == (int) (counter / 10000)) {
				System.out.println("Counter " + counter);
			}

			// generate the tag
			// Note that to avoid key collision all words "word" need to be
			// encoded to have the same length
			byte[] key1 = CryptoPrimitivesModified.generateCmac(key, 1 + word);
			byte[] key2 = CryptoPrimitivesModified.generateCmac(key, 2 + word);

			// generate keys for response-hiding construction for SIV (Synthetic
			// IV)
			byte[] key3 = CryptoPrimitivesModified.generateCmac(master, 3 + new String());

			byte[] key4 = null;
			if (lmm == false) {
				key4 = CryptoPrimitivesModified.generateCmac(master, 4 + word);
			} else {
				key4 = CryptoPrimitivesModified.generateCmac(master, eval);
			}

			// Encryption of the lookup DB(w) deterministically to create unique
			// tags

			List<String> encryptedID = new ArrayList<String>();

			for (String id : lookup.get(word)) {
				encryptedID
						.add(new String(CryptoPrimitivesModified.DTE_encryptAES_CTR_String(key3, key4, id, 20), "ISO-8859-1"));
			}

			String encryptedIdString = "";

			for (String s : encryptedID) {
				encryptedIdString += s + separator;
			}

			int t = (int) Math.ceil((float) lookup.get(word).size() / bigBlock);

			if (lookup.get(word).size() <= smallBlock) {
				// pad DB(w) to "small block"
				byte[] l = CryptoPrimitivesModified.generateCmac(key1, Integer.toString(0));
				random.nextBytes(iv);
				gamma.put(Base64.getEncoder().encodeToString(l), CryptoPrimitivesModified.encryptAES_CTR_String(key2, iv,
						"1" + separator + encryptedIdString, smallBlock * sizeOfFileIdentifer));
			}

			else {

				List<String> listArrayIndex = new ArrayList<String>();

				for (int j = 0; j < t; j++) {

					List<String> encryptedID1 = new ArrayList<String>(encryptedID);

					if (j != t - 1) {

						encryptedID1 = encryptedID1.subList(j * bigBlock, (j + 1) * bigBlock);
					} else {
						int sizeList = encryptedID.size();

						encryptedID1 = encryptedID1.subList(j * bigBlock, encryptedID1.size());

						for (int s = 0; s < ((j + 1) * bigBlock - sizeList); s++) {
							encryptedID1.add(separator);
						}

					}

					encryptedIdString = "";

					for (String s : encryptedID1) {
						encryptedIdString += s + separator;
					}

					// generate the integer which is associated to free[b]
					int position = random.nextInt(free.size());
					int tmpPos = free.get(position);
					free.remove(position);
					random.nextBytes(iv);
					array[tmpPos] = CryptoPrimitivesModified.encryptAES_CTR_String(key2, iv,
							encryptedIdString, bigBlock * sizeOfFileIdentifer);
					listArrayIndex.add(tmpPos + "***");

					

				}

				String listArrayIndexString = "";

				for (String s : listArrayIndex) {
					listArrayIndexString += s + separator;
				}

				// medium case
				if (t <= smallBlock) {
					byte[] l = CryptoPrimitivesModified.generateCmac(key1, Integer.toString(0));
					gamma.put(Base64.getEncoder().encodeToString(l),
							CryptoPrimitivesModified.encryptAES_CTR_String(key2, CryptoPrimitivesModified.randomBytes(16),
									"2" + separator + listArrayIndexString, smallBlock * sizeOfFileIdentifer));
				}
				// big case
				else {
					int tPrime = (int) Math.ceil((float) t / bigBlock);

					List<String> listArrayIndexTwo = new ArrayList<String>();

					for (int l = 0; l < tPrime; l++) {
						List<String> tmpListTwo = new ArrayList<String>(listArrayIndex);

						if (l != tPrime - 1) {
							tmpListTwo = tmpListTwo.subList(l * bigBlock, (l + 1) * bigBlock);
						} else {

							int sizeList = tmpListTwo.size();

							tmpListTwo = tmpListTwo.subList(l * bigBlock, tmpListTwo.size());
							for (int s = 0; s < ((l + 1) * bigBlock - sizeList); s++) {
								tmpListTwo.add("***");
							}
						}

						// generate the integer which is associated to free[b]
						int position = random.nextInt(free.size());
						int tmpPos = free.get(position);
						free.remove(position);

						String tmpListTwoString = "";

						for (String s : tmpListTwo) {
							tmpListTwoString += s + separator;
						}
						random.nextBytes(iv);
						array[tmpPos] = CryptoPrimitivesModified.encryptAES_CTR_String(key2, iv,
								tmpListTwoString, bigBlock * sizeOfFileIdentifer);

						listArrayIndexTwo.add(tmpPos + separator);

					

					}

					String listArrayIndexTwoString = "";

					for (String s : listArrayIndexTwo) {
						listArrayIndexTwoString += s + separator;
					}
					// Pad the second set of identifiers

					byte[] l = CryptoPrimitivesModified.generateCmac(key1, Integer.toString(0));
					gamma.put(Base64.getEncoder().encodeToString(l),
							CryptoPrimitivesModified.encryptAES_CTR_String(key2, CryptoPrimitivesModified.randomBytes(16),
									"3" + separator + listArrayIndexTwoString, smallBlock * sizeOfFileIdentifer));

				}

			}

		}

		return gamma;
	}

	// ***********************************************************************************************//

	///////////////////// Query /////////////////////////////

	// ***********************************************************************************************//
	public static List<String> query(byte[][] keys, HashMap<String, byte[]> dictionary, byte[][] array)
			throws InvalidKeyException, InvalidAlgorithmParameterException, NoSuchAlgorithmException,
			NoSuchProviderException, NoSuchPaddingException, IOException {

		byte[] l = CryptoPrimitivesModified.generateCmac(keys[0], Integer.toString(0));

		byte[] temptest = dictionary.get(Base64.getEncoder().encodeToString(l));

		if (!(temptest==null)) {
			String temp = (new String(CryptoPrimitivesModified.decryptAES_CTR_String(temptest, keys[1])))
					.split("\t\t\t")[0];

			String[] result = temp.split(separator);

			List<String> resultFinal = new ArrayList<String>(Arrays.asList(result));
			// We remove the flag that identifies the size of the dataset

			if (result[0].equals("1")) {
				resultFinal.remove(0);
				return resultFinal;
			}

			else if (result[0].equals("2")) {

				resultFinal.remove(0);

				List<String> resultFinal2 = new ArrayList<String>();

				for (String key : resultFinal) {

					boolean flag = true;
					int counter = 0;
					while (flag) {

						if (counter < key.length() && Character.isDigit(key.charAt(counter))) {

							counter++;
						}

						else {
							flag = false;
						}
					}

					String temp2 = (new String(CryptoPrimitivesModified.decryptAES_CTR_String(
							array[Integer.parseInt((String) key.subSequence(0, counter))], keys[1])))
									.split("\t\t\t")[0];

					String[] result3 = temp2.split(separator);

					List<String> tmp = new ArrayList<String>(Arrays.asList(result3));
					resultFinal2.addAll(tmp);
				}

				return resultFinal2;
			}

			else if (result[0].equals("3")) {

				resultFinal.remove(0);
				List<String> resultFinal2 = new ArrayList<String>();
				for (String key : resultFinal) {

					boolean flag = true;
					int counter = 0;
					while (flag) {

						if (counter < key.length() && Character.isDigit(key.charAt(counter))) {

							counter++;
						}

						else {
							flag = false;
						}
					}
					String temp2 = (new String(CryptoPrimitivesModified.decryptAES_CTR_String(
							array[Integer.parseInt((String) key.subSequence(0, counter))], keys[1])))
									.split("\t\t\t")[0];

					String[] result3 = temp2.split(separator);
					List<String> tmp = new ArrayList<String>(Arrays.asList(result3));
					resultFinal2.addAll(tmp);
				}

				List<String> resultFinal3 = new ArrayList<String>();

				for (String key : resultFinal2) {

					boolean flag = true;
					int counter = 0;

					while (flag) {

						if (counter < key.length() && Character.isDigit(key.charAt(counter))) {

							counter++;
						}

						else {
							flag = false;
						}
					}
					if (counter == 0) {
						break;
					}

					String temp2 = (new String(CryptoPrimitivesModified.decryptAES_CTR_String(
							array[Integer.parseInt((String) key.subSequence(0, counter))], keys[1])))
									.split("\t\t\t")[0];

					String[] result3 = temp2.split(separator);

					List<String> tmp = new ArrayList<String>(Arrays.asList(result3));

					resultFinal3.addAll(tmp);
				}

				return resultFinal3;
			}
		}
		return new ArrayList<String>();
	}

	// ***********************************************************************************************//

	///////////////////// Resolve Algorithm /////////////////////////////

	// ***********************************************************************************************//

	public static List<String> resolve(byte[] key, List<String> list)
			throws InvalidKeyException, InvalidAlgorithmParameterException, NoSuchAlgorithmException,
			NoSuchProviderException, NoSuchPaddingException, IOException {

		List<String> result = new ArrayList<String>();

		for (String id : list) {
			byte[] id2 = id.getBytes("ISO-8859-1");

			result.add(new String(CryptoPrimitivesModified.decryptAES_CTR_String(id2, key)).split("###")[0]);
		}

		return result;
	}

}
