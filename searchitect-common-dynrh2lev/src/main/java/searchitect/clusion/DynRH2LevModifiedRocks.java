/** * Copyright (C) 2016 Tarik Moataz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//***********************************************************************************************//

/////////////////////    Implementation of a dynamic Forward Secure SSE (a variant of the Cash et al. NDSS'14)

/////////////////////				Response Hiding with add operations					///////////

//***********************************************************************************************//	

package searchitect.clusion;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.SecureRandom;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import javax.crypto.NoSuchPaddingException;

import org.bouncycastle.crypto.prng.ThreadedSeedGenerator;

import com.google.common.collect.Multimap;
import com.google.common.collect.Ordering;
import com.google.common.collect.TreeMultimap;


import searchitect.service.SearchDictionary;

public class DynRH2LevModifiedRocks extends RH2LevModifiedRocks {

	public HashMap<String, byte[]> dictionaryUpdates = new HashMap<String, byte[]>();
	//Modification state should be non static if not repeated initialization of indexes will fail
	public HashMap<String, Integer> state = new HashMap<String, Integer>();

	public DynRH2LevModifiedRocks(HashMap<String, byte[]> dictionary, byte[][] arr, HashMap<String, byte[]> dictionaryUpdates,HashMap<String, Integer> state ) {
		super(dictionary, arr);
		this.dictionaryUpdates = dictionaryUpdates;
		this.state = state;

	}

	public HashMap<String, byte[]> getDictionaryUpdates() {
		return dictionaryUpdates;
	}

	public HashMap<String, Integer> getState() {
		return state;
	}

	// ***********************************************************************************************//

	///////////////////// Setup /////////////////////////////

	// ***********************************************************************************************//

	public static DynRH2LevModifiedRocks constructEMMParGMM(final byte[] key, final Multimap<String, String> lookup,
			final int bigBlock, final int smallBlock, final int dataSize)
			throws InterruptedException, ExecutionException, IOException {

		RH2LevModifiedRocks result = constructEMMPar(key, lookup, bigBlock, smallBlock, dataSize);

		System.out.println("Initialization of the Encrypted Dictionary that will handle the updates:\n");

		HashMap<String, byte[]> dictionaryUpdates = new HashMap<String, byte[]>();
		HashMap<String, Integer> state = new HashMap<String, Integer>();

		return new DynRH2LevModifiedRocks(result.getDictionary(), result.getArray(), dictionaryUpdates, state);

	}

	// ***********************************************************************************************//

	///////////////////// Update Token /////////////////////////////

	// ***********************************************************************************************//

	public static TreeMultimap<String, byte[]> tokenUpdate(byte[] key, Multimap<String, String> lookup, HashMap<String, Integer> state)
			throws InvalidKeyException, InvalidAlgorithmParameterException, NoSuchAlgorithmException,
			NoSuchProviderException, NoSuchPaddingException, IOException {

		// We use a lexicographic sorted list such that the server
		// will not know any information of the inserted elements creation order
		TreeMultimap<String, byte[]> tokenUp = TreeMultimap.create(Ordering.natural(), Ordering.usingToString());
		
		//changed iv generation
		ThreadedSeedGenerator thread = new ThreadedSeedGenerator();
		SecureRandom random = new SecureRandom();
		random.setSeed(thread.generateSeed(20, true));

		// Key generation
		for (String word : lookup.keySet()) {

			byte[] key2 = CryptoPrimitivesModified.generateCmac(key, 2 + word);
			// generate keys for response-hiding construction for SIV (Synthetic
			// IV)
			byte[] key3 = CryptoPrimitivesModified.generateCmac(master, 3 + new String());

			byte[] key4 = null;
			if (lmm == false) {
				key4 = CryptoPrimitivesModified.generateCmac(master, 4 + word);
			} else {
				key4 = CryptoPrimitivesModified.generateCmac(master, eval);
			}

			byte[] key5 = CryptoPrimitivesModified.generateCmac(key, 5 + word);

			for (String id : lookup.get(word)) {
				int counter = 0;

				if (state.get(word) != null) {
					counter = state.get(word);
				}

				state.put(word, counter + 1);

				byte[] l = CryptoPrimitivesModified.generateCmac(key5, "" + counter);

				byte[] value =CryptoPrimitivesModified.DTE_encryptAES_CTR_String(key3, key4, id, 20);
				
				//changed iv generation
				byte[] iv = new byte[16];
				random.nextBytes(iv);
				tokenUp.put(Base64.getEncoder().encodeToString(l), CryptoPrimitivesModified.encryptAES_CTR_Byte(key2,
						iv, value, sizeOfFileIdentifer));

			}

		}
	

		return tokenUp;

	}

	// ***********************************************************************************************//

	///////////////////// Update /////////////////////////////

	// ***********************************************************************************************//

	public static void update(SearchDictionary rocks, Map<String, byte[]> update){
		rocks.putAllMap("dynamic", update,true);
	}

	// ***********************************************************************************************//

	///////////////////// Search Token /////////////////////
	///////////////////// /////////////////////////////

	// ***********************************************************************************************//

	public static byte[][] genToken(byte[] key, String word, HashMap<String, Integer> state) throws UnsupportedEncodingException {

		byte[][] keys = new byte[4][];
		keys[0] = CryptoPrimitivesModified.generateCmac(key, 1 + word);
		keys[1] = CryptoPrimitivesModified.generateCmac(key, 2 + word);
		keys[2] = CryptoPrimitivesModified.generateCmac(key, 5 + word);
		if (state.get(word) != null) {
			keys[3] = ByteBuffer.allocate(4).putInt(state.get(word)).array();
		} else {
			keys[3] = ByteBuffer.allocate(4).putInt(0).array();
		}

		return keys;
	}

	// ***********************************************************************************************//

	///////////////////// Test /////////////////////////////

	// ***********************************************************************************************//

	public static List<String> queryDyn(byte[][] keys,  SearchDictionary rocks ) throws InvalidKeyException, InvalidAlgorithmParameterException,
			NoSuchAlgorithmException, NoSuchProviderException, NoSuchPaddingException, IOException {

		List<String> result = query(keys, rocks);

//		for (int i = 0; i < ByteBuffer.wrap(keys[3]).getInt(); i++) {
//			byte[] temp = rocks.get("dynamic",CryptoPrimitives.generateCmac(keys[2], "" + i));
//			String decr = new String(CryptoPrimitives.decryptAES_CTR_String(temp, keys[1])).split("\t\t\t")[0];
//
//			result.add(decr);
//		}
		return result;
	}

	// ***********************************************************************************************//

	///////////////////// Forward Secure versions /////////////////////////////

	// ***********************************************************************************************//

	///////////////////// Forward Secure Token generation /////////////////////
	///////////////////// /////////////////////////////

	// ***********************************************************************************************//

	public static byte[][] genTokenFS(byte[] key, String word, HashMap<String, Integer> state) throws UnsupportedEncodingException {
		int counter = 0;
		if (state.get(word) != null) {
			counter = state.get(word);
		}

		byte[][] keys = new byte[2 + counter][];
		keys[0] = CryptoPrimitivesModified.generateCmac(key, 1 + word);
		keys[1] = CryptoPrimitivesModified.generateCmac(key, 2 + word);
		byte[] temp = CryptoPrimitivesModified.generateCmac(key, 5 + word);

		for (int i = 0; i < counter; i++) {
			keys[2 + i] = CryptoPrimitivesModified.generateCmac(temp, "" + i);
		}

		return keys;
	}

	// ***********************************************************************************************//

	///////////////////// Forward Secure Query /////////////////////////////

	// ***********************************************************************************************//

	public static List<String> queryFS(byte[][] keys, SearchDictionary rocks  ) throws InvalidKeyException, InvalidAlgorithmParameterException,
			NoSuchAlgorithmException, NoSuchProviderException, NoSuchPaddingException, IOException {

		List<String> result = query(keys, rocks);

		for (int i = 2; i < keys.length; i++) {
			byte[] temp = rocks.get("dynamic",keys[i]);
			String decr = Base64.getEncoder().encodeToString(CryptoPrimitivesModified.decryptAES_CTR_String(temp, keys[1]));

			result.add(decr);
		}
		return result;
	}

}
