/** * Copyright (C) 2016 Tarik Moataz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//***********************************************************************************************//

/////////////////////    Implementation of 2Lev scheme of NDSS'14 

/////////////////////				Response Hiding 					///////////

//***********************************************************************************************//	

package searchitect.clusion;

import com.google.common.collect.Multimap;

import searchitect.service.SearchDictionary;

import javax.crypto.NoSuchPaddingException;

import org.bouncycastle.crypto.prng.ThreadedSeedGenerator;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.*;

public class RH2LevModifiedRocks {

	// define the number of character that a file identifier can have
	public static final int sizeOfFileIdentifer = 20;
	public static final int sizeOfEncryptedFileIdentifer = 36;
	public static final int sizeOfIV = 16;
	public static String separator = "***";
	public static byte[] master = null;
	public static boolean lmm = false;
	public static String eval = "";

	public static int counter = 0;

	public HashMap<String, byte[]> dictionary = new HashMap<String, byte[]>();
	public static List<Integer> free = Collections.synchronizedList(new ArrayList<Integer>());
	static byte[][] array = null;
	byte[][] arr = null;

	public RH2LevModifiedRocks(HashMap<String, byte[]> dictionary, byte[][] arr) {
		this.dictionary = dictionary;
		this.arr = arr;
	}

	public HashMap<String, byte[]> getDictionary() {
		return dictionary;
	}

	public void setDictionary(HashMap<String, byte[]> dictionary) {
		this.dictionary = dictionary;
	}

	public byte[][] getArray() {
		return arr;
	}

	public void setArray(byte[][] array) {
		this.arr = array;
	}

	public static RH2LevModifiedRocks constructEMMPar(final byte[] key, final Multimap<String, String> lookup,
			final int bigBlock, final int smallBlock, final int dataSize)
			throws InterruptedException, ExecutionException, IOException {

		final HashMap<String, byte[]> dictionary = new HashMap<String, byte[]>();
		// determine the size f the data set and therefore the size of the array

		array = new byte[dataSize][];
		free.clear();
		for (int i = 0; i < dataSize; i++) {
			// initialize all buckets with random values
			free.add(i);
		}

		List<String> listOfKeyword = new ArrayList<String>(lookup.keySet());
		int threads = 0;
		if (Runtime.getRuntime().availableProcessors() > listOfKeyword.size()) {
			threads = listOfKeyword.size();
		} else {
			threads = Runtime.getRuntime().availableProcessors();
		}

		ExecutorService service = Executors.newFixedThreadPool(threads);
		ArrayList<String[]> inputs = new ArrayList<String[]>(threads);

		final Map<Integer, String> concurrentMap = new ConcurrentHashMap<Integer, String>();
		for (int i = 0; i < listOfKeyword.size(); i++) {
			concurrentMap.put(i, listOfKeyword.get(i));
		}

		for (int j = 0; j < threads; j++) {
			service.execute(new Runnable() {
				@Override
				public void run() {

					while (concurrentMap.keySet().size() > 0) {
						Set<Integer> possibleValues = concurrentMap.keySet();

						Random rand = new Random();

						int temp = rand.nextInt(possibleValues.size());

						List<Integer> listOfPossibleKeywords = new ArrayList<Integer>(possibleValues);

						// set the input as randomly selected from the remaining
						// possible keys
						String[] input = { concurrentMap.get(listOfPossibleKeywords.get(temp)) };

						// remove the key
						concurrentMap.remove(listOfPossibleKeywords.get(temp));

						try {

							HashMap<String, byte[]> output = setup(key, input, lookup, bigBlock, smallBlock, dataSize);
							Set<String> keys = output.keySet();

							for (String k : keys) {
								dictionary.put(k, output.get(k));
							}
						} catch (InvalidKeyException | NoSuchAlgorithmException | NoSuchProviderException
								| NoSuchPaddingException | IOException | InvalidAlgorithmParameterException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				}
			});
		}

		service.shutdown();

		// Blocks until all tasks have completed execution after a shutdown
		// request
		service.awaitTermination(Long.MAX_VALUE, TimeUnit.DAYS);

		return new RH2LevModifiedRocks(dictionary, array);
	}

	public static RH2LevModifiedRocks constructEMMParGMM(final byte[] key, final Multimap<String, String> lookup,
			final int bigBlock, final int smallBlock, final int dataSize)
			throws InterruptedException, ExecutionException, IOException {

		final HashMap<String, byte[]> dictionary = new HashMap<String, byte[]>();

		for (int i = 0; i < dataSize; i++) {
			// initialize all buckets with random values
			free.add(i);
		}

		List<String> listOfKeyword = new ArrayList<String>(lookup.keySet());
		int threads = 0;
		if (Runtime.getRuntime().availableProcessors() > listOfKeyword.size()) {
			threads = listOfKeyword.size();
		} else {
			threads = Runtime.getRuntime().availableProcessors();
		}

		ExecutorService service = Executors.newFixedThreadPool(threads);
		ArrayList<String[]> inputs = new ArrayList<String[]>(threads);

		for (int i = 0; i < threads; i++) {
			String[] tmp;
			if (i == threads - 1) {
				tmp = new String[listOfKeyword.size() / threads + listOfKeyword.size() % threads];
				for (int j = 0; j < listOfKeyword.size() / threads + listOfKeyword.size() % threads; j++) {
					tmp[j] = listOfKeyword.get((listOfKeyword.size() / threads) * i + j);
				}
			} else {
				tmp = new String[listOfKeyword.size() / threads];
				for (int j = 0; j < listOfKeyword.size() / threads; j++) {

					tmp[j] = listOfKeyword.get((listOfKeyword.size() / threads) * i + j);
				}
			}
			inputs.add(i, tmp);
		}

		System.out.println("\t End of Partitionning  \n");

		List<Future<HashMap<String, byte[]>>> futures = new ArrayList<Future<HashMap<String, byte[]>>>();
		for (final String[] input : inputs) {
			Callable<HashMap<String, byte[]>> callable = new Callable<HashMap<String, byte[]>>() {
				public HashMap<String, byte[]> call() throws Exception {

					HashMap<String, byte[]> output = setup(key, input, lookup, bigBlock, smallBlock, dataSize);
					return output;
				}
			};
			futures.add(service.submit(callable));
		}

		service.shutdown();

		for (Future<HashMap<String, byte[]>> future : futures) {
			Set<String> keys = future.get().keySet();

			for (String k : keys) {
				dictionary.put(k, future.get().get(k));
			}

		}

		return new RH2LevModifiedRocks(dictionary, array);
	}

	// ***********************************************************************************************//

	///////////////////// Setup /////////////////////////////

	// ***********************************************************************************************//

	public static HashMap<String, byte[]> setup(byte[] key, String[] listOfKeyword, Multimap<String, String> lookup,
			int bigBlock, int smallBlock, int dataSize) throws InvalidKeyException, InvalidAlgorithmParameterException,
			NoSuchAlgorithmException, NoSuchProviderException, NoSuchPaddingException, IOException {

		HashMap<String, byte[]> gamma = new HashMap<String, byte[]>();

		// changed iv generation
		ThreadedSeedGenerator thread = new ThreadedSeedGenerator();
		SecureRandom random = new SecureRandom();
		random.setSeed(thread.generateSeed(20, true));
		byte[] iv = new byte[sizeOfIV];
		// fix size b and size B
		
	
		for (String word : listOfKeyword) {

			counter++;
			if (((float) counter / 10000) == (int) (counter / 10000)) {
				System.out.println("Counter " + counter);
			}

			// generate the tag
			// Note that to avoid key collision all words "word" need to be
			// encoded to have the same length
			byte[] key1 = CryptoPrimitivesModified.generateCmac(key, 1 + word);
			byte[] key2 = CryptoPrimitivesModified.generateCmac(key, 2 + word);

			// generate keys for response-hiding construction for SIV (Synthetic
			// IV)
			byte[] key3 = CryptoPrimitivesModified.generateCmac(master, 3 + new String());

			byte[] key4 = null;
			if (lmm == false) {
				key4 = CryptoPrimitivesModified.generateCmac(master, 4 + word);
			} else {
				key4 = CryptoPrimitivesModified.generateCmac(master, eval);
			}

			// Encryption of the lookup DB(w) deterministically to create unique
			// tags

			List<byte[]> encryptedID = new ArrayList<byte[]>();

			for (String id : lookup.get(word)) {
				byte[] encID=CryptoPrimitivesModified.DTE_encryptAES_CTR_String(key3, key4, id, sizeOfFileIdentifer);
				encryptedID.add(encID);
			}

			// get t and fix Blocksizes
			int t = (int) Math.ceil((float) lookup.get(word).size() / bigBlock);
			int remainder = lookup.get(word).size() % bigBlock;
			int sizeBucket = encryptedID.get(0).length * smallBlock + 1 + 4;
			int sizeBlock = encryptedID.get(0).length * bigBlock + 1 + 4;

			if (encryptedID.size() <= smallBlock) {
				// pad DB(w) to "small block"
				byte[] l = CryptoPrimitivesModified.generateCmac(key1, Integer.toString(0));
				random.nextBytes(iv);
				byte b1 = 0x01;
				ByteBuffer buffer = ByteBuffer.allocate(sizeBucket);
				buffer.put(b1).putInt(encryptedID.size());
				
				for (int i = 0; i < encryptedID.size(); i++) {
					buffer.put(encryptedID.get(i));
				}
				gamma.put(Base64.getEncoder().encodeToString(l),
						CryptoPrimitivesModified.encryptAES_CTR_Byte(key2, iv, buffer.array(), sizeBucket));
			}

			else {

				List<Integer> listArrayIndex = new ArrayList<Integer>();

				for (int j = 0; j < t; j++) {

					ByteBuffer buffer = ByteBuffer.allocate(sizeBlock);
					if (j != t - 1) {
						buffer.putInt(bigBlock);
						int count=0;
						for (byte[] encId : encryptedID.subList(j *bigBlock, (j +1)*bigBlock)) {
							count++;
							buffer.put(encId);
						}
					} else {
						buffer.putInt(remainder);
						for (byte[] encId : encryptedID.subList(j *bigBlock, encryptedID.size())) {
							buffer.put(encId);
						}
					}

					// generate the integer which is associated to free[b]
					int position = random.nextInt(free.size());
					int tmpPos = free.get(position);
					free.remove(position);
					random.nextBytes(iv);
					array[tmpPos] = CryptoPrimitivesModified.encryptAES_CTR_Byte(key2, iv, buffer.array(), sizeBlock);
					listArrayIndex.add(tmpPos);

				}
				// calculate Blocksize for arrayindex
				int smallBlockInt = (sizeBucket - 5) / 4;

				// medium case
				random.nextBytes(iv);
				if (t <= smallBlockInt) {
					ByteBuffer buffer = ByteBuffer.allocate(sizeBucket);
					byte b2 = 0x02;
					buffer.put(b2).putInt(t);
					for (int i = 0; i < t; i++) {
						buffer.putInt(listArrayIndex.get(i));
					}
					byte[] l = CryptoPrimitivesModified.generateCmac(key1, Integer.toString(0));
					gamma.put(Base64.getEncoder().encodeToString(l),
							CryptoPrimitivesModified.encryptAES_CTR_Byte(key2, iv, buffer.array(), sizeBucket));
				}
				// big case
				else {
					int bigBlockInt = (sizeBlock - 5) / 4;
					int tPrime = (int) Math.ceil((float) t / bigBlockInt);
					int remainderInt = t % bigBlockInt;
					List<Integer> listArrayIndexTwo = new ArrayList<Integer>();

					for (int l = 0; l < tPrime; l++) {

						ByteBuffer buffer = ByteBuffer.allocate(sizeBlock);
						if (l != t - 1) {
							buffer.putInt(bigBlockInt);
							for (int index : listArrayIndex.subList(l*bigBlockInt, (l+1)*bigBlockInt)) {
								buffer.putInt(index);
							}
						} else {
							buffer.putInt(remainderInt);
							for (int index : listArrayIndex.subList(l *bigBlockInt, listArrayIndex.size())) {
								buffer.putInt(index);
							}
						}

						// generate the integer which is associated to free[b]
						int position = random.nextInt(free.size());
						int tmpPos = free.get(position);
						free.remove(position);
						random.nextBytes(iv);
						array[tmpPos] = CryptoPrimitivesModified.encryptAES_CTR_Byte(key2, iv, buffer.array(), sizeBlock);
						listArrayIndexTwo.add(tmpPos);
					}

					ByteBuffer buffer2 = ByteBuffer.allocate(sizeBucket);
					byte b3 = 0x03;
					buffer2.put(b3).putInt(listArrayIndexTwo.size());
					for (int index : listArrayIndexTwo) {
						buffer2.putInt(index);
					}

					random.nextBytes(iv);
					// Pad the second set of identifiers
					byte[] l = CryptoPrimitivesModified.generateCmac(key1, Integer.toString(0));
					gamma.put(Base64.getEncoder().encodeToString(l),
							CryptoPrimitivesModified.encryptAES_CTR_Byte(key2, iv, buffer2.array(), sizeBucket));

				}

			}

		}

		return gamma;
	}

	// ***********************************************************************************************//

	///////////////////// Query /////////////////////////////

	// ***********************************************************************************************//
	public static List<String> query(byte[][] keys, SearchDictionary rocks)
			throws InvalidKeyException, InvalidAlgorithmParameterException, NoSuchAlgorithmException,
			NoSuchProviderException, NoSuchPaddingException, IOException {

		byte[] l = CryptoPrimitivesModified.generateCmac(keys[0], Integer.toString(0));
		byte[] tempList = rocks.get("static", l);
		if (!(tempList == null)) {
			ByteBuffer buffer = ByteBuffer.wrap(CryptoPrimitivesModified.decryptAES_CTR_String(tempList, keys[1]));
			List<String> resultFinal = new ArrayList<String>();
			// We remove the flag that identifies the size of the dataset
			byte b = buffer.get();

			// small case - encid in bucket
			if (b == 0x01) {
				int num = buffer.getInt();
				byte[] encID = new byte[sizeOfEncryptedFileIdentifer];
				for (int i = 0; i < num; i++) {
					buffer.get(encID);
					resultFinal.add(Base64.getEncoder().encodeToString(encID));
				}
				return resultFinal;
			}
			// medium case - array index in bucket and encid in array
			else if (b == 0x02) {
				int num = buffer.getInt();
				for (int i = 0; i < num; i++) {
					byte[] index = new byte[4];
					buffer.get(index);
					byte[] arrayValue = rocks.get("array", index);
					ByteBuffer buffer2 = ByteBuffer
							.wrap(CryptoPrimitivesModified.decryptAES_CTR_String(arrayValue, keys[1]));
					int next = buffer2.getInt();
					for (int j = 0; j < next; j++) {
						byte[] encID = new byte[sizeOfEncryptedFileIdentifer];
						buffer2.get(encID);
						resultFinal.add(Base64.getEncoder().encodeToString(encID));
					}
				}
				return resultFinal;
			}
			// large case - array index in bucket and encid in array
			else if (b == 0x03) {
				int num = buffer.getInt();
				byte[] index = new byte[4];
				for (int i = 0; i < num; i++) {
					buffer.get(index);
					byte[] arrayValue = rocks.get("array", index);
					ByteBuffer buffer2 = ByteBuffer
							.wrap(CryptoPrimitivesModified.decryptAES_CTR_String(arrayValue, keys[1]));
					int num2 = buffer2.getInt();
					byte[] index2 = new byte[4];
					for (int j = 0; j < num2; j++) {
						buffer.get(index2);
						byte[] arrayValue2 = rocks.get("array", index2);
						ByteBuffer buffer3 = ByteBuffer
								.wrap(CryptoPrimitivesModified.decryptAES_CTR_String(arrayValue2, keys[1]));
						int num3 =buffer3.getInt();
						for (int k = 0; k < num3; k++) {
							byte[] encID = new byte[sizeOfEncryptedFileIdentifer];
							buffer3.get(encID);
							resultFinal.add(Base64.getEncoder().encodeToString(encID));

						}
					}
				}

				return resultFinal;
			}
		}
		return new ArrayList<String>();

	}

	// ***********************************************************************************************//

	///////////////////// Resolve Algorithm /////////////////////////////

	// ***********************************************************************************************//

	public static List<String> resolve(byte[] key, List<String> list)
			throws InvalidKeyException, InvalidAlgorithmParameterException, NoSuchAlgorithmException,
			NoSuchProviderException, NoSuchPaddingException, IOException {

		List<String> result = new ArrayList<String>();

		for (String id : list) {
			byte[] id2 = Base64.getDecoder().decode(id);
			result.add(new String(CryptoPrimitivesModified.decryptAES_CTR_String(id2, key)).split("###")[0]);
		}

		return result;
	}

}
