package searchitect.common.viewdynrh2lev;


import java.util.Collection;
import java.util.NavigableMap;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class UpdateIndexdynrh2lev {
	
	private NavigableMap<String, Collection<byte[]>>dictionary;

	
	public UpdateIndexdynrh2lev(NavigableMap<String, Collection<byte[]>> dictionary){
		this.dictionary = dictionary;
	}


	public NavigableMap <String, Collection<byte[]>>  getDictionary() {
		return dictionary;
	}

	public void setUpdateIndex(NavigableMap <String, Collection<byte[]>> dictionary) {
		this.dictionary= dictionary;
	}
	
	
	public UpdateIndexdynrh2lev(){
		
	}
	

}
