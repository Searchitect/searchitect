package searchitect.common.viewdynrh2lev;

import javax.xml.bind.annotation.XmlRootElement;
/**
 * Token - json object used to commit a search contains the token string
 *
 */
@XmlRootElement
public class SearchTokendynrh2lev {

    private byte [][]searchToken;

    public SearchTokendynrh2lev(byte [][] searchToken) {
	this.searchToken = searchToken;
    }

    public byte [][] getSearchToken() {
	return searchToken;
    }

    public void setSearchToken(byte [][] searchToken) {
	this.searchToken = searchToken;
    }

    public SearchTokendynrh2lev() {

    }

}
