package searchitect.common.viewdynrh2lev;

import java.util.HashMap;

import javax.xml.bind.annotation.XmlRootElement;

import searchitect.common.view.Upload;

@XmlRootElement
public class UploadIndexdynrh2levMap extends Upload{
	
	private HashMap<String, byte[]>dictionary;
	private byte[][] array;
	
	public UploadIndexdynrh2levMap(HashMap<String, byte[]> dictionary, byte[][] arr){
		this.dictionary = dictionary;
		this.array = arr;
	}
	
	public byte[][] getArray(){
		return this.array;
	}
	
	public void setArray(byte[][] array){
		this.array = array;
	}


	public HashMap <String, byte[]>  getDictionary() {
		return dictionary;
	}

	public void setSetupIndex(HashMap <String, byte[]> dictionary) {
		this.dictionary= dictionary;
	}
	
	
	public UploadIndexdynrh2levMap(){
		
	}
	

}
