package searchitect.common.viewdynrh2lev;


import static org.junit.Assert.assertArrayEquals;


import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

import searchitect.common.viewdynrh2lev.SearchTokendynrh2lev;



@RunWith(SpringRunner.class)
public class SearchTokendynrh2levTest {
	
	byte [][]searchToken;

	private byte[][] getTokenArrays(){
		byte [][] st = new byte[2][];
		st[0] = "test1".getBytes();
		st[1] = "test2".getBytes();
		return st;
	}


	@Test
	public void constructorWithNormalSearchTokenSuccessTest() {
		SearchTokendynrh2lev searchTokendynrh2lev = new SearchTokendynrh2lev(getTokenArrays());
		assertArrayEquals(getTokenArrays()[0],searchTokendynrh2lev.getSearchToken()[0]);
	} 
	
	@Test
	public void setWithNormalSearchTokenSuccessTest() {
		SearchTokendynrh2lev searchTokendynrh2lev = new SearchTokendynrh2lev();
		searchTokendynrh2lev.setSearchToken(getTokenArrays());
		assertArrayEquals(getTokenArrays()[0],searchTokendynrh2lev.getSearchToken()[0]);
	} 
	
	
	
}
