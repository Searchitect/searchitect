package searchitect.clusion;

import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertThat;

import java.io.IOException;

import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.spec.InvalidKeySpecException;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ExecutionException;

import javax.crypto.NoSuchPaddingException;


import org.bouncycastle.util.encoders.Hex;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;


import searchitect.service.MemoryDictionary;


@RunWith(SpringRunner.class)
public class DynRH2LevModifiedRocksTest {
	
	
public static Multimap<String, String> getMultimap() {

	// Map<String, List<String>>
	// keyword1 -> doc1, doc2, doc3
	// keyword2 -> doc1, doc4, doc5
	// keyword3 -> doc1, doc6, doc7
	// keyword4 -> doc1, doc3, doc5

	Multimap<String, String> multimap = ArrayListMultimap.create();

	multimap.put("keyword1", "doc1");
	multimap.put("keyword1", "doc2");
	multimap.put("keyword1", "doc3");
	multimap.put("keyword1", "doc4");
	multimap.put("keyword1", "doc5");
	multimap.put("keyword1", "doc6");
	multimap.put("keyword1", "doc7");
	multimap.put("keyword1", "doc8");
	multimap.put("keyword1", "doc9");
	multimap.put("keyword1", "doc10");
	multimap.put("keyword1", "doc11");
	multimap.put("keyword1", "doc12");
	multimap.put("keyword1", "doc13");
	multimap.put("keyword1", "doc14");
	multimap.put("keyword1", "doc15");
	multimap.put("keyword1", "doc16");
	multimap.put("keyword1", "doc17");
	multimap.put("keyword1", "doc18");
	multimap.put("keyword1", "doc19");
	multimap.put("keyword1", "doc20");
	multimap.put("keyword1", "doc21");
	multimap.put("keyword1", "doc22");
	multimap.put("keyword1", "doc23");
	multimap.put("keyword1", "doc24");
	multimap.put("keyword1", "doc25");
	multimap.put("keyword1", "doc26");
	multimap.put("keyword1", "doc27");
	multimap.put("keyword1", "doc28");
	multimap.put("keyword2", "doc1");
	multimap.put("keyword2", "doc4");
	multimap.put("keyword3", "doc1");
	multimap.put("keyword3", "doc6");
	multimap.put("keyword3", "doc7");
	multimap.put("keyword4", "doc1");
	multimap.put("keyword4", "doc3");
	multimap.put("keyword4", "doc5");
	multimap.put("keyword5", "doc5");

	return multimap;
}

public static Multimap<String, String> getUpdateMultimap() {

	// Map<String, List<String>>
	// keyword1 -> doc8, doc9, doc7
	// keyword2 -> doc8, doc9, doc7
	Multimap<String, String> multimap = ArrayListMultimap.create();

	multimap.put("keyword1", "doc8");
	multimap.put("keyword1", "doc9");
	multimap.put("keyword1", "doc7");
	multimap.put("keyword2", "doc8");
	multimap.put("keyword2", "doc9");
	multimap.put("keyword2", "doc7");

	return multimap;
}

@Test
public void dynrh2levmodifiedSetupTestSmallCase() throws InvalidKeySpecException, NoSuchAlgorithmException, NoSuchProviderException, InvalidKeyException, InvalidAlgorithmParameterException, NoSuchPaddingException, IOException, InterruptedException, ExecutionException{

	Multimap <String,String> maps =getMultimap();
	//initialize parameters
	int bigBlock = 10;
	int smallBlock = 3;
	int dataSize = maps.size();
//	int records = maps.size();
//	int keys = maps.keySet().size();
//	int smallBlock = (int)Math.ceil((double)(records/keys))+2;
//	int bigBlock = 	(int) Math.ceil(Math.sqrt((double)(28))+1)+2;
	System.out.println("sizeb: " + smallBlock + "sizeB: "+ bigBlock);
	System.out.println("size of multimap: " + maps.size());
	String password ="testsecurepassword";
	
	//setup
	byte[] key = CryptoPrimitivesModified.keyGen(256, password, "salt/salt", 100);
	// do not forget to set the key
	RH2LevModifiedRocks.master = key;
	//DynRH2LevModified dynrh2lev =DynRH2LevModified.constructEMMParGMM(key, getMultimap(), bigBlock, smallBlock, dataSize);
	
	DynRH2LevModifiedRocks dynrh2lev =DynRH2LevModifiedRocks.constructEMMParGMM(key, getMultimap(), bigBlock, smallBlock, dataSize);
	HashMap<String, byte[]> map =dynrh2lev.getDictionary();
	HashMap<String, Integer> state = dynrh2lev.getState();
	byte[][] array = dynrh2lev.getArray();
	System.out.println("keyset Strings"+map.keySet());
	//upload to server
	//String path ="/tmp/testmodified";
	//RocksDBAdapter rocks = new RocksDBAdapter(path);
	MemoryDictionary memo = new MemoryDictionary();
	memo.putAllHashMap("static",map,true);
	Set<byte[]> test = memo.keySet("static");
	for(byte[] byt:test) {
		System.out.println(byt);
	}
	memo.putAllArray("array", dynrh2lev.getArray());
	byte [][] searchToken = DynRH2LevModifiedRocks.genToken(key, "keyword2", state);
	
	for(int i=0; i<array.length;i++){
		if (array[i]!=null){
		System.out.println(i +"value: " + Hex.toHexString(array[i]));
		}
	}
	List<String> result = DynRH2LevModifiedRocks.queryDyn(searchToken, memo);
	List <String> resultclear = DynRH2LevModifiedRocks.resolve(CryptoPrimitivesModified.generateCmac(key, 3 + new String()), result);
	System.out.println("Resultclear:"+resultclear);
	assertThat(resultclear,hasSize(2));

	
}
	

	@Test
	public void dynrh2levmodifiedSetupTest() throws InvalidKeySpecException, NoSuchAlgorithmException, NoSuchProviderException, InvalidKeyException, InvalidAlgorithmParameterException, NoSuchPaddingException, IOException, InterruptedException, ExecutionException{
	
		Multimap <String,String> maps =getMultimap();
		//initialize parameters
		int bigBlock = 10;
		int smallBlock = 3;
		int dataSize = maps.size();
//		int records = maps.size();
//		int keys = maps.keySet().size();
//		int smallBlock = (int)Math.ceil((double)(records/keys))+2;
//		int bigBlock = 	(int) Math.ceil(Math.sqrt((double)(28))+1)+2;
		System.out.println("sizeb: " + smallBlock + "sizeB: "+ bigBlock);
		System.out.println("size of multimap: " + maps.size());
		String password ="testsecurepassword";
		
		//setup
		byte[] key = CryptoPrimitivesModified.keyGen(256, password, "salt/salt", 100);
		// do not forget to set the key
		RH2LevModifiedRocks.master = key;
		//DynRH2LevModified dynrh2lev =DynRH2LevModified.constructEMMParGMM(key, getMultimap(), bigBlock, smallBlock, dataSize);
		
		DynRH2LevModifiedRocks dynrh2lev =DynRH2LevModifiedRocks.constructEMMParGMM(key, getMultimap(), bigBlock, smallBlock, dataSize);
		HashMap<String, byte[]> map =dynrh2lev.getDictionary();
		HashMap<String, Integer> state = dynrh2lev.getState();
		byte[][] array = dynrh2lev.getArray();
		System.out.println("keyset Strings"+map.keySet());
		//upload to server
		//String path ="/tmp/testmodified";
		//RocksDBAdapter rocks = new RocksDBAdapter(path);
		MemoryDictionary memo = new MemoryDictionary();
		memo.putAllHashMap("static",map,true);
		Set<byte[]> test = memo.keySet("static");
		for(byte[] byt:test) {
			System.out.println(byt);
		}
		memo.putAllArray("array", dynrh2lev.getArray());
		byte [][] searchToken = DynRH2LevModifiedRocks.genToken(key, "keyword1", state);
		
		for(int i=0; i<array.length;i++){
			if (array[i]!=null){
			System.out.println(i +"value: " + Hex.toHexString(array[i]));
			}
		}
		List<String> result = DynRH2LevModifiedRocks.queryDyn(searchToken, memo);
		List <String> resultclear = DynRH2LevModifiedRocks.resolve(CryptoPrimitivesModified.generateCmac(key, 3 + new String()), result);
		System.out.println("Resultclear:"+resultclear);
		assertThat(resultclear,hasSize(28));

		
	}
	
	
	@Test
	public void dynrh2levmodifiedSetupTestBig() throws InvalidKeySpecException, NoSuchAlgorithmException, NoSuchProviderException, InvalidKeyException, InvalidAlgorithmParameterException, NoSuchPaddingException, IOException, InterruptedException, ExecutionException{
	
		Multimap <String,String> maps =getMultimap();
		//initialize parameters
		int bigBlock = 5;
		int smallBlock = 3;
		int dataSize = maps.size();
//		int records = maps.size();
//		int keys = maps.keySet().size();
//		int smallBlock = (int)Math.ceil((double)(records/keys))+2;
//		int bigBlock = 	(int) Math.ceil(Math.sqrt((double)(28))+1)+2;
		System.out.println("sizeb: " + smallBlock + "sizeB: "+ bigBlock);
		System.out.println("size of multimap: " + maps.size());
		String password ="testsecurepassword";
		
		//setup
		byte[] key = CryptoPrimitivesModified.keyGen(256, password, "salt/salt", 100);
		// do not forget to set the key
		RH2LevModifiedRocks.master = key;
		//DynRH2LevModified dynrh2lev =DynRH2LevModified.constructEMMParGMM(key, getMultimap(), bigBlock, smallBlock, dataSize);
		
		DynRH2LevModifiedRocks dynrh2lev =DynRH2LevModifiedRocks.constructEMMParGMM(key, getMultimap(), bigBlock, smallBlock, dataSize);
		HashMap<String, byte[]> map =dynrh2lev.getDictionary();
		HashMap<String, Integer> state = dynrh2lev.getState();
		byte[][] array = dynrh2lev.getArray();
		System.out.println("keyset Strings"+map.keySet());
		//upload to server
		//String path ="/tmp/testmodified";
		//RocksDBAdapter rocks = new RocksDBAdapter(path);
		MemoryDictionary memo = new MemoryDictionary();
		memo.putAllHashMap("static",map,true);
		Set<byte[]> test = memo.keySet("static");
		for(byte[] byt:test) {
			System.out.println(byt);
		}
		memo.putAllArray("array", dynrh2lev.getArray());
		byte [][] searchToken = DynRH2LevModifiedRocks.genToken(key, "keyword1", state);
		
		for(int i=0; i<array.length;i++){
			if (array[i]!=null){
			System.out.println(i +"value: " + Hex.toHexString(array[i]));
			}
		}
		List<String> result = DynRH2LevModifiedRocks.queryDyn(searchToken, memo);
		List <String> resultclear = DynRH2LevModifiedRocks.resolve(CryptoPrimitivesModified.generateCmac(key, 3 + new String()), result);
		System.out.println(resultclear);
		assertThat(resultclear,hasSize(28));

		
	}
	
}
