# Searchitect-Common-DynRH2Lev

### Functionality
 * Contains all common classes of the DynRH2Lev searchable encryption scheme an DynRH2LevRocks variant.

### Packages
* searchitect.clusion
	* ClientStateSophos.java - Sophos state contained by the client
	* Sophos.java - implementation of the Sophos searchable encryption scheme
* searchitect.common.viewdynrh2lev - contains Sophos specific classes exchanged between client and server
	* SearchTokendynrh2lev.java
	* UpdateIndexdynrh2lev.java
	* UploadIndexdynrh2levMap.java

### Compile

	mvn clean install