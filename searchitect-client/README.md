# Searchitect-Client

### Functionality
 * Client part of DynRH2LevRocks Searchable Encryption scheme

### Compile using

	mvn clean install
	
	
### Packages
* searchitect
	* ClientRunner.java - just for testing purposes
	* SophosRunner.java -just for testing purpose
* searchitect.client
	* Client.java - communicates to the server and calls methods of the interface implemented by the plugins
	* DocumentParser.java - implements methods to index a document collection by passing the path as string to the method
* searchitect.scheme
	* ClientTemplateImpl.java - client template implementation