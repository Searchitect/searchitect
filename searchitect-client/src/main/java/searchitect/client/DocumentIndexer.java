package searchitect.client;

import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;



import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;

import searchitect.common.clusion.TextExtractPar;
import searchitect.common.clusion.TextProc;


public class DocumentIndexer implements Serializable {
	
	private String path ="";
	private Multimap <String,String> invertedIndex=null;
	private Multimap <String,String> forwardIndex=null;
	private boolean finished = false;
	
	public DocumentIndexer(String path, Multimap <String,String> invertedIndex, Multimap <String,String> forwardIndex, boolean finished){
		this.setPath(path);
		this.setInvertedIndex(invertedIndex);
		this.setForwardIndex(forwardIndex);
		this.setFinished(true);
	}

	/**
	 * parseDirectory
	 * 
	 * @param path
	 * @return
	 */
	public static DocumentIndexer parseDirectory(String path){
	try{
	ArrayList<File> fileList = new ArrayList<>();
	TextExtractPar.lp1 = ArrayListMultimap.create();
	TextProc.listf(path, fileList);
	TextProc.TextProc(false, path);
	return new DocumentIndexer(path, TextExtractPar.lp1, TextExtractPar.lp2, true);
    }
    catch(Exception e){
    	System.out.println("creation of inverted index failed");
    	return null;
    }
    	
    
    }

	/**
	 * @return the path
	 */
	public String getPath() {
		return path;
	}

	/**
	 * @param path the path to set
	 */
	public void setPath(String path) {
		this.path = path;
	}

	/**
	 * @return the invertedIndex
	 */
	public Multimap <String,String> getInvertedIndex() {
		return invertedIndex;
	}

	/**
	 * @param invertedIndex the invertedIndex to set
	 */
	public void setInvertedIndex(Multimap <String,String> invertedIndex) {
		this.invertedIndex = invertedIndex;
	}

	/**
	 * @return the forwardIndex
	 */
	public Multimap <String,String> getForwardIndex() {
		return forwardIndex;
	}

	/**
	 * @param forwardIndex the forwardIndex to set
	 */
	public void setForwardIndex(Multimap <String,String> forwardIndex) {
		this.forwardIndex = forwardIndex;
	}

	/**
	 * @return the finished
	 */
	public boolean isFinished() {
		return finished;
	}

	/**
	 * @param finished the finished to set
	 */
	public void setFinished(boolean finished) {
		this.finished = finished;
	}
	
	/**
	 * @return the forwardIndex
	 */
	public int getSizeOfDocuments() {
		return forwardIndex.keys().size();
	}
    
    
    
    
}
