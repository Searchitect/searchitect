package searchitect.client;

import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.List;

import javax.net.ssl.SSLContext;

import org.apache.http.client.HttpClient;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.ssl.TrustStrategy;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.Multimap;

import searchitect.common.client.ClientScheme;
import searchitect.common.exception.SearchitectException;
import searchitect.common.view.RepositoryInfo;
import searchitect.common.view.SearchResult;
import searchitect.common.view.Upload;
import searchitect.common.view.UserAccountResponse;
import searchitect.common.view.UserAuthRequest;
import searchitect.common.view.UserAuthResponse;


public class Client {
	private String domain;
	private ClientScheme clientimpl;
	private String currentRepositoryName;
	//private static List<RepositoryInfo> repositoryInfo;
	private UserAuthResponse jwToken;
	public RestTemplate restTemplate;
	private String backendName;
	private ObjectMapper mapper;

	public Client(String domain, ClientScheme clientimpl) {
		this.domain = domain;
		this.clientimpl = (ClientScheme) clientimpl;
		this.backendName = clientimpl.getImplName();
		this.mapper = new ObjectMapper();
		// enable TLS connection
		final TrustStrategy trustAllStrategy = (final X509Certificate[] chain, final String authType) -> true;
		SSLContext sslContext;
		try {
			sslContext = org.apache.http.ssl.SSLContexts.custom().loadTrustMaterial(null, trustAllStrategy).build();
			SSLConnectionSocketFactory csf = new SSLConnectionSocketFactory(sslContext);
			HttpClient httpClient = HttpClients.custom().setSSLSocketFactory(csf).build();
			HttpComponentsClientHttpRequestFactory requestFactory = new HttpComponentsClientHttpRequestFactory(
					httpClient);
			this.restTemplate = new RestTemplate(requestFactory);
		} catch (Exception e) {
			System.out.println("Could not establish secure connection");
		}
	}

	/**
	 * Set the domain
	 * 
	 * @param domain
	 */
	public void setDomain(String domain) {
		this.domain = domain;
	}

	/**
	 * Set the Client implementation - better do not set here instantiate new
	 * Client
	 * 
	 * @param clientimpl
	 */
	public void setClientImpl(ClientScheme clientimpl) {
		this.clientimpl = clientimpl;
	}
	
	/**
	 * Get the Client implementation - better do not set here instantiate new
	 * Client
	 * 
	 * @param clientimpl
	 */
	public ClientScheme getClientImpl(ClientScheme clientimpl) {
		return this.clientimpl;
	}

	/**
	 * Current Client scheme
	 * 
	 * @param clientimpl
	 */
	public void setCurrent(ClientScheme clientimpl) {
		this.clientimpl = clientimpl;
	}

	/**
	 * Set the java web token
	 * 
	 * @param token
	 */
	public void setJwToken(String token) {
		this.jwToken = new UserAuthResponse(token);
	}

	/**
	 * Check if gateway service is up
	 * 
	 * @return Hello String
	 */
	public String check() {
		String result = restTemplate.getForObject(domain, String.class);
		return result;
	}

	/**
	 * Check if backend is up
	 * 
	 * @return Hello String
	 */
	public String checkBackend() {
		String result = restTemplate.getForObject(domain.concat("checkbackend/").concat(backendName), String.class);
		return result;
	}

	/**
	 * Do a authentication check
	 * 
	 * @return ok and settings
	 */
	public ResponseEntity<String> checkAuthBackend() {
		HttpEntity<String> request = setRequest(null, true);
		ResponseEntity<String> result = restTemplate.exchange(domain.concat("/checkauthbackend/".concat(backendName)),
				HttpMethod.GET, request, String.class);
		return result;
	}

	/**
	 * Create a new user
	 * 
	 * @param user
	 * @return ok
	 * @throws JsonProcessingException 
	 */
	public ResponseEntity<String> createUser(UserAuthRequest user) throws JsonProcessingException {
	
		HttpEntity<String> request = setRequest(mapper.writeValueAsString(user), false);
		ResponseEntity<String> result = restTemplate.exchange(domain.concat("user"), HttpMethod.POST, request,
				String.class);
		return result;
	}

	/**
	 * Get user account
	 * 
	 * @param user
	 * @return UserAccountResponse contains repositories
	 */
	public UserAccountResponse getUserAccount(UserAuthRequest user) {
		HttpEntity<String> request = setRequest(null, true);
		ResponseEntity<UserAccountResponse> result = restTemplate.exchange(
				domain.concat("user/".concat(user.getUsername())), HttpMethod.GET, request, UserAccountResponse.class);
		return result.getBody();
	}

	/**
	 * Delete the user account
	 * 
	 * @param user
	 * @return true or false
	 */
	public boolean deleteUserAccount(UserAuthRequest user) {
		HttpEntity<String> request = setRequest(null, true);
		ResponseEntity<String> result = restTemplate.exchange(domain.concat("user/".concat(user.getUsername())),
				HttpMethod.DELETE, request, String.class);
		if (result.getStatusCode().is2xxSuccessful()) {
			return true;
		} else
			return false;
	}

	/**
	 * Authenticate your user
	 * 
	 * @param user
	 *            UserAuthRequest contains username and password
	 * @return json web token which is further used for authentication
	 * @throws JsonProcessingException 
	 */
	public UserAuthResponse authenticateUser(UserAuthRequest user) throws JsonProcessingException {
		HttpEntity<String> request = setRequest(mapper.writeValueAsString(user), false);
		UserAuthResponse result = restTemplate.postForObject(domain.concat("auth"), request, UserAuthResponse.class);
		this.jwToken = result;
		return result;
	}

	/**
	 * Setup a new encrypted index repository
	 * 
	 * @param password
	 * @param invertedIndex
	 * @param numberOfDocuments
	 * @return
	 * @throws SearchitectException
	 * @throws JsonProcessingException
	 */
	public RepositoryInfo setup(String password, Multimap<String, String> invertedIndex, int numberOfDocuments)
			throws SearchitectException {
		try {
			Upload ups =clientimpl.setup(password, invertedIndex, numberOfDocuments);
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_JSON);
			List<MediaType> acceptList = new ArrayList<>();
			acceptList.add(MediaType.APPLICATION_JSON);
			headers.setAccept(acceptList);
			String jwt = new StringBuilder().append("Bearer ").append(this.jwToken.getToken().toString()).toString();headers.add("Authorization", jwt);
			HttpEntity<Upload> request = new HttpEntity<>(ups, headers); 
			String requestUrl = this.domain.concat("/backend/").concat(clientimpl.getImplName()).concat("/repository");
			RepositoryInfo result = restTemplate.postForObject(requestUrl, request, RepositoryInfo.class);
			currentRepositoryName = result.getrepositoryName();
			return result;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * search the encrypted index with a single keyword
	 * 
	 * @param repository
	 * @param query
	 * @return resultlist of matches
	 */
	public SearchResult search(String repository, String query) {
		try {
			if (repository instanceof String) {
				currentRepositoryName = repository;
			}
			if(query.length()>20){
				query=query.substring(0, 20);
			}
			HttpEntity<String> request = setRequest(clientimpl.search(query), true);
			String requestUrl = domain.concat("/backend/").concat(clientimpl.getImplName()).concat("/repository/")
					.concat(currentRepositoryName).concat("/search");
			SearchResult res = restTemplate.postForObject(requestUrl, request, SearchResult.class);
			SearchResult result = clientimpl.resolve(res);
			return result;
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(e.getCause());
			throw new SearchitectException();
		}

	}

	/**
	 * Update the repository
	 * 
	 * @param repository
	 * @param invertedIndex
	 * @param username
	 * @return
	 */
	public ResponseEntity<String> update(String repository, Multimap<String, String> invertedIndex, String username) {
		if (repository instanceof String) {
			currentRepositoryName = repository;
		}
		try {
			HttpEntity<String> request = setRequest(clientimpl.update(invertedIndex), true);
			String requestUrl = domain.concat("/backend/").concat(clientimpl.getImplName()).concat("/repository/")
					.concat(currentRepositoryName);
			ResponseEntity<String> result = restTemplate.exchange(requestUrl, HttpMethod.POST, request, String.class);
			return result;
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Update failed");
			return null;
		}
	}

	/**
	 * Delete the repository
	 * 
	 * @param repository
	 */
	public void delete(String repository) {
		if (repository instanceof String) {
			currentRepositoryName = repository;
		}
		try {
			HttpEntity<String> request = setRequest(null, true);
			String requestUrl = domain.concat("/backend/").concat(clientimpl.getImplName()).concat("/repository/")
					.concat(currentRepositoryName);
			restTemplate.exchange(requestUrl, HttpMethod.DELETE, request, String.class,
					1);
		} catch (Exception e) {
			System.out.println("Delete failed");
		}
	}

	private HttpEntity<String> setRequest(String jsonbody, boolean auth) {
		HttpHeaders headers = new HttpHeaders();
		if (jsonbody instanceof String) {
			headers.setContentType(MediaType.APPLICATION_JSON);
		}
		List<MediaType> acceptList = new ArrayList<>();
		acceptList.add(MediaType.APPLICATION_JSON);
		headers.setAccept(acceptList);
		if (auth) {
			String jwt = new StringBuilder().append("Bearer ").append(this.jwToken.getToken().toString()).toString();
			headers.add("Authorization", jwt);
		}
		HttpEntity<String> request = new HttpEntity<>(jsonbody, headers);
		return request;
	}

}
