package searchitect;



import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.ResponseEntity;

import searchitect.client.Client;
import searchitect.client.DocumentIndexer;
import searchitect.client.sophos.ClientSophosImpl;
import searchitect.common.client.ClientScheme;
import searchitect.common.view.RepositoryInfo;
import searchitect.common.view.SearchResult;
import searchitect.common.view.UserAuthRequest;
import searchitect.common.view.UserAuthResponse;

@SpringBootApplication
public class ClientRunner {


    public static void main(String[] args) {
        SpringApplication.run(ClientRunner.class, args);
        System.out.println("welcome to searchitect");
        final String uri = "https://localhost:8433/";
        String setuppath = "/home/nan/Downloads/maildir/allen-p/";
        String updatepath = "../update";
        String username ="xyz";
        String query= "first";
    	
    	DocumentIndexer parser = DocumentIndexer.parseDirectory(setuppath);
		
        try {
       // ClientScheme clientimpl = (ClientScheme) new Clientdynrh2levImpl();
        ClientScheme clientimpl = (ClientScheme) new ClientSophosImpl();
        Client client = new  Client(uri,clientimpl);
        System.out.println(client.check());
        //System.out.println(client.checkBackend());
      try {
      System.out.println("create user\n");
      ResponseEntity<String> string = client.createUser(new UserAuthRequest(username, "notsecurepassword"));
      System.out.println(string.getStatusCodeValue());
	    System.out.println("authenticate user\n");
      }
	    catch(Exception e) {
	    	System.out.println("user exists!! \n");
	    }
  
       	UserAuthResponse token = client.authenticateUser(new UserAuthRequest(username, "notsecurepassword"));
       	System.out.println(token.getToken());
		System.out.println("Check authentication and backend communication: \n");
		System.out.println(client.checkAuthBackend());
	    System.out.println("Setup: \n");
		while(parser.isFinished()==false){
			 System.out.println("...is Parsing");
		}
		RepositoryInfo repo = 	client.setup("testpwd", parser.getInvertedIndex(),parser.getSizeOfDocuments());
        System.out.println("Repository has been initialized: \n" + repo.getrepositoryName());
        System.out.println("1. Search: \n");
        SearchResult result1 = client.search(repo.getrepositoryName(), query);
        System.out.println("Searchresult1: \n" + result1.getResultList().toString());
    	parser = DocumentIndexer.parseDirectory(updatepath);
        System.out.println("Update: \n");
        ResponseEntity<String> up = client.update(repo.getrepositoryName(), parser.getInvertedIndex(), username);
        System.out.println("Update result: \n" + up.getStatusCodeValue());
        System.out.println("2. Search: \n");
        SearchResult result2 =client.search(repo.getrepositoryName(), query);
        System.out.println("Searchresult2: \n" + result2.getResultList().toString());
        System.out.println("2. Update: \n");
        ResponseEntity<String> up2 = client.update(repo.getrepositoryName(), parser.getInvertedIndex(), username);
        System.out.println("Update result: \n" + up2.getStatusCodeValue());
        System.out.println("3. Search: \n");
        SearchResult result3 =client.search(repo.getrepositoryName(), query);
        System.out.println("Searchresult3: \n" + result3.getResultList().toString());
      }
      catch(Exception e){
      	   System.out.println("creation of inverted index failed");
      }
        
        //Template
//        ClientScheme clientimpl = new ClientTemplateImpl();
//        Client client = new Client(uri,clientimpl);
//        System.out.println(client.check());
//        //System.out.println(client.checkBackend());
//        try {
//        System.out.println("create user\n");
//        ResponseEntity<String> string = client.createUser(new UserAuthRequest(username, "notsecurepassword"));
//        System.out.println(string.getStatusCodeValue());
//	    System.out.println("authenticate user\n");
//        }
//	    catch(Exception e) {
//	    	System.out.println("user exists!! \n");
//	    }
//        UserAuthResponse user = client.authenticateUser(new UserAuthRequest(username, "notsecurepassword"));
//        System.out.println(user.getToken());
//		System.out.println("Check authentication and backend communication: \n");
//		System.out.println(client.checkAuthBackend());
//	    System.out.println("setup: \n");
//	    RepositoryInfo repositoryInfo =client.setup(setuppath, null);
//	    System.out.println(repositoryInfo.getrepositoryName());
//        System.out.println("search:\n");
//        SearchResult searchResult =client.search(null,query);
//        System.out.println(searchResult.getResultList());
//        System.out.println("update:\n");
//        ResponseEntity<String> update = client.update(null,updatepath,username);
//        System.out.println(update.getStatusCodeValue());
//        if(client.deleteUserAccount(new UserAuthRequest(username, "notsecurepassword"))){
//        	System.out.println("deleted user!!");
//        }
//        else {
//        	System.out.println("could not delete user!!");
//        }
       	
    }

/*
 * 
 * Option help =new Option("help",);
 * 
 * public void Options(){ Options options = new Options(); options.addOption(
 * help ); options.addOption( version );cd options.addOption( quiet );
 * options.addOption( verbose ); options.addOption( debug ); }
 * 
 * 
 * 
 * ml.options.Options options = new ml.options.Options(args, 1);
 * options.addSet("create", 4).addOption("c"); options.addSet("createBatch",
 * 2).addOption("bc"); options.addSet("update", 3).addOption("u");
 * options.addSet("delete", 2).addOption("d"); options.addSet("select",
 * 2).addOption("s"); options.addSet("addURL", 3).addOption("ac");
 * options.addSet("addURLBatch", 2).addOption("bac");
 * options.addSet("removeURL", 3).addOption("rc");
 * options.addSet("selectAll").addOption("sa");
 * options.addSet("contexts").addOption("con");
 * options.addSet("initTables").addOption("init");
 * options.addSet("deleteTables").addOption("drop");
 * options.addOptionAllSets("v", ml.options.Options.Multiplicity.ZERO_OR_ONE);
 * ml.options.OptionSet optionSet = options.getMatchingSet();
 * 
 */
}
