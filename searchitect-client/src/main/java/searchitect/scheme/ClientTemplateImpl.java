package searchitect.scheme;

import java.io.File;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.Multimap;



import searchitect.common.client.ClientScheme;
import searchitect.common.exception.SearchitectException;
import searchitect.common.view.SearchResult;
import searchitect.common.view.SearchToken;
import searchitect.common.view.Upload;
import searchitect.common.view.UploadIndex;


public class ClientTemplateImpl implements ClientScheme {

	String implName = "template";
	ObjectMapper mapper = new ObjectMapper();

	public String getImplName() {
		return implName;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see searchitect.scheme.IClientScheme#setup(java.lang.String)
	 */
	public Upload setup(String password, Multimap<String, String> invertedIndex, int numberOfDocuments) {
		try {
		return new UploadIndex(invertedIndex);

		} catch (Exception e) {
			((Throwable) e).printStackTrace();
			return null;
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see searchitect.scheme.IClientScheme#search(java.lang.String)
	 */
	public String search(String query) {
		// generate token is kept in plaintext
		SearchToken token = new SearchToken(query);
		try {
			return mapper.writeValueAsString(token);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
			throw new SearchitectException();
		
		}

	}


	public String update (Multimap<String, String> invertedIndex) {
		try {
			return mapper.writeValueAsString(invertedIndex.asMap());
		} catch (JsonProcessingException e) {
			e.printStackTrace();
			throw new SearchitectException();
		}
	}

	
	public SearchResult resolve(SearchResult encrypted) throws SearchitectException{
		return encrypted;
	}


	public ClientScheme generateFromState(File state) throws SearchitectException {
		return this;
	}

}
