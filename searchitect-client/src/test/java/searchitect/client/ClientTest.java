package searchitect.client;

import org.apache.commons.io.FileUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.client.MockRestServiceServer;
import org.springframework.web.client.HttpClientErrorException;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import static org.springframework.test.web.client.match.MockRestRequestMatchers.requestTo;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.method;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.jsonPath;
import static org.junit.Assert.assertNotNull;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.header;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withSuccess;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;

import static org.springframework.test.web.client.response.MockRestResponseCreators.withCreatedEntity;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withBadRequest;

import searchitect.ClientRunner;
import searchitect.common.client.ClientScheme;
import searchitect.common.view.UserAuthRequest;
import searchitect.common.view.UserAuthResponse;
import searchitect.scheme.ClientTemplateImpl;


@RunWith(SpringRunner.class)
@SpringBootTest(classes = ClientRunner.class)
public class ClientTest {

	private final String domain = "https://localhost:8433/";
	private final String backendrequest = "https://localhost:8433/checkbackend/template";
	private ClientScheme clientScheme = new ClientTemplateImpl();
	private Client client = new Client(domain, clientScheme);
	private String username = "xyz";
	private String password = "pwd";
	
	public static String setupPath= "/tmp/test";
	public static String updatePath= "/tmp/update";


	private static final String path1 = "/tmp/test/test1.txt";
	private static final String  path2 = "/tmp/test/test2.txt";
	private static final String  path3 = "/tmp/test/test3.txt";
	private static final String upath1 = "/tmp/update/utest1.txt";
	private static final String  upath2 = "/tmp/update/utest2.txt";
	private static final String  upath3 = "/tmp/update/utest3.txt";
	
	
	public static void createTestFiles() throws IOException {
        Files.createDirectories( Paths.get(path1).getParent());
        Files.createFile(Paths.get(path1));
        PrintWriter writer1 = new PrintWriter(path1, "UTF-8");
        writer1.println("The first line");
        writer1.println("The second line");
        writer1.close();
        Files.createFile(Paths.get(path2));
        PrintWriter writer2 = new PrintWriter(path2, "UTF-8");
        writer2.println("The first keyword1");
        writer2.println("The second keyword2");
        writer2.close();
        Files.createFile(Paths.get(path3));
        PrintWriter writer3 = new PrintWriter(path3, "UTF-8");
        writer3.println("weired keyword1");
        writer3.println("The second keyword2");
        writer3.println(" first ");
        writer3.close();
	}
	
	
	public static void createUpdateTestFiles() throws IOException {
        Files.createDirectories(Paths.get(upath1).getParent());
        Files.createFile(Paths.get(upath1));
        PrintWriter writer4 = new PrintWriter(upath1, "UTF-8");
        writer4.println("The first update line");
        writer4.println("The second update line");
        writer4.close();
        Files.createFile(Paths.get(upath2));
        PrintWriter writer5 = new PrintWriter(upath2, "UTF-8");
        writer5.println("The first update keyword1");
        writer5.println("The second update keyword2");
        writer5.close();
        Files.createFile(Paths.get(upath3));
        PrintWriter writer6 = new PrintWriter(upath3, "UTF-8");
        writer6.println("weired update keyword1");
        writer6.println("The second update keyword2");
        writer6.close();
	}
	
	public static void deleteTestFiles() throws IOException {
		FileUtils.deleteDirectory(new File("/tmp/test/"));
		FileUtils.deleteDirectory(new File("/tmp/update/"));
	}
	

	@SuppressWarnings("rawtypes")
	private HttpMessageConverter mappingJackson2HttpMessageConverter;
	MockRestServiceServer mockServer = MockRestServiceServer.createServer(client.restTemplate);

	@Autowired
	void setConverters(HttpMessageConverter<?>[] converters) {

		this.mappingJackson2HttpMessageConverter = Arrays.asList(converters).stream()
				.filter(hmc -> hmc instanceof MappingJackson2HttpMessageConverter).findAny().orElse(null);

		assertNotNull("the JSON message converter must not be null", this.mappingJackson2HttpMessageConverter);
	}

	@Test
	public void checkTest() {
		mockServer.expect(requestTo(domain))
				.andRespond(withSuccess("Mocked: Greetings from searchitect", MediaType.TEXT_PLAIN));
		client.check();
		mockServer.verify();
	}

	@Test
	public void checkBackendTest() {
		mockServer.expect(requestTo(backendrequest))
				.andRespond(withSuccess("Mocked: Greetings from backend searchitect", MediaType.TEXT_PLAIN));
		client.checkBackend();
		mockServer.verify();
	}

	@Test
	public void createUserSuccessTest() {

		try {
			mockServer.expect(requestTo(new StringBuilder().append(domain).append("user").toString()))
					.andExpect(method(HttpMethod.POST)).andExpect(jsonPath("$.username").value(username))
					.andExpect(jsonPath("$.password").value(password)).andRespond(withCreatedEntity(
							new URI(new StringBuilder().append(domain).append("user/").append(username).toString())));
		} catch (URISyntaxException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		try {
			client.createUser(new UserAuthRequest(username, password));
			mockServer.verify();
		} catch (Exception e) {
		}
	}

	@Test
	public void createUserFailsTest() {
		mockServer.expect(requestTo(new StringBuilder().append(domain).append("user").toString()))
				.andExpect(method(HttpMethod.POST)).andExpect(jsonPath("$.username").value(username))
				.andExpect(jsonPath("$.password").value(password))
				.andRespond(withSuccess("Mocked: User created", MediaType.TEXT_PLAIN));
		try {
			client.createUser(new UserAuthRequest(username, password));
			mockServer.verify();
		} catch (Exception e) {

		}
	}

	@Test
	public void authenticateSuccessTest() throws JsonProcessingException {
		ObjectMapper mapper = new ObjectMapper();
		String jsonStrings = mapper.writeValueAsString(new UserAuthResponse("testToken"));
		mockServer.expect(requestTo(new StringBuilder().append(domain).append("auth").toString()))
				.andExpect(method(HttpMethod.POST)).andExpect(jsonPath("$.username").value(username))
				.andExpect(jsonPath("$.password").value(password))
				.andRespond(withSuccess(jsonStrings, MediaType.APPLICATION_JSON));

		UserAuthResponse userAuth = client.authenticateUser(new UserAuthRequest(username, password));
		mockServer.verify();
		System.out.println(userAuth);
		// assertThat(userAuth.getToken(), containsString("testToken"));
	}

	@Test(expected = HttpClientErrorException.class)
	public void authenticateFailedTest() throws JsonProcessingException {
		String username = "xyz";
		String password = "pwd";
		mockServer.expect(requestTo(new StringBuilder().append(domain).append("auth").toString()))
				.andExpect(method(HttpMethod.POST)).andExpect(jsonPath("$.username").value(username))
				.andExpect(jsonPath("$.password").value(password)).andRespond(withBadRequest());
		client.authenticateUser(new UserAuthRequest(username, password));
		mockServer.verify();
		// assertThat(userAuth.getStatusCode(),is(400));
	}

	@Test
	public void setupSuccessTest() {
		mockServer
				.expect(requestTo(new StringBuilder().append(domain).append("/backend/")
						.append(clientScheme.getImplName()).append("/repository").toString()))
				.andExpect(method(HttpMethod.POST)).andExpect(header("Authorization", "Bearer: testToken"))
				.andExpect(jsonPath("first").value("test1"))
				.andRespond(withSuccess("Mocked: Repository created", MediaType.TEXT_PLAIN));

		try {
			DocumentIndexer parser = DocumentIndexer.parseDirectory("/tmp/test/");
			if (parser.isFinished()) {
				client.setup("testpwd", parser.getInvertedIndex(), parser.getSizeOfDocuments());
				mockServer.verify();
			}
		} catch (Exception e) {

		}
	}


	@Test
	public void deleteSuccessTest() {
		String repositoryName = "/testName";
		mockServer
				.expect(requestTo(new StringBuilder().append(domain).append("/backend/")
						.append(clientScheme.getImplName()).append("/repository").append(repositoryName).toString()))
				.andExpect(method(HttpMethod.DELETE)).andExpect(header("Authorization", "Bearer: testToken"))
				.andRespond(withSuccess("Mocked: delete", MediaType.TEXT_PLAIN));
		client.delete(repositoryName);

	}

}
