package searchitect.controller;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import searchitect.model.UserEDB;
import searchitect.common.view.UserAccountResponse;
import searchitect.common.view.UserAuthRequest;
import searchitect.model.UserAccount;
import searchitect.service.UserAccountRepository;

import java.net.URI;
import java.security.Principal;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@RestController
public class UserAccountController {

	private final UserAccountRepository accountRepository;
	
	private final PasswordEncoder passwordEncoder;

	/**
	 * Constructor of AccountController initializes account Repository
	 * @param accountRepository
	 */
	@Autowired
	UserAccountController(UserAccountRepository accountRepository, 	PasswordEncoder passwordEncoder) {
		this.accountRepository = accountRepository;
		this.passwordEncoder = passwordEncoder;
	}
	
	public UserAccountRepository getUserAccountRepository(){
		return this.accountRepository;
	}
	
	/**
	 * getAllUserAccounts
	 * @return List of all Users
	 */
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	@RequestMapping(method = RequestMethod.GET, value = "users")
	List<UserAccount> getAllUserAccounts() {
		return this.accountRepository.findAll();
	}


	/**
	 * createUserAccount - no need to secure (no sessiontoken)
	 * @param input - Account Json 
	 * @return created user ressource or 
	 */
	@RequestMapping(method = RequestMethod.POST, value = "user")
	ResponseEntity<?> createUserAccount(@RequestBody UserAuthRequest input) {
		Optional <UserAccount> testUser = this.accountRepository.findByUsername(input.getUsername());
		if(!testUser.isPresent()){
			UserAccount result = this.accountRepository.saveAndFlush(new UserAccount(input.getUsername(), passwordEncoder.encode(input.getPassword())));
			URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/"+input.getUsername()).buildAndExpand(result).toUri();
			return ResponseEntity.created(location).build();
		}
		else {
			return ResponseEntity.badRequest().build();
		}
	}
	
	/**
	 * getUserAccount
	 * @param userId
	 * @return
	 */
	@PreAuthorize("hasRole('ROLE_USER')")
	@RequestMapping(method = RequestMethod.GET, value = "user/{u_id}")
	ResponseEntity<?> getUserAccount(@AuthenticationPrincipal Principal principal, @PathVariable ("u_id") String userName) {
		Optional<UserAccount> account = this.accountRepository.findByUsername(userName);
			if (account.isPresent() && principal.getName().equals(userName)){
				
			return ResponseEntity.ok(new UserAccountResponse(account.get().getUsername(),account.get().getEDBasUserEDBResponse(), account.get().getAuthoritiesAsString()));
		}
		else {
			return ResponseEntity.badRequest().build();
		}
	}
	
	/**
	 * UpdateUserAccount
	 *
	 * @param userId
	 * @param input
	 * @return
	 */
	@PreAuthorize("hasRole('ROLE_USER')")
	@RequestMapping(method = RequestMethod.POST, value = "user/{u_id}")
	ResponseEntity<?> updateUserAccount(@AuthenticationPrincipal Principal principal, @PathVariable ("u_id") String userName, @RequestBody UserAuthRequest input) {
		Optional<UserAccount> oldaccount = this.accountRepository.findByUsername(userName);
		Optional<UserAccount> testaccount = this.accountRepository.findByUsername(input.getUsername());
			if (oldaccount.isPresent() && !testaccount.isPresent() && principal.getName().equals(userName)){
				oldaccount.get().setUsername(input.getUsername());
				oldaccount.get().setPassword(input.getPassword());
				this.accountRepository.save(oldaccount.get());
			return ResponseEntity.ok(new UserAccountResponse(oldaccount.get().getUsername(),oldaccount.get().getEDBasUserEDBResponse(), oldaccount.get().getAuthoritiesAsString()));
		}
		else {
			return ResponseEntity.badRequest().build();
		}
	}
	
	
	/**
	 * deleteUserAccountForUser
	 * 
	 * @param userId
	 * @return
	 */
	//check sessiontoken and useridentity 
	// not sure if just testing with sessiontoken is enough - maybe stolen!! maybe better to create a update class which provides old pwd in same jso
	@PreAuthorize("hasRole('ROLE_USER')")
	@RequestMapping(method = RequestMethod.DELETE, value = "user/{u_id}")
	ResponseEntity<?> deleteUserAccountForUser (@AuthenticationPrincipal Principal principal, @PathVariable ("u_id") String userName){
		if(principal.getName().equals(userName)) {
		try{
			deleteAccount(userName);
			return ResponseEntity.ok().build();
		}
		catch(Exception e) {
			return ResponseEntity.badRequest().build();
		}
		}
		else {
			return ResponseEntity.badRequest().build();
		}
	}
	/**
	 * deleteUserAccountForAdmin
	 * 
	 * @param principal
	 * @param userName
	 * @return
	 */
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	@RequestMapping(method = RequestMethod.DELETE, value = "admin/{u_id}")
	ResponseEntity<?> deleteUserAccountForAdmin  (@PathVariable ("u_id") String userName){
		try{
			deleteAccount(userName);
		return ResponseEntity.ok().build();
		}
		catch(Exception e) {
			return ResponseEntity.badRequest().build();
		}
	}
	
	private void deleteAccount(String userName) throws Exception{
		UserAccount account = this.accountRepository.findByUsername(userName).get();
		//delete all backend repositories first
		Set<UserEDB> edbs = account.getEDBs();
		for ( UserEDB e : edbs) {
			final RestTemplate restTemplate = new RestTemplateBuilder().build();
			final String targetUrl = new StringBuilder().append("http://").append(e.getBackendId()).append(":8383/repository/")
					.append(e.getEDBName()).toString();
			restTemplate.delete(targetUrl);
		}
		account.removeAllEDBs(edbs);
		this.accountRepository.delete(account);	
	}
	

}
