package searchitect.controller;

import java.security.Principal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.json.JsonSimpleJsonParser;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import searchitect.model.UserEDB;
import searchitect.common.exception.SearchitectException;
import searchitect.model.UserAccount;
import searchitect.service.EDBRepository;
import searchitect.service.UserAccountRepository;
import searchitect.util.UrlBuilderService;

@RestController
public class RepositoryController {

	private final EDBRepository eDBRepository;
	private final UserAccountRepository accountRepository;
	private final RestTemplate restTemplate;
	private final UrlBuilderService urlBuilder;


	/**
	 * Constructor of RepositoryController initializes account Repository
	 * 
	 * @param accountRepository
	 */
	@Autowired
	RepositoryController(EDBRepository eDBRepository, UserAccountRepository accountRepository, UrlBuilderService urlBuilder) {
		this.eDBRepository = eDBRepository;
		this.accountRepository = accountRepository;
		this.restTemplate = new RestTemplateBuilder().build();
		this.urlBuilder = urlBuilder;
		this.urlBuilder.setBackendPort("8383");
	}

	public RestTemplate getRestTemplate() {
		return this.restTemplate;
	}

	public EDBRepository getEDBRepository() {
		return this.eDBRepository;
	}

	public UserAccountRepository getAccountRepository() {
		return this.accountRepository;
	}

	public UrlBuilderService getUrlBuilderService() {
		return this.urlBuilder;
	}

	/**
	 * 
	 * @param backendName
	 * @return
	 */
	//@PreAuthorize("hasRole('ROLE_ADMIN')")
	@RequestMapping(value = "/backend/{b_id}/repositories", method = RequestMethod.GET)
	public ResponseEntity<String> forwardRepositories(@PathVariable("b_id") String backendName) {
		try {
			final String targetUrl = this.urlBuilder.getTargetUrl(backendName, "repositories", "", "");
			final String response = restTemplate.getForObject(targetUrl, String.class);
			return ResponseEntity.accepted().body(response);
		} catch (Exception e) {
			return ResponseEntity.notFound().build();
		}
	}

	/**
	 * forwardSetup -passes the setup data postData to backend and returns
	 * 
	 * @param postData
	 * @param userName
	 * @param backendName
	 * @return
	 */
	@RequestMapping(value = "/backend/{b_id}/repository", method = RequestMethod.POST)
	public ResponseEntity<String> forwardSetup(@AuthenticationPrincipal Principal principal,
			@RequestBody String postData, @PathVariable("b_id") String backendName) {
		try {
			String targetUrl = this.urlBuilder.getTargetUrl(backendName, "repository", "", "");
			String response = postResponseStringFromBackend(postData, targetUrl);
			// then parse and store repositoryName in userdatabase
			JsonSimpleJsonParser jsonparser = new JsonSimpleJsonParser();
			Map<String, Object> map = jsonparser.parseMap(response);
			String repositoryName = (String) map.get("repositoryName");
			UserAccount user = accountRepository.findByUsername(principal.getName()).get();
			eDBRepository.saveAndFlush(new UserEDB(user, backendName, repositoryName));
			// client doesn't have directly access on resources, gate provides
			// responses of resource as a string - is this ok?
			return ResponseEntity.accepted().body(response);
		} catch (Exception e) {
			return ResponseEntity.notFound().build();
		}

	}

	
	/**
	 * forwardSearch - validates parameter and forwards searchrequest(postData)
	 * to backend
	 * 
	 * @param principal
	 * @param postData
	 * @param backendName
	 * @param repositoryName
	 * @return ok (http 200) and result of search or not found (http 404)
	 */
	@RequestMapping(value = "/backend/{b_id}/repository/{r_id}/search", method = RequestMethod.POST)
	public ResponseEntity<String> forwardSearch(@AuthenticationPrincipal Principal principal,
			@RequestBody String postData, @PathVariable("b_id") String backendName,
			@PathVariable("r_id") String repositoryName) {
	
		try {
			final String targetUrl = this.urlBuilder.getTargetUrl(backendName, "repository", repositoryName, "search");
			validateRepository(principal, repositoryName, backendName);
			String response = postResponseStringFromBackend(postData, targetUrl);
			return ResponseEntity.ok(response);
		} catch (Exception e) {
			return ResponseEntity.notFound().build();
		}
	}

	/**
	 * forwardUpdate
	 * 
	 * @param postData
	 * @param userName
	 * @param backendName
	 * @param repositoryName
	 * @return ok (http 200) or not found (http 404)
	 */
	@RequestMapping(value = "/backend/{b_id}/repository/{r_id}", method = RequestMethod.POST)
	public ResponseEntity<String> forwardUpdate(@AuthenticationPrincipal Principal principal,
			@RequestBody String postData, @PathVariable("b_id") String backendName,
			@PathVariable("r_id") String repositoryName) {
		try {
			final String targetUrl = this.urlBuilder.getTargetUrl(backendName, "repository", repositoryName, "");
			validateRepository(principal, repositoryName, backendName);
			final String response = postResponseStringFromBackend(postData, targetUrl);
			return ResponseEntity.ok(response);
		} catch (Exception e) {
			return ResponseEntity.notFound().build();
		}
	}

	/**
	 * forwardDelete - validates Parameters and sends delete request to backend
	 * 
	 * @param principal
	 * @param backendName
	 * @param repositoryName
	 * @return ok (http 200) or not found (http 404)
	 */
	@RequestMapping(value = "backend/{b_id}/repository/{r_id}", method = RequestMethod.DELETE)
	public ResponseEntity<String> forwardDelete(@AuthenticationPrincipal Principal principal,
			@PathVariable("b_id") String backendName, @PathVariable("r_id") String repositoryName) {

		//final String targetUrl = new StringBuilder().append("http://").append(backendName).append(":8383/repository/")
		//		.append(repositoryName).toString();
		try {
			final String targetUrl = this.urlBuilder.getTargetUrl(backendName,"repository", repositoryName, "");
			validateRepository(principal, repositoryName, backendName);
			restTemplate.delete(targetUrl);
			// delete repository from useraccount
			Collection<UserEDB> edbs = eDBRepository.findByAccountUsername(principal.getName());
			for (UserEDB e : edbs) {
				if (e.getEDBName().equals(repositoryName)) {
					eDBRepository.delete(e);
				}
			}
			return ResponseEntity.ok().build();
		} catch (Exception e) {
			return ResponseEntity.notFound().build();
		}
	}

	private void validateRepository(Principal principal, String repositoryName, String backendName)
			throws SearchitectException {
		boolean test = false;
		Optional<UserAccount> user = accountRepository.findByUsername(principal.getName());
		if (!user.isPresent())
			;
		Set<UserEDB> edbs = user.get().getEDBs();
		for (UserEDB e : edbs) {
			if (e.getBackendId().equals(backendName) && e.getEDBName().equals(repositoryName)) {
				test = true;
				break;
			}
		}
		if (test==false){
			throw new SearchitectException("Parameter verification failed");
			}
	}

	private String postResponseStringFromBackend(String postData, String targetUrl) throws Exception {
		HttpEntity<String> entity;
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(org.springframework.http.MediaType.APPLICATION_JSON);
		List<org.springframework.http.MediaType> list = new ArrayList<>();
		list.add(org.springframework.http.MediaType.APPLICATION_JSON);
		headers.setAccept(list);
		entity = new HttpEntity<>(postData, headers);

		final String response = restTemplate.postForObject(targetUrl, entity, String.class);
		return response;
	}

}
