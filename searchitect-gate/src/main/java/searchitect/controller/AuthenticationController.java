package searchitect.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import searchitect.common.view.UserAuthRequest;
import searchitect.common.view.UserAuthResponse;
import searchitect.security.JwtUserService;
import searchitect.security.JwtUtil;

@RestController
public class AuthenticationController {

	@Autowired
	private AuthenticationManager authenticationManager;

	@Autowired
	private JwtUtil jwtUtil;
	

	@Autowired
	@Qualifier("jwtUserService")
	//private UserDetailsService userDetailsService;
	private JwtUserService jwtUserDetailsService;
	
	//"${jwt.authentication.path}"
	@RequestMapping(value = "/auth", method = RequestMethod.POST)
	public ResponseEntity<?> createJwt(@RequestBody UserAuthRequest authRequest) throws AuthenticationException {
	
		authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(authRequest.getUsername(), authRequest.getPassword()));	  
		final UserDetails userDetails = (UserDetails) jwtUserDetailsService.loadUserByUsername(authRequest.getUsername());
		final String token = jwtUtil.generateToken(userDetails);
		// Return the token
		return ResponseEntity.ok(new UserAuthResponse(token));
	}
	

    @ExceptionHandler({AuthenticationException.class})
    public ResponseEntity<String> handleAuthenticationException(AuthenticationException e) {
        return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(e.getMessage());
}
}
