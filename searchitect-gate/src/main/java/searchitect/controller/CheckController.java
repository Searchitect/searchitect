package searchitect.controller;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import searchitect.util.UrlBuilderService;

import java.security.Principal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@RestController
public class CheckController {
    
	final RestTemplate restTemplate = new RestTemplateBuilder().build();

	private final UrlBuilderService urlBuilder;
	
	@Autowired
	CheckController(UrlBuilderService urlBuilder){
	this.urlBuilder = urlBuilder;
	this.urlBuilder.setBackendPort("8383");
	}
	
	public RestTemplate getRestTemplate() {
		return restTemplate;
	}
	
	public UrlBuilderService getUrlBuilderService() {
		return this.urlBuilder;
	} 
	
    @RequestMapping(value= "/", method = RequestMethod.GET)
    public String checkSearchitectGate() {
        return "Greetings from searchitect!";
    }
    
    
    
    @RequestMapping(value="/checkbackend/{b_id}", method = RequestMethod.GET)
    public String checkSebackend(@PathVariable ("b_id") String backendName) {
    	
    	String url = urlBuilder.getTargetUrl(backendName, "", "", "");
    	String response = restTemplate.getForObject(url, String.class);
        return response;
    }
    
    @RequestMapping(value = "/checkauthbackend/{b_id}", method = RequestMethod.GET)
   	public ResponseEntity<String> checkAuthenticated(@AuthenticationPrincipal Principal principal, @PathVariable ("b_id") String backendName) {
    	String url = urlBuilder.getTargetUrl(backendName, "", "", "");
    	String response = restTemplate.getForObject(url, String.class);
   		String name = principal.getName();
   		return ResponseEntity.ok(response + name );
   	}
       


}
