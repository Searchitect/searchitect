package searchitect.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonIgnore;
@Entity
public class UserEDB {

	@JsonIgnore
	@Id
    @GeneratedValue
    private Long eDBId;
	
	private String backendId;
	
	private String eDBName;
	
	@JsonIgnore
    @ManyToOne
    private UserAccount account;
	
	UserEDB() { // jpa only
    }
	
	public UserEDB(UserAccount account, String backendName, String eDBName ){
		this.account = account;
		this.setBackendName(backendName);
		this.setEDBName(eDBName);
	}

	public String getBackendId() {
		return backendId;
	}

	public void setBackendName(String backendName) {
		this.backendId = backendName;
	}

	public String getEDBName() {
		return eDBName;
	}

	public void setEDBName(String eDBName) {
		this.eDBName = eDBName;
	}
	
	
	 
}
