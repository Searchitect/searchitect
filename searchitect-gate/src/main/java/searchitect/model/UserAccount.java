package searchitect.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import searchitect.common.view.UserEDBResponse;
import searchitect.util.TimeProvider;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.GeneratedValue;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
public class UserAccount {

	
	@Id
	@GeneratedValue
	private Long id;

	//@NotNull
	//@Size(min = 3, max = 100)
	private String username;

	
	//@NotNull
	//@Size(min = 6, max = 100)
	private String password;
	private boolean enabled;

	@OneToMany(mappedBy = "account")
	private Set<UserEDB> repositories = new HashSet<>();

	@ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinTable(name = "user_authority", joinColumns = @JoinColumn(name = "user_id", referencedColumnName = "id"), inverseJoinColumns = @JoinColumn(name = "authority_id", referencedColumnName = "id"))
	private List<Authority> authorities;

	@Temporal(TemporalType.TIMESTAMP)
	private Date lastPasswordResetDate;

	public UserAccount(String name, String password) {
		this.username = name;
		this.password = password;
		this.enabled = true;
		this.authorities = new ArrayList<Authority>();
		authorities.add(new Authority(AuthorityName.ROLE_USER));
		this.lastPasswordResetDate = new TimeProvider().now();
	}

	public UserAccount() { // jpa only
	}

	public Set<UserEDB> getEDBs() {
		return repositories;
	}
	
	public Set<UserEDBResponse> getEDBasUserEDBResponse() {
		Set<UserEDBResponse> response = new HashSet<>();
		for(UserEDB u: repositories) {
			response.add(new UserEDBResponse(u.getBackendId(),u.getEDBName()));
		}
		return response;
	}


	public void setEDBs(Set<UserEDB> edbs) {
		this.repositories= edbs;
	}
	public void removeAllEDBs(Set<UserEDB> all) {
		repositories.removeAll(all);
	}
	
	public void removeEDB(UserEDB edb){
		repositories.remove(edb);
		}

	public Long getId() {
		return id;
	}

	public String getUsername() {
		return username;
	}

	@JsonIgnore
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public void setId(long id) {
		this.id = id;
	}

	public void setLastPasswordResetDate(Date lastPasswordResetDate) {
		this.lastPasswordResetDate = lastPasswordResetDate;
	}
	
	public Date getLastPasswordResetDate()
	{
		return this.lastPasswordResetDate;
	}
	
	public boolean isEnabled(){
		return this.enabled;
	}
	
	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	
	public void setAuthorities(List<Authority> a) {
		this.authorities = a;
	}

	public List<Authority> getAuthorities() {
		return this.authorities;
	}

	public List<String> getAuthoritiesAsString() {
		List <String>response = new ArrayList<String>();
		for (Authority a:this.authorities) {
			response.add(a.getAuthority());
		}
		return response;
	}

}
