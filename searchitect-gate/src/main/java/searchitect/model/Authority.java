package searchitect.model;


import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import org.springframework.security.core.GrantedAuthority;


//import org.springframework.security.core.GrantedAuthority;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Authority extends Object implements GrantedAuthority {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3976195521566614378L;

	@Id
    @GeneratedValue
    private Long id;

    private AuthorityName name;

    public Authority(){
    	
    }
   
    public Authority(AuthorityName role) {
	this.setName(role);
	}
    
	public String getAuthority() {
        return name.toString();
    }

    public void setName(AuthorityName name) {
        this.name = name;
    }


    @JsonIgnore
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

}
