package searchitect.service;

import org.springframework.data.jpa.repository.JpaRepository;

import searchitect.model.UserEDB;

import java.util.Collection;


public interface EDBRepository extends JpaRepository<UserEDB, Long> {
    Collection<UserEDB> findByAccountUsername(String username);
}

