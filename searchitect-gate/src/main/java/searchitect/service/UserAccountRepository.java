package searchitect.service;

import org.springframework.data.jpa.repository.JpaRepository;

import searchitect.model.UserAccount;

import java.util.Optional;


public interface UserAccountRepository extends JpaRepository<UserAccount, Long> {
    Optional<UserAccount> findByUsername(String username);
}
