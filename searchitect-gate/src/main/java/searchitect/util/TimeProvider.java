package searchitect.util;

import java.util.Calendar;
import java.util.Date;

import org.springframework.stereotype.Service;

@Service
public class TimeProvider {

	public Date now() {
		return Calendar.getInstance().getTime();
	}

	public Date addSeconds(long expires) {
		long newtime = Calendar.getInstance().getTimeInMillis() + expires * 1000;
		Calendar calnew = Calendar.getInstance();
		calnew.setTimeInMillis(newtime);
		return calnew.getTime();
	}

	public boolean isBeforeNow(Date expire) {
		return Calendar.getInstance().before(expire);
	}

}