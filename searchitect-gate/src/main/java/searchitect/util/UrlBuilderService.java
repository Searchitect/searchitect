package searchitect.util;

import java.util.Map;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import searchitect.common.exception.SearchitectException;



@Service
public class UrlBuilderService {


	@Value("#{${backend.ports}}")
	private Map<String,String> backends;

	
	private String backendPort;
	
	public UrlBuilderService(){
		
	}
	
	public String getTargetUrl(String backendName, String part1, String part2, String part3) throws SearchitectException{
		//first check if backend and backendport exists
		if(backends.containsKey(backendName)){
			backendPort = backends.get(backendName);
		}
		else{
			throw new SearchitectException();
		}
		//then build the url
		final StringBuilder targetUrlBuilder = new StringBuilder().append("http://").append(backendName).append(":")
				.append(backendPort.trim()).append("/").append(part1);
		if (!part2.isEmpty()) {
			targetUrlBuilder.append("/").append(part2);

			if (!part3.isEmpty()) {
				targetUrlBuilder.append("/").append(part3);
			}

		}
		String targetUrl = targetUrlBuilder.toString();
		return targetUrl;

	}

	public void setBackendPort(String backendPort) {
		this.backendPort = backendPort;
	}

	public void setBackendPorts(Map<String,String> backends){
		this.backends = backends;
	}
}
