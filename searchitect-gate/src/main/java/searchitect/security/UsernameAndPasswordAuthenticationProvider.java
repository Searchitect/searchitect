package searchitect.security;

import java.util.Collection;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;

public class UsernameAndPasswordAuthenticationProvider implements AuthenticationProvider {

	@Autowired
	JwtUserService jwtUserService;
	
	@Autowired 
	PasswordEncoder passwordEncoder;

	public UsernameAndPasswordAuthenticationProvider(JwtUserService jwtUserService, PasswordEncoder passwordEncoder) {
	 this.jwtUserService = jwtUserService;
	 this.passwordEncoder=passwordEncoder;
 }

	@Override
		public Authentication authenticate(Authentication authentication) throws AuthenticationException {
		String username = authentication.getName();
		UserDetails udexpected = jwtUserService.loadUserByUsername(username);
	
		if(	passwordEncoder.matches(authentication.getCredentials().toString(), udexpected.getPassword())){
				Collection<? extends GrantedAuthority> auths = udexpected.getAuthorities();
			return new UsernamePasswordAuthenticationToken(username, authentication.getCredentials(), auths);
		}
		else {
			throw new SearchitectAuthenticationException("Bad credentials");
		}
		}

	@Override
	public boolean supports(Class<?> authentication) {
		return UsernamePasswordAuthenticationToken.class.isAssignableFrom(authentication);
	}

}
