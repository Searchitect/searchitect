package searchitect.security;

import java.util.Optional;

import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import searchitect.model.UserAccount;
import searchitect.service.UserAccountRepository;

@Service
public class JwtUserService  implements UserDetailsService {
	
	 private UserAccountRepository userAccountRepository;

	    public JwtUserService(UserAccountRepository userAccountRepository) {
	        this.userAccountRepository = userAccountRepository;
	    }

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		Optional<UserAccount> user = userAccountRepository.findByUsername(username);
		if (user.isPresent() && user.get().isEnabled()){
		return new User(user.get().getUsername(), user.get().getPassword(), user.get().getAuthorities());
	
		}
		else{
			throw new UsernameNotFoundException("User not found");
		}
	}

}
