package searchitect.security;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import searchitect.util.TimeProvider;

import static java.util.stream.Collectors.toList;


public class JwtUtil {

	private SignatureAlgorithm SIGNATURE_ALGORITHM = SignatureAlgorithm.HS512;
	
	@Autowired
	TimeProvider timeProvider;

	@Value("${app.name}")
	private String APP_NAME;

	@Value("${jwt.secret}")
	private String SECRET;

	@Value("${jwt.expiration}")
	private int EXPIRATION;

	private Claims getAllClaimsFromToken(String token) {
		Claims claims;
		claims = Jwts.parser().requireIssuer(APP_NAME).setSigningKey(SECRET).parseClaimsJws(token).getBody();
		return claims;
	}

	public String getUsernameFromToken(String token)  {
		final Claims claims = this.getAllClaimsFromToken(token);
		return claims.getSubject();
	}

	public Date getExpiresAtDateFromToken(String token) {
			final Claims claims = this.getAllClaimsFromToken(token);
		return claims.getExpiration();
	}
	
	public List <SimpleGrantedAuthority> getGrantedAuthorities(String token){
		final Claims claims = this.getAllClaimsFromToken(token);	
		@SuppressWarnings("unchecked")
		List<String> scopes = (List<String>)claims.get("scopes");
		List<SimpleGrantedAuthority> authorities =scopes.stream().map(SimpleGrantedAuthority::new).collect(toList());	
		return authorities;
	}
	

	public Boolean validateToken(String token, UserDetails userDetails) {
		final String username = getUsernameFromToken(token);
		return (username != null && username.equals(userDetails.getUsername()));
	}

	/**
	 * Generates a JWT  containing username as subject, and scope as additional claims. These properties are taken from UserDetails object. 
	 * 
	 * @param userDetails for specific user the token should authenticate    
	 * @return jwt as string
	 */
	public String generateToken(UserDetails user ) {
		Claims claims = Jwts.claims().setIssuer(APP_NAME).setSubject(user.getUsername());
        claims.put("scopes", user.getAuthorities().stream().map(s -> s.getAuthority().toString()).collect(Collectors.toList()));
		return Jwts.builder()
				.setClaims(claims)
				.setIssuedAt(timeProvider.now())
				.setExpiration(timeProvider.addSeconds(EXPIRATION))
				.signWith(SIGNATURE_ALGORITHM, SECRET).compact();
	}

}
