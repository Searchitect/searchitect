package searchitect.security;

import org.springframework.security.core.AuthenticationException;

public class SearchitectAuthenticationException extends AuthenticationException{

	/**
	 * 
	 */
	private static final long serialVersionUID = -7934121251936451249L;

	public SearchitectAuthenticationException(String msg) {
		super(msg);
	}
	
	public SearchitectAuthenticationException(String message, Throwable cause) {
	        super(message, cause);
	}

}
