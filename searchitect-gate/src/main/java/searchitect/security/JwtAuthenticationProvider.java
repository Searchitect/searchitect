package searchitect.security;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import searchitect.security.JwtUtil;;



public class JwtAuthenticationProvider implements AuthenticationProvider  {


    private JwtUtil jwtUtil;

    @Autowired
    public JwtAuthenticationProvider(JwtUtil jwtUtil) {
        this.jwtUtil = jwtUtil;
    }
    
    @Override
	public boolean supports(Class<?> authentication) {
		return JwtAuthenticationToken.class.isAssignableFrom(authentication);
	}

	@Override
	public Authentication authenticate(Authentication authentication) throws AuthenticationException {
		//get token and extract username and authorities
		JwtAuthenticationToken token = (JwtAuthenticationToken) authentication;
		String username = jwtUtil.getUsernameFromToken(token.getToken());
		List<SimpleGrantedAuthority> auths = jwtUtil.getGrantedAuthorities(token.getToken());
		return new JwtAuthenticationToken(auths, username, token.getToken());
	}
}