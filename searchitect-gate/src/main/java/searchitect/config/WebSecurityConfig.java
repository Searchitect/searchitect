package searchitect.config;

import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;

import searchitect.security.JwtAuthenticationFilter;
import searchitect.security.JwtAuthenticationProvider;
import searchitect.security.JwtAuthenticationSuccessHandler;
import searchitect.security.JwtUserService;
import searchitect.security.JwtUtil;
import searchitect.security.RestAuthenticationEntryPoint;
import searchitect.security.UsernameAndPasswordAuthenticationProvider;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

	//@Value("$jwt.authentication.path")
	private String authenticationPath = "/auth";

	@Autowired
	private JwtUserService jwtUserService;

	@Autowired
	private JwtAuthenticationProvider jwtAuthenticationProvider;
	
	@Autowired
	private UsernameAndPasswordAuthenticationProvider upAuthenticationProvider;
	
	@Autowired
	RestAuthenticationEntryPoint restAuthenticationEntryPoint;
	
	@Autowired
	JwtUtil jwtUtil;

	
	@Bean JwtUtil jwtUtil(){
		return new JwtUtil();
	}

	@Bean
	public RestAuthenticationEntryPoint restAuthenticationEntryPoint() {
		return new RestAuthenticationEntryPoint();
	}
	
	@Bean
	public JwtAuthenticationProvider jwtAuthenticationProvider(JwtUtil util) {
		return new JwtAuthenticationProvider(jwtUtil());
	}

	@Bean
	public UsernameAndPasswordAuthenticationProvider upAuthenticationProvider() {
		return new UsernameAndPasswordAuthenticationProvider(jwtUserService, passwordEncoderBean());
	}
	
	@Bean
	public PasswordEncoder passwordEncoderBean() {
		return new BCryptPasswordEncoder();
	}

	@Bean
	@Override
	public AuthenticationManager authenticationManagerBean() throws Exception {
		return super.authenticationManagerBean();
	}
	
	@Bean
    public FilterRegistrationBean<CorsFilter> corsFilterRegistrationBean() {
        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        CorsConfiguration config = new CorsConfiguration();
        config.applyPermitDefaultValues();
        config.setAllowCredentials(true);
        config.setAllowedOrigins(Arrays.asList("*"));
        config.setAllowedHeaders(Arrays.asList("*"));
        config.setAllowedMethods(Arrays.asList("*"));
        config.setExposedHeaders(Arrays.asList("content-length"));
        config.setMaxAge(3600L);
        source.registerCorsConfiguration("/**", config);
        FilterRegistrationBean<CorsFilter> bean = new FilterRegistrationBean<CorsFilter>(new CorsFilter(source));
        bean.setOrder(0);
        return bean;
    }

	
	@Override
	protected void configure(AuthenticationManagerBuilder auth) {
		auth.authenticationProvider(jwtAuthenticationProvider);
		auth.authenticationProvider(upAuthenticationProvider);
		
	}

	@Autowired
	public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
		auth.userDetailsService(jwtUserService)
		.passwordEncoder(passwordEncoderBean());
	}


	@Override
	public void configure(WebSecurity web) throws Exception {
		
		// JwtAuthenticationFilter ignores authentication path, create user account and availabiltiy checks 
		web		.ignoring().antMatchers(HttpMethod.POST, authenticationPath)
				.and().ignoring().antMatchers(HttpMethod.POST, "/user")
				.and().ignoring().antMatchers(HttpMethod.GET, "/")
				.and().ignoring().antMatchers(HttpMethod.GET, "/v2/api-docs/**")
				.and().ignoring().antMatchers(HttpMethod.GET, "/swagger-ui.html")
				.and().ignoring().antMatchers(HttpMethod.GET, "/webjars/**")
				.and().ignoring().antMatchers(HttpMethod.GET, "/swagger-resources/**")
				.and().ignoring().antMatchers(HttpMethod.GET, "/checkbackend/template");
	}
	

		
		protected JwtAuthenticationFilter buildJwtTokenAuthenticationProcessingFilter() throws Exception {
			JwtAuthenticationFilter jwtfilter = new JwtAuthenticationFilter();
			jwtfilter.setAuthenticationManager(this.authenticationManager());
			jwtfilter.setAuthenticationSuccessHandler(new JwtAuthenticationSuccessHandler());
			return jwtfilter;
		}

		protected void configure(HttpSecurity http) throws Exception {
			
			http
					//insert jwt filter into filterchain
					.addFilterBefore(buildJwtTokenAuthenticationProcessingFilter(),BasicAuthenticationFilter.class)
					.exceptionHandling().authenticationEntryPoint(restAuthenticationEntryPoint).and()
					// http stateless - no session handling
					.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS).and()
					.authorizeRequests()
					//exclude user creation, usernamepassword authentication and the check urls from authorization
					.antMatchers(HttpMethod.POST, "/user").permitAll()
					.antMatchers("/v2/api-docs/**").permitAll()
					.antMatchers("/swagger-ui.html").permitAll()
					.antMatchers("/").permitAll()
					.antMatchers("/swagger-resources/**").permitAll()
					.antMatchers(HttpMethod.POST, authenticationPath).permitAll()
					.antMatchers("/webjars/**").permitAll()
					.antMatchers(HttpMethod.GET, "/checkbackend/**").permitAll()
					//authenticate jwt token of any other request 
					.anyRequest().authenticated();
					
			http
				// disable cross cite request forgery tokens, http basic authentication, form login and logout
				.csrf().disable()
                .httpBasic().disable()
                .formLogin().disable()
                .logout().disable();		


		}
		

}
