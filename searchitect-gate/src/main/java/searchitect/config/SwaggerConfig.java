package searchitect.config;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import com.google.common.base.Predicate;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.ParameterBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.schema.ModelRef;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.service.Parameter;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import static com.google.common.base.Predicates.*;
import static springfox.documentation.builders.PathSelectors.*;

import java.util.ArrayList;
import java.util.List;

@Configuration
@EnableSwagger2
@ComponentScan
@EnableAutoConfiguration
public class SwaggerConfig {

	@Bean
	public Docket api() {
		ParameterBuilder aParameterBuilder = new ParameterBuilder();
		aParameterBuilder.name("headerName").modelRef(new ModelRef("string")).parameterType("header").required(false)
				.build();
		List<Parameter> aParameters = new ArrayList<Parameter>();
		aParameters.add(aParameterBuilder.build());
		return new Docket(DocumentationType.SWAGGER_2).select()
				.apis(RequestHandlerSelectors.basePackage("searchitect.controller")).paths(PathSelectors.any()).build()
				.apiInfo(apiInfo()).pathMapping("").globalOperationParameters(aParameters);

	}


	private ApiInfo apiInfo() {
		return new ApiInfoBuilder().title("Searchitect API")
				.description("Webservice, which supports multiple searchable encryption techniques")
				.termsOfServiceUrl("http://springfox.io")
				.contact(new Contact("searchitect team", "https://localhost:8433", "mathias.tausig@fh-campuswien.ac.at"))
				.license("GNU General Public License Version 3.0").licenseUrl("https://www.gnu.org/licenses/")
				.version("3.0").build();
	}

	@Bean
	public Docket checkControllerApi() {
		return new Docket(DocumentationType.SWAGGER_2).groupName("searchitect-api").apiInfo(apiInfo()).select()
				.paths(searchitectPaths()).build();

	}

	@SuppressWarnings("unchecked")
	private Predicate<String> searchitectPaths() {
		return or(regex("/user.*"), regex("/backend.*"), regex("/users.*"), regex("/auth.*"), regex("/api/store.*"));
	}
}
