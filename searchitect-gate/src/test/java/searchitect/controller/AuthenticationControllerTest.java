package searchitect.controller;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;

import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.List;

import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.test.context.support.WithAnonymousUser;

import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.google.gson.Gson;

import searchitect.common.view.UserAuthRequest;
import searchitect.model.Authority;
import searchitect.model.AuthorityName;
import searchitect.security.JwtUserService;

@RunWith(SpringRunner.class)
@SpringBootTest
public class AuthenticationControllerTest {

	String username = "user";
	String password = "secretpassword";

	private MockMvc mockMvc;

	@Autowired
	private WebApplicationContext context;

	@Autowired
	private PasswordEncoder passwordEncoder;

	@MockBean
	private JwtUserService jwtUserService;

	@InjectMocks
	private AuthenticationController authenticationController;

	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);
		mockMvc = MockMvcBuilders.webAppContextSetup(context).apply(springSecurity()).build();
		//Mock the test user in jwtUserService
		String encodedpassword = passwordEncoder.encode(password);
		List<GrantedAuthority> gaList = new ArrayList<GrantedAuthority>();
		gaList.add(new Authority(AuthorityName.ROLE_USER));
		Mockito.when(jwtUserService.loadUserByUsername(username))
				.thenReturn(new User(username, encodedpassword, gaList));
		

	}

	@Test
	@WithAnonymousUser
	public void successfulAuthenticationWithUserTest() throws Exception {
		UserAuthRequest userAuthRequest = new UserAuthRequest(username, password);
		Gson gson = new Gson();
		String request = gson.toJson(userAuthRequest);
		MvcResult result = mockMvc.perform(post("/auth").contentType(MediaType.APPLICATION_JSON).content(request))
				.andExpect(status().isOk()).andReturn();
		System.out.println(result.getResponse().getContentAsString());
	}

	@Test
	@WithAnonymousUser
	public void AuthenticationWithMissingCredentialsFailsTest() throws Exception {
		String request = "";
		MvcResult result = mockMvc.perform(post("/auth").contentType(MediaType.APPLICATION_JSON).content(request))
				.andExpect(status().isBadRequest()).andReturn();
		System.out.println(result.getResponse().getContentAsString());
	}

	@Test()
	@WithAnonymousUser
	public void AuthenticationWithWrongPasswordFailsTest() throws Exception {
		String wrongpassword = "wrongpassword";
		UserAuthRequest userAuthRequest = new UserAuthRequest(username, wrongpassword);
		Gson gson = new Gson();
		String request = gson.toJson(userAuthRequest);
		MvcResult result = mockMvc.perform(post("/auth").contentType(MediaType.APPLICATION_JSON).content(request))
				.andExpect(status().isUnauthorized()).andReturn();
		System.out.println(result.getResponse().getContentAsString());
	}
	
	@Test()
	@WithAnonymousUser
	public void AuthenticationWithMissingPasswordFailsTest() throws Exception {
		UserAuthRequest userAuthRequest = new UserAuthRequest(username, "");
		Gson gson = new Gson();
		String request = gson.toJson(userAuthRequest);
		MvcResult result = mockMvc.perform(post("/auth").contentType(MediaType.APPLICATION_JSON).content(request))
				.andExpect(status().isUnauthorized()).andReturn();
		System.out.println(result.getResponse().getContentAsString());
	}

}
