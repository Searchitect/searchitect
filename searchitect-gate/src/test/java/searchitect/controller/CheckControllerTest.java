package searchitect.controller;

import static org.hamcrest.Matchers.equalTo;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.method;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.requestTo;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withSuccess;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Map;

import org.springframework.security.test.context.support.WithMockUser;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.client.MockRestServiceServer;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import searchitect.util.UrlBuilderService;


@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@TestPropertySource("classpath:application-test.properties")
public class CheckControllerTest {

	
	private MockRestServiceServer mockServer;
	UrlBuilderService urlService;
	
	@Value("#{${backend.ports}}")
	private Map<String,String> backends;
	
	@Autowired
	private CheckController check;
	

	@Autowired
	private MockMvc mvc;
	

	 @Before
	 public void setUp() {
	 mockServer = MockRestServiceServer.createServer(check.getRestTemplate());
	 this.urlService = check.getUrlBuilderService();
	 this.urlService.setBackendPorts(backends);
	 }
	 
	

	@Test
	@WithMockUser
	public void getCheck() throws Exception {
		mvc.perform(MockMvcRequestBuilders.get("/").accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
				.andExpect(content().string(equalTo("Greetings from searchitect!")));
	}

	@Test
	@WithMockUser
	public void getCheckBackend() throws Exception {
		mockServer.expect(requestTo("http://template:8383/")).andExpect(method(HttpMethod.GET))
				.andRespond(withSuccess("Mocked: Greetings from backend!!", MediaType.TEXT_PLAIN));
		mvc.perform(MockMvcRequestBuilders.get("/checkbackend/template").accept(MediaType.TEXT_PLAIN))
				.andExpect(status().isOk()).andExpect(content().string(equalTo("Mocked: Greetings from backend!!")));
	}
}