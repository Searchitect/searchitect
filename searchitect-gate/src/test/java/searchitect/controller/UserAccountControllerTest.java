package searchitect.controller;


import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertNotNull;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.mock.http.MockHttpOutputMessage;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import searchitect.common.view.UserAuthRequest;
import searchitect.model.AuthorityName;
import searchitect.model.UserEDB;
import searchitect.model.UserAccount;
import searchitect.security.JwtUtil;
import searchitect.service.EDBRepository;
import searchitect.service.UserAccountRepository;


@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@TestPropertySource("classpath:application-test.properties")
public class UserAccountControllerTest {


    private MediaType contentType = new MediaType(MediaType.APPLICATION_JSON.getType(),
            MediaType.APPLICATION_JSON.getSubtype(),
            Charset.forName("utf8"));

    @Autowired
    private MockMvc mockMvc;

    private String userName1 = "alice";
    private String passwd = "totalsecretpassword";
	String token = "test-token";	
	String headerstring = new StringBuilder().append("Bearer ").append(token).toString();
	
	
    @SuppressWarnings("rawtypes")
	private HttpMessageConverter mappingJackson2HttpMessageConverter;

  
    @Autowired
    private UserAccountRepository accountRepository;
    
	@MockBean
	private EDBRepository eDBRepository;

	@MockBean
	private JwtUtil util;
	
	@Autowired
	UserAccountController mockedUserAccountController;

    @Autowired
    void setConverters(HttpMessageConverter<?>[] converters) {

        this.mappingJackson2HttpMessageConverter = Arrays.asList(converters).stream()
            .filter(hmc -> hmc instanceof MappingJackson2HttpMessageConverter)
            .findAny()
            .orElse(null);

        assertNotNull("the JSON message converter must not be null",
                this.mappingJackson2HttpMessageConverter);
    }

    @Before
    public void setup() throws Exception {
        this.accountRepository = mockedUserAccountController.getUserAccountRepository();
    }
    
    @After
    public void cleanUp(){
    	this.accountRepository.deleteAllInBatch();
    }
  
    
    @Test
    @WithMockUser
    public void getUserAccountFailsNotFoundTest() throws Exception {
    	  mockJwtandUserAccountRepository( "user", AuthorityName.ROLE_USER);
        mockMvc.perform(get("/user/test").header("Authorization", headerstring))
                .andExpect(status().isBadRequest());
    }
    
    @Test
    @WithMockUser
    public void getUserAccountForWrongUserFailsTest() throws Exception {
    	mockJwtandUserAccountRepository( "user", AuthorityName.ROLE_USER);
    	this.accountRepository.save(new UserAccount("wronguser", passwd));  
        mockMvc.perform(get("/user/wronguser").header("Authorization", headerstring))
                .andExpect(status().isBadRequest());
    }
    
    @Test
    @WithMockUser
    public void getUserAccountSuccessTest() throws Exception {
  	  mockJwtandUserAccountRepository( "alice", AuthorityName.ROLE_USER);
  	MvcResult test =  mockMvc.perform(get("/user/alice").header("Authorization", headerstring))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.username", is("alice")))
                .andReturn();   
        System.out.println(test.getResponse().getContentAsString());
    }
    
    @Test
    @WithMockUser
    public void getAllUserAccountsAdminSuccessTest() throws Exception {
    	mockJwtandUserAccountRepository( "admin", AuthorityName.ROLE_ADMIN);
    	this.accountRepository.save(new UserAccount(userName1, passwd));
        mockMvc.perform(get("/users").header("Authorization", headerstring))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$[0].id", is(this.accountRepository.findByUsername("admin").get().getId().intValue())))
                .andExpect(jsonPath("$[1].id", is(this.accountRepository.findByUsername(userName1).get().getId().intValue())));      
    } 
    
    @Test
    @WithMockUser
    public void getAllUserAccountsUserFailsTest() throws Exception {
    	mockJwtandUserAccountRepository( "user", AuthorityName.ROLE_USER);
        mockMvc.perform(get("/users").header("Authorization", headerstring))
                .andExpect(status().isForbidden());
    }
    
    @Test
    public void createUserAccountWithoutAuthorizationHeaderSuccessTest() throws Exception {
    	String userJson = json(new UserAuthRequest("marvin","testpassword"));
    	  mockMvc.perform(post("/user")
    		        .contentType(contentType)
    		        .content(userJson))
    		        .andExpect(status().isCreated());
    }
    
    @Test
    public void createTwoUserAccountsWithSameNameWithoutAuthorizationHeaderFailsTest() throws Exception {
    	String userJson = json(new UserAuthRequest("marvin","testpassword"));
    	  mockMvc.perform(post("/user")
    		        .contentType(contentType)
    		        .content(userJson))
    		        .andExpect(status().isCreated());
    	  mockMvc.perform(post("/user")
  		        .contentType(contentType)
  		        .content(userJson))
  		        .andExpect(status().isBadRequest());
    }
    
    @Test
    public void updateUserAccountWrongUserFailsTest() throws Exception {
    	mockJwtandUserAccountRepository( "user", AuthorityName.ROLE_USER);
    	String userJson=json(new UserAccount("modifiedusername","modifiedpwd"));
    	  mockMvc.perform(post("/user/alice").header("Authorization", headerstring)
  		        .contentType(contentType)
  		        .content(userJson))
  		        .andExpect(status().isBadRequest());
    }
    
    @Test
    public void updateUserAccountSuccessTest() throws Exception {
    	mockJwtandUserAccountRepository( "user", AuthorityName.ROLE_USER);
    	String userJson=json(new UserAuthRequest("modifiedusername","modifiedpwd"));
    	  mockMvc.perform(post("/user/user").header("Authorization", headerstring)
  		        .contentType(contentType)
  		        .content(userJson))
  		        .andExpect(status().isOk());	
    }
    
    @Test
    public void updateUserAccountExistingUsernameFailsTest() throws Exception {
    	mockJwtandUserAccountRepository( "user", AuthorityName.ROLE_USER);
    	this.accountRepository.save(new UserAccount("existingusername", "anypwd"));
    	String userJson=json(new UserAccount("existingusername","modifiedpwd"));
    	  mockMvc.perform(post("/user/user").header("Authorization", headerstring)
  		        .contentType(contentType)
  		        .content(userJson))
  		        .andExpect(status().isBadRequest());	
    }
    
    @Test
    public void deleteUserAccountSuccessTest() throws Exception {
    	mockJwtandUserAccountRepository( "user", AuthorityName.ROLE_USER);
    	  mockMvc.perform(delete("/user/user").header("Authorization", headerstring))
  		        .andExpect(status().isOk());	
    }
    
    
   
	@SuppressWarnings("unchecked")
	protected String json(Object o) throws IOException {
        MockHttpOutputMessage mockHttpOutputMessage = new MockHttpOutputMessage();
        this.mappingJackson2HttpMessageConverter.write( o, MediaType.APPLICATION_JSON, mockHttpOutputMessage);
        return mockHttpOutputMessage.getBodyAsString();
    }
    
    
	private void mockJwtandUserAccountRepository( String username, AuthorityName aname) {
		UserAccount user = new UserAccount(username, "secretpassword");
		this.accountRepository.save(user);
		UserEDB edb = new UserEDB(user, "template", "myRepository");
		this.eDBRepository.save(edb);
		List<SimpleGrantedAuthority> authorities = new ArrayList<>();
		authorities.add(new SimpleGrantedAuthority(aname.name()));
		given(util.getUsernameFromToken(token)).willReturn(username);
		given(util.getGrantedAuthorities(token)).willReturn(authorities);
	}
    
    
}

