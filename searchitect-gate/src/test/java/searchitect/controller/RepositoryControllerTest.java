package searchitect.controller;

import static org.springframework.test.web.client.match.MockRestRequestMatchers.method;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.requestTo;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withSuccess;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withBadRequest;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withStatus;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.hamcrest.Matchers;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.assertThat;
import static org.mockito.BDDMockito.given;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNot.not;
import static org.hamcrest.CoreMatchers.equalTo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.client.MockRestServiceServer;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import searchitect.controller.RepositoryController;
import searchitect.model.AuthorityName;
import searchitect.model.UserEDB;
import searchitect.model.UserAccount;
import searchitect.security.JwtUtil;
import searchitect.service.EDBRepository;
import searchitect.service.UserAccountRepository;
import searchitect.util.UrlBuilderService;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@PropertySource("classpath:application-test.properties")
public class RepositoryControllerTest {
	// final String domain = "http://localhost:8080/";

	@Autowired
	private MockMvc mockMvc;

	@Autowired
	private RepositoryController repoco;

	@MockBean
	private JwtUtil util;


	private UserAccountRepository accountRepository;
	private MockRestServiceServer mockServer;
	private EDBRepository eDBRepository;

	String token = "test-token";
	String username = "user";
	String headerstring = new StringBuilder().append("Bearer ").append(token).toString();
	UrlBuilderService urlService;
	
	@Value("#{${backend.ports}}")
	private Map<String,String> backends;

	@Before
	public void setup() throws Exception {

		this.mockServer = MockRestServiceServer.createServer(repoco.getRestTemplate());
		this.eDBRepository = repoco.getEDBRepository();
		this.accountRepository = repoco.getAccountRepository();
		this.accountRepository.deleteAllInBatch();
		this.urlService = repoco.getUrlBuilderService();
		this.urlService.setBackendPorts(backends);
		
	}

	@After
	public void cleanUp() throws Exception {
		this.eDBRepository.deleteAllInBatch();
		this.accountRepository.deleteAllInBatch();
	}

	@Test
	@WithMockUser
	public void forwardRepositoriesTestdy2levBackend() throws Exception {
		mockJwtandUserAccountRepository();
		String testexpected = "Mocked: List repositories";
		String testurl = urlService.getTargetUrl("dynrh2lev", "repositories", "", "");
		mockServer.expect(requestTo(testurl)).andExpect(method(HttpMethod.GET))
				.andRespond(withSuccess(testexpected, MediaType.TEXT_PLAIN));
		MvcResult result = mockMvc.perform(get("/backend/dynrh2lev/repositories").header("Authorization", headerstring))
				.andExpect(status().isAccepted()).andReturn();
		assertThat(result.getResponse().getContentAsString(), is(testexpected));
	}

	@Test
	@WithMockUser
	public void forwardRepositoriesTestFails() throws Exception {
		mockJwtandUserAccountRepository();
		String testexpected = "Mocked: List repositories";
		String testurl = urlService.getTargetUrl("template", "repositories", "", "");
		mockServer.expect(requestTo(testurl)).andExpect(method(HttpMethod.GET)).andRespond(withBadRequest());
		MvcResult result = mockMvc.perform(get("/backend/template/repositories").header("Authorization", headerstring))
				.andExpect(status().isNotFound()).andReturn();
		assertThat(result.getResponse().getContentAsString(), is(not(testexpected)));
	}
	
	@Test
	@WithMockUser
	public void wrongBackendFailsTest() throws Exception {
		mockJwtandUserAccountRepository();
		String testexpected = "Mocked: List repositories";
		MvcResult result = mockMvc.perform(get("/backend/test/repositories").header("Authorization", headerstring))
				.andExpect(status().isNotFound()).andReturn();
		assertThat(result.getResponse().getContentAsString(), is(not(testexpected)));
	}


	@Test
	@WithMockUser
	public void UnauthenticatedforwardRepositoriesTestFails() throws Exception {
		String testexpected = "Mocked: List repositories";
		String testurl = urlService.getTargetUrl("template", "repositories", "", "");
		mockServer.expect(requestTo(testurl)).andExpect(method(HttpMethod.GET))
				.andRespond(withSuccess(testexpected, MediaType.TEXT_PLAIN));
		MvcResult result = mockMvc.perform(get("/backend/template/repositories")).andExpect(status().isUnauthorized())
				.andReturn();
		assertThat(result.getResponse().getContentAsString(), is(not(testexpected)));
	}

	@Test
	@WithMockUser
	public void forwardRepositoriesTestSuccess() throws Exception {
		mockJwtandUserAccountRepository();
		String testexpected = "Mocked: Just another string";
		mockServer.expect(requestTo("http://template:8383/repositories")).andExpect(method(HttpMethod.GET))
				.andRespond(withSuccess(testexpected, MediaType.TEXT_PLAIN));

		MvcResult result = mockMvc.perform(get("/backend/template/repositories").header("Authorization", headerstring)).andExpect(status().isAccepted())
				.andReturn();
		assertThat(result.getResponse().getContentAsString(), is(testexpected));
	}

	@SuppressWarnings("unchecked")
	@Test
	@WithMockUser
	public void forwardSetupTestSuccess() throws Exception {
		mockJwtandUserAccountRepository();
		JSONObject response = new JSONObject();
		response.put("repositoryName", "myNewRepository");
		mockServer.expect(requestTo("http://template:8383/repository")).andExpect(method(HttpMethod.POST))
				.andRespond(withSuccess(response.toString(), MediaType.APPLICATION_JSON));

		JSONObject request = new JSONObject();
		JSONArray keyword1 = new JSONArray();
		keyword1.add("doc1");
		keyword1.add("doc2");
		keyword1.add("doc3");
		request.put("keyword1", keyword1);
		JSONArray keyword2 = new JSONArray();
		keyword2.add("doc4");
		keyword2.add("doc1");
		keyword2.add("doc5");
		request.put("keyword2", keyword2);

		MvcResult result = mockMvc.perform(post("/backend/template/repository").header("Authorization", headerstring)
				.contentType(MediaType.APPLICATION_JSON).content(request.toJSONString())).andReturn();

		assertThat(result.getResponse().getContentAsString(), is(response.toString()));
		List<UserEDB> edblist = (List<UserEDB>) eDBRepository.findByAccountUsername("user");
		assertThat(edblist, Matchers.hasSize(2));
		assertThat(edblist.get(0).getEDBName(), is("myRepository"));
		assertThat(edblist.get(1).getEDBName(), is("myNewRepository"));
	}

	@Test
	@WithMockUser
	public void forwardSetupTestFailed() throws Exception {
		mockJwtandUserAccountRepository();
		mockServer.expect(requestTo("http://template:8383/repository")).andExpect(method(HttpMethod.POST))
				.andRespond(withStatus(HttpStatus.NOT_FOUND));

		JSONObject request = new JSONObject();

		MvcResult result = mockMvc.perform(MockMvcRequestBuilders.post("/backend/template/repository").header("Authorization", headerstring)
				.contentType(MediaType.APPLICATION_JSON).content(request.toJSONString())).andReturn();
		assertThat(result.getResponse().getStatus(), is(equalTo(HttpStatus.NOT_FOUND.value())));

	}

	@SuppressWarnings("unchecked")
	@Test
	@WithMockUser
	public void forwardSearchTestSuccess() throws Exception {
		mockJwtandUserAccountRepository();
		JSONObject response = new JSONObject();
		JSONArray resultlist = new JSONArray();
		resultlist.add("doc1");
		resultlist.add("doc2");
		resultlist.add("doc3");
		response.put("resultList", resultlist);

		mockServer.expect(requestTo("http://template:8383/repository/myRepository/search"))
				.andExpect(method(HttpMethod.POST))
				.andRespond(withSuccess(response.toString(), MediaType.APPLICATION_JSON));

		JSONObject request = new JSONObject();
		request.put("token", "keyword1");

		MvcResult result = mockMvc
				.perform(MockMvcRequestBuilders.post("/backend/template/repository/myRepository/search").header("Authorization", headerstring)
						.contentType(MediaType.APPLICATION_JSON).content(request.toJSONString()))
				.andReturn();
		assertThat(result.getResponse().getContentAsString(), is(equalTo(response.toString())));
	}

	@SuppressWarnings("unchecked")
	@Test
	@WithMockUser
	public void forwardSearchTestToWrongRepositoryFails() throws Exception {
		mockJwtandUserAccountRepository();
		JSONObject response = new JSONObject();
		JSONArray resultlist = new JSONArray();
		resultlist.add("doc1");
		resultlist.add("doc2");
		resultlist.add("doc3");
		response.put("resultList", resultlist);

		mockServer.expect(requestTo("http://template:8383/repository/myWrongRepository/search"))
				.andExpect(method(HttpMethod.POST))
				.andRespond(withSuccess(response.toString(), MediaType.APPLICATION_JSON));

		JSONObject request = new JSONObject();
		request.put("token", "keyword1");

		MvcResult result = mockMvc
				.perform(MockMvcRequestBuilders.post("/backend/template/repository/myWrongRepository/search").header("Authorization", headerstring)
						.contentType(MediaType.APPLICATION_JSON).content(request.toJSONString()))
				.andReturn();
		assertThat(result.getResponse().getStatus(), is(equalTo(HttpStatus.NOT_FOUND.value())));
	}
	
	@SuppressWarnings("unchecked")
	@Test
	@WithMockUser
	public void forwardSearchTestFailedWrongFormatted() throws Exception {
		mockServer.expect(requestTo("http://template:8383/repository/myRepository/search"))
				.andExpect(method(HttpMethod.POST)).andRespond(withStatus(HttpStatus.BAD_REQUEST));

		JSONObject request = new JSONObject();
		request.put("token", "keyword800");

		MvcResult result = mockMvc
				.perform(MockMvcRequestBuilders.post("/backend/template/repository/myRepository/search").header("Authorization", headerstring)
						.contentType(MediaType.APPLICATION_JSON).content(request.toJSONString()))
				.andReturn();

		assertThat(result.getResponse().getStatus(), is(equalTo(HttpStatus.NOT_FOUND.value())));
	}

	@SuppressWarnings("unchecked")
	@Test
	@WithMockUser
	public void forwardUpdateTestSuccess() throws Exception {
		mockJwtandUserAccountRepository();
		JSONObject response = new JSONObject();
		response.put("repositoryName", "myNewRepository");
		mockServer.expect(requestTo("http://template:8383/repository/myRepository"))
				.andExpect(method(HttpMethod.POST)).andRespond(withSuccess());

		JSONObject request = new JSONObject();
		JSONArray keyword1 = new JSONArray();
		keyword1.add("doc3");
		keyword1.add("doc4");
		keyword1.add("doc5");
		request.put("keyword8", keyword1);
		JSONArray keyword2 = new JSONArray();
		keyword2.add("doc4");
		keyword2.add("doc1");
		keyword2.add("doc5");
		request.put("keyword3", keyword2);

		MvcResult result = mockMvc.perform(post("/backend/template/repository/myRepository").header("Authorization", headerstring)
				.contentType(MediaType.APPLICATION_JSON).content(request.toJSONString())).andReturn();

		assertThat(result.getResponse().getStatus(), is(equalTo(HttpStatus.OK.value())));
	}

	@SuppressWarnings("unchecked")
	@Test
	@WithMockUser
	public void forwardUpdateTestFailsBadRequest() throws Exception {
		mockJwtandUserAccountRepository();
		JSONObject response = new JSONObject();
		response.put("repositoryName", "myRepository");
		mockServer.expect(requestTo("http://template:8383/repository/myRepository"))
				.andExpect(method(HttpMethod.POST)).andRespond(withStatus(HttpStatus.BAD_REQUEST));

		JSONObject request = new JSONObject();
		JSONArray keyword1 = new JSONArray();
		keyword1.add("doc3");
		keyword1.add("doc4");
		keyword1.add("doc5");
		request.put("keyword8", keyword1);
		JSONArray keyword2 = new JSONArray();
		keyword2.add("doc4");
		keyword2.add("doc1");
		keyword2.add("doc5");
		request.put("keyword3", keyword2);

		MvcResult result = mockMvc.perform(post("/backend/template/repository/myRepository").header("Authorization", headerstring)
				.contentType(MediaType.APPLICATION_JSON).content(request.toJSONString())).andReturn();

		assertThat(result.getResponse().getStatus(), is(equalTo(HttpStatus.NOT_FOUND.value())));
	}

	@SuppressWarnings("unchecked")
	@Test
	@WithMockUser
	public void forwardUpdateTestFailsNotExistingUser() throws Exception {
		mockJwtandUserAccountRepository();
		mockServer.expect(requestTo("http://template:8383/repository/myRepository"))
				.andExpect(method(HttpMethod.POST)).andRespond(withStatus(HttpStatus.BAD_REQUEST));

		JSONObject request = new JSONObject();
		JSONArray keyword1 = new JSONArray();
		keyword1.add("doc3");
		keyword1.add("doc4");
		keyword1.add("doc5");
		request.put("keyword8", keyword1);
		JSONArray keyword2 = new JSONArray();
		keyword2.add("doc4");
		keyword2.add("doc1");
		keyword2.add("doc5");
		request.put("keyword3", keyword2);

		MvcResult result = mockMvc.perform(post("/backend/template/repository/myRepository").header("Authorization", headerstring)
				.contentType(MediaType.APPLICATION_JSON).content(request.toJSONString())).andReturn();

		assertThat(result.getResponse().getStatus(), is(equalTo(HttpStatus.NOT_FOUND.value())));
	}

	@Test
	@WithMockUser
	public void forwardDeleteSuccess() throws Exception {
		mockJwtandUserAccountRepository();
		mockServer.expect(requestTo("http://template:8383/repository/myRepository"))
				.andExpect(method(HttpMethod.DELETE)).andRespond(withSuccess());

		MvcResult result = mockMvc.perform(delete("/backend/template/repository/myRepository").header("Authorization", headerstring)).andReturn();
		assertThat(result.getResponse().getStatus(), is(equalTo(HttpStatus.OK.value())));
	}

	private void mockJwtandUserAccountRepository() {
		UserAccount user = new UserAccount("user", "secretpassword");
		this.accountRepository.save(user);
		UserEDB edb = new UserEDB(user, "template", "myRepository");
		this.eDBRepository.save(edb);
		List<SimpleGrantedAuthority> authorities = new ArrayList<>();
		authorities.add(new SimpleGrantedAuthority(AuthorityName.ROLE_USER.name()));
		given(util.getUsernameFromToken(token)).willReturn("user");
		given(util.getGrantedAuthorities(token)).willReturn(authorities);
	}

}
