package searchitect.common;

import static org.junit.Assert.assertEquals;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.PropertySource;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import searchitect.Application;
import searchitect.util.UrlBuilderService;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
@TestPropertySource("classpath:application-test.properties")
public class UrlBuilderServiceTest {

	@Autowired
	UrlBuilderService urlBuilder;

	@Test
	public void getTargetUrlTest1Param() {
		assertEquals("http://dynrh2lev:8282/", urlBuilder.getTargetUrl("dynrh2lev", "", "", ""));
	}
	
	@Test
	public void getTargetUrlTest2Param() {
		assertEquals("http://template:8383/test", urlBuilder.getTargetUrl("template", "test", "", ""));
	}

	@Test
	public void getTargetUrlTest3Param() {
		assertEquals("http://template:8383/test1/test2", urlBuilder.getTargetUrl("template", "test1", "test2", ""));
	}

	@Test
	public void getTargetUrlTest4Param() {
		assertEquals("http://template:8383/test1/test2/test3",urlBuilder.getTargetUrl("template", "test1", "test2", "test3"));
	}

}
