package searchitect.security;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.assertj.core.util.DateUtil;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import static org.mockito.Mockito.when;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.PropertySource;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;

import io.jsonwebtoken.ExpiredJwtException;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertEquals;

import searchitect.model.Authority;
import searchitect.model.AuthorityName;
import searchitect.util.TimeProvider;

@RunWith(SpringRunner.class)
@SpringBootTest
@PropertySource("classpath:application-test.properties")
public class JwtUtilTest {

	String expectedUsername = "testusername";
	String expectedPassword = "testsecret";

	@Mock
	TimeProvider timeProvider;

	@InjectMocks
	JwtUtil jwtUtil;

	// @Value("${jwt.expiration}")
	private int EXPIRATION = 300;

	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);
		ReflectionTestUtils.setField(jwtUtil, "EXPIRATION", 300);
		ReflectionTestUtils.setField(jwtUtil, "SECRET", "this secret key");
		ReflectionTestUtils.setField(jwtUtil, "APP_NAME", "searchitect_test");
	}

	@Test
	public void getUsernameFromTokenTest() {
		mockNormalTimeProvider();
		String username = jwtUtil.getUsernameFromToken(generateToken());
		assertEquals(username, expectedUsername);
	}

	@Test(expected = ExpiredJwtException.class)
	public void expiredTokenThrowsExceptionTest() {
		when(timeProvider.now()).thenReturn(DateUtil.now());
		when(timeProvider.addSeconds(EXPIRATION)).thenReturn(new Date(DateUtil.now().getTime() + 1));
		String expiredtoken = generateToken();
		jwtUtil.getExpiresAtDateFromToken(expiredtoken);
	}

	//sometimes it is only io.jsonwebtoken.MalformedJwtException;
//	@Test(expected = SignatureException.class)
//	public void modifiedTokenThrowsExceptionTest() {
//		mockNormalTimeProvider();
//		String token = generateToken();
//		System.out.println(token);
//		String modifiedtoken = token.replaceAll("U", "b").toString();
//		System.out.println(modifiedtoken);
//		jwtUtil.getUsernameFromToken(modifiedtoken);
//	}

	@Test
	public void getExpiresAtDateFromTokenTest() {
		mockNormalTimeProvider();
		Date date = jwtUtil.getExpiresAtDateFromToken(generateToken());
		System.out.println(date);
		assert (date.before(timeProvider.addSeconds(EXPIRATION)));
	}


	@Test
	public void getGrantedAuthoritiesTest() {
		mockNormalTimeProvider();
		List<SimpleGrantedAuthority> ga = jwtUtil.getGrantedAuthorities(generateToken());
		assert (ga.contains(new SimpleGrantedAuthority("ROLE_USER"))&& ga.contains(new SimpleGrantedAuthority("ROLE_ADMIN")));

	}

	@Test
	public void validateTokenWithvalidTokenTest() {
		mockNormalTimeProvider();
		String token = generateToken();
		Collection<Authority> auths = new ArrayList<Authority>();
		auths.add(new Authority(AuthorityName.ROLE_USER));
		UserDetails user = new User(expectedUsername, expectedPassword, auths);
		boolean test = jwtUtil.validateToken(token, user);
		assertTrue(test);

	}

	public void mockNormalTimeProvider() {
		when(timeProvider.now()).thenReturn(DateUtil.now());
		when(timeProvider.addSeconds(EXPIRATION)).thenReturn(new Date(DateUtil.now().getTime() + EXPIRATION * 1000));
	}

	public String generateToken() {
		Collection<Authority> auths = new ArrayList<Authority>();
		auths.add(new Authority(AuthorityName.ROLE_USER));
		auths.add(new Authority(AuthorityName.ROLE_ADMIN));
		UserDetails user = new User(expectedUsername, expectedPassword, auths);
		return jwtUtil.generateToken(user);
	}

}
