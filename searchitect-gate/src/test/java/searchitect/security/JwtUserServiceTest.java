package searchitect.security;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import java.util.Optional;


import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.PropertySource;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.test.context.junit4.SpringRunner;

import searchitect.model.UserAccount;
import searchitect.service.UserAccountRepository;

@RunWith(SpringRunner.class)
@SpringBootTest
@PropertySource("classpath:application-test.properties")
public class JwtUserServiceTest {
	
	String username ="user";
	String password ="expectedpassword";
	
	@Mock
	UserAccountRepository mockedUserAccountRepository;
	
	@InjectMocks
	JwtUserService jwtUserService;
	
	
	@Before
	public void setup(){
		 MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void loadUserByUserNameSuccessTest(){
		when(mockedUserAccountRepository.findByUsername(username))
		 .thenReturn( (Optional.of(new UserAccount(username,password))));
		UserDetails userDetails =jwtUserService.loadUserByUsername(username);
		assertEquals(userDetails.getPassword(),password);
		
	}
	
	

}
