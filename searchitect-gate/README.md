# Searchitect-Gate

### Functionality
* external interface of searchitect-Framework responsable for user management tasks and forwarding SE request to specific SE backend module
* works like a proxy by validating all request and on successful authentication and authorization forwarding the query


### Security implementation in searchitect gate

* Transport layer encryption via TLS - now selfsigned snakeoil certificate for testing
* Username-password authentication filter for first authentication, password is saved using bcrypt password encoder
* JSON Web Token (JWT) is used after successful username-password authentication, because there is no need for further evaluation of database entries for each request, validity of jwt is tested by integrity check of a keyed hash value

#### Generate Key in Terminal

	keytool -genkey -alias localhost -keyalg RSA -keysize 2048 -keystore src/main/resources/tomcat.keystore

##### migrate key to pkcs12 compatible keystore

	keytool -importkeystore -srckeystore tomcat.keystore -destkeystore tomcat.keystore -deststoretype pkcs12
	
	

### Compile
execute the shell script in the root searchitect directory in a terminal window

	./run.sh

or compile each project on your own, has to be in the same order like the script run.sh

	mvn clean install


### Deployment with docker

	docker-compose build
	docker-compose up

The interface description of the gateway is available calling following url in your browser
	
	https://localhost:8433/swagger-ui.html
	



