package searchitect.client.dynrh2levplugin;

import static org.hamcrest.Matchers.hasSize;


import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;

import java.nio.file.Paths;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.spec.InvalidKeySpecException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.NavigableMap;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import javax.crypto.NoSuchPaddingException;

import org.apache.commons.io.FileUtils;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;

import searchitect.clusion.CryptoPrimitivesModified;
import searchitect.clusion.DynRH2LevModifiedMap;
import searchitect.common.clusion.TextExtractPar;
import searchitect.common.clusion.TextProc;
import searchitect.common.exception.SearchitectException;
import searchitect.common.view.Upload;
import searchitect.common.viewdynrh2lev.SearchTokendynrh2lev;
import searchitect.common.viewdynrh2lev.UpdateIndexdynrh2lev;
import searchitect.common.viewdynrh2lev.UploadIndexdynrh2levMap;



@RunWith(SpringRunner.class)
public class Clientdynrh2levImplTest {
	
	static String path= "/tmp/test/";
	static String updatePath ="/tmp/update/";
	static String path1 = "/tmp/test/test1.txt";
	static String  path2 = "/tmp/test/test2.txt";
	static String  path3 = "/tmp/test/test3.txt";
	static String upath1 = "/tmp/update/utest1.txt";
	static String  upath2 = "/tmp/update/utest2.txt";
	static String  upath3 = "/tmp/update/utest3.txt";
	static Multimap<String,String> invertedMMSetup;
	static Multimap<String,String> invertedMMUpdate;
	static int size;
	
	public static void createTestFiles() throws IOException {
        Files.createDirectories( Paths.get(path1).getParent());
        Files.createFile(Paths.get(path1));
        PrintWriter writer1 = new PrintWriter(path1, "UTF-8");
        writer1.println("The first line ");
        writer1.close();
        Files.createFile(Paths.get(path2));
        PrintWriter writer2 = new PrintWriter(path2, "UTF-8");
        writer2.println("The first keyword1 ");
        writer2.println("The second keyword2");
        writer2.close();
        Files.createFile(Paths.get(path3));
        PrintWriter writer3 = new PrintWriter(path3, "UTF-8");
        writer3.println("weired keyword1");
        writer3.println("The second keyword2");
        writer3.close();
	}
	
	
	public static void createUpdateTestFiles() throws IOException {
        Files.createDirectories(Paths.get(upath1).getParent());
        Files.createFile(Paths.get(upath1));
        PrintWriter writer1 = new PrintWriter(upath1, "UTF-8");
        writer1.println("The first update line");
        writer1.println("The second update line");
        writer1.close();
        Files.createFile(Paths.get(upath2));
        PrintWriter writer2 = new PrintWriter(upath2, "UTF-8");
        writer2.println("The first update keyword1");
        writer2.println("The second update keyword2  ");
        writer2.close();
        Files.createFile(Paths.get(upath3));
        PrintWriter writer3 = new PrintWriter(upath3, "UTF-8");
        writer3.println("weired update keyword1");
        writer3.println("The second update keyword2");
        writer3.println(" first ");
        writer3.close();
	}
	
	public static void deleteTestFiles() throws IOException {
		FileUtils.deleteDirectory(new File("/tmp/test/"));
		FileUtils.deleteDirectory(new File("/tmp/update/"));
	}
	

    @BeforeClass 
    public static void setUpClass() throws IOException, InvalidKeyException, InvalidAlgorithmParameterException, NoSuchAlgorithmException, NoSuchProviderException, NoSuchPaddingException, InvalidKeySpecException {      
        createTestFiles();
        createUpdateTestFiles();
        ArrayList<File> fileList = new ArrayList<>();
		TextProc.listf(path, fileList);
		TextProc.TextProc(false, path);
		invertedMMSetup =TextExtractPar.lp1;
		
		size = TextExtractPar.lp2.size();
		System.out.println("setupsize: "+invertedMMSetup.size());
		TextExtractPar.lp1.clear();
		TextExtractPar.lp2.clear();
		ArrayList<File> fileList2 = new ArrayList<>();
		TextProc.listf(updatePath, fileList2);
		TextProc.TextProc(false, updatePath);
		invertedMMUpdate =TextExtractPar.lp1;
		System.out.println("Update size: "+invertedMMUpdate.size());
    }
	
    @AfterClass 
    public static void afterClass() throws IOException {      
        deleteTestFiles();
    }
    
//	@Test
//	public 	void Clientdynrh2levImplConstructorNullTest() {
//		Clientdynrh2levImpl dynrh2lev = new Clientdynrh2levImpl();
//		assertEquals(dynrh2lev.getImplName(),"dynrh2lev");
//		
//	}
//	
//	@Test
//	public 	void Clientdynrh2levImplConstructorStateTest() {
//		String repositoryName = "testname";
//		byte[] sk = "assumedsecret".getBytes();
//		HashMap<String, Integer> stateCounter = new HashMap<String, Integer>();
//		stateCounter.put("keyword1", 1);
//		stateCounter.put("keyword2",5);
//		Clientdynrh2levImpl dynrh2lev = new Clientdynrh2levImpl(new ClientStatedynrh2lev(repositoryName,sk,stateCounter));
//		assertEquals(dynrh2lev.getImplName(),"dynrh2lev");
//	}
	
	@Test
	public 	void setupSuccessTest() throws InvalidKeyException, InvalidAlgorithmParameterException, NoSuchAlgorithmException, NoSuchProviderException, NoSuchPaddingException, InvalidKeySpecException, IOException {
	
		String password = "assumedsecure";
		Clientdynrh2levImpl dynrh2lev = new Clientdynrh2levImpl();;
		Upload up = dynrh2lev.setup( password, invertedMMSetup, TextExtractPar.lp2.size());
		ObjectMapper mapper = new ObjectMapper();
		String json = mapper.writeValueAsString(up);
		assertEquals(dynrh2lev.getImplName(),"dynrh2lev");
		assertTrue(json.contains("dictionary"));
	}
	
	@Test(expected = SearchitectException.class)
	public 	void setupMultimapNullFailsTest() throws InvalidKeyException, InvalidAlgorithmParameterException, NoSuchAlgorithmException, NoSuchProviderException, NoSuchPaddingException, InvalidKeySpecException, IOException {
		Clientdynrh2levImpl dynrh2lev = new Clientdynrh2levImpl();
		String password = "assumedsecure";
		Multimap<String,String> invertedIndex =null;
		Upload up = dynrh2lev.setup( password, invertedIndex, TextExtractPar.lp2.size());
		ObjectMapper mapper = new ObjectMapper();
		String json = mapper.writeValueAsString(up);
		System.out.println(json);
		assertEquals(dynrh2lev.getImplName(),"dynrh2lev");
		assertTrue(json.contains("dictionary"));
	}	
	
	@Test(expected = SearchitectException.class)
	public 	void setupPasswordNullFailsTest() throws InvalidKeyException, InvalidAlgorithmParameterException, NoSuchAlgorithmException, NoSuchProviderException, NoSuchPaddingException, InvalidKeySpecException, IOException {
		String path= "/tmp/test";
		String password = null;
		Clientdynrh2levImpl dynrh2lev = new Clientdynrh2levImpl();
		ObjectMapper mapper = new ObjectMapper();
		Upload up = dynrh2lev.setup( password, invertedMMSetup, TextExtractPar.lp2.size());
		String json = mapper.writeValueAsString(up);
		System.out.println(json);
		assertEquals(dynrh2lev.getImplName(),"dynrh2lev");
		assertTrue(json.contains("dictionary"));
	}

	@Test
	public 	void setupKeywordSearchSuccessTest() throws InvalidKeyException, InvalidAlgorithmParameterException, NoSuchAlgorithmException, NoSuchProviderException, NoSuchPaddingException, IOException, InvalidKeySpecException {
		String path= "/tmp/test";
	
		String password = "assumedsecure";
		
		Clientdynrh2levImpl dynrh2lev = new Clientdynrh2levImpl();
		Upload ups = dynrh2lev.setup( password, invertedMMSetup, size);
		ObjectMapper mapper = new ObjectMapper();
		String json = mapper.writeValueAsString(ups);
		UploadIndexdynrh2levMap up = mapper.readValue(json, UploadIndexdynrh2levMap.class);
		String query= dynrh2lev.search("first");
		HashMap<String, byte[]> stateCounter = new HashMap<String, byte[]>();
		SearchTokendynrh2lev token =  mapper.readValue(query, SearchTokendynrh2lev.class);
		HashMap<String, byte[]> map = up.getDictionary();
    	System.out.println(token.getSearchToken().toString());
    	List<String> response = DynRH2LevModifiedMap.queryFS(token.getSearchToken(), map, up.getArray(), stateCounter);
    	byte [] key = CryptoPrimitivesModified.generateCmac(dynrh2lev.state.sk, 3 + new String());
    	 List<String> result =  DynRH2LevModifiedMap.resolve(key, response);
    	System.out.println("Result: "+ result);
		assertTrue(query.contains("searchToken"));
		assertThat(result,hasSize(3));
	}
	
	
	@Test
	public 	void retrieveUpdateUploadSearchSize() throws InvalidKeyException, InvalidAlgorithmParameterException, NoSuchAlgorithmException, NoSuchProviderException, NoSuchPaddingException, InvalidKeySpecException, IOException {
		String path= "/tmp/test";
		String password = "assumedsecure";
		//setup
		Clientdynrh2levImpl dynrh2lev = new Clientdynrh2levImpl();
		Upload ups = dynrh2lev.setup( password, invertedMMSetup, size);
		ObjectMapper mapper = new ObjectMapper();
		String json = mapper.writeValueAsString(ups);
		UploadIndexdynrh2levMap up = mapper.readValue(json, UploadIndexdynrh2levMap.class);
		HashMap <String,byte[]>map =up.getDictionary();
		for (String s : map.keySet()){
			System.out.println("Upload Labelsize:" + s.length());
		}
		for (byte[]b :map.values()){
				System.out.println("Upload Valuesize:" + b.length);
		}
		byte[][] bytearray =up.getArray();
		
		//System.out.println("Size of bytes in Array:" + bytearray[0].length);
		//update
		String updateString = dynrh2lev.update(invertedMMUpdate);
		System.out.println(updateString);
		
		UpdateIndexdynrh2lev update = mapper.readValue(updateString, UpdateIndexdynrh2lev.class);
		NavigableMap <String,Collection<byte[]>>upmap = update.getDictionary();
		HashMap <String,byte[]>createupdatemap =new HashMap<String,byte[]>();
		for (String s :upmap.navigableKeySet()){
			System.out.println("Update Labelsize:" + s.length());
			for (byte[] a : upmap.get(s))
			createupdatemap.put(s,a);
		}
		for (Collection<byte[]>bcoll :upmap.values()){
			for (byte[] b :bcoll){
				System.out.println("Update Valuesize:" + b.length);
			}
		}
		
		
		//query
		String query= dynrh2lev.search("first");
		SearchTokendynrh2lev token =  mapper.readValue(query, SearchTokendynrh2lev.class);
    	System.out.println(token.getSearchToken().toString());
    	for(byte[] btoken:token.getSearchToken())
    	System.out.println("Searchtoken length:"+ btoken.length);
    	List<String> response = DynRH2LevModifiedMap.queryFS(token.getSearchToken(), map, up.getArray(), createupdatemap);
    	byte [] key = CryptoPrimitivesModified.generateCmac(dynrh2lev.state.sk, 3 + new String());
    	 List<String> result =  DynRH2LevModifiedMap.resolve(key, response);
    	System.out.println("Resultsize: "+result.size());
    	assertThat(result,hasSize(6));
	}

}
