package searchitect.client.dynrh2levplugin;

import java.io.Serializable;
import java.util.HashMap;

/**
 * ClientStatedynrh2lev - holds client state of dynrh2lev implementation
 */
public class ClientStatedynrh2lev implements Serializable{

	
	private static final long serialVersionUID = -828866469850486251L;
	String implName = "dynrh2lev";
	String repositoryName;
	byte[] sk;
	HashMap<String, Integer> stateCounter;
	
	public ClientStatedynrh2lev() {}
	
	public ClientStatedynrh2lev(String repositoryName, byte[] sk, HashMap<String, Integer> stateCounter) {
		super();
		this.repositoryName = repositoryName;
		this.sk = sk;
		this.stateCounter = stateCounter;
	}

	public String getImplName() {
		return implName;
	}

	public String getRepositoryName() {
		return repositoryName;
	}
	public void setRepositoryName(String repositoryName) {
		this.repositoryName = repositoryName;
	}
	public byte[] getSk() {
		return sk;
	}
	public void setSk(byte[] sk) {
		this.sk = sk;
	}
	public HashMap<String, Integer> getStateCounter() {
		return stateCounter;
	}
	public void setStateCounter(HashMap<String, Integer> statecounter) {
		this.stateCounter = statecounter;
	}
	
	
	
	
	
}
