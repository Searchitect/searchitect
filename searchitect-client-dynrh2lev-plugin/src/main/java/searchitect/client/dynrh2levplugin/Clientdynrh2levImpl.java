package searchitect.client.dynrh2levplugin;


import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.spec.InvalidKeySpecException;
import java.util.List;
import java.util.concurrent.ExecutionException;

import javax.crypto.NoSuchPaddingException;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.Multimap;
import com.google.common.collect.TreeMultimap;


import searchitect.common.client.ClientScheme;
import searchitect.common.exception.SearchitectException;
import searchitect.common.view.SearchResult;
import searchitect.common.view.Upload;
import searchitect.common.viewdynrh2lev.SearchTokendynrh2lev;
import searchitect.common.viewdynrh2lev.UpdateIndexdynrh2lev;
import searchitect.common.viewdynrh2lev.UploadIndexdynrh2levMap;
import searchitect.client.dynrh2levplugin.ClientStatedynrh2lev;
import searchitect.clusion.CryptoPrimitivesModified;
import searchitect.clusion.DynRH2LevModifiedMap;
import searchitect.clusion.RH2LevModifiedMap;
public class Clientdynrh2levImpl implements ClientScheme{

	ClientStatedynrh2lev state;
	ObjectMapper mapper;
	
	Clientdynrh2levImpl(ClientStatedynrh2lev state){
		this.mapper = new ObjectMapper();
		if (state == null) {
			this.state = new ClientStatedynrh2lev();
		}
		else {
			this.state = state;
		}
	}
	

	public Clientdynrh2levImpl() {
		this.mapper = new ObjectMapper();
		this.state = new ClientStatedynrh2lev();
	}

	public String getImplName() {
		return state.getImplName();
	}
	
	/*
	 * (non-Javadoc)
	 * @see searchitect.scheme.IClientScheme#setup(java.lang.String)
	 */
	public Upload setup(String password, Multimap<String,String> invertedIndex, int numberOfDocuments ) throws SearchitectException {
	
		try {
			//generate key
			state.sk = CryptoPrimitivesModified.keyGen(256, password, "salt/salt", 100);
			
			// The two parameters depend on the size of the dataset. Change
			// accordingly to have better search performance
			//adjust blocks!!
			int records = invertedIndex.size();
			int keys = invertedIndex.keySet().size();
			int smallBlock = (int)Math.ceil((double)(records/keys))+2;	
			int bigBlock = 	(int) Math.ceil(Math.sqrt((double)(numberOfDocuments))+1)+2;
			System.out.println("small: "+smallBlock+" big: "+bigBlock);
			int dataSize = invertedIndex.size();
		
			RH2LevModifiedMap.master = state.sk;

			DynRH2LevModifiedMap twolev = (DynRH2LevModifiedMap) DynRH2LevModifiedMap.constructEMMParGMM(state.sk,invertedIndex , bigBlock, smallBlock, dataSize);
		
			state.setStateCounter(twolev.getState());
			return new UploadIndexdynrh2levMap(twolev.getDictionary(),twolev.getArray());
	
		} catch (NullPointerException | InvalidKeySpecException | NoSuchAlgorithmException | NoSuchProviderException | InterruptedException | ExecutionException | IOException e) {
			//e.printStackTrace();
			System.out.println("Setup failed");
			throw new SearchitectException(e.getMessage());
			
		}		
	}
	
	/*
	 * (non-Javadoc)
	 * @see searchitect.scheme.IClientScheme#search(java.lang.String)
	 */
	public String search(String query) throws SearchitectException {
		try {
			//generate search token
			byte[][] token = DynRH2LevModifiedMap.genTokenFS(state.sk, query,state.getStateCounter());
			ObjectMapper mapper = new ObjectMapper();
			String tokenJsonString = mapper.writeValueAsString(new SearchTokendynrh2lev(token));
			return tokenJsonString;
		} catch (UnsupportedEncodingException | JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println("Token generation failed");
			throw new SearchitectException(e.getMessage());
		}
	}
	
	/*
	 * (non-Javadoc)
	 * @see searchitect.scheme.IClientScheme#update(java.lang.String)
	 */
	public String update (Multimap<String,String> invertedIndex) throws SearchitectException {
		try {
			TreeMultimap<String, byte[]> tokenUp = DynRH2LevModifiedMap.tokenUpdate(state.sk, invertedIndex,state.getStateCounter());
			String updateJsonString = mapper.writeValueAsString(new UpdateIndexdynrh2lev(tokenUp.asMap()));	
			return updateJsonString;
		} catch (InvalidKeyException | InvalidAlgorithmParameterException | NoSuchAlgorithmException
				| NoSuchProviderException | NoSuchPaddingException | IOException e) {
			e.printStackTrace();
			System.out.println("Update failed" + e.getMessage());
			throw new SearchitectException(e.getMessage());
		}
	}


	public SearchResult resolve(SearchResult encrypted) throws SearchitectException{
		try {
			List<String> decrypted = DynRH2LevModifiedMap.resolve(CryptoPrimitivesModified.generateCmac(state.sk, 3 + new String()), encrypted.getResultList());
			return new SearchResult(decrypted);
		} catch (InvalidKeyException | InvalidAlgorithmParameterException | NoSuchAlgorithmException
				| NoSuchProviderException | NoSuchPaddingException | IOException e) {
			// TODO Auto-generated catch block
			System.out.println("Decryption of Result failed");
			e.printStackTrace();
			throw new SearchitectException(e.getMessage());
		}
	
		
	}

	
}
