# Searchitect-Client-DynRH2Lev-Plugin

### Functionality
 * Client part of DynRH2Lev Searchable Encryption scheme

### Compile using

	mvn clean install
	
	
### Packages
* searchitect.client.sophos
	* Clientdynrh2levImpl.java - implements the ClientScheme interface and all methods
	* ClientSatedynrh2lev.java - maintains the client state