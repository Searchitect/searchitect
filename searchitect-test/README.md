# Searchitect-Test

### Preliminary
tested with ubuntu 16.04 and openjdk 10 (oracle jdk 10 causes a bc provider signing issue)

### Functionality
* Test the implementations, the tests are design to figure out the limits therefore all tests will end up with some exceptions like OutOfMemoryError: Java heap space and they are specific to the test sets generated with searchitect-testset, but may be adapted easily in case. For example hard coded source file directories are set at the beginning of the cases:
* Synthetic: /tmp/testset/synthetic/
* Real: /tmp/testset/synthetic/
* ReportPath: /tmp/report/
If you have chosen a different output path for the testset just change them.

* Test Options:
- Synthetic case:
	Input :
		* first parameter: scheme [dynrh2lev|dynrh2levrocks] 
 	Output: 
 		* [scheme]synthetictest 
- Real case: 
	Input: 
		* first parameter: scheme [dynrh2lev|dynrh2levrocks] 
		* second parameter: path to wordlist wordlistPath
	Output: 
		* console output of testreport can be written to a file using 2>&1 | tee /tmp/reportfile.txt
		* search and setup or update report 


### Preliminaries
* The docker container with searchitect server instances should be deployed 
* The serialized java objects are stored in a directory e.g. /tmp/testset/real/
* It did make a difference in the testing but we set the Java Runtime Environment (JRE) heap space to 8 GB by issuing export _JAVA_OPTIONS= -Xmx8G, because then the docker container start to swap therefore better do not use

### Packages
* searchitect.test
	* Testrunner - Java application used to call the static methods in Dynrh2levTest depending on the command line input parameters
	* StaticTest.java - provides the static test methods for the synthetic and real test set for dynrh2lev and dynrh2levrocks
	* DynamicTest.java - provides methods for testing dynamic updates for the synthetic and the real testset
	
	
### Usage

* Compile with maven

		mvn clean install
		
* move to the target directory
		
		cd target

## Usage examples

#### Static synthetic tests the Setup and Search protocol

	java -jar searchitect-test-0.0.1.jar -synthetic dynrh2lev /tmp/testset/synthetic/ 
	java -jar searchitect-test-0.0.1.jar -synthetic dynrh2levrocks /tmp/testset/synthetic/  

#### Static realistic tests the Setup and Search protocol

	java -Xmx2g  -jar searchitect-test-0.0.1.jar -real dynrh2lev /tmp/testset/real/  
	java -Xmx2g  -jar searchitect-test-0.0.1.jar -real dynrh2levrocks /tmp/testset/real/  


#### Dynamic synthetic tests the Update and Search protocol


	java -jar searchitect-test-0.0.1.jar -dynamicsynthetic dynrh2lev /tmp/testset/synthetic/ 
	java -jar searchitect-test-0.0.1.jar -dynamicsynthetic sophos /tmp/testset/synthetic/ 
	java -jar searchitect-test-0.0.1.jar -dynamicsynthetic dynrh2levrocks /tmp/testset/synthetic/  

#### Dynamic real tests  the Update and Search protocol

	java -jar searchitect-test-0.0.1.jar -dynamicreal dynrh2lev /tmp/testset/dynamicreal/ 
	java -jar searchitect-test-0.0.1.jar -dynamicreal dynrh2levrocks /tmp/testset/dynamicreal/ 
	java -jar searchitect-test-0.0.1.jar -dynamicreal sophos /tmp/testset/dynamicreal/ 
