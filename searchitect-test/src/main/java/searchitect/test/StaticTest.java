package searchitect.test;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;


import com.google.common.collect.Multimap;

import files.FileIO;
import searchitect.client.Client;
import searchitect.client.dynrh2levplugin.Clientdynrh2levImpl;
import searchitect.client.dynrh2levplugin.Clientdynrh2levrocksImpl;
import searchitect.common.client.ClientScheme;
import searchitect.common.view.RepositoryInfo;
import searchitect.common.view.SearchResult;
import searchitect.common.view.UserAuthRequest;

public class StaticTest {

	final static String pathsynthetic = "/tmp/testset/synthetic/";
	final static String pathreal = "/tmp/testset/real/";
	final static String partOfFileName = "synthUpdate";
	final static String partOfFileNameForward = "forward";
	final static String partOfFileNameInverted = "inverted";
	final static String uri = "https://localhost:8433/";
	final static String username = "testuser";
	final static String reportPath = "/tmp/report/";

	
	/**
	 * static synthetic test using the synthetic testset at path pathsynthetic
	 * test is specific to the hardcoded sizearray
	 * 
	 * @param testcase dynrh2lev and dynrh2levrocks
	 */
	public static void testSynthetic(String testcase) {

		try {
			int[] sizearray = new int[] { 10, 100, 250, 500, 750, 1000, 2000, 3000,5000,10000 };;
			(new File(reportPath)).mkdir();
			ClientScheme clientimpl;
			if (testcase.equals("dynrh2lev")) {
				clientimpl = (ClientScheme) new Clientdynrh2levImpl();
			} else {
				clientimpl = (ClientScheme) new Clientdynrh2levrocksImpl();
			}
			Client client = new Client(uri, clientimpl);
			Date time = Calendar.getInstance().getTime();
			System.out.println("Test of " + clientimpl.getImplName() + " using synthetic testset startet at " + time);

			try {
				System.out.println("\n create user");
				client.createUser(new UserAuthRequest(username, "notsecurepassword"));

			} catch (Exception e) {
				System.out.println("user exists!! \n");
			}
			// write header
			String logheader = new StringBuilder()
					.append("keywords").append(";")
					.append("mmsize").append(";")
					.append("records").append(";")
					.append("setuptime").append(";")
					.append("searchtime").append("\n")
					.toString();
			FileIO.appendStringToFile(logheader, reportPath.concat(clientimpl.getImplName() + "synthetictest"));

			// loop over different size of documents defined in sizearray
			for (int size : sizearray) {
				time = Calendar.getInstance().getTime();
				System.out.println(size + " testset of synthetic testset startet at " + time);
				System.out.println("Authenticate user");
				client.authenticateUser(new UserAuthRequest(username, "notsecurepassword"));
				System.out.println("Check authentication and backend communication: ");
				System.out.println(client.checkAuthBackend());
				byte[] data;
				String filePath = pathsynthetic + size + partOfFileName;
				data = FileIO.readBytesFromFile(filePath);
				long length = FileIO.getFileSize(filePath);
				System.out.println("Length of Multimap: " + length);
				// test dezerialize with object mapper
				@SuppressWarnings("unchecked")
				Multimap<String, String> map = (Multimap<String, String>) Serializer.deserialize(data);
				try {
					long start1 = System.currentTimeMillis();
					RepositoryInfo repo = client.setup("testpwd", map, size);
					long setuptime = System.currentTimeMillis() - start1;
					System.out.println("Repository has been initialized: " + repo.getrepositoryName());
					System.out.println("Setup time: " + setuptime);
					// search for each of 100 keywords
					long sum = 0L;
					for (int i = 1; i < 100; i++) {
						String query = size + "keyword" + i;
						try {
							long start2 = System.currentTimeMillis();
							SearchResult searchresult = client.search(repo.getrepositoryName(), query);
							long searchtime = System.currentTimeMillis() - start2;
							//System.out.println(searchresult.getResultList());
							// validate result
							assert (searchresult.getResultList().size() == size);
							System.out.println(i + " search time: " + searchtime);
							sum += searchtime;
						} catch (Exception e) {
							System.out.println("Test failed.");
						}
					}
					// calculate average
					long averagerun = sum / 100;
					System.out.println("Average search time: " + averagerun);
					// write result to testreport
					// size of resultset; length of multimap in bytes; multimap
					// values; time for setup; average time for search
					String log = new StringBuilder()
							.append(size).append(";")
							.append(length).append(";")
							.append(map.values().size()).append(";")
							.append(setuptime).append(";")
							.append(averagerun).append("\n")
							.toString();
					FileIO.appendStringToFile(log, reportPath.concat(clientimpl.getImplName() + "synthetictest"));
					System.out.println("Delete repository");
					client.delete(repo.getrepositoryName());
				} catch (Exception e) {
					System.out.println("Setup failed.");
				}
			}

		} catch (IOException | ClassNotFoundException e) {
			// TODO Auto-generated catch block
			System.out.println("Tests failed");
			e.printStackTrace();
		}
	}
	/**
	 * static synthetic test using the synthetic testset at path pathsynthetic
	 * test is specific becauseof the id array of user and the number of documents contained in the multimaps
	 * 
	 * @param testcase
	 * @param pathToWordLists
	 */
	public static void testReal(String testcase, String pathToWordLists) {

		try {
			 char[] id = new char[] { 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j','k', 'l', 'm', 'n' };
			 int[]numberOfDocuments = new int[]{35,132, 248,378,437,563, 642,721,994,1116,2249,3272, 4796, 8009,18687};
			(new File(reportPath)).mkdir();
			ClientScheme clientimpl;
			if (testcase.equals("dynrh2lev")) {
				clientimpl = (ClientScheme) new Clientdynrh2levImpl();
			} else {
				clientimpl = (ClientScheme) new Clientdynrh2levrocksImpl();
			}
			Client client = new Client(uri, clientimpl);
			Date time = Calendar.getInstance().getTime();
			System.out.println("Test of " + clientimpl.getImplName() + " using real testset startet at " + time);
			
			
			try {
				System.out.println("\n create user");
				client.createUser(new UserAuthRequest(username, "notsecurepassword"));
			} catch (Exception e) {
				System.out.println("user exists!! \n");
			}
			// write table headers of log files for search and setup file
			String logsetupheader = new StringBuilder()
					.append("user").append(";")
					.append("keywords").append(";")
					.append("mmspace").append(";")
					.append("records").append(";")
					.append("setuptime").append("\n")
					.toString();
			FileIO.appendStringToFile(logsetupheader, reportPath.concat(clientimpl.getImplName() + "realsetuptest"));
			String logsearchheader = new StringBuilder()
					.append("user").append(";")
					.append("keywords").append(";")
					.append("records").append(";")
					.append("keyword").append(";")
					.append("searchtime").append(";")
					.append("result").append("\n")
					.toString();
			FileIO.appendStringToFile(logsearchheader, reportPath.concat(clientimpl.getImplName() + "realsearchtest"));

			int i = 0;
			// loop over alle user repositories
			for (char user : id) {

				time = Calendar.getInstance().getTime();
				System.out.println(user + " testset of synthetic testset startet at " + time);
				System.out.println("Authenticate user");
				client.authenticateUser(new UserAuthRequest(username, "notsecurepassword"));
				System.out.println("Check authentication and backend communication: ");
				System.out.println(client.checkAuthBackend());

				// String filePathForward = pathreal + user +
				// partOfFileNameForward;
				String filePathInverted = pathreal + user + partOfFileNameInverted;
				byte[] data = FileIO.readBytesFromFile(filePathInverted);
				long length = FileIO.getFileSize(filePathInverted);
				System.out.println("Length of Multimap: " + length);
				// test dezerialize with object mapper
				@SuppressWarnings("unchecked")
				Multimap<String, String> map = (Multimap<String, String>) Serializer.deserialize(data);
				int numberOfKeywords = map.keys().elementSet().size();
				int numberOfRecords = map.values().size();

				System.out.println("Number of keywords: " + numberOfKeywords);
				System.out.println("Number of records: " + numberOfRecords);
				try {
					long start1 = System.currentTimeMillis();
					RepositoryInfo repo = client.setup("testpwd", map, numberOfDocuments[i]);
					long setuptime = System.currentTimeMillis() - start1;
					System.out.println("Repository has been initialized: " + repo.getrepositoryName());
					System.out.println("Setup time: " + setuptime);
					i++;
					// write to setup testreport - to *realsetupfile
					//  number of documents was determined by shell commands
					// user;number of keywords ; length of multimap in bytes;
					// number
					// of multimap values; time for setup
					String log = new StringBuilder()
							.append(user + "_user").append(";")
							.append(numberOfKeywords).append(";")
							.append(length).append(";")
							.append(numberOfRecords).append(";")
							.append(setuptime).append("\n")
							.toString();
					FileIO.appendStringToFile(log, reportPath.concat(clientimpl.getImplName() + "realsetuptest"));

					// search for 100 random chosen keywords
					// https://www.randomlists.com/random-words?qty=100#
					String[] words = FileIO.readStringFromFile(pathToWordLists + user + "_wordlist").split("\n");
					SearchResult searchresult;
					long searchtime;
					for (String keyword : words) {
						String[] split =keyword.split(";");
						try {
							long start2 = System.currentTimeMillis();
							searchresult = client.search(repo.getrepositoryName(), split[0]);
							searchtime = System.currentTimeMillis() - start2;
						} catch (Exception e) {
							System.out.println(e.getMessage());
							List<String> l = new ArrayList<String>();
							searchresult = new SearchResult(l);
							searchtime = 0;
						}
						// validate result
						int expectedresult = map.get(split[0]).size();
						int searchresultsize = searchresult.getResultList().size();
						System.out.println("Expected:" + expectedresult);
						System.out.println("Result:" + searchresultsize);
						assert (searchresultsize == expectedresult);
						System.out.println(
								searchresultsize + " matches for " + keyword + " with search time: " + searchtime);

						// write result to testreport to * realsearchtest file
						// size of resultset; multimap values; time for setup;
						// average time for search
						String logsearch = new StringBuilder()
								.append(user + "_user").append(";")
								.append(numberOfKeywords).append(";")
								.append(map.values().size()).append(";")
								.append(split[0]).append(";")
								.append(searchtime).append(";")
								.append(expectedresult)
								.append(";").append("\n").toString();
						FileIO.appendStringToFile(logsearch,
								reportPath.concat(clientimpl.getImplName() + "realsearchtest"));
					}

					System.out.println("Delete repository");
					client.delete(repo.getrepositoryName());
				} catch (Exception e) {
					System.out.println("Setup failed" + e.getMessage());
				}
			}

		} catch (IOException | ClassNotFoundException e) {
			// TODO Auto-generated catch block
			System.out.println("Tests failed");
			e.printStackTrace();
		}
	}

}
