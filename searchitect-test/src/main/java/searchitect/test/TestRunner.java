package searchitect.test;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.security.Security;

@SpringBootApplication
public class TestRunner {

	public static void main(String[] args) {

		Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());

    	SpringApplication.run(TestRunner.class, args);
        System.out.println("welcome to searchitect");
        if (args.length > 1) {
			switch (args[0]) {
			case "-synthetic": {
				StaticTest.testSynthetic(args[1]);
				break;
			}
			case "-real": {
				if(args.length>2) {
				StaticTest.testReal(args[1], args[2]); 
				}
				else {
					System.out.println("Missing parameter \n");
					printInfoMessage();
				}
				break;
			}
			case "-dynamicsynthetic": {
				DynamicTest.testSynthetic(args[1]);
				break;
			}
			case "-dynamicreal": {
				if (args.length>2) {
				DynamicTest.testReal(args[1], args[2]); 
				}
				else{
					DynamicTest.testReal(args[1], null); 
				}
				break;
			}
			default: break;
        
			}
        }
			else {
				printInfoMessage();
			}
    }

	private static void printInfoMessage() {
		System.out.println("Define test case first param [-synthetic|-real|-dynamicsynthetic|-dynamicreal]\n"
				+ "in static case [-synthetic|-real] the second parameter is the instance to test [dynrh2lev|dynrh2levrocks] and \n"
				+ "\t in case of -real third parameter is path to wordlist to test\n"
				+ "in dynamic case [-dynamicsynthetic|-dynamicreal] the second parameter is the instance to test [dynrh2lev|dynrh2levrocks|sophos] and \n"
				+ "\t in case of -real third parameter is path to wordlist to test\n");
	}
}