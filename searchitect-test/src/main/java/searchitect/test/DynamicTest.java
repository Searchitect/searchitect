package searchitect.test;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import com.google.common.collect.Multimap;

import files.FileIO;
import searchitect.client.Client;
import searchitect.client.dynrh2levplugin.Clientdynrh2levImpl;
import searchitect.client.dynrh2levplugin.Clientdynrh2levrocksImpl;
import searchitect.client.sophos.ClientSophosImpl;
import searchitect.common.client.ClientScheme;
import searchitect.common.view.RepositoryInfo;
import searchitect.common.view.SearchResult;
import searchitect.common.view.UserAuthRequest;

public class DynamicTest {

	final static String pathSynthetic = "/tmp/testset/dynamicsynthetic/";
	final static String pathReal = "/tmp/testset/dynamicreal/";
	final static String partOfFileName = "synthUpdate";
	final static String partOfFileNameForward = "forward";
	final static String partOfFileNameInverted = "inverted";
	final static String uri = "https://localhost:8433/";
	final static String username = "testuser";
	final static String reportPath = "/tmp/report/";

	/**
	 * dynamic synthetic test - tests update and search of synthetic test set at pathSynthetic
	 * 
	 * @param testcase is dynrh2lev, dynrh2levrocks, or sophos
	 */
	public static void testSynthetic(String testcase) {

		try {
			int[] sizearray = new int[] { 10, 100 };
			int[] test = new int[] {20,10};
			(new File(reportPath)).mkdir();
			ClientScheme clientimpl;
			if (testcase.equals("dynrh2levrocks")) {
				clientimpl = (ClientScheme) new Clientdynrh2levrocksImpl();
			} else if (testcase.equals("sophos")) {
				clientimpl = (ClientScheme) new ClientSophosImpl();
			} else {
				clientimpl = (ClientScheme) new Clientdynrh2levImpl();
			}
			Client client = new Client(uri, clientimpl);
			Date time = Calendar.getInstance().getTime();
			System.out.println(
					"Test of dynamic" + clientimpl.getImplName() + " using synthetic testset startet at " + time);

			try {
				System.out.println("\n create user");
				client.createUser(new UserAuthRequest(username, "notsecurepassword"));

			} catch (Exception e) {
				System.out.println("user exists!! \n");
			}
			
			// write header to setupoutputfile testing difference in initialization overhead
			// for one word
			// the file is the same for all tested implementations
			String setupHeaderString = new StringBuilder().append("implname").append(";").append("size").append(";")
					.append("length").append(";").append("setuptime \n").toString();
			FileIO.appendStringToFile(setupHeaderString, reportPath.concat("dynamicsyntheticsetuptest"));

			
		
			// loop for defined update size
			for (int iter=0; iter<sizearray.length;iter++) {
				int size =sizearray[iter];
				// write header to update/search outputfile for specific test implementation
				String searchHeaderString = new StringBuilder().append("documentmatches").append(";").append("records").append(";")
						.append("mmsum").append(";").append("updatetime").append(";").append("searchtime\n").toString();
				FileIO.appendStringToFile(searchHeaderString,
						reportPath.concat(clientimpl.getImplName() + "dynamicsyntheticsearchtest"));

				time = Calendar.getInstance().getTime();
				// do the auth routine
				System.out.println(size + " dynamic testset of synthetic testset startet at " + time);
				System.out.println("Authenticate user");
				client.authenticateUser(new UserAuthRequest(username, "notsecurepassword"));
				System.out.println("Check authentication and backend communication: ");
				System.out.println(client.checkAuthBackend());
				// setup with the setup multimap file containing just one word
				String setupPath = pathSynthetic + "setup";
				byte[] setupdata = FileIO.readBytesFromFile(setupPath);
				long setuplength = FileIO.getFileSize(setupPath);
				@SuppressWarnings("unchecked")
				Multimap<String, String> setupmap = (Multimap<String, String>) Serializer.deserialize(setupdata);
				long start1 = System.currentTimeMillis();
				RepositoryInfo repo = client.setup("testpwd", setupmap, size);
				long setuptime = System.currentTimeMillis() - start1;
				System.out.println("Repository has been initialized: " + repo.getrepositoryName());
				System.out.println("Setup time: " + setuptime);

				// write output to setupoutputfile testing difference in initialization overhead
				// for one word
				String setupTimeString = new StringBuilder().append(clientimpl.getImplName()).append(";").append(size)
						.append(";").append(setuplength).append(";").append(setuptime).append("\n").toString();
				FileIO.appendStringToFile(setupTimeString, reportPath.concat("dynamicsyntheticsetuptest"));

				// initialize variables outside update loop
				long sumlength = 0;
				long records = 0;

				// do all updates in a loop
				for (int count = 0; count < test[iter]; count++) {

					// read in update multimap from file
					String filePath = pathSynthetic + count + partOfFileName + size;
					byte[] data = FileIO.readBytesFromFile(filePath);
					System.out.println(count + " update of size " + size + " documents per keyword");
					long length = FileIO.getFileSize(filePath);
					sumlength = sumlength + length;
					System.out.println("Size of plain inverted index: " + length);

					// test deserialize with object mapper
					@SuppressWarnings("unchecked")
					Multimap<String, String> map = (Multimap<String, String>) Serializer.deserialize(data);
					int resultsize = 0;
					try {
						start1 = System.currentTimeMillis();
						client.update(repo.getrepositoryName(), map, username);
						long updatetime = System.currentTimeMillis() - start1;
						records = records + map.size();
						System.out.println("Update time: " + updatetime);

						// initialie variables outside search loop
						long sum = 0L;
						long searchtime = 0L;
						long start2;

						// search loop iterates over all 100 keywords
						for (int i = 0; i < 100; i++) {
							String query = size + "keyword" + i;
							try {
								start2 = System.currentTimeMillis();
								SearchResult searchresult = client.search(repo.getrepositoryName(), query);
								searchtime = System.currentTimeMillis() - start2;

								// validate result
								//System.out.println(searchresult.getResultList());
								resultsize = searchresult.getResultList().size();
								System.out.println(i + "results " + resultsize + " search time: " + searchtime);
								sum = sum + searchtime;
							} catch (Exception e) {
								e.printStackTrace();
								System.out.println("Search failed.");
							}
						}
						// calculate average
						long averagerun = sum / 100;
						System.out.println("Average search time: " + averagerun);
						// write search time result to output testreport
						// size of resultset; total records; sum length of multimap in bytes; time for
						// update; average time for search
						String log = new StringBuilder().append(resultsize).append(";").append(records).append(";")
								.append(sumlength).append(";").append(updatetime).append(";").append(averagerun)
								.append("\n").toString();
						FileIO.appendStringToFile(log,
								reportPath.concat(clientimpl.getImplName() + "dynamicsyntheticsearchtest"));

					} catch (Exception e) {
						System.out.println("Setup failed.");
					}
				}

				System.out.println("Delete repository");
				client.delete(repo.getrepositoryName());
			}

		} catch (IOException | ClassNotFoundException e) {
			// TODO Auto-generated catch block
			System.out.println("Tests failed");
			e.printStackTrace();
		}
	}

	/**
	 * dynamic real test - tests update and search of real test set at pathReal 
	 * the search is done after each update over the similar named word list
	 * 
	 * @param testcase is dynrh2lev, dynrh2levrocks, or sophos
	 * @param pathToWordLists
	 */
	public static void testReal(String testcase, String pathToWordLists) {

		try {
			if(pathToWordLists==null) {
				pathToWordLists=pathReal;
			}
			
			char[] ids = new char[] { 'f', 'h' ,'n'};
			//char[] ids = new char[] { 'a', 'b' };
			//int[] numberOfDocuments = new int[] { 35, 132 };
			int[] numberOfDocuments = new int[] { 563, 721 ,8009};
			// int[]numberOfDocuments = new int[]{35,132, 248,378,437,563,
			// 642,721,994,1116,2249,3272, 4796, 8009,18687};
			int[] updateSize = new int[] { 100 };
			(new File(reportPath)).mkdir();
			ClientScheme clientimpl;

			if (testcase.equals("dynrh2levrocks")) {
				clientimpl = (ClientScheme) new Clientdynrh2levrocksImpl();
			} else if (testcase.equals("sophos")) {
				clientimpl = (ClientScheme) new ClientSophosImpl();
			} else {
				clientimpl = (ClientScheme) new Clientdynrh2levImpl();
			}
			Client client = new Client(uri, clientimpl);
			Date time = Calendar.getInstance().getTime();
			System.out.println("Test of " + clientimpl.getImplName() + " using real testset startet at " + time);

			try {
				System.out.println("\n create user");
				client.createUser(new UserAuthRequest(username, "notsecurepassword"));
			} catch (Exception e) {
				System.out.println("user exists!! \n");
			}
			
			// loop over number of document size
			for (int amount : updateSize) {
				String pathRealPlusAmount = pathReal + amount;
				int userIterator = 0;
				// write table headers of log files for update and search file
				String logupdateheader = new StringBuilder().append("user").append(";").append("keywords").append(";")
						.append("mmspace").append(";").append("records").append(";").append("updatetime").append("\n")
						.toString();
				FileIO.appendStringToFile(logupdateheader,
						reportPath.concat(amount+clientimpl.getImplName() + "dynamicrealupdatetest"));
				String logsearchheader = new StringBuilder().append("user").append(";").append("keywords").append(";")
						.append("records").append(";").append("keyword").append(";").append("expected").append(";").append("searchtime").append(";")
						.append("result").append("\n").toString();
				FileIO.appendStringToFile(logsearchheader,
						reportPath.concat(amount+clientimpl.getImplName() + "dynamicrealsearchtest"));

				
				
				// loop over user repositories
				for (char user : ids) {

					pathRealPlusAmount = pathRealPlusAmount +"/";
					time = Calendar.getInstance().getTime();
					System.out.println(user + " testset of dynamic real testset startet at " + time);
					System.out.println("Authenticate user");
					client.authenticateUser(new UserAuthRequest(username, "notsecurepassword"));
					System.out.println("Check authentication and backend communication: ");
					System.out.println(client.checkAuthBackend());

					// do the setup with one file with the first
					// String filePathForward = pathreal + user + partOfFileNameForward;
					String filePathSetup = pathReal.concat("setup");
					byte[] data = FileIO.readBytesFromFile(filePathSetup);
					long length = FileIO.getFileSize(filePathSetup);
					System.out.println("Length of Multimap: " + length);
					// test deserialize with object mapper
					@SuppressWarnings("unchecked")
					Multimap<String, String> setupMap = (Multimap<String, String>) Serializer.deserialize(data);

					long start1 = System.currentTimeMillis();
					RepositoryInfo repo = client.setup("testpwd", setupMap, 1);
					long setuptime = System.currentTimeMillis() - start1;
					System.out.println("Repository has been initialized: " + repo.getrepositoryName());
					System.out.println("Setup done in time: " + setuptime);
					// calculate the number of updates
					int numberOfUpdates = (int) numberOfDocuments[userIterator] / amount + 1;
					System.out.println("Total number of updates: " + numberOfUpdates);

					long sumlength=0;
					long records=0;
					// loop over updates
					for (int updateIterator = 1; updateIterator  < numberOfUpdates; updateIterator++) {

						// better readable updates
						System.out.print("\n");
						for (int a = 0; a < updateIterator; a++) {
							System.out.print("*");
						}
						System.out.print("\n");
						System.out.println(updateIterator  + " update of user " + user);
						String filePathUpdate = pathRealPlusAmount + updateIterator+user + partOfFileNameInverted;
						byte[] updateData = FileIO.readBytesFromFile(filePathUpdate);
						long updateLength = FileIO.getFileSize(filePathUpdate);
						Multimap<String, String> updateMap = (Multimap<String, String>) Serializer.deserialize(updateData);
						int numberOfKeywords = updateMap.keys().elementSet().size();
						int numberOfRecords = updateMap.values().size();
						records=records+numberOfRecords;
						sumlength= sumlength + updateLength;
						System.out.println("Byte Length of Update Multimap: " +updateLength );
						System.out.println("Number of keywords: " + numberOfKeywords);
						System.out.println("Number of records: " + numberOfRecords + " total number: "+ records);
						System.out.println(repo.getrepositoryName());
					
						try {
							start1 = System.currentTimeMillis();
							client.update(repo.getrepositoryName(), updateMap,username);
							long updateTime = System.currentTimeMillis() - start1;
							System.out.println("Update time: " + updateTime);
					

							// write to update testreport - to <user>dynamicrealupdatetest
							// number of documents was determined by shell commands
							// user;number of keywords ; length of multimap in bytes;
							// number
							// of multimap values; time for setup
							
							String log = new StringBuilder().append(updateIterator).append("_").append(user).append("_user").append(";").append(numberOfKeywords)
									.append(";").append(sumlength).append(";").append(records).append(";")
									.append(updateTime).append("\n").toString();
							FileIO.appendStringToFile(log,
									reportPath.concat(amount+clientimpl.getImplName() + "dynamicrealupdatetest"));
						

							String[] words = FileIO.readStringFromFile(pathRealPlusAmount +updateIterator+ user + "_wordlist")
									.split("\n");
						
							SearchResult searchresult;
							long searchtime;
							
							//loop over keywords to search
							for (String keyword : words) {
								String[] split =keyword.split(";");
								try {
									long start2 = System.currentTimeMillis();
									searchresult = client.search(repo.getrepositoryName(), split[0]);
									searchtime = System.currentTimeMillis() - start2;
								} catch (Exception e) {
									System.out.println(e.getMessage());
									List<String> l = new ArrayList<String>();
									searchresult = new SearchResult(l);
									searchtime = 0;
								}
								// validate result
								int expectedresult = Integer.parseInt(split[1]);
								int searchresultsize = searchresult.getResultList().size();
								System.out.println("Expected:" + expectedresult);
								System.out.println("Result:" + searchresultsize);
								assert (searchresultsize== expectedresult);
								System.out.println(searchresultsize + " matches for " + keyword + " with search time: "
										+ searchtime);

								// write result to testreport to * realsearchtest file
								// size of resultset; multimap values; time for setup;
								// average time for search
								String logsearch = new StringBuilder().append(user + "_user").append(";")
										.append(numberOfKeywords).append(";").append(updateMap.values().size()).append(";")
										.append(keyword).append(";").append(searchtime).append(";")
										.append(searchresultsize).append("\n").toString();
								FileIO.appendStringToFile(logsearch,
										reportPath.concat(amount+clientimpl.getImplName() + "dynamicrealsearchtest"));
							}

							
						} catch (Exception e) {
							System.out.println("Update failed" + e.getMessage());
							e.printStackTrace();
						}
						
					}
					userIterator++;
					System.out.println("Delete repository");
					client.delete(repo.getrepositoryName());
				}
			}
		} catch (IOException | ClassNotFoundException e) {
			// TODO Auto-generated catch block
			System.out.println("Tests failed");
			e.printStackTrace();
		}
	}
}
