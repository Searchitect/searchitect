package searchitect.client.sophos;


import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;

import java.nio.file.Paths;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.KeyException;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.crypto.NoSuchPaddingException;

import org.apache.commons.io.FileUtils;
import org.bouncycastle.asn1.x509.SubjectPublicKeyInfo;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;


import searchitect.client.sophos.ClientSophosImpl;
import searchitect.common.clusion.TextExtractPar;
import searchitect.common.clusion.TextProc;
import searchitect.common.exception.SearchitectException;
import searchitect.common.sophos.Sophos;
import searchitect.common.sophos.scapi.FactoriesException;
import searchitect.common.sophos.view.SearchTokenSophos;
import searchitect.common.sophos.view.UploadIndexSophos;
import searchitect.common.view.Upload;
import searchitect.service.MemoryDictionary;
import searchitect.service.SearchDictionary;



@RunWith(SpringRunner.class)
public class ClientSophosImplTest {
	
	
	static String path1 = "/tmp/test/testoderso1.txt";
	static String  path2 = "/tmp/test/test2.txt";
	static String  path3 = "/tmp/test/test3.txt";
	static String upath1 = "/tmp/update/utest1.txt";
	static String  upath2 = "/tmp/update/utest2.txt";
	static String  upath3 = "/tmp/update/utest3.txt";
	static ObjectMapper mapper; 
	
	public static void createTestFiles() throws IOException {
        Files.createDirectories( Paths.get(path1).getParent());
        Files.createFile(Paths.get(path1));
        PrintWriter writer1 = new PrintWriter(path1, "UTF-8");
        writer1.println("The first line");
        writer1.println("The second line");
        writer1.close();
        Files.createFile(Paths.get(path2));
        PrintWriter writer2 = new PrintWriter(path2, "UTF-8");
        writer2.println("The first keyword1");
        writer2.println("The second keyword2");
        writer2.close();
        Files.createFile(Paths.get(path3));
        PrintWriter writer3 = new PrintWriter(path3, "UTF-8");
        writer3.println("weired keyword1");
        writer3.println("The second keyword2");
        writer3.println(" first ");
        writer3.close();
	}
	
	
	public static void createUpdateTestFiles() throws IOException {
        Files.createDirectories(Paths.get(upath1).getParent());
        Files.createFile(Paths.get(upath1));
        PrintWriter writer1 = new PrintWriter(upath1, "UTF-8");
        writer1.println("The first update line");
        writer1.println("The second update line");
        writer1.close();
        Files.createFile(Paths.get(upath2));
        PrintWriter writer2 = new PrintWriter(upath2, "UTF-8");
        writer2.println("The first update keyword1");
        writer2.println("The second update keyword2");
        writer2.close();
        Files.createFile(Paths.get(upath3));
        PrintWriter writer3 = new PrintWriter(upath3, "UTF-8");
        writer3.println("weired update keyword1");
        writer3.println("The second update keyword2");
        writer3.println(" first ");
        writer3.close();
	}
	
	public static void deleteTestFiles() throws IOException {
		FileUtils.deleteDirectory(new File("/tmp/test/"));
		FileUtils.deleteDirectory(new File("/tmp/update/"));
	}
	

    @BeforeClass 
    public static void setUpClass() throws IOException {   
    	mapper = new ObjectMapper();
        createTestFiles();
    }
	
    @AfterClass 
    public static void afterClass() throws IOException {      
        deleteTestFiles();
    }
	
	@Test
	public 	void setupSuccessTest() throws InvalidKeyException, InvalidAlgorithmParameterException, NoSuchAlgorithmException, NoSuchProviderException, NoSuchPaddingException, InvalidKeySpecException, IOException {
		String path= "/tmp/test";
		String password = "assumedsecure";
		ClientSophosImpl sophos = new ClientSophosImpl(null);
		Multimap<String,String> invertedIndex = getMultimap(path);
		Upload up = sophos.setup(password, invertedIndex, 0);
		ObjectMapper mapper = new ObjectMapper();
		String json = mapper.writeValueAsString(up);
		System.out.println(json);
		assertEquals(sophos.getImplName(),"sophos");
		assertTrue(json.contains("encodedKey"));
	}
		
	
	@Test(expected = SearchitectException.class)
	public 	void setupPasswordNullFailsTest() throws InvalidKeyException, InvalidAlgorithmParameterException, NoSuchAlgorithmException, NoSuchProviderException, NoSuchPaddingException, InvalidKeySpecException, IOException {
		String path= "/tmp/test";
		String password = null;
		ClientSophosImpl sophos = new ClientSophosImpl(null);
		Multimap<String,String> invertedIndex = getMultimap(path);
		Upload up = sophos.setup(password, invertedIndex, 0);
		assertEquals(sophos.getImplName(),"sophos");
		
	}

	@Test
	public 	void setupKeywordSearchSuccessTest() throws KeyException, NoSuchAlgorithmException, InvalidKeySpecException, FactoriesException, InvalidAlgorithmParameterException, NoSuchProviderException, NoSuchPaddingException, IOException {
		String path= "/tmp/test";
		String password = "assumedsecure";
		//initialize client
		ClientSophosImpl sophos = new ClientSophosImpl(null);
		Multimap<String,String> invertedIndex = getMultimap(path);
		Upload up = sophos.setup(password, invertedIndex, 0);
		ObjectMapper mapper = new ObjectMapper();
		String json = mapper.writeValueAsString(up);
		//update server
		UploadIndexSophos ups = mapper.readValue(json, UploadIndexSophos.class);
		SearchDictionary server = (SearchDictionary) new MemoryDictionary();
		Sophos.updateServer(server, ups.getDictionary());
		//generate search token client
		String query= sophos.search("first");
		SearchTokenSophos token =  mapper.readValue(query, SearchTokenSophos.class);
		//query at server
	    PublicKey pubKey = BouncyCastleProvider.getPublicKey(SubjectPublicKeyInfo.getInstance(ups.getEncodedKey()));
    	List<String> response = Sophos.query(token, pubKey, server);
    	System.out.println("Result: "+ response);
		assertTrue(query.contains("searchToken"));
	}
	
	
	@Test
	public 	void setupSearchSuccessTest() throws InvalidAlgorithmParameterException, NoSuchAlgorithmException, NoSuchProviderException, NoSuchPaddingException, InvalidKeySpecException, IOException, KeyException, FactoriesException {
		String path= "/tmp/test";
		String password = "assumedsecure";
		
		ClientSophosImpl sophos = new ClientSophosImpl(null);
		Multimap<String,String> invertedIndex = getMultimap(path);
		Upload up = sophos.setup(password, invertedIndex, 0);
		ObjectMapper mapper = new ObjectMapper();
		String json = mapper.writeValueAsString(up);
		System.out.println(json);
		UploadIndexSophos ups = mapper.readValue(json, UploadIndexSophos.class);
		HashMap<String,byte[]> index = ups.getDictionary();
		SearchDictionary server = (SearchDictionary) new MemoryDictionary();
		Sophos.updateServer(server, ups.getDictionary());
		//generate search token client
		String query= sophos.search("first");
		SearchTokenSophos token =  mapper.readValue(query, SearchTokenSophos.class);
		//query at server
	    PublicKey pubKey = KeyFactory.getInstance("RSA").generatePublic(new X509EncodedKeySpec(ups.getEncodedKey()));
    	List<String> response = Sophos.query(token, pubKey, server);
    	assertThat(response,hasSize(3));
		
	}
	
	private Multimap <String, String> getMultimap(String path) throws InvalidKeyException, InvalidAlgorithmParameterException, NoSuchAlgorithmException, NoSuchProviderException, NoSuchPaddingException, InvalidKeySpecException, IOException{
	ArrayList<File> fileList = new ArrayList<>();
	TextExtractPar.lp1 = ArrayListMultimap.create();
	TextExtractPar.lp2 = ArrayListMultimap.create();
	TextProc.listf(path, fileList);
	TextProc.TextProc(false, path);
	return TextExtractPar.lp1;
	}
	
	@Test
	public 	void retrieveUpdateUploadSearchSize() throws KeyException, NoSuchAlgorithmException, InvalidKeySpecException, FactoriesException, InvalidAlgorithmParameterException, NoSuchProviderException, NoSuchPaddingException, IOException {
		String path= "/tmp/test";
		String password = "assumedsecure";
		String updatePath= "/tmp/update/";
		//initialize client
		ClientSophosImpl sophos = new ClientSophosImpl(null);
		Multimap<String,String> invertedIndex = getMultimap(path);
		Upload up = sophos.setup(password, invertedIndex, 0);
		ObjectMapper mapper = new ObjectMapper();
		String json = mapper.writeValueAsString(up);
		//update server
		UploadIndexSophos ups = mapper.readValue(json, UploadIndexSophos.class);
		byte [] pkey = ups.getEncodedKey();
		System.out.println("Size of pk:" +pkey.length);
		HashMap<String,byte[]> index = ups.getDictionary();
		for (String s : index.keySet()){
			System.out.println("Size of label (UT): " + s.length());
			System.out.println("Size of value: " + index.get(s).length);
		}
		
		SearchDictionary server = (SearchDictionary) new MemoryDictionary();
		Sophos.updateServer(server, ups.getDictionary());
		//generate search token client
		String query= sophos.search("first");
		SearchTokenSophos token =  mapper.readValue(query, SearchTokenSophos.class);
		System.out.println("length of ST: "+token.getSearchToken().length);
		System.out.println("length of wkey: "+token.getWkey().length);
		System.out.println("length of int: "+ Integer.BYTES);
		//query at server
	    PublicKey pubKey = KeyFactory.getInstance("RSA").generatePublic(new X509EncodedKeySpec(ups.getEncodedKey()));
    	List<String> response = Sophos.query(token, pubKey, server);
    	//invertedIndex = getMultimap(updatePath);
    	String updateJson = sophos.update(invertedIndex);
    	UploadIndexSophos update = mapper.readValue(updateJson, UploadIndexSophos.class);
    	HashMap<String,byte[]> updateindex = update.getDictionary();
		server.putAllHashMap("dynamic", updateindex, true);
    	query = sophos.search("first");
    	token =  mapper.readValue(query, SearchTokenSophos.class);
    	response = Sophos.query(token, pubKey, server);
    	System.out.println("Result: "+ response);
		assertTrue(query.contains("searchToken"));
		assertThat(response,hasSize(6));
	}

}
