package searchitect.client.sophos;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

import java.security.KeyException;

import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.InvalidParameterSpecException;
import java.util.HashMap;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.Multimap;

import searchitect.common.client.ClientScheme;
import searchitect.common.exception.SearchitectException;
import searchitect.common.sophos.ClientStateSophos;
import searchitect.common.sophos.Sophos;
import searchitect.common.sophos.scapi.FactoriesException;
import searchitect.common.sophos.view.SearchTokenSophos;
import searchitect.common.sophos.view.UpdateIndexSophos;
import searchitect.common.sophos.view.UploadIndexSophos;
import searchitect.common.view.SearchResult;
import searchitect.common.view.Upload;

public class ClientSophosImpl implements ClientScheme {

	searchitect.common.sophos.ClientStateSophos state;

	int icount = 100000;
	int keysizesym = 256;
	int keysizeasym = 2048;
	String implName = "sophos";
	ObjectMapper mapper;


	ClientSophosImpl(ClientStateSophos state) {
		this.mapper = new ObjectMapper();
		if (state == null) {
			
		} else {
			this.state = state;
		}
	}

	public ClientSophosImpl() {
		this.mapper = new ObjectMapper();
	}

	public String getImplName() {
		return implName;
	}

	public String getState() {
		return state.getImplName();
	}
	
	public void setState(ClientStateSophos state) {
		this.state = state;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see searchitect.scheme.IClientScheme#setup(java.lang.String)
	 */
	public Upload setup(String password, Multimap<String,String>invertedIndex,int numberOfDocuments) throws SearchitectException {

		try {
			this.state = Sophos.setup(keysizesym, keysizeasym, password, icount);
			HashMap<String, byte[]>tokenUp = Sophos.updateClient("add", invertedIndex, state);		
			return new UploadIndexSophos(this.state.getAsymmetricKeys().getPublic().getEncoded(),tokenUp);
		} catch (InvalidKeySpecException | NoSuchAlgorithmException | NoSuchProviderException
				| InvalidParameterSpecException | FactoriesException | IOException | KeyException | NullPointerException e) {
			throw new SearchitectException (e.getMessage());
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see searchitect.scheme.IClientScheme#search(java.lang.String)
	 */
	public String search(String query) throws SearchitectException {
		try {
			// generate search token
			SearchTokenSophos token = Sophos.token(query, state);
			String tokenJsonString = mapper.writeValueAsString(token);
			return tokenJsonString;
		} catch (UnsupportedEncodingException | KeyException | FactoriesException | JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println("Sophos Token generation failed");
			throw new SearchitectException(e.getMessage());
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see searchitect.scheme.IClientScheme#update(java.lang.String)
	 */
	public String update(Multimap<String,String>invertedIndex) throws SearchitectException {
		try {
			HashMap<String, byte[]>tokenUp = Sophos.updateClient("add", invertedIndex, state);
			// save update state;
			String updateJsonString = mapper.writeValueAsString(new UpdateIndexSophos(tokenUp));
			return updateJsonString;
		} catch ( IOException | KeyException | FactoriesException e) {
			e.printStackTrace();
			System.out.println("Sophos Update  failed" + e.getMessage());
			throw new SearchitectException(e.getMessage());
		}
	}


	/**
	 * This method is not used by Sophos, because Sophos is resource revealing
	 */
	public SearchResult resolve(SearchResult encrypted) throws SearchitectException {
		return encrypted;

	}



}
