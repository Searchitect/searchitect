# Searchitect-Client-Sophos-Plugin

### Functionality
 * Client part of Sophos Searchable Encryption scheme

### Compile using

	mvn clean install

### Packages
* searchitect.client.sophos
	* ClientSophosImpl.java - implements the ClientScheme interface and all methods