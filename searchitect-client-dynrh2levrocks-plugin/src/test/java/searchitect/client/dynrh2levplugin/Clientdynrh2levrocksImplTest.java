package searchitect.client.dynrh2levplugin;

import static org.junit.Assert.*;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.spec.InvalidKeySpecException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


import javax.crypto.NoSuchPaddingException;

import org.apache.commons.io.FileUtils;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.Multimap;

import searchitect.clusion.CryptoPrimitivesModified;
import searchitect.clusion.DynRH2LevModifiedRocks;
import searchitect.common.clusion.TextExtractPar;
import searchitect.common.clusion.TextProc;
import searchitect.common.exception.SearchitectException;
import searchitect.common.view.Upload;
import searchitect.common.viewdynrh2lev.SearchTokendynrh2lev;
import searchitect.common.viewdynrh2lev.UploadIndexdynrh2levMap;
import searchitect.service.MemoryDictionary;
import searchitect.service.SearchDictionary;

@RunWith(SpringRunner.class)
public class Clientdynrh2levrocksImplTest {
	
	
	static String path1 = "/tmp/test/test1.txt";
	static String  path2 = "/tmp/test/test2.txt";
	static String  path3 = "/tmp/test/test3.txt";
	static String upath1 = "/tmp/update/utest1.txt";
	static String  upath2 = "/tmp/update/utest2.txt";
	static String  upath3 = "/tmp/update/utest3.txt";

	
	public static void createTestFiles() throws IOException {
        Files.createDirectories( Paths.get(path1).getParent());
        Files.createFile(Paths.get(path1));
        PrintWriter writer1 = new PrintWriter(path1, "UTF-8");
        writer1.println("The first line");
        writer1.println("The second line");
        writer1.close();
        Files.createFile(Paths.get(path2));
        PrintWriter writer2 = new PrintWriter(path2, "UTF-8");
        writer2.println("The first keyword1");
        writer2.println("The second keyword2");
        writer2.close();
        Files.createFile(Paths.get(path3));
        PrintWriter writer3 = new PrintWriter(path3, "UTF-8");
        writer3.println("weired keyword1");
        writer3.println("The second keyword2");
        writer3.println(" first ");
        writer3.close();
	}
	
	
	public static void createUpdateTestFiles() throws IOException {
        Files.createDirectories(Paths.get(upath1).getParent());
        Files.createFile(Paths.get(upath1));
        PrintWriter writer1 = new PrintWriter(upath1, "UTF-8");
        writer1.println("The first update line");
        writer1.println("The second update line");
        writer1.close();
        Files.createFile(Paths.get(upath2));
        PrintWriter writer2 = new PrintWriter(upath2, "UTF-8");
        writer2.println("The first update keyword1");
        writer2.println("The second update keyword2");
        writer2.close();
        Files.createFile(Paths.get(upath3));
        PrintWriter writer3 = new PrintWriter(upath3, "UTF-8");
        writer3.println("weired update keyword1");
        writer3.println("The second update keyword2");
        writer3.println(" first ");
        writer3.close();
	}
	
	public static void deleteTestFiles() throws IOException {
		FileUtils.deleteDirectory(new File("/tmp/test/"));
		FileUtils.deleteDirectory(new File("/tmp/update/"));
	}
	

    @BeforeClass 
    public static void setUpClass() throws IOException {      
        createTestFiles();
    }
	
    @AfterClass 
    public static void afterClass() throws IOException {      
        deleteTestFiles();
    }
    
	@Test
	public 	void Clientdynrh2levImplConstructorNullTest() {
		Clientdynrh2levrocksImpl dynrh2lev = new Clientdynrh2levrocksImpl();
		assertEquals("dynrh2levrocks",dynrh2lev.getImplName());
		
	}
	
	@Test
	public 	void Clientdynrh2levImplConstructorStateTest() {
		String repositoryName = "testname";
		byte[] sk = "assumedsecret".getBytes();
		HashMap<String, Integer> stateCounter = new HashMap<String, Integer>();
		stateCounter.put("keyword1", 1);
		stateCounter.put("keyword2",5);
		Clientdynrh2levrocksImpl dynrh2lev = new Clientdynrh2levrocksImpl(new ClientStatedynrh2levrocks(repositoryName,sk,stateCounter));
		assertEquals("dynrh2levrocks",dynrh2lev.getImplName());
	}
	
	@Test
	public 	void setupSuccessTest() throws InvalidKeyException, InvalidAlgorithmParameterException, NoSuchAlgorithmException, NoSuchProviderException, NoSuchPaddingException, InvalidKeySpecException, IOException {
		String path= "/tmp/test";
		String password = "assumedsecure";
		Clientdynrh2levrocksImpl dynrh2lev = new Clientdynrh2levrocksImpl();
		ArrayList<File> fileList = new ArrayList<>();
		TextProc.listf(path, fileList);
		TextProc.TextProc(false, path);
		Multimap<String,String> invertedIndex =TextExtractPar.lp1;
		System.out.println("Index:" +invertedIndex);
		Upload up = dynrh2lev.setup( password, invertedIndex, TextExtractPar.lp2.size());
		ObjectMapper mapper = new ObjectMapper();
		String json = mapper.writeValueAsString(up);
		System.out.println(json);
		assertEquals("dynrh2levrocks",dynrh2lev.getImplName());
		assertTrue(json.contains("dictionary"));
	}
	
	@Test(expected = SearchitectException.class)
	public 	void setupMultimapNullFailsTest() throws InvalidKeyException, InvalidAlgorithmParameterException, NoSuchAlgorithmException, NoSuchProviderException, NoSuchPaddingException, InvalidKeySpecException, IOException {
		Clientdynrh2levrocksImpl dynrh2lev = new Clientdynrh2levrocksImpl();
		String password = "assumedsecure";
		Multimap<String,String> invertedIndex =null;
		Upload up  = dynrh2lev.setup( password, invertedIndex, TextExtractPar.lp2.size());
		assertEquals(dynrh2lev.getImplName(),"dynrh2lev");
	
	}	
	
	@Test(expected = SearchitectException.class)
	public 	void setupPasswordNullFailsTest() throws InvalidKeyException, InvalidAlgorithmParameterException, NoSuchAlgorithmException, NoSuchProviderException, NoSuchPaddingException, InvalidKeySpecException, IOException {
		String path= "/tmp/test";
		String password = null;
		Clientdynrh2levrocksImpl dynrh2lev = new Clientdynrh2levrocksImpl();
		ArrayList<File> fileList = new ArrayList<>();
		TextProc.listf(path, fileList);
		TextProc.TextProc(false, path);
		Multimap<String,String> invertedIndex =TextExtractPar.lp1;
		Upload up = dynrh2lev.setup( password, invertedIndex, TextExtractPar.lp2.size());
		assertEquals(dynrh2lev.getImplName(),"dynrh2lev");
	}

	@Test
	public 	void setupKeywordSearchSuccessTest() throws InvalidKeyException, InvalidAlgorithmParameterException, NoSuchAlgorithmException, NoSuchProviderException, NoSuchPaddingException, IOException, InvalidKeySpecException {
		String path= "/tmp/test";
		String password = "assumedsecure";
		
		Clientdynrh2levrocksImpl dynrh2lev = new Clientdynrh2levrocksImpl();
		ArrayList<File> fileList = new ArrayList<>();
		TextProc.listf(path, fileList);
		TextProc.TextProc(false, path);
		Multimap<String,String> invertedIndex =TextExtractPar.lp1;
		Upload up  = dynrh2lev.setup( password, invertedIndex, TextExtractPar.lp2.size());
		ObjectMapper mapper = new ObjectMapper();
		String json = mapper.writeValueAsString(up);
		UploadIndexdynrh2levMap ups = mapper.readValue(json, UploadIndexdynrh2levMap.class);
		String query= dynrh2lev.search("first");
		HashMap<String, byte[]> stateCounter = new HashMap<String, byte[]>();
		SearchTokendynrh2lev token =  mapper.readValue(query, SearchTokendynrh2lev.class);
		HashMap<String, byte[]> map = ups.getDictionary();
		SearchDictionary memo = new MemoryDictionary();
		memo.putAllHashMap("static", map, true);
    	System.out.println(token.getSearchToken().toString());
    	List<String> response = DynRH2LevModifiedRocks.queryFS(token.getSearchToken(), memo);
    	byte [] key = CryptoPrimitivesModified.generateCmac(dynrh2lev.state.sk, 3 + new String());
    	 List<String> result =  DynRH2LevModifiedRocks.resolve(key, response);
    	System.out.println("Result: "+ result);
		assertTrue(query.contains("searchToken"));
	}
	
	
	@Test
	public 	void setupUpdateSearchSuccessTest() throws InvalidKeyException, InvalidAlgorithmParameterException, NoSuchAlgorithmException, NoSuchProviderException, NoSuchPaddingException, InvalidKeySpecException, IOException {
		String path= "/tmp/test";
		String password = "assumedsecure";
		
		Clientdynrh2levrocksImpl dynrh2lev = new Clientdynrh2levrocksImpl();
		ArrayList<File> fileList = new ArrayList<>();
		TextProc.listf(path, fileList);
		TextProc.TextProc(false, path);
		Multimap<String,String> invertedIndex =TextExtractPar.lp1;
		dynrh2lev.setup( password, invertedIndex, TextExtractPar.lp2.size());
	
		
	}

}
