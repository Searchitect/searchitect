# Searchitect-Client-DynRH2LevRocks-Plugin

### Functionality
 * Client part of DynRH2LevRocks Searchable Encryption scheme

### Compile using

	mvn clean install
	
	
### Packages
* searchitect.client.sophos
	* Clientdynrh2levrocksImpl.java - implements the ClientScheme interface and all methods
	* ClientSatedynrh2levrocks.java - maintains the client state