package searchitect.service;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import searchitect.model.IndexImplRocks;

/**
 * IndexRepository - interface to the persisted index repositories
 *
 */
public interface IndexRepository extends JpaRepository<IndexImplRocks, Long> {

	/**
	 * findByRepositoryName searches for the repository of the index
	 * 
	 * @param repositoryName
	 * @return IndexImpl or an empty Optional
	 */
	Optional<IndexImplRocks> findByRepositoryName(String repositoryName);

}
