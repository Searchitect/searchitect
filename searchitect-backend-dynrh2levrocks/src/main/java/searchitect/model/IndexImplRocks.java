package searchitect.model;

import java.io.File;
import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;

import java.util.List;
import java.util.UUID;

import javax.crypto.NoSuchPaddingException;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;

import searchitect.service.RocksDbWrapperModified;
import searchitect.clusion.DynRH2LevModifiedRocks;
import searchitect.common.view.SearchResult;
import searchitect.common.viewdynrh2lev.SearchTokendynrh2lev;
import searchitect.common.viewdynrh2lev.UpdateIndexdynrh2lev;
import searchitect.common.viewdynrh2lev.UploadIndexdynrh2levMap;

/**
 * 
 * IndexImplRocks holds a repositoryname and a path
 *
 */
@Entity
public class IndexImplRocks {

	@Id
	@GeneratedValue
	private Long id;

	private String repositoryName;
	private String path ="/tmp/dynrh2levrocksb/";
	
	private static String[] columnFamilies = { "static", "dynamic", "array" };


	protected IndexImplRocks() { // jpa only
	}

	/**
	 * Get the name of the repository
	 * 
	 * @return the name of the repository
	 */
	public String getRepositoryName() {
		return repositoryName;
	}

	/**
	 * Get the rocksDB path for this repository
	 * 
	 * @return path 
	 */
	public String getRocksDBPath() {
		return path;
	}


	/**
	 * Constructor for IndexImplRocksgeneration
	 * 
	 * @param uploadIndex
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	public IndexImplRocks(UploadIndexdynrh2levMap uploadIndex)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		// test if index is valid multimap
		this.repositoryName = UUID.randomUUID().toString();
		this.path = path.concat(this.repositoryName);
		RocksDbWrapperModified rockwrap = RocksDbWrapperModified.openReadWrite(path, columnFamilies);
		rockwrap.putAllHashMap("static", uploadIndex.getDictionary(),true);
		if(uploadIndex.getArray()!= null) {
		rockwrap.putAllArray("array", uploadIndex.getArray());
		}
		rockwrap.close();
	}

	/**
	 * update - the update hashmap using UpdateIndexdynrh2lev which contains a
	 * navigableMap <String, Collection <byte[]>>
	 * 
	 * @param updateIndex
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	public void update(UpdateIndexdynrh2lev updateIndex)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {	
		RocksDbWrapperModified rockwrap = RocksDbWrapperModified.openReadWrite(path, columnFamilies);
		rockwrap.putAllMapCollection("dynamic", updateIndex.getDictionary());
		rockwrap.close();
	}

	/**
	 * search - uses Searchtokendynrh2lev array to search for
	 * 
	 * @param token
	 * @return
	 * @throws InvalidKeyException
	 * @throws InvalidAlgorithmParameterException
	 * @throws NoSuchAlgorithmException
	 * @throws NoSuchProviderException
	 * @throws NoSuchPaddingException
	 * @throws IOException
	 * @throws NullPointerException
	 */
	public SearchResult search(SearchTokendynrh2lev token)
			throws InvalidKeyException, InvalidAlgorithmParameterException, NoSuchAlgorithmException,
			NoSuchProviderException, NoSuchPaddingException, IOException, NullPointerException {
		RocksDbWrapperModified rockwrap = RocksDbWrapperModified.openReadOnly(path);
		List <String> resultList;
		if (token.getSearchToken().length<3){
		resultList = DynRH2LevModifiedRocks.query(token.getSearchToken(), rockwrap);
		}
		else{
		resultList = DynRH2LevModifiedRocks.queryFS(token.getSearchToken(), rockwrap);
		}
		rockwrap.close();
		return new SearchResult(resultList);

	}
	
	/**
	 * delete rocksdb
	 * 
	 * @throws IOException
	 */
	public void delete() throws IOException{
		FileUtils.deleteDirectory(new File(path));

	}

}
