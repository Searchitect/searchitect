package searchitect.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.spec.InvalidKeySpecException;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.concurrent.ExecutionException;

import javax.crypto.NoSuchPaddingException;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.core.Is.is;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;

import searchitect.Application;
import searchitect.TestUtil;
import searchitect.model.*;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
public class IndexRepositoryTest {

	@Autowired
	private IndexRepository indexRepo;
	
	@BeforeClass
	public static void setup() throws InvalidKeyException, InvalidKeySpecException, NoSuchAlgorithmException, NoSuchProviderException, InvalidAlgorithmParameterException, NoSuchPaddingException, IOException, InterruptedException, ExecutionException{
		TestUtil.initialize("/tmp/indexrepo/");
	}
	
	@After
	public void deleteRepos(){
		indexRepo.deleteAllInBatch();
	}




	@Test
	public void findByRepositoryNameTestCorrectName() throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		IndexImplRocks indexExpected = new IndexImplRocks(TestUtil.getUploadIndex());
		String repositoryName = indexExpected.getRepositoryName();
		indexRepo.saveAndFlush(indexExpected);
		assertEquals(indexExpected.getRepositoryName(), indexRepo.findByRepositoryName(repositoryName).get().getRepositoryName());
	}
	
	@Test()
	public void findByRepositoryNameTestWrongName()
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		IndexImplRocks indexExpected = new IndexImplRocks(TestUtil.getUploadIndex());
		String wrongRepositoryName = "wrongName";
		indexRepo.saveAndFlush(indexExpected);
		assertEquals(false, indexRepo.findByRepositoryName(wrongRepositoryName).isPresent());
	
	}
	
	@Test(expected = NoSuchElementException.class)
	public void findByRepositoryNameTestWrongNameGetThrowsException()
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		IndexImplRocks indexExpected = new IndexImplRocks(TestUtil.getUploadIndex());
		String wrongRepositoryName = "wrongName";
		indexRepo.saveAndFlush(indexExpected);
		indexRepo.findByRepositoryName(wrongRepositoryName).get();
	}
	
	@Test()
	public void findByRepositoryNameTestNullName()
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		IndexImplRocks indexExpected = new IndexImplRocks(TestUtil.getUploadIndex());
		String nullRepositoryName = null;
		indexRepo.saveAndFlush(indexExpected);
		assertEquals(false, indexRepo.findByRepositoryName(nullRepositoryName).isPresent());
	}
	
	@Test()
	public void findByRepositoryNameTestEmptyName()
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		IndexImplRocks indexExpected = new IndexImplRocks(TestUtil.getUploadIndex());
		String emptyRepositoryName = "";
		indexRepo.saveAndFlush(indexExpected);
		assertEquals(false, indexRepo.findByRepositoryName(emptyRepositoryName).isPresent());
	}
	
	@Test(expected = NoSuchElementException.class)
	public void deleteRepositoryAndSearchForIt()
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		IndexImplRocks indexExpected = new IndexImplRocks(TestUtil.getUploadIndex());
		indexRepo.saveAndFlush(indexExpected);
		indexRepo.delete(indexRepo.findByRepositoryName(indexExpected.getRepositoryName()).get());
		indexRepo.findByRepositoryName(indexExpected.getRepositoryName()).get();
	}
	
	@Test
	public void deleteRepositoryNotSavedIndexImpl()
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		indexRepo.delete(new IndexImplRocks(TestUtil.getUploadIndex()));
	}
	
	@Test
	public void getAllRepositoriesNoSavedIndexImpl() {
		List <IndexImplRocks> indexList = indexRepo.findAll();
		assertThat(indexList.isEmpty(), is(true));
	}
	
	@Test
	public void getAllRepositoriesTwoSavedIndexImpl() 
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		IndexImplRocks firstIndex = new IndexImplRocks(TestUtil.getUploadIndex());
		indexRepo.saveAndFlush(firstIndex);
		IndexImplRocks secondIndex = new IndexImplRocks(TestUtil.getUploadIndex());
		indexRepo.saveAndFlush(secondIndex);
		List <IndexImplRocks> indexList = indexRepo.findAll();
		assertThat(indexList.size(), is(2));
	}

}
