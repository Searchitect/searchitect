package searchitect.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.not;

import java.io.IOException;
import java.nio.charset.Charset;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.spec.InvalidKeySpecException;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutionException;

import javax.crypto.NoSuchPaddingException;

import org.hamcrest.Matchers;
import org.hamcrest.collection.IsEmptyCollection;
import org.junit.After;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.mock.http.MockHttpOutputMessage;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;

import searchitect.Application;
import searchitect.TestUtil;
import searchitect.clusion.DynRH2LevModifiedRocks;
import searchitect.common.crypto.CryptoPrimitives;
import searchitect.common.view.RepositoryInfo;
import searchitect.common.view.SearchResult;


@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
@AutoConfigureMockMvc
public class BackendControllerTest {

	private MediaType contentType = new MediaType(MediaType.APPLICATION_JSON.getType(),
			MediaType.APPLICATION_JSON.getSubtype(), Charset.forName("utf8"));

	byte[][] token;
	String word = "keyword1";

	String emptyJsonString = "";
	String nullJsonString = null;
	String jsonToken;
	String notExistingJsonToken;
	String wrongJsonToken;
	static ObjectMapper mapper;

	@Autowired
	private MockMvc mockMvc;

	@SuppressWarnings("rawtypes")
	private HttpMessageConverter mappingJackson2HttpMessageConverter;

	@Autowired
	private WebApplicationContext webApplicationContext;

	@Autowired
	ApplicationContext applicationContext;

	@Autowired
	void setConverters(HttpMessageConverter<?>[] converters) {

		this.mappingJackson2HttpMessageConverter = Arrays.asList(converters).stream()
				.filter(hmc -> hmc instanceof MappingJackson2HttpMessageConverter).findAny().orElse(null);

		assertNotNull("the JSON message converter must not be null", this.mappingJackson2HttpMessageConverter);
	}

	@BeforeClass
	public static void setUpClass() throws IOException, InvalidKeySpecException, NoSuchAlgorithmException,
			NoSuchProviderException, InterruptedException, ExecutionException, InvalidKeyException,
			NoSuchPaddingException, InvalidAlgorithmParameterException, NullPointerException{
		TestUtil.initialize("/tmp/backendc/");
		mapper = new ObjectMapper();
	}

	

	@Before
	public void set() throws IOException, InvalidKeyException, InvalidKeySpecException, NoSuchAlgorithmException, NoSuchProviderException, InvalidAlgorithmParameterException, NoSuchPaddingException, InterruptedException, ExecutionException {
		this.mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
	


	}

	@After
	public void deleteAll() throws Exception {
			this.mockMvc.perform(delete("/repositories"));
	}

	public String createRepository() throws Exception {
		MvcResult result = (MvcResult) mockMvc
				.perform(post("/repository").contentType(contentType)
						.content(TestUtil.setupIndexJson))
				.andExpect(status().isOk()).andReturn();
		String json = result.getResponse().getContentAsString();
		Gson gson = new Gson();
		RepositoryInfo repi = gson.fromJson(json, RepositoryInfo.class);
		return repi.getrepositoryName();

	}

	public String getSearchQuery(String repositoryName) {
		return new StringBuilder().append("/repository/").append(repositoryName).append("/search").toString();
	}

	@Test
	public void setupWithNormalJsonStringSuccessTest() throws Exception {
		// test with correct json String of multimap - positiv expectation: 200
		// OK
		String json = TestUtil.setupIndexJson;
		mockMvc.perform(post("/repository").contentType(contentType).content(json).accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$").value(org.hamcrest.Matchers.hasKey("repositoryName"))).andReturn();
	}

	@Test
	public void setupWithWrongJsonStringFailsTest() throws Exception {
		// test with incorrect json String of multimap - negativ expectation:
		// 400 BAD REQUEST
		mockMvc.perform(post("/repository").contentType(contentType)
				.content(TestUtil.setupIndexJson.substring(5)).accept(MediaType.APPLICATION_JSON))
				.andExpect(status().is(400)).andReturn();
	}

	@Test
	public void setupWithEmptyJsonStringFailsTest() throws Exception {
		// test with incorrect json String of multimap - negativ expectation:
		// 400 BAD
		// REQUEST
		mockMvc.perform(post("/repository").contentType(contentType).content(emptyJsonString)
				.accept(MediaType.APPLICATION_JSON)).andExpect(status().is(400)).andReturn();
	}

	@Test
	public void deleteTest() throws Exception {
		// test delete existing repository - positiv expectation: 200 OK
		String repositoryName = createRepository();
		String query = new StringBuilder().append("/repository/").append(repositoryName).toString();
		mockMvc.perform(delete(query)).andExpect(status().isOk());

		// test delete not existing repository - negativ expectation: 404 NOT
		// FOUND
		mockMvc.perform(delete(query)).andExpect(status().is(404));
	}

	@Test
	public void listRepositoriesWithTwoInsertedRepositoriesSuccessTest() throws Exception {
		// create two repositories - positiv expectation: 200 OK and print
		// response
		createRepository();
		createRepository();
		// test list those 2 repositories - positiv expectation: 200 OK and 2
		// matches
		MvcResult result = mockMvc.perform(get("/repositories").accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk()).andExpect(jsonPath("$", Matchers.hasSize(2))).andReturn();
		System.out.println(result.getResponse().getContentAsString());
	}

	@Test
	public void listRepositoriesWithNoInsertedRepositoriesFailsTest() throws Exception {
		// test list no repositories - negative expectation: 404 NOT FOUND
		mockMvc.perform(get("/repositories").accept(MediaType.APPLICATION_JSON)).andExpect(status().is(404))
				.andReturn();
	}

	@Test
	public void searchWithNormalJsonStringSuccessTest() throws Exception {
		// create repository
		String repositoryName = createRepository();
		String query = getSearchQuery(repositoryName);
		// test existing token - positiv expectation: 200 OK and 3 matches in
		// resultList
		// and print response

		MvcResult mv = mockMvc.perform(post(query).contentType(contentType)
				.content(TestUtil.jsonSearchToken1).accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk()).andReturn();
		System.out.println(mv.getResponse().getContentAsString());
		SearchResult res = mapper.readValue(mv.getResponse().getContentAsString(), SearchResult.class);
		assertThat(res.getResultList(), hasSize(3));
		List<String> resultList = DynRH2LevModifiedRocks.resolve(CryptoPrimitives.generateCmac(TestUtil.key, 3 + new String()),
				res.getResultList());
		System.out.println(resultList);

	}

	@Test
	public void searchWithWrongFormattedJsonTokenFailsTest() throws Exception{
		// test wrong formatted token - negative expectation: 400 BAD REQUEST
		
			String repositoryName = createRepository();
			String query = getSearchQuery(repositoryName);
			mockMvc.perform(post(query).contentType(contentType)
					.content(TestUtil.jsonSearchToken1.substring(2))
					.accept(MediaType.APPLICATION_JSON)).andExpect(status().is(400)).andReturn();
		
	}

	@Test
	public void searchNotExistingTokenFailsTest() throws Exception {
		// test not existing token - negativ expectation: 200 OK and matches in
		// resultList are 0
		String repositoryName = createRepository();
		String query = getSearchQuery(repositoryName);
		System.out.println(query);
		System.out.println(TestUtil.jsonNotExistingSearchToken);
		String notexisting = TestUtil.notExistingJsonToken;
		mockMvc.perform(post(query).contentType(contentType)
				.content(notexisting )
				.accept(MediaType.APPLICATION_JSON)).andExpect(status().is(200))
				.andReturn();
		//.andExpect(jsonPath("$.resultList", Matchers.hasSize(0)))
	}

	@Test
	public void searchWrongMediaTypeNotExistingTokenFailsTest() throws Exception {
		// test wrong MediaType for content - negative expectation 415
		// Unsupported Media Type
		String repositoryName = createRepository();
		String query = getSearchQuery(repositoryName);
		mockMvc.perform(post(query).contentType(MediaType.TEXT_PLAIN)
				.content(TestUtil.jsonNotExistingSearchToken)
				.accept(MediaType.APPLICATION_JSON)).andExpect(status().is(415)).andReturn();
	}

	@Test
	public void searchWrongMediaTypeNotExistingRepositoryFailsTest() throws Exception {
		// test not existing repository - negativ expectation: 404 NOT FOUND
		String query = getSearchQuery("notexisting");
		mockMvc.perform(post(query).contentType(contentType)
				.content(TestUtil.jsonSearchToken1).accept(MediaType.APPLICATION_JSON))
				.andExpect(status().is(404)).andReturn();
	}

	@Test
	public void updateWithValidUpdateSuccessTest() throws Exception {
		// create repository
		String repositoryName = createRepository();
		String query = new StringBuilder().append("/repository/").append(repositoryName).toString();

		// test valid update - positive expectation: 200 OK
		mockMvc.perform(post(query).contentType(contentType)
				.content(TestUtil.updateIndexJson1).accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk()).andReturn();
		// test search resultList after update - should contain 6 matches for
		// keyword1 and print
		String searchquery = getSearchQuery(repositoryName);
		System.out.println(query);
		MvcResult result2 = mockMvc
				.perform(post(searchquery).contentType(contentType)
						.content(TestUtil.jsonSearchToken2).accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk()).andReturn();
	
		SearchResult res = mapper.readValue(result2.getResponse().getContentAsString(), SearchResult.class);
		assertThat(res.getResultList(), hasSize(5));
		List<String> resultList = DynRH2LevModifiedRocks.resolve(CryptoPrimitives.generateCmac(TestUtil.key, 3 + new String()),
				res.getResultList());
		System.out.println(resultList);
		assertThat(resultList, not(IsEmptyCollection.empty()));

	}

	@Test
	public void updateMultipleUpdatesSuccessTest() throws Exception {

		// test if multiple updates possible needed to enhance indexString
		// column size
		// in IndexImpl to max size of varchar (app. 2GB)
		String repositoryName = createRepository();
		String query = new StringBuilder().append("/repository/").append(repositoryName).toString();

		mockMvc.perform(post(query).contentType(contentType)
				.content(TestUtil.updateIndexJson1).accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk()).andReturn();
		mockMvc.perform(post(query).contentType(contentType)
				.content(TestUtil.updateIndexJson2).accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk()).andReturn();
		mockMvc.perform(post(query).contentType(contentType)
				.content(TestUtil.updateIndexJson3).accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk()).andReturn();
		mockMvc.perform(post(query).contentType(contentType)
				.content(TestUtil.updateIndexJson4).accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk()).andReturn();
		mockMvc.perform(post(query).contentType(contentType)
				.content(TestUtil.updateIndexJson5).accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk()).andReturn();
		// // test search resultList after 5 additional updates - positive
		// expectation 200
		// // OK and should contain 3 + 10 matches for keyword1 and print
		String searchquery = getSearchQuery(repositoryName);
		System.out.println("size:"+ TestUtil.jsonSearchToken3.length());
		MvcResult result3 = mockMvc
				.perform(post(searchquery).contentType(contentType)
						.content(TestUtil.jsonSearchToken3).accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk()).andReturn();
		SearchResult res = mapper.readValue(result3.getResponse().getContentAsString(), SearchResult.class);
		assertThat(res.getResultList(), hasSize(13));
		List<String> resultList = DynRH2LevModifiedRocks.resolve(CryptoPrimitives.generateCmac(TestUtil.key, 3 + new String()),
				res.getResultList());
		System.out.println(resultList);
	}

	@Test
	public void updateNotExistingRepositoryFailsTest() throws Exception {
		createRepository();
		// test update of not existing repository - negativ expectation: 404 NOT
		// FOUND
		String wrongQuery = new StringBuilder().append("/repository/").append("notexisting").toString();
		mockMvc.perform(post(wrongQuery).contentType(contentType)
				.content(TestUtil.jsonNotExistingSearchToken).accept(MediaType.APPLICATION_JSON))
				.andExpect(status().is(404)).andReturn();

	}

	@Test
	public void updateWrongFormattedUpdateFailsTest() throws Exception {
		String repositoryName = createRepository();
		String query = new StringBuilder().append("/repository/").append(repositoryName).toString();
		// test update wrong formatted json string update - negative
		// expectation: 400
		// BAD REQUEST
		String wrongJsonUpdateString = TestUtil.notExistingJsonToken;
		mockMvc.perform(
				post(query).contentType(contentType).content(wrongJsonUpdateString).accept(MediaType.APPLICATION_JSON))
				.andExpect(status().is(400)).andReturn();
		mockMvc.perform(delete("/repositories"));

	}
	
	@Test
	public void deleteSuccessTest() throws Exception {
		String repositoryName = createRepository();
		String query = new StringBuilder().append("/repository/").append(repositoryName).toString();
		mockMvc.perform(delete(query).contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON)).andExpect((status().isOk()));
		
	}


	@SuppressWarnings("unchecked")
	protected String json(Object o) throws IOException {
		MockHttpOutputMessage mockHttpOutputMessage = new MockHttpOutputMessage();
		this.mappingJackson2HttpMessageConverter.write(o, MediaType.APPLICATION_JSON, mockHttpOutputMessage);
		return mockHttpOutputMessage.getBodyAsString();
	}

}