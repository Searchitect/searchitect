package searchitect.model;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.hamcrest.Matchers.hasSize;

import java.io.File;
import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.spec.InvalidKeySpecException;
import java.util.List;
import java.util.concurrent.ExecutionException;

import javax.crypto.NoSuchPaddingException;

import org.apache.commons.io.FileUtils;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;


import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;

import searchitect.Application;
import searchitect.TestUtil;
import searchitect.clusion.DynRH2LevModifiedRocks;
import searchitect.common.view.SearchResult;
import searchitect.common.viewdynrh2lev.SearchTokendynrh2lev;
import searchitect.common.viewdynrh2lev.UpdateIndexdynrh2lev;
import searchitect.common.viewdynrh2lev.UploadIndexdynrh2levMap;
import searchitect.common.crypto.CryptoPrimitives;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
public class IndexImplRocksTest {
	

@BeforeClass
public static void setup() throws InvalidKeyException, InvalidKeySpecException, NoSuchAlgorithmException, NoSuchProviderException, InvalidAlgorithmParameterException, NoSuchPaddingException, IOException, InterruptedException, ExecutionException{
	TestUtil.initialize("/tmp/indeximplrocks/");
}

@AfterClass
public static void cleanup() throws IOException{
	FileUtils.deleteDirectory(new File("/tmp/dynrh2levrocksb/"));
}
	@Test
	public void IndexImplConstructorSuccessTest()throws Exception {
		IndexImplRocks index = new IndexImplRocks(new UploadIndexdynrh2levMap(TestUtil.dic, TestUtil.getArray()));
		assertFalse(index.getRepositoryName().isEmpty());
		String repo = index.getRepositoryName();
		assertEquals("/tmp/dynrh2levrocksb/" + repo, index.getRocksDBPath());
	
	}


	@Test(expected = NullPointerException.class)
	public void IndexImplConstructorUploadIndexNullFailsTest() throws Exception {
		new IndexImplRocks(null);
	}

	@Test(expected = NullPointerException.class)
	public void IndexImplConstructorUploadIndexDictionaryNullFailsTest() throws Exception {
		new IndexImplRocks(new UploadIndexdynrh2levMap(null, TestUtil.getArray()));
	}
	
	@Test
	public void IndexImplConstructorUploadIndexArrayNullSuccessTest() throws Exception {
		IndexImplRocks index = new IndexImplRocks(new UploadIndexdynrh2levMap(TestUtil.dic, null));
		assertFalse(index.getRepositoryName().isEmpty());
	}
	
	@Test
	public void IndexImplUpdateUploadIndexSuccessTest() throws Exception {
		IndexImplRocks index = new IndexImplRocks(new UploadIndexdynrh2levMap(TestUtil.dic,TestUtil.array));
		UpdateIndexdynrh2lev ul = new UpdateIndexdynrh2lev(TestUtil.tokenUp1.asMap());
		index.update(ul);
		assertFalse(index.getRepositoryName().isEmpty());
	}

	@Test(expected = NullPointerException.class)
	public void IndexImplUpdateUploadIndexDictionaryNullFailsTest() throws Exception {
		IndexImplRocks index = new IndexImplRocks(new UploadIndexdynrh2levMap(TestUtil.dic,TestUtil.array));
	 	index.update(new UpdateIndexdynrh2lev(null));
	}

	@Test(expected = NullPointerException.class)
	public void IndexImplUpdateUploadIndexNullFailsTest()
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		new IndexImplRocks(null);
	}

	public void IndexImplSearchNullTokenFailsTest()throws Exception {
		IndexImplRocks index = new IndexImplRocks(new UploadIndexdynrh2levMap(TestUtil.dic, TestUtil.array));
		SearchResult rs = index.search(new SearchTokendynrh2lev(null));
		DynRH2LevModifiedRocks.resolve(CryptoPrimitives.generateCmac(TestUtil.key, 3 + new String()), rs.getResultList());
	}
	
	@Test
	public void IndexImplSearchAfterSetUpSuccessTest()throws Exception {
		IndexImplRocks index = new IndexImplRocks(new UploadIndexdynrh2levMap(TestUtil.dic, TestUtil.array));
		SearchTokendynrh2lev test = new SearchTokendynrh2lev(TestUtil.searchToken1);
		SearchResult rs = index.search(test);
		System.out.println(rs.getResultList());
		List <String> resultclear = DynRH2LevModifiedRocks.resolve(CryptoPrimitives.generateCmac(TestUtil.key, 3 + new String()), rs.getResultList());
		System.out.println(resultclear);
		assertThat(resultclear,hasSize(3));
	 	assertFalse(index.getRepositoryName().isEmpty());
	}
	
	
	@Test
	public void IndexImplSearchAfterUpdateSuccessTest()throws Exception {
		IndexImplRocks index = new IndexImplRocks(new UploadIndexdynrh2levMap(TestUtil.dic, TestUtil.array));
		index.update(new UpdateIndexdynrh2lev(TestUtil.tokenUp1.asMap()));
		SearchTokendynrh2lev test = new SearchTokendynrh2lev(TestUtil.searchToken2);
		SearchResult rs = index.search(test);
		System.out.println(rs.getResultList());
		List <String> resultclear = DynRH2LevModifiedRocks.resolve(CryptoPrimitives.generateCmac(TestUtil.key, 3 + new String()), rs.getResultList());
		System.out.println(resultclear);
		assertThat(resultclear,hasSize(5));
	 	assertFalse(index.getRepositoryName().isEmpty());
	}
	
	@Test
	public void DeleteTest() throws Exception{
		IndexImplRocks index = new IndexImplRocks(new UploadIndexdynrh2levMap(TestUtil.dic, TestUtil.array));
		String path =index.getRocksDBPath();
		index.delete();
		File f =new File(path);
		assert(f.exists()==false);
	}

	

	
}
