# Searchitect-Backend-Dynrh2lev

### Functionality
 * Serverpart of a Searchable Encryption scheme dynrh2lev
 * Uses dynrh2lev implementation from the Clusion library
 * Stores index repositories

### Packages
* searchitect
  * Application.java - starts the Spring Boot application
* searchitect.controller
  * CheckController.java - just for testing purpose
  * BackendController.java - Restful Interface of backend implementation
* searchitect.model
  * Dictionary.java 
  	* wraps the Multimap <String,byte[]> index dictionary
  * IndexImpl.java
  	* Entity which gets persisted - persistent fields are a  Dictionary instance, an array and the update HashMap
  	* contains the repositoryName 
  	* uses the dynrh2lev implementation of the Clusion library for the searchable encryption methods setup, update, search
* searchitect.services
  * IndexRepository.java -  like DAO extends JPARepository
  

### Build instructions
from inside the directory searchitect-backend-dynrh2lev

	mvn clean install


### Deploy for modul test:

    docker build . -t dynrh2lev
    docker run -p 127.0.0.1:8282:8282 dynrh2lev

