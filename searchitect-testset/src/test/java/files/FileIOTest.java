package files;

import static org.junit.Assert.*;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

import org.assertj.core.util.Files;
import org.bouncycastle.util.encoders.Hex;
import org.junit.Assert;
import org.junit.Test;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;


public class FileIOTest {

	int dataSize=10;
	String path1 = "/tmp/filetests1";
	String path2 = "/tmp/filetests2";
	@Test
	public void test() throws FileNotFoundException, IOException {
		byte[][] array = new byte[dataSize][];
	    List<Integer> free = new ArrayList<Integer>();
		Multimap<String, byte[]> gamma = ArrayListMultimap.create();
		array[1]= Hex.decode("ffffffff");
		array[2]= Hex.decode("00000000");
		array[3]= Hex.decode("aaaaaaaa");
		array[4]= Hex.decode("00000000");
		
		byte[] test2 = Hex.decode("ffffffff");
	
		System.out.println("arraysize" + array.length + " x " + array[1].length);
		ObjectOutputStream objectOutputStream = new ObjectOutputStream(
                new FileOutputStream(path1));
		objectOutputStream.writeObject(array);
		objectOutputStream.flush();
		objectOutputStream.close();
	
		FileIO.writeBytesToFile(test2, "filetests2", "/tmp");
		byte[] in1 =FileIO.readBytesFromFile(path1);
		byte[] in2 =FileIO.readBytesFromFile(path2);
		System.out.println("filelength" + in1.length);
		System.out.println(Hex.toHexString(in1));
		System.out.println("filelength" + in2.length);
		System.out.println(Hex.toHexString(in2));
		Files.delete(new File(path1));
		Files.delete(new File(path2));
		Assert.assertTrue(true);
		
		
	}

}
