package generator;

import java.util.Collection;
import java.util.Map;

public class Indexed {
	
	private Map<String, Collection <String>>dictionary;

	
	public Indexed(Map<String, Collection <String>>dictionary){
		this.dictionary=dictionary;
	}
	/**
	 * @return the dictionary
	 */
	public Map<String, Collection <String>> getDictionary() {
		return dictionary;
	}

	/**
	 * @param dictionary the dictionary to set
	 */
	public void setDictionary(Map<String, Collection <String>> dictionary) {
		this.dictionary = dictionary;
	}
}
