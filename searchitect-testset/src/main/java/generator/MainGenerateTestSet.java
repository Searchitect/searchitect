package generator;

import java.io.IOException;

public class MainGenerateTestSet {
	public static void main(String args[]) throws ClassNotFoundException, IOException {
//		try {
			if (args.length > 2) {

				switch (args[0]) {
				case "-full": {

					GenerateInvertedIndex.genTestsetFullFileCollection(args[1], args[2]);
					break;
				}
				case "-batched": {
					GenerateInvertedIndex.genTestsetBatchedFileCollection(args[1], args[2],false);
					break;
				}
				case "-synthetic": {
					if (args[2].equals("json") || args[2].equals("java")) {
						GenerateInvertedIndex.genTestsetSyntheticIndex(args[1], args[2]);
						break;
					} else {
						System.out.println("Expected second parameter is json or java");
						break;
					}
				}
				
				case "-dynamicsynthetic":{
					if (args[2].equals("json") || args[2].equals("java")) {
						GenerateInvertedIndex.genTestsetDynamicSyntheticIndex(args[1], args[2]);
						break;
					} else {
						System.out.println("Expected second parameter is json or java");
						break;
					}
					
				}
				case "-dynamicbatched": {
					GenerateInvertedIndex.genTestsetBatchedFileCollection(args[1], args[2],true);
					break;
				}
				case "-ok": {
					System.out.println("perfect!");
					break;
				}
				default: {
					System.out.println("It is your choice to request a -full, -batched, -synthetic, -dynamicsynthetic ot -dynamicbatched as second parameter testset");
					break;
				}
				}
			}
			//
			else {
				System.out.println(
						"You need to enter an option [-full | -batched |-synthetic |-dynamicsynthetic|-dynamicbatched |-ok] and src and destination path. \n"
								+ "pattern for -full, -batched or -dynamicbatched: java -jar searchi... option src-path dst-path \n"
								+ "example java -jar searchitect-testset-0.0.1-SNAPSHOT.jar -full /home/nan/Downloads/maildir/allen-p/ /tmp/testset/ \n"
								+ "pattern for -synthetic -dynamicsynthetic  or : java -jar searchi... option dst-path [json|java] "
								+ "second parameter in synthetic needs to be json or java"
								+ "do not forget the slash at the end!! ");
			}

//		} catch (ClassNotFoundException | IOException e) {
//			System.out.println("Unfortunately we catched some exception: " + e.getMessage());
//		}
	}

}
