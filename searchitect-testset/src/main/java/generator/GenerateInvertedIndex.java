package generator;

import java.io.File;
import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.spec.InvalidKeySpecException;
import java.util.ArrayList;
import java.util.Calendar;

import java.util.Date;
import java.util.Iterator;

import java.util.Random;
import java.util.concurrent.ExecutionException;

import javax.crypto.NoSuchPaddingException;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;

import files.FileIO;
import files.TextExtractParModified;
import files.TextProc;
import searchitect.common.clusion.Serializer;

public class GenerateInvertedIndex {
	final static int wordListSize = 100;
	static int iterator;

	public static void genTestsetFullFileCollection(String srcPath, String dPath)
			throws ClassNotFoundException, IOException {
		String defaultDstPath = "/tmp/testsets/";
		if (srcPath.isEmpty()) {
			srcPath = "/tmp/";
		}
		if (dPath.isEmpty()) {
			dPath = defaultDstPath;
		}
		// create path if not existing
		File dir = new File(dPath);
		if (!dir.exists()) {
			dir.mkdirs();
		}
		String name = new File(srcPath).getName();
		String dstPath = dPath.concat("real/");
		// String jsonDstPath = dstPath.concat("json/");
		Date time = Calendar.getInstance().getTime();
		System.out.println("\n *******************************************\n");
		System.out.println("Creation of full " + name + " testset startet at " + time);
		ArrayList<File> fileList = new ArrayList<File>();
		// empty previous multimap
		TextExtractParModified.lp1 = ArrayListMultimap.create();
		TextExtractParModified.lp2 = ArrayListMultimap.create();
		TextProc.listf(srcPath, fileList);
		try {
			long startTime = System.currentTimeMillis();
			TextProc.TextProc(false, srcPath);
			long resultTime = System.currentTimeMillis() - startTime;
			System.out.println("Generate" + srcPath);
			System.out.println("\n Running time for inverted index construction " + resultTime + "ms");
			// test object serialization
			byte[] serializedInverted = Serializer.serialize(TextExtractParModified.lp1);
			FileIO.writeBytesToFile(serializedInverted, name.concat("inverted"), dstPath);
			// test deserialization
			byte[] data = FileIO.readBytesFromFile(dstPath.concat(name.concat("inverted")));
			// test java deserialization with object mapper
			@SuppressWarnings("unchecked")
			Multimap<String, String> map = (Multimap<String, String>) Serializer.deserialize(data);
			int keywords = map.keySet().size();
			int records = map.values().size();
			int documents = TextExtractParModified.lp2.asMap().keySet().size();
			long size = FileIO.getFileSize(dstPath.concat(name.concat("inverted")));
			System.out.println("Total number of keywords: " + keywords);
			System.out.println("Total number of values: " + records);
			System.out.println("Size of Documents: " + documents);
			System.out.println("Size of Inverted Index File: " + data.length);
			String log = new StringBuilder().append(name).append(";").append(keywords).append(";").append(documents)
					.append(";").append(records).append(";").append(size).append(";").append(resultTime).append(";")
					.append("\n").toString();
			FileIO.appendStringToFile(log, dstPath + "realtestsetlog");
			// run keyword extraction
			extractKeywords(map, documents, name, dstPath);

		} catch (InvalidKeyException | InvalidAlgorithmParameterException | NoSuchAlgorithmException
				| NoSuchProviderException | NoSuchPaddingException | InvalidKeySpecException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public static void genTestsetBatchedFileCollection(String srcPath, String dstPath, boolean dyn)
			throws ClassNotFoundException, IOException {
		File[] directories = new File(srcPath).listFiles(File::isDirectory);

		String logPath;
		// define paths for dynamic and static testresults
		if (dyn) {
			logPath = dstPath.concat("dynamicreal/");
		} else {
			logPath = dstPath.concat("real/");
			File dir = new File(logPath);
			if (!dir.exists()) {
				dir.mkdirs();
			}
			// write log header to correct path
			String log = new StringBuilder().append("name").append(";").append("keywords").append(";")
					.append("documents").append(";").append("records").append(";").append("filesize").append(";")
					.append("time").append(";").append("\n").toString();
			FileIO.appendStringToFile(log, logPath + "realtestsetlog");
		}
		

		if (directories.length == 0) {
			System.out.println("No files found in source path");
		} else {
			for (File f : directories) {
				if (dyn) {
					try {
						// create setup file
						genSetupOneWordMultimap(dstPath+"dynamicreal/");
						// create update inverted indexes
						genTestsetFullDynamicFileCollection(f.getAbsolutePath(), dstPath);
					} catch (InterruptedException | ExecutionException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				} else {
					// create setup inverted indexes
					genTestsetFullFileCollection(f.getAbsolutePath(), dstPath);
				}
			}
		}
	}

	public static void genTestsetFullDynamicFileCollection(String srcPath, String dPath)
			throws ClassNotFoundException, IOException, InterruptedException, ExecutionException {
		String defaultDstPath = "/tmp/testsets/";

		if (dPath.isEmpty()) {
			dPath = defaultDstPath;
		}
		// create path if not existing
		File dir = new File(dPath);
		if (!dir.exists()) {
			dir.mkdirs();
		}
		String name = new File(srcPath).getName();

		int[] test = new int[] { 100 };

		String dstPath = dPath.concat("dynamicreal/");
		// String jsonDstPath = dstPath.concat("json/");
		Date time = Calendar.getInstance().getTime();
		System.out.println("\n *******************************************\n");
		System.out.println("Creation of dynamic " + name + " testset startet at " + time);
		System.out.println("Generate" + srcPath);
		ArrayList<File> fileList = new ArrayList<File>();
		TextProc.listf(srcPath, fileList);

		// begin loop over sizes
		for (int i : test) {
			String dstPathDyn = dstPath + i + "/";
			File direc = new File(dstPathDyn);
			direc.mkdirs();
			String log = new StringBuilder().append("name").append(";").append("keywords").append(";")
					.append("documents").append(";").append("records").append(";").append("filesize").append(";")
					.append("time").append(";").append("\n").toString();
			FileIO.appendStringToFile(log, dstPathDyn + "realtestsetlog");
			Iterator<File> it = fileList.iterator();
			// temporary filelist for this update
			ArrayList<File> partOfFiles = new ArrayList<File>();
			// growing filelist for the keywordlist extraction
			ArrayList<File> growingOfFiles = new ArrayList<File>();
			// loop over files in size i
			int count = 0;
			while (it.hasNext()) {
				// clear the temporary file list
				partOfFiles.clear();
				for (int loop = 0; loop < i; loop++) {
					if (it.hasNext()) {
						// add i of test files to lists
						File f = it.next();
						partOfFiles.add(f);
						growingOfFiles.add(f);
					} else {
						break;
					}
				}
				System.out.println(partOfFiles);
				// Better readable count
				System.out.print("\n");
				for (int a = 0; a < count; a++) {
					System.out.print("*");
				}
				System.out.println("\n Loop count:" + count);
				count++;
				String tmpname = count + name;
				// empty previous multimaps
				TextExtractParModified.lp1 = ArrayListMultimap.create();
				TextExtractParModified.lp2 = ArrayListMultimap.create();

				long startTime = System.currentTimeMillis();
				try {
					TextProc.TextProcFileList(false, partOfFiles, srcPath);
				} catch (InvalidKeyException | InvalidAlgorithmParameterException | NoSuchAlgorithmException
						| NoSuchProviderException | NoSuchPaddingException | InvalidKeySpecException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				long resultTime = System.currentTimeMillis() - startTime;
				System.out.println("Running time for inverted index construction " + resultTime + "ms");
				// test object serialization
				byte[] serializedInverted = Serializer.serialize(TextExtractParModified.lp1);
				// write inverted index to output path
				FileIO.writeBytesToFile(serializedInverted, tmpname.concat("inverted"), dstPathDyn);
				// test deserialization
				byte[] data = FileIO.readBytesFromFile(dstPathDyn.concat(tmpname.concat("inverted")));
				// test java deserialization with object mapper
				@SuppressWarnings("unchecked")
				Multimap<String, String> map = (Multimap<String, String>) Serializer.deserialize(data);

				int keywords = map.keySet().size();
				int records = map.values().size();
				int documents = TextExtractParModified.lp2.asMap().keySet().size();

				long size = FileIO.getFileSize(dstPathDyn.concat(tmpname.concat("inverted")));
				System.out.println("Total number of keywords: " + keywords);
				System.out.println("Total number of values: " + records);
				System.out.println("Size of Documents: " + documents);
				System.out.println("Size of Inverted Index File: " + data.length);
				System.out.println("File size: " + size);
				log = new StringBuilder().append(tmpname).append(";").append(keywords).append(";").append(documents)
						.append(";").append(records).append(";").append(size).append(";").append(resultTime).append(";")
						.append("\n").toString();
				// write file log
				FileIO.appendStringToFile(log, dstPathDyn + "realtestsetlog");

				// new keywordlist collector works with growing of Files of all previous
				// uploaded updates
				// clean textextraction
				TextExtractParModified.lp1 = ArrayListMultimap.create();
				TextExtractParModified.lp2 = ArrayListMultimap.create();
				// run text extraction on growing updated database
				try {
					TextProc.TextProcFileList(false, growingOfFiles, srcPath);
				} catch (InvalidKeyException | InvalidAlgorithmParameterException | NoSuchAlgorithmException
						| NoSuchProviderException | NoSuchPaddingException | InvalidKeySpecException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				documents = TextExtractParModified.lp2.asMap().keySet().size();
				extractKeywords(TextExtractParModified.lp1, documents, tmpname, dstPathDyn);
			}
		}

	}

	public static void genTestsetSyntheticIndex(String dstPath, String outputType)
			throws ClassNotFoundException, IOException {
		Date time = Calendar.getInstance().getTime();
		System.out.println("Creation of synthetic testset " + outputType + "startet at " + time);
		// create path if not existing
		File dir = new File(dstPath);
		if (!dir.exists()) {
			dir.mkdirs();
		}
		String syntheticDstPath = dstPath.concat("synthetic/");

		int[] size = new int[] { 10, 100, 250, 500, 750, 1000, 2000, 3000, 5000, 10000 };

		for (int s : size) {
			Multimap<String, String> map = ArrayListMultimap.create();
			map = genTestsetSyntheticIndexPart(s, map);
			byte[] serializedInverted = Serializer.serialize(map);
			if (outputType.equals("java")) {
				FileIO.writeBytesToFile(serializedInverted, s + "synthUpdate", syntheticDstPath);
			} else {
				ObjectMapper mapper = new ObjectMapper();
				// test serialization of json string needs to be wrapped in a
				// object
				// indexed
				FileIO.writeStringToFile(mapper.writeValueAsString(new Indexed(map.asMap())), s + "synthUpdateJson",
						syntheticDstPath);
			}
		}
		System.out.println("Synthetic testset generation finished");
	}

	public static Multimap<String, String> genTestsetSyntheticIndexPart(int size, Multimap<String, String> map) {
		// creates 100 keywords with size random document matches
		for (int i = 0; i < 100; i++) {
			Random r = new Random();
			int[] randomNumberArrayInRange = r.ints(size, 0, size).toArray();
			for (int j = 0; j < size; j++) {
				map.put(size + "keyword" + i, "doc" + randomNumberArrayInRange[j]);
			}
		}
		return map;
	}

	public static void genTestsetDynamicSyntheticIndex(String dstPath, String outputType)
			throws ClassNotFoundException, IOException {
		Date time = Calendar.getInstance().getTime();
		System.out.println("Creation of synthetic testset " + outputType + "startet at " + time);
		// create path if not existing
		File dir = new File(dstPath);
		if (!dir.exists()) {
			dir.mkdirs();
		}

		// create setup file
		String syntheticDstPath = dstPath.concat("dynamicsynthetic/");
		genSetupOneWordMultimap(syntheticDstPath);

		int[] size = new int[] { 10, 100 };
		int[] test = new int[] {20,10};
		// loop test times over document size contained in a update defined in size array
		for (int iter=0; iter<size.length;iter++) {
			int s =size[iter];
			// loop to create 10 updates
			for (int i = 0; i < test[iter]; i++) {
				Multimap<String, String> map = ArrayListMultimap.create();
				map = genTestsetDynamicSyntheticIndexPart(s, i, map);
				byte[] serializedInverted = Serializer.serialize(map);
				// for java
				if (outputType.equals("java")) {
					FileIO.writeBytesToFile(serializedInverted, i + "synthUpdate" + s, syntheticDstPath);
				} else {
					ObjectMapper mapper = new ObjectMapper();
					FileIO.writeStringToFile(mapper.writeValueAsString(new Indexed(map.asMap())),
							i + "synthUpdateJson" + s, syntheticDstPath);
				}
			}
		}
		System.out.println("Dynamic synthetic testset generation finished");
	}

	private static void genSetupOneWordMultimap(String dstPath) throws IOException {
		Multimap<String, String> setup = ArrayListMultimap.create();
		setup.put("setup", "setup");
		byte[] serializedSetup = Serializer.serialize(setup);
		FileIO.writeBytesToFile(serializedSetup, "setup", dstPath);
	}

	public static Multimap<String, String> genTestsetDynamicSyntheticIndexPart(int size, int docs,
			Multimap<String, String> map) {
		// creates 100 keywords with size random document matches in the new range
		for (int i = 0; i < 100; i++) {
			Random r = new Random();
			int[] randomNumberArrayInRange = r.ints(size, size * docs, size * docs + size).toArray();
			for (int j = 0; j < size; j++) {
				map.put(size + "keyword" + i, "doc" + randomNumberArrayInRange[j]);
			}
		}
		return map;
	}

	public static void extractKeywords(Multimap<String, String> map, int documents, String name, String dstPath)
			throws IOException {
		// we like to find g1,g2,g3 numbers keywords which are 1/3 of wordListSize
		// all g1,g2,g3 keywords in G1,G2,G3 should follow following pattern
		// of document matches
		// G1 < 1/3 of total document result matches
		// 1/3 total documents <G2 < 2/3 total document result match
		// G3 >2/3 total document result matches

		map = TextExtractParModified.lp1;
		int sep = (int) Math.ceil(documents / 3);
		System.out.println("sep:" + sep);
		String wordlist = "";
		int g1 = 0;
		int g2 = 0;
		int g3 = 0;
		for (String label : map.keys().elementSet()) {
			// 1/3 33
			int entries = map.get(label).size();
			if (entries < sep && g1 < 33) {
				wordlist = wordlist.concat(label +";"+entries+ "\n");
				g1++;
			} else {
				if (entries > sep) {
					if (entries > 2 * sep && g3 < 33) {
						wordlist = wordlist.concat(label +";"+entries+  "\n");
						g3++;
					} else {
						if (entries < 2 * sep && g2 < 33) {
							wordlist = wordlist.concat(label+";"+entries+  "\n");
							g2++;
						}
					}
				}
			}
			if (g1 == 32 && g2 == 32 && g3 == 32) {
				break;
			}
		}
		System.out.println("# g1 in G1 :" + g1 + " # g2 in G2: " + g2 + "# g3 in G3: " + g3);
		FileIO.writeStringToFile(wordlist, name + "_wordlist", dstPath);
		System.out.println("Keyword extraction finished");
	}

}
