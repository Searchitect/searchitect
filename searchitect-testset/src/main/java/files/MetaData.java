package files;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.attribute.FileTime;
import java.nio.file.attribute.PosixFileAttributes;

public class MetaData {
	
	String filename;
	String path;
	long size;
	FileTime created;
	FileTime modified;
	String owner;
	String group;
	
	public MetaData(File file) throws IOException{
		PosixFileAttributes attr = Files.readAttributes(file.toPath(), PosixFileAttributes.class);
		filename = file.getName();
		path = file.getAbsolutePath();
		size = attr.size();
		created = attr.creationTime();
		modified = attr.lastModifiedTime();
		owner =attr.owner().getName();
		group = attr.group().getName();
		
	}

}
