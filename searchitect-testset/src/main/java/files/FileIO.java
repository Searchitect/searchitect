package files;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;


public class FileIO {

	/**
	 * writes a string to a file
	 * 
	 * @param input
	 * @param fileName
	 * @param dirPath
	 * @throws IOException
	 */
	public static void writeStringToFile(String input, String fileName, String dirPath) throws IOException {
		// creation of a directory if it is not created
		(new File(dirPath)).mkdir();
		try (OutputStreamWriter writer = new OutputStreamWriter(new FileOutputStream(dirPath + "/" + fileName),
				StandardCharsets.UTF_8)) {
			writer.write(input);
			writer.close();
		}
	}

	/**
	 * append a String to a file
	 * @param input
	 * @param filePath
	 * @throws IOException
	 */
	public static void appendStringToFile(String input, String filePath) throws IOException {
		// creation of a directory if it is not created
		File f = new File(filePath);
		if (!f.exists()) {
			new File(filePath).createNewFile();			
		}
		Writer writer = new BufferedWriter(new FileWriter(f, true));
		writer.append(input);
		writer.close();
	}
	
	/**
	 * get the size of the file
	 * 
	 * @param filePath
	 * @return
	 */
	public static long getFileSize(String filePath){
		File f = new File(filePath);
		long size =f.length();
		return size;
	}
	

	/**
	 * reads a string from a file (limit 2 GB)
	 * 
	 * @param filePath
	 * @return a string of file contents
	 * @throws IOException
	 */
	public static String readStringFromFile(String filePath) throws IOException {
		// limit 2GB for Files.readAllBytes
		String readed = new String(Files.readAllBytes(Paths.get(filePath)));
		return readed;
	}

	/**
	 * writeBytesToFile
	 * 
	 * @param input
	 * @param fileName
	 * @param dirPath
	 * @throws IOException
	 */
	public static void writeBytesToFile(byte[] input, String fileName, String dirPath) throws IOException {
		(new File(dirPath)).mkdir();
		Files.write(Paths.get(dirPath + "/" + fileName), input, StandardOpenOption.CREATE);
	}

	/**
	 * read all bytes from a file (max 2GB)
	 * 

	 * @param dirPath
	 * @return
	 * @throws IOException
	 */
	public static byte[] readBytesFromFile(String dirPath) throws IOException {
		return Files.readAllBytes(Paths.get(dirPath));
	}

}
