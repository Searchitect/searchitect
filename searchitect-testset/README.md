# Searchitect-Testset

### Functionality
Generates the test-sets
 	* synthetic:  generates several setup multimaps containing 100 keywords each mapping 10, 100, 250, 500, 750, 1000, 2000, 3000, 5000, 10000 random document identifiers. The setup multimap is serialized and stored to a file in the output path (/dstpath/) as Java object or json string.
 	* dynamicsynthetic:  generates 20 update multimaps containing 100 keywords each mapping 10 newly inserted document identifiers and 10 multimaps containing 100 keywords each mapping 100 newly inserted document identifiers.
 	* batched: expects a source path and destination path as input. Indexes all files contained in the subdirectories of the sourcepath and outputs them to the destination directory. Additionally wordlists which list keywords that match the multimap are saved also to a file. The keywords are selected in a specific manner that the list will contain also keywords that have a large result set.
 	* dynamicbatched: expects a source path and a destination path as input. Divides the files at the source path in the subdirectories into groups of 100 files. Then the indexing processes is performed on them to generate update multimaps named by the  iteration and subdirectoryname. These are stored to the destination directory. Further in each iteration a wordlist is generated containing words which match on results in the update map. The selection of the words is calculated on the per update growing multimap again in this three different groups of resultsets.
 	

Parameters: 
	1. kind of testset: -batched, -synthetic, -dynamicbatched, -dynamicsynthetic
	2. in case of -synthetic and -dynamicsynthetic dstpath and and java or json for output
	3. in case of -batched and -dynamicbatched source path of the root directory and the 
	
 
### Compile

	mvn clean install
	cd target
	
## Usage examples

	java -jar searchitect-testset-0.0.1-SNAPSHOT.jar -batched /srcpath/ /dstpath/
	java -jar searchitect-testset-0.0.1-SNAPSHOT.jar -synthetic /dstpath/ java

