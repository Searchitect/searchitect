package searchitect.model;

import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.crypto.NoSuchPaddingException;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Lob;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;

import searchitect.clusion.DynRH2LevModifiedMap;
import searchitect.common.view.SearchResult;
import searchitect.common.viewdynrh2lev.SearchTokendynrh2lev;
import searchitect.common.viewdynrh2lev.UpdateIndexdynrh2lev;
import searchitect.common.viewdynrh2lev.UploadIndexdynrh2levMap;


/**
 * 
 * IndexImpl holds the dictionary, array and update hashmap and uses the Clusion
 * library for the operations
 *
 */
@Entity
public class IndexImpl {

	@Id
	@GeneratedValue
	private Long id;

	private String repositoryName;

	@Lob
	@Column(name = "dictionary", columnDefinition = "BLOB")
	private HashMap<String, byte[]> dictionary;

	@Lob
	@Column(name = "update_dictionary", columnDefinition = "BLOB")
	private HashMap<String, byte[]> updateDictionary;

	@Lob
	@Column(name = "array", columnDefinition = "BLOB")
	byte[][] array;

	protected IndexImpl() { // jpa only
	}

	public String getRepositoryName() {
		return repositoryName;
	}

	public Map<String, byte[]> getDictionary() {
		return dictionary;
	}

	public void setDictionary(HashMap<String, byte[]> dictionary) {
		this.dictionary = dictionary;
	}

	public HashMap<String, byte[]> getUdateDictionary() {
		return updateDictionary;
	}

	public HashMap<String, byte[]> getIndex() {
		return dictionary;
	}

	public void setIndex(HashMap<String, byte[]> index) {
		this.dictionary = index;
	}
	
	/**
	 * @return the array
	 */
	public byte[][] getArray() {
		return array;
	}

	/**
	 * @param array the array to set
	 */
	public void setArray(byte[][] array) {
		this.array = array;
	}

	/**
	 * Constructor for IndexImpl generation
	 * 
	 * @param uploadIndex
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	public IndexImpl(UploadIndexdynrh2levMap uploadIndex)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		// test if index is valid multimap
		this.dictionary = uploadIndex.getDictionary();
		this.array = uploadIndex.getArray();
		this.updateDictionary = new HashMap<String, byte[]>();
		this.repositoryName = UUID.randomUUID().toString();
	}

	/**
	 * update - the update hashmap using UpdateIndexdynrh2lev which contains a
	 * navigableMap <String, Collection <byte[]>>
	 * 
	 * @param updateIndex
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	public void update(UpdateIndexdynrh2lev updateIndex)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		for (String label : updateIndex.getDictionary().keySet()) {
			for (byte[] item : updateIndex.getDictionary().get(label)) {
				updateDictionary.put(label, item);
			}
		}
	}

	/**
	 * search - uses Searchtokendynrh3lev array to search for
	 * 
	 * @param token
	 * @return
	 * @throws InvalidKeyException
	 * @throws InvalidAlgorithmParameterException
	 * @throws NoSuchAlgorithmException
	 * @throws NoSuchProviderException
	 * @throws NoSuchPaddingException
	 * @throws IOException
	 * @throws NullPointerException
	 */
	public SearchResult search(SearchTokendynrh2lev token)
			throws InvalidKeyException, InvalidAlgorithmParameterException, NoSuchAlgorithmException,
			NoSuchProviderException, NoSuchPaddingException, IOException, NullPointerException {
		List <String> resultList;
		if(updateDictionary.size()==0){
			resultList = DynRH2LevModifiedMap.query(token.getSearchToken(), dictionary, array, updateDictionary);
		}
		else{
			resultList = DynRH2LevModifiedMap.queryFS(token.getSearchToken(), dictionary, array, updateDictionary);
		}
		return new SearchResult(resultList);

	}

}
