package searchitect;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.spec.InvalidKeySpecException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.ExecutionException;


import javax.crypto.NoSuchPaddingException;

import org.apache.commons.io.FileUtils;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import com.google.common.collect.TreeMultimap;

import searchitect.clusion.DynRH2LevModifiedMap;
import searchitect.clusion.RH2LevModifiedMap;
import searchitect.common.clusion.TextExtractPar;
import searchitect.common.clusion.TextProc;
import searchitect.common.crypto.CryptoPrimitives;
import searchitect.common.viewdynrh2lev.SearchTokendynrh2lev;
import searchitect.common.viewdynrh2lev.UpdateIndexdynrh2lev;
import searchitect.common.viewdynrh2lev.UploadIndexdynrh2levMap;


public class TestUtil {
	
	String pass = "assumedsecure";
	byte[] sk;
	
	public TestUtil(){
		try {
			sk = CryptoPrimitives.keyGen(256, pass, "salt/salt", 100000);
		} catch (InvalidKeySpecException | NoSuchAlgorithmException | NoSuchProviderException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static byte[][] searchToken1;
	public static byte[][] searchToken2;
	public static byte[][] searchToken3;
	public static byte[][] notExistingSearchToken;
		
	public static byte[][] array;
	public static byte [][] notExistingToken;
	public static HashMap <String, byte[]> dic;
	public static TreeMultimap<String, byte[]> tokenUp1;
	public static TreeMultimap<String, byte[]> tokenUp2;
	public static String word = "first";
	public static byte[] key;
	public static String emptyJsonString = "";
	public static String nullJsonString = null;
	public static String jsonSearchToken1;
	public static String jsonSearchToken2;
	public static String jsonSearchToken3;
	public static String jsonNotExistingSearchToken;
	public static String notExistingJsonToken;
	public static String wrongJsonToken;
	public static String setupIndexJson;
	public static String updateIndexJson1;
	public static String updateIndexJson2;
	public static String updateIndexJson3;
	public static String updateIndexJson4;
	public static String updateIndexJson5;
	public static String setupPath;
	public static String updatePath;
	static ObjectMapper mapper = new ObjectMapper();

	static String path1 = "test/test1.txt";
	static String  path2 = "test/test2.txt";
	static String  path3 = "test/test3.txt";
	static String upath1 = "update/utest1.txt";
	static String  upath2 = "update/utest2.txt";
	static String  upath3 = "update/utest3.txt";
	
	
	public static void createTestFiles(String path) throws IOException {
        Files.createDirectories( Paths.get(path+path1).getParent());
        Files.createFile(Paths.get(path+path1));
        PrintWriter writer1 = new PrintWriter(path+path1, "UTF-8");
        writer1.println("The first line");
        writer1.println("The second line");
        writer1.close();
        Files.createFile(Paths.get(path+path2));
        PrintWriter writer2 = new PrintWriter(path+path2, "UTF-8");
        writer2.println("The first keyword1");
        writer2.println("The second keyword2");
        writer2.close();
        Files.createFile(Paths.get(path+path3));
        PrintWriter writer3 = new PrintWriter(path+path3, "UTF-8");
        writer3.println("weired keyword1");
        writer3.println("The second keyword2");
        writer3.println(" first ");
        writer3.close();
	}
	
	
	public static void createUpdateTestFiles(String path) throws IOException {
        Files.createDirectories(Paths.get(path+upath1).getParent());
        Files.createFile(Paths.get(path+upath1));
        PrintWriter writer4 = new PrintWriter(path+upath1, "UTF-8");
        writer4.println("The first update line");
        writer4.println("The second update line");
        writer4.close();
        Files.createFile(Paths.get(path+upath2));
        PrintWriter writer5 = new PrintWriter(path+upath2, "UTF-8");
        writer5.println("The first update keyword1");
        writer5.println("The second update keyword2");
        writer5.close();
        Files.createFile(Paths.get(path+upath3));
        PrintWriter writer6 = new PrintWriter(path+upath3, "UTF-8");
        writer6.println("weired update keyword1");
        writer6.println("The second update keyword2");
        writer6.close();
	}
	
	public static void deleteTestFiles(String path) throws IOException {
		FileUtils.deleteDirectory(new File(path));
	}
	

    public static void initialize(String path) throws IOException, InvalidKeySpecException, NoSuchAlgorithmException, NoSuchProviderException, InterruptedException, ExecutionException, InvalidKeyException, InvalidAlgorithmParameterException, NoSuchPaddingException {      
    	searchToken1 =null; searchToken2 =null; searchToken3=null; array=null; notExistingToken = null;
    	dic= null; tokenUp1=null;tokenUp2=null;key=null;
    	jsonSearchToken1= null;jsonSearchToken2= null;jsonSearchToken3=null;
    	setupIndexJson=null;
    	updateIndexJson1=null;updateIndexJson2=null;updateIndexJson3=null;updateIndexJson4=null;updateIndexJson5=null;
    	deleteTestFiles(path);
    	//create the test files
    	createTestFiles(path);
    	createUpdateTestFiles(path);
    	setupPath= path+"test/";
    	updatePath =path+"update/";
    	//initialize Dyn2lev parameters
    	int bigBlock = 100;
		int smallBlock = 2;
		int dataSize = getMultimap().size();
		ArrayList<File> fileList = new ArrayList<>();
		////generate key
    	key = CryptoPrimitives.keyGen(256, "testpwd", "testkey", 20);
		// Construction of the global multi-map
		TextProc.listf(setupPath, fileList);
		Multimap<String, String> lookupinverse = ArrayListMultimap.create();
		TextExtractPar.lp1 =lookupinverse;
		TextExtractPar.extractTextPar(fileList);
		lookupinverse = TextExtractPar.lp1;
		RH2LevModifiedMap.master = key;
		//extract encrypted index as json string
		DynRH2LevModifiedMap twolev = DynRH2LevModifiedMap.constructEMMParGMM(key, lookupinverse, bigBlock, smallBlock, dataSize);
		//while(!twolev.getFinished()){};
		array= twolev.getArray();
		dic = twolev.getDictionary();
		HashMap<String, Integer> state = twolev.getState();
	
		setupIndexJson =mapper.writeValueAsString(new UploadIndexdynrh2levMap(dic,array));
		//generate token
		searchToken1 = DynRH2LevModifiedMap.genToken(key, word,state);
		jsonSearchToken1 = mapper.writeValueAsString(new SearchTokendynrh2lev(searchToken1));
		notExistingSearchToken = DynRH2LevModifiedMap.genTokenFS(key, "wrongword",state);
		jsonNotExistingSearchToken = mapper.writeValueAsString(new SearchTokendynrh2lev(notExistingSearchToken ));
	
		// empty multimap!!
		TextExtractPar.lp1 = ArrayListMultimap.create();
		fileList.clear();
		TextProc.listf(updatePath, fileList);
		TextProc.TextProc(false, updatePath);
		tokenUp1 = DynRH2LevModifiedMap.tokenUpdate(key, TextExtractPar.lp1,state);
		updateIndexJson1 = mapper.writeValueAsString(new UpdateIndexdynrh2lev(tokenUp1.asMap()));
		searchToken2 = DynRH2LevModifiedMap.genTokenFS(key, word,state);
		jsonSearchToken2 = mapper.writeValueAsString(new SearchTokendynrh2lev(searchToken2));
		tokenUp2 = DynRH2LevModifiedMap.tokenUpdate(key, TextExtractPar.lp1,state);
		updateIndexJson2 = mapper.writeValueAsString(new UpdateIndexdynrh2lev(tokenUp2.asMap()));
		tokenUp2 = DynRH2LevModifiedMap.tokenUpdate(key, TextExtractPar.lp1,state);
		updateIndexJson3 = mapper.writeValueAsString(new UpdateIndexdynrh2lev(tokenUp2.asMap()));
		tokenUp2 = DynRH2LevModifiedMap.tokenUpdate(key, TextExtractPar.lp1,state);
		updateIndexJson4 = mapper.writeValueAsString(new UpdateIndexdynrh2lev(tokenUp2.asMap()));
		tokenUp2 = DynRH2LevModifiedMap.tokenUpdate(key, TextExtractPar.lp1,state);
		updateIndexJson5 = mapper.writeValueAsString(new UpdateIndexdynrh2lev(tokenUp2.asMap()));
		searchToken3 = DynRH2LevModifiedMap.genTokenFS(key, word,state);
		jsonSearchToken3 = mapper.writeValueAsString(new SearchTokendynrh2lev(searchToken3));
		notExistingToken = new byte[3][];
		notExistingToken[0] = CryptoPrimitives.generateCmac(key, 1 + "notexisting");
		notExistingToken[1] = CryptoPrimitives.generateCmac(key, 2 + "notexisting");
		notExistingJsonToken = mapper.writeValueAsString(new SearchTokendynrh2lev(notExistingToken));
		wrongJsonToken = jsonSearchToken1.substring(2);
		deleteTestFiles(path);
	
    }

	
	
	
	public static Multimap<String, byte[]> getMultimap() {

		// Map<String, List<String>>
		// keyword1 -> doc1, doc2, doc3
		// keyword2 -> doc1, doc4, doc5
		// keyword3 -> doc1, doc6, doc7
		// keyword4 -> doc1, doc3, doc5

		Multimap<String, byte[]> multimap = ArrayListMultimap.create();

		multimap.put("keyword1", "doc1".getBytes());
		multimap.put("keyword1", "doc2".getBytes());
		multimap.put("keyword1", "doc3".getBytes());
		multimap.put("keyword2", "doc1".getBytes());
		multimap.put("keyword2", "doc4".getBytes());
		multimap.put("keyword2", "doc5".getBytes());
		multimap.put("keyword3", "doc1".getBytes());
		multimap.put("keyword3", "doc6".getBytes());
		multimap.put("keyword3", "doc7".getBytes());
		multimap.put("keyword4", "doc1".getBytes());
		multimap.put("keyword4", "doc3".getBytes());
		multimap.put("keyword4", "doc5".getBytes());

		return multimap;
	}


	public static Multimap<String, byte[]> getUpdateMultimap() {

		// Map<String, List<String>>
		// keyword1 -> doc8, doc9, doc7
		// keyword2 -> doc8, doc9, doc7
		Multimap<String, byte[]> multimap = ArrayListMultimap.create();

		multimap.put("keyword1", "doc8".getBytes());
		multimap.put("keyword1", "doc9".getBytes());
		multimap.put("keyword1", "doc7".getBytes());
		multimap.put("keyword2", "doc8".getBytes());
		multimap.put("keyword2", "doc9".getBytes());
		multimap.put("keyword2", "doc7".getBytes());

		return multimap;
	}


	public static byte[][] getArray() {
		byte[][] array = new byte[2][];
		array[0]= "test1".getBytes();
		array[1]= "test2".getBytes();
		return array;
	}
	public static String getBase64EncodedSetUpIndexasJsonString() throws JsonProcessingException {
		return mapper.writeValueAsString(setupIndexJson);
		
	}
	
	public static UploadIndexdynrh2levMap getUploadIndex() {
		return new UploadIndexdynrh2levMap(TestUtil.dic,TestUtil.getArray());
	}
	
	public static String getUploadIndexasJsonString() throws JsonProcessingException {
		UploadIndexdynrh2levMap ui= new UploadIndexdynrh2levMap(TestUtil.dic,TestUtil.getArray());
		return mapper.writeValueAsString(ui);
	}
	
	public static String getUploadIndexasWrongJsonString() throws JsonProcessingException {
		UploadIndexdynrh2levMap ui= new UploadIndexdynrh2levMap(TestUtil.dic,null);
		return mapper.writeValueAsString(ui);
	}
	
	public static String getUpdateIndexasJsonString() throws JsonProcessingException {
		UploadIndexdynrh2levMap ui= new UploadIndexdynrh2levMap(TestUtil.dic,null);
		return mapper.writeValueAsString(ui);
	}

}
