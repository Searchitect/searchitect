package searchitect.model;
import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.hamcrest.Matchers.hasSize;

import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.spec.InvalidKeySpecException;
import java.util.List;
import java.util.concurrent.ExecutionException;

import javax.crypto.NoSuchPaddingException;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;


import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;


import searchitect.TestUtil;

import searchitect.clusion.DynRH2LevModifiedMap;
import searchitect.common.crypto.CryptoPrimitives;
import searchitect.common.view.SearchResult;
import searchitect.common.viewdynrh2lev.SearchTokendynrh2lev;
import searchitect.common.viewdynrh2lev.UpdateIndexdynrh2lev;
import searchitect.common.viewdynrh2lev.UploadIndexdynrh2levMap;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = ApplicationArguments.class)
public class IndexImplTest {
	

@BeforeClass
public static void setup() throws InvalidKeyException, InvalidKeySpecException, NoSuchAlgorithmException, NoSuchProviderException, InvalidAlgorithmParameterException, NoSuchPaddingException, IOException, InterruptedException, ExecutionException{
	TestUtil.initialize("/tmp/indeximpl/");
}

@AfterClass
public static void cleanup() throws IOException{
}
	@Test
	public void IndexImplConstructorSuccessTest()throws Exception {
		IndexImpl index = new IndexImpl(new UploadIndexdynrh2levMap(TestUtil.dic, TestUtil.getArray()));
		assertArrayEquals(index.array[0], "test1".getBytes());
	 	assertFalse(index.getRepositoryName().isEmpty());
	}


	@Test(expected = NullPointerException.class)
	public void IndexImplConstructorUploadIndexNullFailsTest() throws Exception {
		new IndexImpl(null);
	}

	@Test
	public void IndexImplConstructorUploadIndexDictionaryNullFailsTest() throws Exception {
		new IndexImpl(new UploadIndexdynrh2levMap(null, TestUtil.getArray()));
	}
	
	@Test
	public void IndexImplConstructorUploadIndexArrayNullSuccessTest() throws Exception {
		IndexImpl index = new IndexImpl(new UploadIndexdynrh2levMap(TestUtil.dic, null));
		assertFalse(index.getRepositoryName().isEmpty());
	}
	
	@Test
	public void IndexImplUpdateUploadIndexSuccessTest() throws Exception {
		IndexImpl index = new IndexImpl(new UploadIndexdynrh2levMap(TestUtil.dic,TestUtil.array));
		UpdateIndexdynrh2lev ul = new UpdateIndexdynrh2lev(TestUtil.tokenUp1.asMap());
		index.update(ul);
		assertFalse(index.getRepositoryName().isEmpty());
	}

	@Test(expected = NullPointerException.class)
	public void IndexImplUpdateUploadIndexDictionaryNullFailsTest() throws Exception {
		IndexImpl index = new IndexImpl(new UploadIndexdynrh2levMap(TestUtil.dic,TestUtil.array));
	 	index.update(new UpdateIndexdynrh2lev(null));
	}

	@Test(expected = NullPointerException.class)
	public void IndexImplUpdateUploadIndexNullFailsTest()
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		new IndexImpl(null);
	}

	public void IndexImplSearchNullTokenFailsTest()throws Exception {
		IndexImpl index = new IndexImpl(new UploadIndexdynrh2levMap(TestUtil.dic, TestUtil.array.clone()));
		SearchResult rs = index.search(new SearchTokendynrh2lev(null));
		DynRH2LevModifiedMap.resolve(CryptoPrimitives.generateCmac(TestUtil.key, 3 + new String()), rs.getResultList());
	}
	
	@Test
	public void IndexImplSearchAfterSetupSuccessTest()throws Exception {
		IndexImpl index = new IndexImpl(new UploadIndexdynrh2levMap(TestUtil.dic, TestUtil.array));
		SearchTokendynrh2lev test = new SearchTokendynrh2lev(TestUtil.searchToken1);
		SearchResult rs = index.search(test);
		System.out.println(rs.getResultList());
		List <String> resultclear = DynRH2LevModifiedMap.resolve(CryptoPrimitives.generateCmac(TestUtil.key, 3 + new String()), rs.getResultList());
		System.out.println(resultclear);
		assertThat(resultclear,hasSize(3));
	 	assertFalse(index.getRepositoryName().isEmpty());
	}
	
	@Test
	public void IndexImplSearchAfterUpdateSuccessTest()throws Exception {
		IndexImpl index = new IndexImpl(new UploadIndexdynrh2levMap(TestUtil.dic, TestUtil.array));
		index.update(new UpdateIndexdynrh2lev(TestUtil.tokenUp1.asMap()));
		SearchTokendynrh2lev test = new SearchTokendynrh2lev(TestUtil.searchToken2);
		SearchResult rs = index.search(test);
		System.out.println(rs.getResultList());
		List <String> resultclear = DynRH2LevModifiedMap.resolve(CryptoPrimitives.generateCmac(TestUtil.key, 3 + new String()), rs.getResultList());
		System.out.println(resultclear);
		assertThat(resultclear,hasSize(5));
	 	assertFalse(index.getRepositoryName().isEmpty());
	}
	

	

	
}
