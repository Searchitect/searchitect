# Searchitect-Searchable Encryption Framework


#### Description of the Searchitect Framework:
This framework enables the integration of SE schemes.

#### General description of SE
A SE scheme enables a server to search over an encrypted database on behalf of a client without revealing the content to the server.
A SE scheme provides 3 protocols:
1. Setup - First the client is indexing a document collection contained in a directory. This plaintext index gets encrypted by a specific implementation of an encryption scheme and uploaded to the server.

2. Search - After Setup the client is able to search over the data by passing the keyword to the search protocol, which computes a search token which is sent to the server. This search Token enables the server to search over the encrypted data and return the resulting document matches. In resource hiding schemes these are encrypted and therfore a second Resolve procedure at the client is needed to decrypt document identifiers.
3. Update - Dynamic schemes support a update of the documents contained in the encrypted index.

#### Framework Architecture:
* Client/server architecture based on microservices
* SOA (service oriented architecture) based on RESTful webservices

#### Implementation:
* Basic implementations
	* searchitect-common - classes shared between server and client
  	* searchitect-client - common client implementation
 	* searchitect-gateway - external interface
  	* searchitect-backend-module - backend side of the implemented scheme
* Added schemes appear in the project forms of:
	* searchitect-common-scheme
	* searchitect-client-scheme-plugin
	* searchitect-backend-scheme

#### The interface description of the gateway is after deployment available at:

	https://localhost:8433/swagger-ui.html

### Compile

Run the following command in the top level directory

    mvn clean install

### General deployment using Docker-compose
Docker enables a containerized easy deployment, the docker-compose configuration file is called docker-compose.yml.

    docker-compose build
    docker-compose up

### How to add a new scheme
1. Implement your scheme in a new  searchitect-common-scheme project
2. Create a new searchitect-client-scheme-plugin project which implements the client plugin interface. This interface can be found in searchitect.common.client.ClientScheme
3. Create a new project which implements the searchitect-backend-scheme at the server side, take a look to the other implementations the interface of the controller needs to be similar
4. Adapt the Docker File in your searchitect.common.client.ClientScheme, take care with the port, choose one that is still available
5. Add your new Searchitect-backend implementation in the application properties of the searchitect-gate project to the list. The name needs to be the same as in the docker-compose file (docker-compose.yml).
6. Recompile the whole workspace and test.
