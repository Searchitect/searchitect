# Searchitect-Common

### Functionality
 * Contains common classes between server and client 
 
 
### Compile

	mvn clean install

### Packages
* searchitect.common.client
	* ClientScheme.java - interface of the client plugins of specific implementations of schemes
* searchitect.common.crypto
	* Crypto.java - usefull crypto methods for all projects backed by Bouncy Castle
	* CryptoPrimitives.java - Crypto methods from the Clusion library
* searchitect.common.exception
	* SearchitectException.java - exception thrown in this project
* searchitect.common.view - classes commonly used for communication between client and server
	* RepositoryInfo.java 
	* SearchResult.java
	* SearchToken.java
	* Upload.java
	* UploadIndex.java
	* UserAccountResponse.java
	* UserAuthRequest.java
	* UserAuthRequest.java
	* UserEDBResponse.java
* searchitect.service.java
	* MemoryDictionary.java - just for testing
	* RocksDbWrapperModified - taken from com.github.ddth.commons.rocksdb (author is Thanh Nguyen)
	* SearchDictionary.java - interface used for Memorydictionary and RocksDbWrapperModified