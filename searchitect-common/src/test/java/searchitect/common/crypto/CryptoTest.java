package searchitect.common.crypto;

import static org.junit.Assert.assertArrayEquals;

import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import org.bouncycastle.crypto.InvalidCipherTextException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
public class CryptoTest {
	
	@Test
	public void aesccmEncryptValidParamsSuccessTest() throws NoSuchAlgorithmException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException, NoSuchProviderException, IllegalStateException, InvalidCipherTextException, InvalidKeyException, InvalidAlgorithmParameterException{
		
		byte [] data = "test bouncy castle aead encryption".getBytes();
		byte [] key = Crypto.randomBytes(16);
		byte [] assocText = "searchitect".getBytes();
		
		byte [] encrypted = Crypto.encryptAESGCM(key, data, assocText);
		byte [] decrypted = Crypto.decryptAESGCM(key, encrypted, assocText);
		System.out.println(new String(decrypted));
		assertArrayEquals(data, decrypted);
	}

}
