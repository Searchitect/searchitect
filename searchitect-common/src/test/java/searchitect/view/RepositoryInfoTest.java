package searchitect.view;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

import searchitect.common.view.RepositoryInfo;


@RunWith(SpringRunner.class)
public class RepositoryInfoTest {
	
	String normalString = "testrepositoryinfo";
	String emptyString ="";
	String nullString = null;

	@Test
	public void constructorWithNormalStringSuccessTest() {
		RepositoryInfo rinfo = new RepositoryInfo(normalString);
		assertEquals(normalString,rinfo.getrepositoryName());
	} 
	
	@Test
	public void setWithNormalStringSuccessTest() {
		RepositoryInfo rinfo = new RepositoryInfo();
		rinfo.setrepositoryName(normalString);
		assertEquals(normalString,rinfo.getrepositoryName());
	} 
	
	@Test
	public void setWithNullStringSuccessTest() {
		RepositoryInfo rinfo = new RepositoryInfo();
		rinfo.setrepositoryName(nullString);
		assertEquals(nullString,rinfo.getrepositoryName());
	} 
	
	@Test
	public void setWithEmptyStringSuccessTest() {
		RepositoryInfo rinfo = new RepositoryInfo();
		rinfo.setrepositoryName(emptyString);
		assertEquals(emptyString,rinfo.getrepositoryName());
	} 
}
