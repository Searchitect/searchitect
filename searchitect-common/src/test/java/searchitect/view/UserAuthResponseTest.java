package searchitect.view;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

import searchitect.common.view.UserAuthResponse;


@RunWith(SpringRunner.class)
public class UserAuthResponseTest {
	
	String normalString = "testrepositoryinfo";
	String emptyString ="";
	String nullString = null;

	@Test
	public void constructorTestWithNormalString() {
		UserAuthResponse token = new UserAuthResponse(normalString);
		assertEquals(normalString,token.getToken());
	} 
	
	@Test
	public void setTokenTestWithNormalString() {
		UserAuthResponse token = new UserAuthResponse();
		token.setToken(normalString);
		assertEquals(normalString,token.getToken());
	} 
	
	@Test
	public void setTokenTestWithNullString() {
		UserAuthResponse token = new UserAuthResponse();
		token.setToken(nullString);
		assertEquals(nullString,token.getToken());
	} 
	
	@Test
	public void setTokenTestWithEmptyString() {
		UserAuthResponse token = new UserAuthResponse();
		token.setToken(emptyString);
		assertEquals(emptyString,token.getToken());
	} 
	
	
	
}
