package searchitect.view;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

import searchitect.common.view.SearchResult;



@RunWith(SpringRunner.class)
public class SearchResultTest {
	
	List<String> normalList = Arrays.asList("doc1", "doc2", "doc3");
	List<String> nullList = null;
	List<String> emptyList = Arrays.asList();


	@Test
	public void constructorTestWithNormalList() {
		SearchResult searchResult = new SearchResult(normalList);
		assertEquals(normalList,searchResult.getResultList());
	} 
	
	@Test
	public void setSearchResultTestWithNormalList() {
		SearchResult searchResult = new SearchResult();
		searchResult.setSearchResult(normalList);
		assertEquals(normalList,searchResult.getResultList());
	} 
	
	@Test
	public void setSearchResultTestWithNullList() {
		SearchResult searchResult = new SearchResult();
		searchResult.setSearchResult(nullList);
		assertEquals(nullList,searchResult.getResultList());
	} 
	
	@Test
	public void setSearchResultTestWithEmptyList() {
		SearchResult searchResult = new SearchResult();
		searchResult.setSearchResult(emptyList);
		assertEquals(emptyList,searchResult.getResultList());
	} 
	
	
	
}
