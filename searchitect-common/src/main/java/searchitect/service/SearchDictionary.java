package searchitect.service;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import com.google.common.collect.Multimap;

import searchitect.common.exception.SearchitectException;

//maps normaly do not accept byte[] arrays as keys but rocksdb does therefore this abstraction

public interface SearchDictionary {
	/**
	 * put a label and a value
	 * 
	 * @param columnFamily
	 * @param key
	 * @param value
	 * @throws SearchitectException
	 */
	public void put(String columnFamily, byte[] label, byte[] value) throws SearchitectException;
	/**
	 * put all multimap used to insert al key/values pairs in a search dictionary implementation
	 * 
	 * @param columnFamily
	 * @param map
	 * @throws SearchitectException
	 */
	public void putAllMultimap(String columnFamily, Multimap<String, byte[]> map) throws SearchitectException;
	/**
	 * put all map collection used to insert a map where the value is a collection of values
	 * 
	 * @param columnFamily
	 * @param map
	 * @throws SearchitectException
	 */
	public void putAllMapCollection(String columnFamily, Map<String, Collection<byte[]>> map) throws SearchitectException;
	/**
	 * put all map - to insert all label/value pairs in a search dictionary
	 * 
	 * @param columnFamily
	 * @param map
	 * @param bas64 is true when label is encoded using base64 encoding
	 * @throws SearchitectException
	 */
	public void putAllMap(String columnFamily, Map<String, byte[]> map, boolean base64) throws SearchitectException;
	/**
	 * put all array - used to insert the index and value of the elements into a search dicitionary
	 * 
	 * @param columnFamily
	 * @param arr
	 */
	public void putAllArray(String columnFamily, byte[][] arr);
	/**
	 * get a value by stating a columnfamily and a label
	 * 
	 * @param columnFamily
	 * @param label
	 * @throws SearchitectException
	 */
	public byte[] get(String columnFamily,byte[] label) throws SearchitectException;
	/**
	 * get all labels
	 * 
	 * @param columnFamily
	 * @return a set of byte[]
	 */
	public Set < byte[]> keySet(String columnFamily);
	/**
	 * Get the value by the array index i
	 * 
	 * @param columnFamily
	 * @param i
	 * @throws SearchitectException
	 */
	public void getByintFromArray(String columnFamily, int i)throws SearchitectException;
	/**
	 * delete a label/value pair in the column family 
	 * 
	 * @param columnFamily
	 * @param klabel
	 */
	public void delete(String columnFamily, byte[] label);
	/**
	 * put all hashmap inserts a hashmap into specified columnfamily 
	 * 
	 * @param columnFamily
	 * @param map
	 * @param base64 true if the value is encoded with base64 encoding
	 * @throws SearchitectException
	 */
	void putAllHashMap(String columnFamily, HashMap<String, byte[]> map, boolean base64) throws SearchitectException;

}
