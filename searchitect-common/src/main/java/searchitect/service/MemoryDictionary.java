package searchitect.service;

import java.nio.ByteBuffer;
import java.util.Base64;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.bouncycastle.util.encoders.Hex;

import com.google.common.collect.Multimap;

import searchitect.common.exception.SearchitectException;
import searchitect.service.SearchDictionary;

//hides that maps usually do not allow byte[] arrays as keys but rocksdb does
public class MemoryDictionary implements SearchDictionary{

	HashMap<String, byte[]> stat;
	HashMap<String, byte[]> dynamic;
	byte[][] arr;
	
	public MemoryDictionary () {
		this.stat= new HashMap<String, byte[]> ();
		this.dynamic= new HashMap<String, byte[]> ();
		this.arr =null;
	}
	@Override
	public void put(String columnFamily, byte[] key, byte[] value) throws SearchitectException{
		if (columnFamily.contentEquals("dynamic")){
			this.dynamic.put(Hex.toHexString(key), value);
		}
		else{
		
			this.stat.put(Hex.toHexString(key), value);
		}
	
	}
	
	@Override
	public void putAllArray(String columnFamily, byte[][] arr) {
			this.arr =arr;
	}
	

	@Override
	public byte[] get(String columnFamily,byte[] label) throws SearchitectException {
		byte [] result;
		switch(columnFamily){
			case("dynamic"): {
				result = dynamic.get(Base64.getEncoder().encodeToString(label)); 
				break;
			}
			case("static"): {
				System.out.println("test: " + Base64.getEncoder().encodeToString(label));
				result = stat.get(Base64.getEncoder().encodeToString(label)); 
				System.out.println("result: "+result);
				break;
			}
			case("array"): {
				System.out.println("Int?: " + ByteBuffer.wrap(label).getInt());
				result = arr[ByteBuffer.wrap(label).getInt()];
				break;
			}
			default:
				result=null;
		}
		return result;
	}

	@Override
	public Set<byte[]> keySet(String columnFamily) {
		Set <byte[]>ret = new HashSet<byte[]>();
		if (columnFamily.contentEquals("dynamic")){
			for (String s: dynamic.keySet() ) {
				ret.add(Base64.getDecoder().decode(s));
			}
		}
		else if(columnFamily.contentEquals("stat")){
			for (String s: stat.keySet() ) {
				ret.add(Base64.getDecoder().decode(s));
			}	
		}
		return ret;
	}

	public void putAllMultimap(String columnFamily, Multimap<String, byte[]> map) throws SearchitectException {
		System.out.println("1 put all!!" );
		if (columnFamily.equals("static")){
			System.out.println("put all!!" );
			
			for (String label : map.keySet()) {
				for (byte[] value : map.get(label)) {
					System.out.println("labels: " +label);
					stat.put(label, value);
				}
			}
		}
		else{
			for (String label : map.keySet()) {
				for (byte[] value : map.get(label)) {
					dynamic.put(Base64.getEncoder().encodeToString(label.getBytes()), value);
				}
			}
		}
	}
	@Override
	public void getByintFromArray(String columnFamily, int i) throws SearchitectException {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void delete(String cfh, byte[] key) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void putAllMapCollection(String columnFamily, Map<String, Collection<byte[]>> map)
			throws SearchitectException {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void putAllMap(String columnFamily, Map<String, byte[]> map, boolean hex) throws SearchitectException {
		if(columnFamily.equals("dynamic")){
		for(String label : map.keySet()){
			dynamic.put(label, map.get(label));
		}
		}
		else{	for(String label : map.keySet()){
			stat.put(label, map.get(label));
		};
		}
		
	}
	
	@Override
	public void putAllHashMap(String columnFamily, HashMap<String, byte[]> map, boolean hex) throws SearchitectException {
		if(columnFamily.equals("dynamic")){
		for(String label : map.keySet()){
			dynamic.put(label, map.get(label));
		}
		}
		else{
			this.stat=map;
		}
		
	}
	

		

}
