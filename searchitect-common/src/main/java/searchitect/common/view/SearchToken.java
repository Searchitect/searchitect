package searchitect.common.view;

import javax.xml.bind.annotation.XmlRootElement;
/**
 * SearchToken - json object used to commit a search contains the token string
 *
 */
@XmlRootElement
public class SearchToken {

    private String searchToken;

    public SearchToken(String searchToken) {
	this.searchToken = searchToken;
    }

    public String getSearchToken() {
	return searchToken;
    }

    public void setSearchToken(String searchToken) {
	this.searchToken = searchToken;
    }

    public SearchToken() {

    }

}

