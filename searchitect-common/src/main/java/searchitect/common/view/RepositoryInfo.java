package searchitect.common.view;

import javax.xml.bind.annotation.XmlRootElement;


@XmlRootElement
public class RepositoryInfo {
	
	private String repositoryName;
	
	public RepositoryInfo(String repositoryname){
		this.repositoryName = repositoryname;
	}

	public String getrepositoryName() {
		return repositoryName;
	}

	public void setrepositoryName(String repositoryName) {
		this.repositoryName = repositoryName;
	}
	
	public RepositoryInfo(){
		
	}
	

}
