package searchitect.common.view;


import javax.xml.bind.annotation.XmlRootElement;

/**
 * UserAuthResponse - json object used to retrieve json web token from server
 *
 */
@XmlRootElement
public class UserAuthResponse{
    private String jwtoken;

    public UserAuthResponse() {
    }
    public UserAuthResponse(String token) {
        this.jwtoken = token;
    }

    public String getToken() {
        return this.jwtoken;
}
    
    public void setToken(String token) {
      this.jwtoken = token;
}
	

}
