package searchitect.common.view;

import java.util.List;
import java.util.Set;

import javax.xml.bind.annotation.XmlRootElement;

import searchitect.common.view.UserEDBResponse;

/**
 * UserAcccountResponse - json object used to retrieve user account data from server
 *
 */


@XmlRootElement
public class UserAccountResponse {
	

	private String username;
	private Set<UserEDBResponse> repositories;
	private List<String> authorities;

	public UserAccountResponse(String username,Set<UserEDBResponse> set, List <String> authorities) {
		this.setUsername(username);
		this.setAuthorities(authorities);
		this.setRepositories(set);
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public Set<UserEDBResponse> getRepositories() {
		return repositories;
	}

	public void setRepositories(Set<UserEDBResponse> repositories) {
		this.repositories = repositories;
	}

	public List<String> getAuthorities() {
		return authorities;
	}

	public void setAuthorities(List<String> authorities) {
		this.authorities = authorities;
	}


}
