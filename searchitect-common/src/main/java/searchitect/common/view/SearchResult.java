package searchitect.common.view;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.*;
/**
 * SearchResult - json object used to retrieve search result strings 
 *
 */
@XmlRootElement
public class SearchResult {
	
	private List <String> resultList;
	
	public SearchResult(List <String> resultlist){
		this.resultList = resultlist;
	}

	public List <String> getResultList() {
		return resultList;
	}

	public void setSearchResult(List <String> resultlist) {
		this.resultList = resultlist;
	}
	
	public SearchResult(){
		
	}
	

}
