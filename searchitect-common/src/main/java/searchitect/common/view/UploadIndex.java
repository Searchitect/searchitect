package searchitect.common.view;

import javax.xml.bind.annotation.XmlRootElement;
import com.google.common.collect.Multimap;
/**
 * UploadIndex - used to upload index in dummy implementation
 *
 */
@XmlRootElement
public class UploadIndex extends Upload{
	
	private Multimap<String, String> setupIndex;
	
	public UploadIndex(Multimap <String, String> setupindex){
		this.setupIndex = setupindex;
	}

	public Multimap <String, String>  getsetupIndex() {
		return setupIndex;
	}

	public void setSetupIndex(Multimap <String, String> setupindex ) {
		this.setupIndex = setupindex;
	}
	
	
	public UploadIndex(){
		
	}
	

}
