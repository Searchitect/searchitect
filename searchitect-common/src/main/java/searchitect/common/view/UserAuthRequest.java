package searchitect.common.view;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * UserAuthRequest - json object used to pass credentials for authentication
 *
 */
@XmlRootElement
public class UserAuthRequest {
	private String username;
	private String password;
	
	public UserAuthRequest(String username, String password) {
		this.password = password;
		this.username = username;
	}
	
	
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	

}
