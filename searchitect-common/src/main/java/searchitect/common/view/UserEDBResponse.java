package searchitect.common.view;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * UserEDBResponse - json object used to retrieve EDB attributes after setup
 *
 */
@XmlRootElement
public class UserEDBResponse {
	private String backendId;
	private String eDBName;
	
	public UserEDBResponse(String backendName, String eDBName ){
		this.setBackendName(backendName);
		this.setEDBName(eDBName);
	}

	public String getBackendId() {
		return backendId;
	}

	public void setBackendName(String backendName) {
		this.backendId = backendName;
	}

	public String getEDBName() {
		return eDBName;
	}

	public void setEDBName(String eDBName) {
		this.eDBName = eDBName;
	}
	
	
	 
}
