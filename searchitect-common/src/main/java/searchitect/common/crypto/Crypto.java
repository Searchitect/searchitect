package searchitect.common.crypto;

import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.SecureRandom;
import java.security.Security;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;

import org.bouncycastle.crypto.InvalidCipherTextException;
import org.bouncycastle.crypto.digests.Blake2bDigest;
import org.bouncycastle.crypto.digests.Blake2sDigest;
import org.bouncycastle.crypto.digests.KeccakDigest;
import org.bouncycastle.crypto.digests.SHA256Digest;
import org.bouncycastle.crypto.macs.HMac;
import org.bouncycastle.crypto.params.KeyParameter;
import org.bouncycastle.crypto.prng.ThreadedSeedGenerator;
import org.bouncycastle.jcajce.spec.AEADParameterSpec;
import org.bouncycastle.util.encoders.Hex;

public class Crypto {
	
	static {
		Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());
	}

	private Crypto() {
	}

	// ***********************************************************************************************//
	// taken from the Clusion library 
	///////////////////// KeyGen return a raw key based on PBE
	///////////////////// PKCS12 mostly taken from
	///////////////////// org.bouncycastle.jce.provider.test.PBETest
	///////////////////// check also doc in
	///////////////////// http://docs.oracle.com/javase/7/docs/technotes/guides/security/StandardNames.html#SecretKeyFactory/////////////////////////////
	// ***********************************************************************************************//

	public static byte[] keyGenSetM(String pass, byte[] salt, int icount, int keySize)
			throws InvalidKeySpecException, NoSuchAlgorithmException, NoSuchProviderException {

		// With Java 8, use "PBKDF2WithHmacSHA256/512" instead
		SecretKeyFactory factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA256");
		KeySpec spec = new PBEKeySpec(pass.toCharArray(), salt, icount, keySize);
		SecretKey tmp = factory.generateSecret(spec);
		SecretKey secret = new SecretKeySpec(tmp.getEncoded(), "AES");
		return secret.getEncoded();

	}
	
	
	// ***********************************************************************************************//

	///////////////////// Salt generation/RandomBytes: it is generated just once
	///////////////////// and it is not necessary to keep it secret
	///////////////////// can also be used for random bit generation
	// ***********************************************************************************************//

	public static byte[] randomBytes(int sizeOfSalt) {
		byte[] salt = new byte[sizeOfSalt];
		ThreadedSeedGenerator thread = new ThreadedSeedGenerator();
		SecureRandom random = new SecureRandom();
		random.setSeed(thread.generateSeed(20, true));
		random.nextBytes(salt);
		return salt;
	}
	
	
	/**
	 * hmac 
	 * @param key
	 * @param string 
	 * @param type
	 * @return
	 * @throws UnsupportedEncodingException
	 */
	public static byte[] hmac(byte[] key, String string, String type) throws UnsupportedEncodingException {
		HMac hmac;
		switch(type){
		case("SHA256"):{
			hmac = new HMac(new SHA256Digest());
			break;
		}
		case("Blake2b"):{
			hmac = new HMac(new Blake2bDigest());
			break;
		}
		case("Blake2s"):{
			hmac = new HMac(new Blake2sDigest());
			break;
		}
		default:
			hmac = new HMac(new SHA256Digest());
			break;
		}
		byte[] result = new byte[hmac.getMacSize()];
		byte[] byteArray = string.getBytes("UTF-8");
		hmac.init(new KeyParameter(key));
		hmac.reset();
		hmac.update(byteArray, 0,byteArray.length);
		hmac.doFinal(result, 0);
		return result;
	}
	
	/**
	 * hmac provides different hmac implementations sha256, blake2b, blake2s, and keccak
	 * 
	 * @param key
	 * @param byteArray
	 * @param type
	 * @return
	 * @throws UnsupportedEncodingException
	 */
	public static byte[] hmac(byte[] key, byte[] byteArray, String type) throws UnsupportedEncodingException {
		HMac hmac;
		switch(type){
		case("SHA256"):{
			hmac = new HMac(new SHA256Digest());
			break;
		}
		case("Blake2b"):{
			hmac = new HMac(new Blake2bDigest());
			break;
		}
		case("Blake2s"):{
			hmac = new HMac(new Blake2sDigest());
			break;
		}
		case("Keccak"):{
			hmac = new HMac(new KeccakDigest());
			break;
		}
		default:
			hmac = new HMac(new SHA256Digest());
			break;
		}
		byte[] result = new byte[hmac.getMacSize()];
		hmac.init(new KeyParameter(key));
		hmac.reset();
		hmac.update(byteArray, 0, byteArray.length);
		hmac.doFinal(result, 0);
		return result;
	}
	
	
	/**
	 * 
	 * @param key
	 * @param byteArray
	 * @return
	 * @throws UnsupportedEncodingException
	 */
	public static byte[] longHmac(byte[] key, byte[] byteArray) throws UnsupportedEncodingException {
		HMac hmac= new HMac(new KeccakDigest());
		hmac.getMacSize();
		
		byte[] result = new byte[hmac.getMacSize()];
		hmac.init(new KeyParameter(key));
		hmac.reset();
		hmac.update(byteArray, 0, byteArray.length);
		hmac.doFinal(result, 0);
		return result;
	}
	
	
	
	
	
	/**
	 * xor two byte arrays - reduces the min length of a and b
	 * 
	 * @param a
	 * @param b
	 * @return
	 */
	public static byte[] xor(byte[] a, byte[] b) {
		  byte[] result = new byte[Math.min(a.length, b.length)];

		  for (int i = 0; i < result.length; i++) {
		    result[i] = (byte) (((int) a[i]) ^ ((int) b[i]));
		  }

		  return result;
		}
	
	
	/**
	 * encryptAESGCM AEAD cipher encryption of a byte array of data
	 * 
	 * @param key
	 * @param data
	 * @param assocText
	 * @return encrypted data as byte array
	 * @throws NoSuchAlgorithmException
	 * @throws NoSuchProviderException
	 * @throws NoSuchPaddingException
	 * @throws InvalidKeyException
	 * @throws InvalidAlgorithmParameterException
	 * @throws IllegalBlockSizeException
	 * @throws BadPaddingException
	 */
	public static byte[] encryptAESGCM(byte[] key,byte [] data, byte[] assocText ) throws NoSuchAlgorithmException, NoSuchProviderException, NoSuchPaddingException, InvalidKeyException, InvalidAlgorithmParameterException, IllegalBlockSizeException, BadPaddingException {
	int maclength = 128;
	int noncelength = 10;
	byte [] nonce = randomBytes(noncelength);

	AEADParameterSpec algspec = new AEADParameterSpec(nonce, maclength, assocText);
	Cipher cipher = Cipher.getInstance("AES/GCM/NoPadding", "BC");
	SecretKeySpec skey = new SecretKeySpec(key, "AES");
	cipher.init( Cipher.ENCRYPT_MODE , skey, algspec);
	byte [] enc=cipher.doFinal(data, 0, data.length);
	System.out.println("datalength:" + data.length +"data: "+ Hex.toHexString(data));
	byte[] encrypted = new byte[enc.length+noncelength];
	System.arraycopy(enc, 0, encrypted, nonce.length, enc.length);
	System.arraycopy(nonce, 0, encrypted, 0, nonce.length);
	return encrypted;
	}
	
	/**
	 * decrypt AESGCM - AEAD decryption and validation of a byte array of data
	 * @param key
	 * @param data
	 * @param assocText
	 * @return decrypted data as byte array
	 * @throws NoSuchAlgorithmException
	 * @throws NoSuchPaddingException
	 * @throws IllegalBlockSizeException
	 * @throws BadPaddingException
	 * @throws NoSuchProviderException
	 * @throws IllegalStateException
	 * @throws InvalidCipherTextException
	 * @throws InvalidKeyException
	 * @throws InvalidAlgorithmParameterException
	 */
	public static byte[] decryptAESGCM(byte[] key,byte [] data, byte[] assocText ) throws NoSuchAlgorithmException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException, NoSuchProviderException, IllegalStateException, InvalidCipherTextException, InvalidKeyException, InvalidAlgorithmParameterException{
	int maclength = 128;
	int noncelength = 10;
	byte [] nonce = new byte[noncelength];
	System.arraycopy(data, 0, nonce, 0, noncelength);
	AEADParameterSpec algspec = new AEADParameterSpec(nonce, maclength, assocText);
	Cipher cipher = Cipher.getInstance("AES/GCM/NoPadding", "BC");
	SecretKeySpec skey = new SecretKeySpec(key, "AES");
	cipher.init( Cipher.DECRYPT_MODE , skey, algspec);
	byte [] decrypted=cipher.doFinal(data, nonce.length, data.length-nonce.length);
	return decrypted;
	}
	

}
