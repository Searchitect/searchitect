package searchitect.common.client;

import com.google.common.collect.Multimap;

import searchitect.common.exception.SearchitectException;
import searchitect.common.view.SearchResult;
import searchitect.common.view.Upload;


public interface ClientScheme {
	
	/**
	 * returns the name of the implementation
	 * 
	 * @return string implementation name
	 */
	public String getImplName();
	/**
	 * search for a keyword in the query
	 * 
	 * @param query contains the keyword
	 * @return returns a json string with the results
	 * @throws SearchitectException
	 */
	public String search(String query) throws SearchitectException;
	/**
	 * update an EDB with the inverted index
	 * 
	 * @param invertedIndex
	 * @return json string of the update 
	 * @throws SearchitectException
	 */
	public String update(Multimap<String,String> invertedIndex) throws SearchitectException;
	/**
	 * resolve decrypts the search result
	 * 
	 * @param encrypted search result
	 * @return returns the search result in plain text
	 * @throws SearchitectException
	 */
	public SearchResult resolve(SearchResult encrypted) throws SearchitectException;
	/**
	 * setup generates an upload encrypted index from the plaintext inverted index
	 * 
	 * @param password - used to derive a key for the repository
	 * @param invertedIndex - plaintext inverted index
	 * @param numberOfDocuments 
	 * @return the encrypted upload index which needs to get passed to the server
	 * @throws SearchitectException
	 */
	public Upload setup(String password, Multimap<String, String> invertedIndex, int numberOfDocuments)
			throws SearchitectException;
}
