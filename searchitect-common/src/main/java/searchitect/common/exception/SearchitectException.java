package searchitect.common.exception;

public class SearchitectException extends RuntimeException{
	
	private static final long serialVersionUID = 1L;

	public SearchitectException() {
		super();
	}

	public SearchitectException(String message, Throwable cause) {
		super(message, cause);
	}

	public SearchitectException(String message) {
		super(message);
		System.out.println(message);
	}

}
